﻿using UnityEngine;
using System.Collections;

public class EnemyEntity : CombatEntity {

    protected override void Awake() {
        base.Awake();
        type = EntityType.ENEMY;
    }

	// Use this for initialization
    protected override void Start() {
        base.Start();
	}

    protected override void SetBaseValues() {
        base.SetBaseValues();

        modifierAugment = new AugmentData(AugmentType.MODIFIER);
        potencyAugment = new AugmentData(AugmentType.POTENCY);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
