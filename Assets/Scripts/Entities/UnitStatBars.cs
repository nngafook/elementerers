﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.Events;

public class UnitStatBars : MonoBehaviour {

    [HideInInspector]
    public UnityEvent OnTimerCompleteEvent;

    private EntityType entityType;

    private float currentHealthValue;
    private float maxHealthValue;

    private float maxHealthScale;
    private float maxTimeScale;

    [SerializeField]
    private float timeBetweenAttacks;
    // Stores the base time, and is used to reset after every attack due to knockbacks
    private float originalTimeBetweenAttacks;
    // When an event increases a units speed, it'll increase this value, and this will be taken off originalTimeBetweenAttacks...
    private float timeReducedByEvents = 0;

    private float elapsedTime = 0;
    private float healthBarAnimationSpeed = 0.5f;
    private float timerBarFadeDuration = 0.25f;
    private float timerBarFadeValue = 0.5f;

    private bool gotKnockbacked = false;

    private Tween timerFadeTween;
    private SpriteRenderer healthBarRenderer;
    private SpriteRenderer timerBarRenderer;

    [Header("DEBUG")]
    public bool debugPaused = false;

    [HideInInspector]
    public CombatEntity entityOwner;

    public Transform healthBar;
    public Transform timerBar;

    [Header("Colors")]
    public Color playerHealthBarColor;
    public Color enemyHealthBarColor;
    public Color timerBarColor;

    void Awake() {
        maxHealthScale = healthBar.localScale.x;
        maxTimeScale = timerBar.localScale.x;

        healthBarRenderer = healthBar.GetComponent<SpriteRenderer>();
        timerBarRenderer = timerBar.GetComponent<SpriteRenderer>();

        timerFadeTween = timerBarRenderer.DOFade(timerBarFadeValue, timerBarFadeDuration).SetLoops(-1, LoopType.Yoyo);
    }

	// Use this for initialization
	void Start () {
	
	}

    private IEnumerator LerpTimer () {
        float i = 0;
        Vector2 startSize = new Vector2(0, timerBar.localScale.y);
        Vector2 endSize = new Vector2(maxTimeScale, timerBar.localScale.y);
        elapsedTime = 0;
        CombatManager combatManager = CombatManager.Instance;

        while (elapsedTime < timeBetweenAttacks) {
            if ((combatManager.CurrentCombatState == CombatState.ACTIVE || gotKnockbacked) && !debugPaused) {
                elapsedTime += Time.deltaTime;
                i = elapsedTime / timeBetweenAttacks;
                timerBar.localScale = Vector3.Lerp(startSize, endSize, i);
                gotKnockbacked = false;
            }
            yield return null;
        }
        OnTimerFull();
    }

    public void AddTimeBetweenAttacks(float value) {
        timeReducedByEvents += value;
        timeBetweenAttacks = originalTimeBetweenAttacks - timeReducedByEvents;
    }

    public void StartTimer() {
        timeBetweenAttacks = originalTimeBetweenAttacks - timeReducedByEvents;
        timerFadeTween.Pause();
        timerBarRenderer.DOKill();
        timerBarRenderer.color = timerBarColor;
        timerBar.localScale = timerBar.localScale.SetX(0);
        //timerBar.DOScaleX(maxTimeScale, timeBetweenAttacks).SetEase(Ease.Linear).OnComplete(OnTimerFull);
        StartCoroutine("LerpTimer");
    }

    public void StopTimer() {
        timerFadeTween.Pause();
        timerBarRenderer.color = timerBarColor;
        //timerBar.DOKill();
        StopCoroutine("LerpTimer");
        timerBar.localScale = timerBar.localScale.SetX(0);
    }

    private void OnTimerFull() {
        if (OnTimerCompleteEvent != null) {
            OnTimerCompleteEvent.Invoke();
        }

        if (entityOwner.HasTarget || entityOwner.IsDefending || entityOwner.DefendQueued) {
            timerFadeTween.Restart();
            timerBarRenderer.DOFade(timerBarFadeValue, timerBarFadeDuration).SetLoops(-1, LoopType.Yoyo);
        }
    }

    public void EntityDied() {
        StopTimer();
        healthBar.DOKill();
    }

    private void SetColors() {
        if (entityType == EntityType.PLAYER) {
            healthBarRenderer.color = playerHealthBarColor;
        }
        else if (entityType == EntityType.ENEMY) {
            healthBarRenderer.color = enemyHealthBarColor;
        }
        timerBarRenderer.color = timerBarColor;
    }

    /// <summary>
    /// Sets the scale of the health bar to the current health
    /// </summary>
    private void SetHealth() {
        float percentage = ((currentHealthValue / maxHealthValue)) * maxHealthScale;
        healthBar.localScale = healthBar.localScale.SetX(percentage);
    }

    /// <summary>
    /// Updates the health bar scale via tweening. Value is current health passed from the entity
    /// </summary>
    /// <param name="value"></param>
    public void UpdateHealth(float value) {
        currentHealthValue = value;
        float percentage = ((currentHealthValue / maxHealthValue)) * maxHealthScale;
        healthBar.DOScaleX(percentage, healthBarAnimationSpeed);
    }

    /// <summary>
    /// When an entity gets hit, their timer is reduced slightly, unless already full
    /// </summary>
    public void ApplyKnockback(float amount) {
        if (timerBar.localScale.x != maxTimeScale) {
            elapsedTime -= amount;
            timerBar.localScale = timerBar.localScale.SetX(timerBar.localScale.x - amount);
            gotKnockbacked = true;
        }
    }

    public void Init(CombatEntity entity) {
        entityOwner = entity;
        maxHealthValue = entityOwner.Data.maxHealth;
        currentHealthValue = maxHealthValue;
        timeBetweenAttacks = entityOwner.Data.timeBetweenAttacks.Random;
        originalTimeBetweenAttacks = timeBetweenAttacks;
        entityType = entityOwner.Type;
        SetColors();

        SetHealth();

        timerBar.localScale = timerBar.localScale.SetX(0);
    }

	// Update is called once per frame
	void Update () {

	}
}
