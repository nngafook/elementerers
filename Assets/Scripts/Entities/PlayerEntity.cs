﻿using UnityEngine;
using System.Collections;

public class PlayerEntity : CombatEntity {

    protected override void Awake() {
        base.Awake();
        type = EntityType.PLAYER;
    }

	// Use this for initialization
    protected override void Start() {
        base.Start();
    }

    protected override SpecialAttackData GetSpecialAttackData() {
        return GameProgress.Instance.EquippedSpecialAttackData(characterData.characterID);
    }

    protected override void SetBaseValues() {
        base.SetBaseValues();

        CharacterLevelStats levelStats = DataManager.Instance.CharacterLevelStatsObject.StatsByID(this.Data.characterID);

        //!++ I think this is bad. This should not default at this point I think... FIX IT!
        if (saveInfo == null) {
            saveInfo = new CharacterSaveInfo(this.Data.characterID, Data.specialAttackType);
        }

        modifierAugment = DataManager.Instance.AugmentDataByID(saveInfo.modifierAugmentSlotID);
        potencyAugment = DataManager.Instance.AugmentDataByID(saveInfo.potencyAugmentSlotID);

        if (modifierAugment == null) {
            modifierAugment = new AugmentData(AugmentType.MODIFIER);
        }

        if (potencyAugment == null) {
            potencyAugment = new AugmentData(AugmentType.POTENCY);
            potencyAugment.potencyType = PotencyType.ADD_EOT;
            potencyAugment.chance = new PercentageFloat(100);
            potencyAugment.impact = 12;
            potencyAugment.duration = 4;
        }

        //!++ FIX THIS SAFETY CHECK!
        if (levelStats != null && saveInfo != null) {
            baseAtkDMG.AddToRange(levelStats.damageLevelValues[saveInfo.level - 1]);
            baseDefense.AddToRange(levelStats.defenseLevelValues[saveInfo.level - 1]);
            baseEvasion.AddToBase(levelStats.evasionLevelValues[saveInfo.level - 1]);
            maxHealth += levelStats.maxHealthLevelValues[saveInfo.level - 1];
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
