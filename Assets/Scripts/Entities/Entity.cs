﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

    protected EntityType type;
    protected UID uid;

    public UID UID { get { return uid; } }
    public Vector2 ScreenPosition { get { return Camera.main.WorldToScreenPoint(this.transform.position); } }
    public Vector2 Position { get { return this.transform.position; } }

    protected virtual void Awake() {
        uid = new UID();
        this.gameObject.name = this.gameObject.name + " (UID: " + uid.Value + ")";
    }

	// Use this for initialization
    protected virtual void Start() {
        
	}

    /// <summary>
    /// Called after all entities have been created, and this assigns targets and starts timers.
    /// </summary>
    public virtual void InitStart() {

    }

	// Update is called once per frame
	void Update () {
	
	}
}
