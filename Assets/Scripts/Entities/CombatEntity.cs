﻿using Spine;
using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using AnimationState = Spine.AnimationState;
using Event = Spine.Event;
using UnityEngine.UI;

[RequireComponent(typeof(CombatStrategyController))]
public class CombatEntity : Entity {
    
    private const string ITEM_SLOT_NAME = "item_near";
    private const string SWORD_ATTACHMENT = "sword_1";
    private const string MACHINEGUN_ATTACHMENT = "machineGun_1";

    private const string IDLE_ANIMATION_NAME = "idle";
    private const string RUN_ANIMATION_NAME = "run2";
    private const string TAKE_DAMAGE_ANIMATION_NAME = "hit1";
    private const string MELEE_ANIMATION_NAME = "meleeSwing1-fullBody";
    private const string RANGED_ANIMATION_NAME = "machineGunShoot";
    private const string GUN_RELOAD_ANIMATION_NAME = "machineGunReload";
    private const string HEAL_ANIMATION_NAME = "punch3";
    private const string CELEBRATION_ANIMATION_NAME = "celebration";
    private const string JUMP_ANIMATION_NAME = "celebration";
    private const string DODGE_ANIMATION_NAME = "skid";
    private const string DEFEND_ANIMATION_NAME = "block";
    private const string DEFEND_HIT_ANIMATION_NAME = "blockHit";
    private const string IDLE_HURT_ANIMATION_NAME = "idleTired";

    private const string HIT_EVENT_NAME = "meleeHit";

    private WaitForSeconds shortWait = new WaitForSeconds(0.15f);
    private Vector2 basePosition;
    private bool isAlive = true;
    private bool isIdle = false;
    private bool isFlipped = false;
    private bool isSelected = false;
    private bool isDefending = false;
    private bool specialAttackOnCooldown = false;
    private bool specialAttackQueued = false;
    private bool healSpecialQueued = false;
    private bool defendActionQueued = false;
    // Finds a new target if their target dies.
    // TODO :  Expose this to the player on a per entity basis
    private bool autoTarget = true;

    // Might not be needed?
    private bool getRandomTarget = false;

    public bool debugBreak = false;

    private int listIndex = -1;
    private int hitsBeforeTargetSwitch = 0;
    private int attacksCompleted = 0;
    private int attacksIncoming = 0;
    private float moveTimer = 0.5f;
    private float defendReductionFactor = 2.5f;
    private float currentHealth;
    private float height = 0;
    private float maxRangedAnimationLoops = 4f;
    private float specialAttackCooldownCounter = 0;
    private float specialAttackCooldown = 10;


    // ================================ //
    private bool moveAdded = false;
    private bool moveRemoved = false;
    private bool swingAdded = false;
    private bool swingRemoved = false;
    private bool hitAdded = false;
    private bool hitRemoved = false;
    // ================================ //


    private RangedFloat elementalReductionFactor = new RangedFloat(0.08f, 0.15f);      // In percentage, i.e. 8% - 15% less damage

    protected float maxHealth;

    protected PercentageFloat baseEvasion = new PercentageFloat();
    protected RangedFloat baseAtkDMG = new RangedFloat();
    protected RangedFloat baseSPAtkDMG = new RangedFloat();
    protected RangedFloat baseDefense = new RangedFloat();

    protected CharacterData characterData;
    protected CharacterSaveInfo saveInfo;
    protected AugmentData modifierAugment;
    protected AugmentData potencyAugment;

    private SpecialAttackData specialAttackData;
    private CombatEntity queuedTargetEntity;
    private CombatEntity targetEntity;
    private CombatEntity lastTargetEntity;
    private BoxCollider2D spriteCollider;
    private CombatStrategyController strategyController;
    private CombatOrder orderToExecute;     // Set by the strategy controller. When a target/order is chosen, this gets set and used for the action.

    public SkeletonAnimation skeletonAnimationObject;
    public UnitStatBars statBars;
    public CharacterDataObject characterDataObject;
    public GameObject meleeDamageVFXObject;
    public GameObject gunDamageVFXObject;
    public Transform bottomVFXAnchor;
    public Transform topVFXAnchor;

    [Space(10)]
    [Header("UI Canvas")]
    public EOTManager eotManager;
    public TargetPointer targetPointer;
    public RectTransform rangedReticle;
    public QueuedAttackIcon queuedActionIcon;
    public ElementalUnitIcon elementalUnitIcon;

    public Vector2 ScreenPositionTop { get { return ScreenPosition.AddY((height / 2)); } }
    public float CurrentHealth { get { return currentHealth; } set { currentHealth = value; } } // The set in here is strictly for the cheatmenu
    public float MaxHealth { get { return maxHealth; } set { maxHealth = value; } }             // The set in here is strictly for the cheatmenu
    public float SpecialAttackCooldown { get { return specialAttackCooldown; } }
    public float SpecialAttackCooldownCounter { get { return specialAttackCooldownCounter; } }
    public int AttacksIncoming { get { return attacksIncoming; } set { attacksIncoming = value; } }
    public int ListIndex { get { return listIndex; } }
    public bool SpecialAttackOnCooldown { get { return specialAttackOnCooldown; } set { specialAttackOnCooldown = value; } }        // The set in here is strictly for the cheatmenu
    public bool IsAlive { get { return isAlive; } }
    public bool IsIdle { get { return isIdle; } }
    public bool CanAttack { get { return (attacksIncoming == 0 && isIdle && isAlive); } }
    public bool HasTarget { get { return targetEntity != null; } }
    public bool IsSelected { get { return isSelected; } set { isSelected = value; } }
    public bool IsDefending { get { return isDefending; } }
    public bool SpecialAttackQueued { get { return (specialAttackQueued || healSpecialQueued); } set { specialAttackQueued = value; } }
    public bool AttackQueued { get { return (!SpecialAttackQueued && !isDefending && !defendActionQueued); } }
    public bool DefendQueued { get { return defendActionQueued; } set { defendActionQueued = value; } }
    public RangedFloat BaseAtkDamage { get { return baseAtkDMG; } set { baseAtkDMG = value; } }
    public RangedFloat Defense { get { return baseDefense; } set { baseDefense = value; } }
    public CombatEntity TargetEntity { get { return targetEntity; } }
    public Vector2 TargetPosition { get { return targetEntity.Position; } }
    public CharacterData Data { get { return characterData; } }
    public SpecialAttackData SpecialAtkData { get { return specialAttackData; } }
    public CombatStrategyController StrategyController { get { return strategyController; } }
    public CombatOrder OrderToExecute { get { return orderToExecute; } set { orderToExecute = value; } }
    public EntityType Type { get { return Data.entityType; } }

    protected override void Awake() {
        base.Awake();

        strategyController = this.GetComponent<CombatStrategyController>();
        spriteCollider = skeletonAnimationObject.GetComponent<BoxCollider2D>();
        height = spriteCollider.size.y;

        strategyController.SetOwner(this);
    }

    // Use this for initialization
    protected override void Start() {
        base.Start();

        if (this.Type == EntityType.ENEMY) {
            FlipX();
        }

        SetSkinFromData();

        HideTargetBox();

        CombatManager.Instance.OnCombatStateChanged.AddListener(CombatStateChanged);

        getRandomTarget = Data.getRandomTarget;
        hitsBeforeTargetSwitch = Data.timeBetweenAttacks.RandomInt;
    }

    protected virtual SpecialAttackData GetSpecialAttackData() {
        SpecialAttackData rVal = DataManager.Instance.SpecialAttackDataByType(characterData.specialAttackType);
        if (rVal == null) {
            rVal = new SpecialAttackData();
        }
        return rVal;
    }

    #region PRIVATE_METHODS

    private void SetDataValues() {
        SetBaseValues();

        SetStrategyPriority();

        // WUT... this is temp. Resetting it to 0.5 so it doesn't compoundingly get slower every respawn
        moveTimer = 0.5f;

        //moveTimer += moveTimer - ((0.5f / 100) * characterData.moveSpeed);
        moveTimer += moveTimer - ((0.5f / 100) * (Random.Range(1, 10) * 10));

        statBars.Init(this);
        StopCoroutine("ProcessSpecialAttackCooldown");
        // I think this needs to only happen when the entity is in position and not dead and/or running into camera
        EntityManager.Instance.RegisterCombatEntity(this);
    }

    /// <summary>
    /// Sets all base values taking event changes into consideration
    /// </summary>
    protected virtual void SetBaseValues() {
        characterData = characterDataObject.data;
        saveInfo = GameProgress.Instance.GetCharacterSaveInfo(characterData.characterID);

        maxHealth = characterData.maxHealth;
        currentHealth = maxHealth;

        specialAttackData = GetSpecialAttackData();
        specialAttackCooldown = specialAttackData.cooldown;
        specialAttackCooldownCounter = 0;
        elementalUnitIcon.SetElementalType(characterData.elementalAffinity);

        EventChanges eventChanges = LevelManager.Instance.CurrentEventChanges;

        baseDefense = new RangedFloat(Data.defense.min, Data.defense.max);
        baseAtkDMG = new RangedFloat(Data.attackDamage.min, Data.attackDamage.max);
        baseSPAtkDMG = new RangedFloat(Data.specialAttackDamage.min, Data.specialAttackDamage.max);

        baseEvasion = new PercentageFloat(characterData.evasion.baseValue);

        baseAtkDMG.AddToRange(eventChanges.AtkDMGChanged(this.Type));
        baseSPAtkDMG.AddToRange(eventChanges.SPAtkDMGChanged(this.Type));
        statBars.AddTimeBetweenAttacks(eventChanges.SPDChanged(this.Type));
    }

    /// <summary>
    /// Gets the commandOrder list from GameProgess and save it in the controller
    /// </summary>
    private void SetStrategyPriority() {
        if (this.Type == EntityType.PLAYER) {
            strategyController.SetPriorities(ref saveInfo.combatOrders, ref saveInfo.fallbackCombatOrder);
        }
        else {
            strategyController.SetPriorities(ref Data.combatOrders.orderList, ref Data.combatOrders.fallbackOrder);
        }
    }

    /// <summary>
    /// Resets a unit. Puts them off screen and they run back
    /// </summary>
    IEnumerator ResetFromDeath() {
        while (attacksIncoming > 0) {
            yield return null;
        }

        if (this.Type != EntityType.PLAYER && LevelManager.Instance.EnemiesLeftInLevel) {
            ResetData();
            RunIntoCamera();
        }
    }

    private void RunIntoCamera() {
        PlayRunAnimation();
        transform.DOMove(basePosition, moveTimer).OnComplete(RunIntoCameraComplete);
    }

    private void RunIntoCameraComplete() {
        PlayIdleAnimation();

        isIdle = true;
        isAlive = true;

        if (getRandomTarget) {
            EntityType targetType = (this.Type == EntityType.PLAYER) ? EntityType.ENEMY : EntityType.PLAYER;
            SetTarget(EntityManager.Instance.GetRandomTarget(targetType));
        }

        //! Init target box color?

        statBars.OnTimerCompleteEvent.AddListener(OnAttackTimerComplete);
        statBars.StartTimer();

        StopCoroutine("SearchForTarget");
        StartCoroutine("SearchForTarget");
    }

    private void OnAttackTimerComplete() {
        if (targetEntity != null || defendActionQueued) {
            CombatManager.Instance.AddEntityTurn(this);
        }
        else {
            statBars.StartTimer();
        }
    }

    private void SetSkinFromData() {
        skeletonAnimationObject.skeleton.SetSkin(Data.SkinName);

        if (Type == EntityType.PLAYER) {
            string attachment = (Data.attackType == AttackType.MELEE) ? SWORD_ATTACHMENT : MACHINEGUN_ATTACHMENT;
            skeletonAnimationObject.skeleton.SetAttachment(ITEM_SLOT_NAME, attachment);
        }
    }

    private void FlipX() {
        isFlipped = !isFlipped;
        skeletonAnimationObject.skeleton.flipX = isFlipped;
    }

    private void CheckFlip() {
        if (this.Type == EntityType.ENEMY && !isFlipped) {
            FlipX();
        }
    }

    private void AssignTargetEntityAndStoreLast(CombatEntity e) {
        lastTargetEntity = targetEntity;
        targetEntity = e;
    }

    private int CalculatePotencyDamage() {
        int rVal = baseSPAtkDMG.RandomInt;

        if (potencyAugment != null && potencyAugment.potencyType == PotencyType.INCREASE_MULTIPLIER) {
            rVal = Mathf.RoundToInt(rVal * (specialAttackData.multiplier + potencyAugment.impact));
        }
        else {
            rVal *= specialAttackData.multiplier;
        }

        return rVal;
    }

    private IEnumerator HealTarget() {
        if (targetEntity != null) {
            //! If we decide we want to heal targets even if they're not idle, remove this while loop 
            while (!isIdle || !targetEntity.IsIdle || CombatManager.Instance.CurrentCombatState == CombatState.WAITING) {
                yield return null;
            }

            //CombatManager.Instance.RemoveFromTurnQueue(this);

            if (isAlive && targetEntity.IsAlive) {
                isIdle = false;
                //healSpecialQueued = true;

                targetEntity.AttacksIncoming++;

                statBars.StopTimer();

                PlayHealAnimation();
            }
            else if (isAlive) {
                // Target died
                Debug.Log("Target died before heal could come off".Bold().Colored(Color.magenta));
                queuedActionIcon.SetAttackQueued();
                queuedTargetEntity = (lastTargetEntity != null && lastTargetEntity.Type != this.Type) ? lastTargetEntity : GetRandomTarget();
                Debug.Log(this.UID.Value + " ActionComplete: HealTarget");
                ActionComplete();
            }
        }
        else {
            Debug.Log("Got a heal on a target entity that was null... odd");
            //statBars.StartTimer();
        }
    }

    private IEnumerator MoveToAttack() {
        if (targetEntity != null) {
            // TODO: if target entity is dead, get a new random one
            while ((targetEntity != null) && ((!targetEntity.IsIdle) || (this.attacksIncoming > 0))) {
                yield return null;
            }
            
            //! While stuck in the while loop, the entity could actually become null
            if (isAlive) {
                if (targetEntity != null && isAlive && targetEntity.IsAlive) {
                    isIdle = false;
                    targetEntity.AttacksIncoming++;
                    Vector2 targetPosition = targetEntity.Position;
                    float direction = ((spriteCollider.size.x / 2) + (targetEntity.spriteCollider.size.x / 2));
                    direction = (this.Type == EntityType.PLAYER) ? (direction *= -1) : direction;
                    targetPosition.x += direction;

                    PlayRunAnimation();
                    transform.DOMove(targetPosition, moveTimer).OnComplete(MoveToTargetComplete).SetEase(Ease.Linear);
                }
            }
        }
        else {
            statBars.StartTimer();
        }
    }

    private IEnumerator TakeAimForAttack() {
        if (targetEntity != null) {
            // TODO: if target entity is dead, get a new random one
            while ((targetEntity != null) && ((!targetEntity.IsIdle) || (this.attacksIncoming > 0))) {
                yield return null;
            }

            if (isAlive) {
                if (targetEntity != null && isAlive && targetEntity.IsAlive) {
                    if (!isAlive) {
                        Debug.Log("Why am I trying to attack while dead? UID: " + UID.Value);
                    }

                    isIdle = false;
                    targetEntity.AttacksIncoming++;

                    targetEntity.PlayRangedDamageVFX();
                    PlayRangedAttackAnimation();
                }
            }
        }
        else {
            statBars.StartTimer();
        }
    }

    private IEnumerator ExecuteSpecialAttack() {
        if (!specialAttackOnCooldown) {
            if (targetEntity != null && targetEntity.IsAlive) {
                List<CombatEntity> targets = EntityManager.Instance.GetTargetsBasedOnEffectType(targetEntity, modifierAugment);

                if (isAlive) {
                    bool allTargetsIdle = false;
                    while ((!allTargetsIdle) || (this.attacksIncoming > 0)) {
                        for (int i = 0; i < targets.Count; i++) {
                            if (!targets[i].IsIdle && targets[i].IsAlive) {
                                allTargetsIdle = false;
                                break;
                            }
                            allTargetsIdle = true;
                        }
                        yield return null;
                    }

                    isIdle = false;

                    // TODO: Remove this temp hack
                    //int damageInt = baseSPAtkDMG.RandomInt * specialAttackData.multiplier; // plus augment damage
                    int damageInt = CalculatePotencyDamage();
                    int mainDamage = (modifierAugment.effectType  == ModifierType.TARGET_ALL) ? damageInt - (damageInt / 3) : damageInt;
                    int collateralDamage = (modifierAugment.effectType == ModifierType.TARGET_ALL) ? mainDamage : Mathf.CeilToInt(mainDamage / 2);

                    for (int i = 0; i < targets.Count; i++) {
                        if (targets[i].IsAlive) {
                            targets[i].AttacksIncoming++;
                            if (i == 0) {
                                // Main target
                                targets[i].TakeDamage(mainDamage, true, potencyAugment, specialAttackData.elementalType, specialAttackData.knockback);
                            }
                            else {
                                // Other targets
                                targets[i].TakeDamage(collateralDamage, true, potencyAugment, specialAttackData.elementalType, specialAttackData.knockback);
                            }
                        }
                    }

                    //! THIS NEEDS TO BE SPECIAL ACTION ANIMATION
                    PlayJumpAnimation();

                    SpecialActionComplete(ref targets);

                    queuedActionIcon.SetAttackQueued();
                    specialAttackQueued = false;
                    specialAttackOnCooldown = true;
                    StartCoroutine("ProcessSpecialAttackCooldown");
                    
                }
                else {
                    Debug.Log("Why am I trying to attack while dead? UID: " + UID.Value);
                }
            }
            else {
                queuedActionIcon.SetAttackQueued();
                specialAttackQueued = false;
                SetTarget(GetRandomTarget()); 
                statBars.StartTimer();
            }
        }
    }

    private IEnumerator SearchForTarget() {
        CombatManager cm = CombatManager.Instance;
        while (cm.BattleOngoing) {
            if  (isIdle && !isDefending && !defendActionQueued && CombatManager.Instance.CurrentCombatState == CombatState.ACTIVE && (targetEntity == null || !targetEntity.IsAlive)) {
                if (specialAttackQueued) {
                    queuedActionIcon.SetAttackQueued();
                    specialAttackQueued = false;
                }
                SetTarget(GetRandomTarget());
            }
            yield return shortWait;
        }
    }

    private void MoveToTargetComplete() {
        // Last minute check if the target is still alive.
        if (isAlive) {
            if (targetEntity.isAlive) {
                PlayMeleeAttackAnimation();
            }
            else {
                MoveBackToBasePosition();
            }
        }
    }

    private void MoveBackToBasePosition() {
        moveAdded = true;
        moveRemoved = false;

        PlayRunAnimation();
        FlipX();
        transform.DOMove(basePosition, moveTimer).OnComplete(OnMoveBackToBaseComplete).SetEase(Ease.Linear);
    }

    private void OnMoveBackToBaseComplete() {
        moveRemoved = true;

        FlipX();
        PlayIdleAnimation();
        ActionComplete();
    }

    private void ActionComplete() {
        if (isAlive) {
            isIdle = true;
            targetEntity.AttacksIncoming--;
            if (targetEntity.AttacksIncoming < 0) {
                Debug.Log("Attacks Incoming dropped below 0, Target UID: " + targetEntity.UID.Value + " Target Attacks Incoming: " + targetEntity.attacksIncoming + " Attacking Entity UID: " + UID.Value);
                targetEntity.AttacksIncoming = 0;
                Debug.Log(moveAdded + " || " + moveRemoved + " || " + hitAdded + " || " + hitRemoved + " || " + swingAdded + " || " + swingRemoved);
            }
            

            if (queuedTargetEntity != null) {
                if (isSelected && targetEntity != null) {
                    targetEntity.HideTargetBox();
                }
                SetTarget(queuedTargetEntity);
            }
            else if (!targetEntity.IsAlive) {
                //! Not Alive
                // if queued target is null (cause there were no more targets),
                // and the target entity is dead, then stop timer
            }
            statBars.StartTimer();
        }
        else {
            Debug.Log("LULUL");
        }
    }

    private void HealActionComplete() {
        queuedActionIcon.SetAttackQueued();
        queuedTargetEntity = (lastTargetEntity != null && lastTargetEntity.Type != this.Type) ? lastTargetEntity : GetRandomTarget();

        healSpecialQueued = false;
        specialAttackOnCooldown = true;
        StartCoroutine("ProcessSpecialAttackCooldown");
    }

    private void SpecialActionComplete(ref List<CombatEntity> targets) {
        isIdle = true;
        CombatEntity target = null;
        for (int i = 0; i < targets.Count; i++) {
            target = targets[i];
            target.AttacksIncoming--;
            if (target.AttacksIncoming < 0) {
                Debug.Log("Attacks Incoming dropped below 0, Target UID: " + target.UID.Value + " Target Attacks Incoming: " + target.attacksIncoming + " Attacking Entity UID: " + UID.Value);
                target.AttacksIncoming = 0;
            }
        }

        if (queuedTargetEntity != null) {
            if (isSelected && targetEntity != null) {
                targetEntity.HideTargetBox();
            }
            SetTarget(queuedTargetEntity);
        }
        else if (!targetEntity.IsAlive) {
            Debug.Log("Not alive");
            // if queued target is null (cause there were no more targets),
            // and the target entity is dead, then stop timer
        }  
        statBars.StartTimer();
    }

    /// <summary>
    /// Used after the regular attack animations are complete. Called via the animation events indirectly
    /// </summary>
    private void ApplyAttackDamage() {
        // Is checking if the entity is dead and respawning. Maybe need to get its own bool?
        if (targetEntity.IsAlive) {
            targetEntity.TakeDamage(baseAtkDMG.RandomInt, false, potencyAugment, Data.elementalAffinity, Data.knockback);
            if (Data.attackType == AttackType.RANGED) {
                targetEntity.StopRangedDamageVFX();
            }
        }

        if (!targetEntity.IsAlive && autoTarget) {
            queuedTargetEntity = GetRandomTarget();
            attacksCompleted = 0;
        }
        else if (getRandomTarget) {
            attacksCompleted++;
            if (attacksCompleted >= hitsBeforeTargetSwitch) {
                queuedTargetEntity = GetRandomTarget();
                attacksCompleted = 0;
            }
        }
    }

    /// <summary>
    /// Called after the heal animation is complete
    /// </summary>
    private void ApplyHeal() {
        float potencyMultiplier = (potencyAugment.potencyType == PotencyType.INCREASE_MULTIPLIER) ? potencyAugment.impact : 0;
        
        targetEntity.BeHealed(baseSPAtkDMG.RandomInt * (specialAttackData.multiplier + potencyMultiplier), potencyAugment);
        PlayIdleAnimation();
        HealActionComplete();
        ActionComplete();
    }

    /// <summary>
    /// Used to process heal amount after all the calculations. RE: BeHealed(potencyAugment), BeHealed()
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="isEOT"></param>
    private void ProcessHeal(float amount, bool isEOT) {
        if (!isEOT) {
            FastPoolManager.GetPool((int)VFXId.HEAL_VFX, null, false).FastInstantiate(this.bottomVFXAnchor);
        }

        ScoreFlash.Instance.PushScreen(ScreenPositionTop, amount.ToString(), Color.green);
        currentHealth += amount;
        if (currentHealth > maxHealth) {
            currentHealth = maxHealth;
        }
        statBars.UpdateHealth(currentHealth);
    }

    /// <summary>
    /// Used to process damage after all of the calculations. RE: TakeDamage(), TakeEOTDamage()
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="isSpAtk"></param>
    /// <param name="knockback"></param>
    /// <param name="isEOT"></param>
    private void ProcessDamage(float damage, bool isSpAtk, float knockback, bool isEOT) {
        ScoreFlash.Instance.PushScreen(ScreenPositionTop, damage.ToString());

        //! TODO: Check if melee or spatk and do different vfx
        if (isSpAtk) {
            //! Get rid of this and put play spatk vfx method
            PlayMeleeDamageVFX();
        }
        else {
            PlayMeleeDamageVFX();
        }

        if (isEOT) { }
        else {
            PlayTakeDamageAnimation();
        }

        currentHealth -= damage;
        statBars.ApplyKnockback(knockback);
        statBars.UpdateHealth(currentHealth);

        if (currentHealth < 0) {
            currentHealth = 0;
            Die();
        }
    }

    private CombatEntity GetRandomTarget() {
        CombatEntity rVal = null;
        EntityType targetType = (this.Type == EntityType.PLAYER) ? EntityType.ENEMY : EntityType.PLAYER;
        rVal = EntityManager.Instance.GetRandomTarget(targetType);
        return rVal;
    }

    private IEnumerator ProcessSpecialAttackCooldown() {
        specialAttackCooldownCounter = specialAttackCooldown;
        while (specialAttackCooldownCounter > 0) {
            if (CombatManager.Instance.CurrentCombatState == CombatState.ACTIVE) {
                specialAttackCooldownCounter -= Time.deltaTime;
            }
            yield return null;
        }
        specialAttackCooldownCounter = 0;
        specialAttackOnCooldown = false;
    }

    private void ShowTargetBox(bool isTarget = false) {
        if (targetPointer != null) {
            targetPointer.Show(this.Type, isTarget);
        }
    }

    private void ShowTargetBox(Color c, bool isTarget = false) {
        if (targetPointer != null) {
            targetPointer.Show(c, this.Type, isTarget);
        }
    }

    private void HideTargetBox() {
        if (targetPointer != null) {
            targetPointer.Hide();
        }
    }

    private bool IsLowHealth() {
        return (currentHealth <= (maxHealth / 4));
    }

    #region ANIMATION
    private void PlayIdleAnimation() {
        if (isDefending) {
            PlayDefendAnimation();
        }
        else if (IsLowHealth()) {
            PlayIdleHurtAnimation();
        }
        else {
            skeletonAnimationObject.loop = true;
            skeletonAnimationObject.AnimationName = (this.Data.entityType == EntityType.PLAYER) ? IDLE_ANIMATION_NAME : "tritchIdle";
        }
    }

    private void PlayDefendAnimation() {
        skeletonAnimationObject.loop = true;
        skeletonAnimationObject.AnimationName = DEFEND_ANIMATION_NAME;
    }

    private void PlayDodgeAnimation() {
        skeletonAnimationObject.loop = false;
        skeletonAnimationObject.AnimationName = (Type == EntityType.PLAYER) ? DODGE_ANIMATION_NAME : "tritchDodge";
        skeletonAnimationObject.state.Complete += OnDodgeAnimationComplete;
    }

    private void OnDodgeAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnDodgeAnimationComplete;
        PlayIdleAnimation();
    }

    private void PlayRunAnimation() {
        skeletonAnimationObject.loop = true;
        skeletonAnimationObject.AnimationName = (Type == EntityType.PLAYER) ? RUN_ANIMATION_NAME : "tritchRun";
    }

    private void PlayHealAnimation() {
        skeletonAnimationObject.loop = false;
        skeletonAnimationObject.AnimationName = HEAL_ANIMATION_NAME;
        skeletonAnimationObject.state.Complete += OnHealAnimationComplete;
    }

    private void OnHealAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnHealAnimationComplete;
        if (isAlive) {
            ApplyHeal();
        }
    }

    private void PlayMeleeAttackAnimation() {
        swingAdded = true;
        swingRemoved = false;
        hitAdded = true;
        hitRemoved = false;

        skeletonAnimationObject.loop = false;
        skeletonAnimationObject.AnimationName = (this.Data.entityType == EntityType.PLAYER) ? MELEE_ANIMATION_NAME : "tritchMelee";
        skeletonAnimationObject.state.Event += MeleeSwingHitEvent;
        skeletonAnimationObject.state.Complete += OnMeleeSwingAnimationComplete;
        //! These event listeners could still be active when the entity dies, cause they can now die while moving.
    }

    private void MeleeSwingHitEvent(AnimationState state, int trackIndex, Event e) {
        if (e.Data.name == HIT_EVENT_NAME) {
            hitRemoved = true;
            skeletonAnimationObject.state.Event -= MeleeSwingHitEvent;
            ApplyAttackDamage();
        }
    }

    private void OnMeleeSwingAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnMeleeSwingAnimationComplete;
        swingRemoved = true;
        if (isAlive) {
            MoveBackToBasePosition();
        }
    }

    private void PlayRangedAttackAnimation() {
        skeletonAnimationObject.loop = true;
        skeletonAnimationObject.AnimationName = RANGED_ANIMATION_NAME;
        skeletonAnimationObject.state.Complete += OnRangedAnimationComplete;
    }

    private void OnRangedAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        if (loopCount == maxRangedAnimationLoops) {
            skeletonAnimationObject.state.Complete -= OnRangedAnimationComplete;
            if (isAlive) {
                ApplyAttackDamage();
                PlayReloadAnimation();
            }
        }
    }

    private void PlayReloadAnimation() {
        if (isAlive) {
            skeletonAnimationObject.loop = false;
            skeletonAnimationObject.AnimationName = GUN_RELOAD_ANIMATION_NAME;
            skeletonAnimationObject.state.Complete += OnReloadAnimationComplete;
        }
    }

    private void OnReloadAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnReloadAnimationComplete;
        if (isAlive) {
            PlayIdleAnimation();
            ActionComplete();
        }
    }

    private void PlayJumpAnimation() {
        skeletonAnimationObject.loop = false;
        skeletonAnimationObject.AnimationName = JUMP_ANIMATION_NAME;
        skeletonAnimationObject.state.Complete += OnJumpAnimationComplete;
    }

    private void OnJumpAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnJumpAnimationComplete;
        PlayIdleAnimation();
    }

    private void PlayCelebrationAnimation() {
        skeletonAnimationObject.loop = true;
        skeletonAnimationObject.AnimationName = CELEBRATION_ANIMATION_NAME;
    }

    private void PlayTakeDamageAnimation() {
        if (skeletonAnimationObject.AnimationName != TAKE_DAMAGE_ANIMATION_NAME || skeletonAnimationObject.AnimationName != "tritchTakeDamage") {
            skeletonAnimationObject.loop = false;
            skeletonAnimationObject.AnimationName = (isDefending) ? DEFEND_HIT_ANIMATION_NAME : (Type == EntityType.PLAYER) ? TAKE_DAMAGE_ANIMATION_NAME : "tritchTakeDamage";
            skeletonAnimationObject.state.Complete += OnTakeDamageAnimationComplete;
        }
    }

    private void OnTakeDamageAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnTakeDamageAnimationComplete;
        if (skeletonAnimationObject.AnimationName == TAKE_DAMAGE_ANIMATION_NAME || skeletonAnimationObject.AnimationName == "tritchTakeDamage" || skeletonAnimationObject.AnimationName == DEFEND_HIT_ANIMATION_NAME) {
            PlayIdleAnimation();
        }
    }

    private void PlayIdleHurtAnimation() {
        skeletonAnimationObject.loop = true;
        skeletonAnimationObject.AnimationName = (this.Data.entityType == EntityType.PLAYER) ? IDLE_HURT_ANIMATION_NAME : "tritchIdleHurt";
    }

    #endregion ANIMATION

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    #region VFX
    //public void PlaySelectionVFX() {
    //    selectionVFXObject.SetActive(true);
    //}

    public void PlayMeleeDamageVFX() {
        meleeDamageVFXObject.SetActive(true);
    }

    public void PlayRangedDamageVFX() {
        rangedReticle.DOScale(1, 0.5f).SetEase(Ease.OutElastic).OnComplete(OnRangedReticleScaleUpComplete);
        gunDamageVFXObject.SetActive(true);
    }

    private void OnRangedReticleScaleUpComplete() {
        rangedReticle.DOScale(0, 0.5f).SetEase(Ease.InBack);
    }

    public void StopRangedDamageVFX() {
        gunDamageVFXObject.SetActive(false);
    }
    #endregion VFX

    #region DEBUG
    //! REMOVE THIS SHIT (CheatMenu)
    public bool IsDebugPaused { get { return statBars.debugPaused; } set { statBars.debugPaused = value; } }
    public void DebugThink() {
        strategyController.StartThinking();
    }
    #endregion DEBUG

    public void BeginCombat() {
        RunIntoCamera();
    }

    public void SetDataObject(CharacterID id) {
        characterDataObject = DataManager.Instance.CharacterDataObjectByID(id);
        if (characterDataObject == null) {
            Debug.Log("The id passed to SetDataObject (presumably from the gameprogress data file) is invalid. Defaulted entity to Assassin");
            characterDataObject = DataManager.Instance.CharacterDataObjectByID(CharacterID.ASSASSIN);
        }

        SetDataValues();
    }

    public void SetListIndex(int value) {
        listIndex = value;
    }

	public void SetOffScreen() {
        float x = (this.Type == EntityType.PLAYER) ? -1000 : 1000;
		transform.position = new Vector2(x, transform.position.y);
	}

    public void SetBasePosition(Vector2 pos) {
        basePosition = pos;
    }

    public void AddBaseAtkDMG(float amount) {
        baseAtkDMG.AddToRange(amount);
    }

    public void AddBaseSPAtkDMG(float amount) {
        baseSPAtkDMG.AddToRange(amount);
    }

    public void AddBaseSPD(float amount) {
        statBars.AddTimeBetweenAttacks(amount);
    }

    public void ResetData() {
        CheckFlip();
        SetDataObject(LevelManager.Instance.GetNextEnemy());
        maxHealth = Data.maxHealth;
        currentHealth = maxHealth;

        elementalUnitIcon.SetElementalType(characterData.elementalAffinity);

        SetSkinFromData();
    }

    public void CombatStateChanged(CombatState state) {
        //! This is foolishly assuming that the only time the entity would be defending AND have a target, is when the player set the entity to attack
        //! something during a waiting phase
        if (state == CombatState.ACTIVE) {
            if (isDefending && targetEntity != null) {
                if (isDefending) {
                    isDefending = false;
                    statBars.StartTimer();
                    PlayIdleAnimation();
                }
            }
            if (healSpecialQueued) {
                StartCoroutine(HealTarget());
            }
        }
        else if (state == CombatState.WAITING) {
            
        }
    }

    public void ShowBoxOnTarget() {
        if (isSelected) {
            HideBoxOnTarget();
            if (targetEntity != null) {
                targetEntity.ShowTargetBox(Color.red, true);
            }
            this.ShowTargetBox();
        }
    }

    public void HideBoxOnTarget() {
        if (targetEntity != null) {
            targetEntity.HideTargetBox();
        }
        this.HideTargetBox();
    }

    public void SetTarget(CombatEntity entity) {
        if (getRandomTarget && this.Type == EntityType.PLAYER) {
            getRandomTarget = false;
            attacksCompleted = 0;
        }

        if (isIdle) {
            if (isSelected && targetEntity != null) {
                targetEntity.HideTargetBox();
            }
            AssignTargetEntityAndStoreLast(entity);
            queuedTargetEntity = null;
            if (isSelected) {
                ShowBoxOnTarget();
            }
        }
        else {
            queuedTargetEntity = entity;
        }
    }

    public void Attack() {
        strategyController.StartThinking();
        targetEntity = strategyController.Target;

        if (targetEntity != null) {

            switch (orderToExecute.actionOrder) {
                case CombatStrategyActionOrder.Heal:
                    //!+ The whole execute action into actually doing the action system needs to be cleaned up...
                    //ExecuteHealAction(targetEntity);

                    SetTarget(targetEntity);
                    queuedActionIcon.SetHealSpecialQueued();

                    healSpecialQueued = true;
                    specialAttackQueued = false;
                    defendActionQueued = false;

                    //CombatUIManager.Instance.DeselectAction();

                    StartCoroutine(HealTarget());
                    break;
                case CombatStrategyActionOrder.Attack:
                    if (Data.attackType == AttackType.MELEE) {
                        StartCoroutine(MoveToAttack());
                    }
                    else if (Data.attackType == AttackType.RANGED) {
                        StartCoroutine(TakeAimForAttack());
                    }
                    break;
                case CombatStrategyActionOrder.SpecialAttack:
                    StartCoroutine(ExecuteSpecialAttack());
                    break;
                case CombatStrategyActionOrder.Defend:
                    isDefending = true;
                    defendActionQueued = false;
                    PlayDefendAnimation();
                    break;
                case CombatStrategyActionOrder.None:
                default:
                    Debug.Log("This shouldn't have happened".Colored(Color.yellow).Sized(10));
                    break;
            }

        }
    }

    public void TakeDamage(float damage, bool isSpAtk, AugmentData potAugment, ElementalType elementalType, float knockback) {
        bool dodged = (isSpAtk) ? false : baseEvasion.Success;
        bool ignoreElement = (potAugment.potencyType == PotencyType.IGNORE_ELEMENT);
        knockback += (potAugment.potencyType == PotencyType.INCREASE_KNOCKBACK) ? potAugment.impact : 0;

        // If elemental reduction is needed
        if (!ignoreElement && elementalType != ElementalType.NONE && elementalType == Data.elementalAffinity) {
            damage -= Mathf.RoundToInt(damage * elementalReductionFactor.Random);
        }

        if (dodged) {
            ScoreFlash.Instance.PushScreen(ScreenPositionTop, "Dodge");

            PlayDodgeAnimation();
        }
        else {
            if (isDefending && !isSpAtk) {
                damage /= defendReductionFactor;
                damage = Mathf.RoundToInt(damage);
            }

            if (potAugment.potencyType == PotencyType.ADD_EOT) {
                if (potAugment.chance.Success) {
                    eotManager.AddEOT(potAugment.impact, false, potAugment.duration);
                }
            }

            ProcessDamage(damage, isSpAtk, knockback, false);
        }
    }

    public void TakeEOTDamage(float value) {
        // The true being passed in here is just setting the damage to show a spatk vfx (when it is eventually implemented)
        ProcessDamage(value, true, 0, true);
    }

    /// <summary>
    /// Generic Heal Call
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="isEOT"></param>
    public void BeHealed(float amount, bool isEOT) {
        ProcessHeal(amount, isEOT);
        statBars.UpdateHealth(currentHealth);
    }

    /// <summary>
    /// Heal Call when cast by an entity with a potency augment
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="potAug"></param>
    public void BeHealed(float amount, AugmentData potAug) {
        if (potAug != null && potAug.potencyType == PotencyType.ADD_EOT) {
            if (potAug.chance.Success) {
                eotManager.AddEOT(potAug.impact, true, potAug.duration);
            }
        }

        ProcessHeal(amount, false);
    }

    public void Die() {
        if (!isIdle) {
            Debug.Log("UID: " + UID.Value + " just died and was NOT idle. It was targetting UID: " + ((targetEntity != null) ? targetEntity.UID.Value : -99));
            if (targetEntity.IsAlive) {
                targetEntity.AttacksIncoming--;
                if (targetEntity.AttacksIncoming < 0) {
                    Debug.Log("Attacks Incoming dropped below 0, Target UID: " + targetEntity.UID.Value + " Target Attacks Incoming: " + targetEntity.attacksIncoming + " Attacking Entity UID: " + UID.Value);
                    targetEntity.AttacksIncoming = 0;
                    Debug.Log(moveAdded + " || " + moveRemoved + " || " + hitAdded + " || " + hitRemoved + " || " + swingAdded + " || " + swingRemoved);
                }
            }
        }

        transform.DOKill();
        PlayIdleAnimation();
        eotManager.Clear();

        HideTargetBox();
        CombatManager.Instance.FireDeathSkullVFX(Position);
        CombatManager.Instance.RemoveFromTurnQueue(this);

        float x = (this.Type == EntityType.PLAYER) ? -1000 : 1000;
        Vector2 pos = new Vector2(x, transform.position.y);
        transform.position = pos;
        isIdle = false;
        isAlive = false;

        statBars.OnTimerCompleteEvent.RemoveListener(OnAttackTimerComplete);
        statBars.EntityDied();

        EntityManager.Instance.UnregisterCombatEntity(this);
        
        StartCoroutine(ResetFromDeath());
    }

    #endregion PUBLIC_METHODS

    void OnDrawGizmos() {
        if (targetEntity != null) {
            Gizmos.color = (this.Type == EntityType.PLAYER) ? Color.blue : Color.red;
            Gizmos.DrawIcon(transform.position, "GizmoTarget.png", true);
            Gizmos.DrawLine(transform.position, targetEntity.Position);

            if (attacksIncoming > 0) {
                Gizmos.DrawWireCube(transform.position, new Vector2(50, 50));
            }
        }
    }

    // Update is called once per frame
    void Update() {

    }

}
