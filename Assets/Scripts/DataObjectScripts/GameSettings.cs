﻿using UnityEngine;
using System.Collections;

public class GameSettings : GameDataObject {

    private static GameSettings instance;
    public static GameSettings Instance {
        get {
            if (!instance) {
                if (System.IO.File.Exists(SavedSettingsPath)) {
                    LoadFromJSON();
                }
                else {
                    InitializeFromDefault(Resources.Load<GameSettings>("Data/GameSettings"));
                }
            }
        #if UNITY_EDITOR
            if (!instance) {
                if (System.IO.File.Exists(SavedSettingsPath)) {
                    LoadFromJSON();
                }
                else {
                    InitializeFromDefault((GameSettings)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/GameSettings.asset", typeof(SpecialAttackDataInventory)));
                }
            }
        #endif
            return instance;
        }
    }

    public static string SavedSettingsPath {
        get {
            return System.IO.Path.Combine(Application.persistentDataPath, "Game-Settings.json");
        }
    }

    public GameSettings TemplateObject {
        get {
            #if UNITY_EDITOR
                return (GameSettings)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/GameSettings.asset", typeof(GameSettings));
            #else
                return Resources.Load<GameSettings>("Data/GameSettings");
#endif
        }
    }

    public CombatState userCombatState = CombatState.WAITING;
    public bool showUnitElementalAffinity = true;
    public bool showUnitQueuedAction = true;


    #if UNITY_EDITOR
    [UnityEditor.MenuItem("Window/Game Settings")]
    public static void ShowGameSettings() {
        UnityEditor.Selection.activeObject = UnityEditor.AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/GameSettings.asset");
        UnityEditor.EditorGUIUtility.PingObject(UnityEditor.Selection.activeObject);
    }
    #endif

    public override GameDataObject InitializeFromTemplate() {
        return InitializeFromDefault(TemplateObject);
    }

    public override GameDataObject LoadFromLocalJSON() {
        return LoadFromJSON();
    }

    public override void SaveToLocalJSON() {
        SaveToJSON();
    }

    public override bool LocalExists() {
        return System.IO.File.Exists(SavedSettingsPath);
    }

    public static GameSettings InitializeFromDefault(GameSettings settings) {
        if (instance) {
            //DestroyImmediate(instance);
            instance = null;
        }
        //instance = settings;
        instance = Instantiate(settings);
        instance.SaveToJSON();
        //instance = Instantiate(settings);
        //instance.hideFlags = HideFlags.HideAndDontSave;
        return instance;
    }

    public static GameSettings LoadFromJSON() {
        if (instance) {
            //DestroyImmediate(instance);
            instance = null;
        }
        instance = ScriptableObject.CreateInstance<GameSettings>();
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(SavedSettingsPath), instance);
        //instance.hideFlags = HideFlags.HideAndDontSave;
        return instance;
    }

#if UNITY_EDITOR
    public static void OverwriteFromJSON() {
        instance = Resources.Load<GameSettings>("Data/GameSettings");
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(SavedSettingsPath), instance);
        UnityEditor.AssetDatabase.SaveAssets();
    }
#endif

    public void SaveToJSON() {
        Debug.LogFormat("Saving game settings to {0}", SavedSettingsPath);
        System.IO.File.WriteAllText(SavedSettingsPath, JsonUtility.ToJson(this, true));
    }

}
