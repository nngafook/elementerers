﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class CharacterDataObject : ScriptableObject {

    public CharacterData data;

}

[Serializable]
public class CharacterData {
    [Tooltip("Not the same as CharacterID. This one is used per entity and is unique. Also matches the sprite name in the ui spritesheet. CharacterID can be the same across multiple entities")]
    public string id;
    public string characterName;

    [Space(10)]
    public EntityType entityType;
    public AttackType attackType;

    [Space(10)]
    [Tooltip("Used by the level and other things to identify which character the entity is. Is not unique per entity type")]
    public CharacterID characterID = CharacterID.NONE;
    [Tooltip("Used to set the skin of the spine skeleton")]
    public CharacterSkinName skinName;
    
    [Space(10)]
    [Tooltip("I believe this is only the special attack type that the default character comes with. This will be overridden at runtime by the save info")]
    public SpecialAttackType specialAttackType;
    [Space(10)]
    public Sprite uiImage;

    [Space(10)]
    [Range(0, 100)]
    [Tooltip("At 100% move speed, it takes 0.5 seconds to get to the target. Anything less is added on to 0.5 seconds.")]
    public int moveSpeed = 100;
    
    public float maxHealth = 1000;
    public float knockback = 0.75f;
    public PercentageFloat evasion = new PercentageFloat(5);

    [Space(10)]
    [Tooltip("Element that the entity is strong against, and attacks with")]
    public ElementalType elementalAffinity = ElementalType.NONE;

    [Space(10)]
    public RangedFloat timeBetweenAttacks = new RangedFloat(2, 4);
    public RangedFloat attackDamage;
    public RangedFloat specialAttackDamage;
    public RangedFloat defense;

    [Header("AI")]
    public bool getRandomTarget = false;
    public RangedFloat attacksBeforeTargetSwitch = new RangedFloat(2, 8);
    [Tooltip("This is only used for enemy AI. Player combat orders will be stored in GameProgress.")]
    public CombatOrderListDataObject combatOrders;

    [Space(20)]
    [Header("Notes")]
    public List<StringStringKeyValue> notes;

    public string SkinName {
        get {
            string rVal = "";

            rVal = skinName.ToString();
            rVal = rVal.Replace('_', '-');
            
            //! TEMP
            rVal = "Rogue";
            if (skinName == CharacterSkinName.PamelaFrost_03) { rVal = "Pamela"; }
            if (skinName == CharacterSkinName.MetalMan) { rVal = "Tritch"; }
            if (skinName == CharacterSkinName.Fletch) { rVal = "Tritch"; }
            if (skinName == CharacterSkinName.Dummy) { rVal = "Tritch"; }
            
            return rVal;
        }
    }

    public SpecialAttackType SpecialAttackType { 
        get {
            return specialAttackType; 
        } 
    }

}

[Serializable]
public class CharacterSaveInfo {
    public CharacterID ownerID;
    public SpecialAttackType equippedSpecialAttack;
    public string modifierAugmentSlotID = "";
    public string potencyAugmentSlotID = "";
    public int level = 1;
    public int exp = 0;
    public List<CombatOrder> combatOrders = new List<CombatOrder>();
    public CombatOrder fallbackCombatOrder;

    public CharacterSaveInfo(CharacterID id, SpecialAttackType spAtk) {
        ownerID = id;
        level = 1;
        equippedSpecialAttack = spAtk;
        combatOrders = new List<CombatOrder>();
        for (int i = 0; i < level; i++) {
            combatOrders.Add(new CombatOrder());
        }
        fallbackCombatOrder = new CombatOrder();
    }
}

public enum CharacterSkinName {
    Assassin                      = 0,
    BeardyBuck                  = 10,
    ChuckMatthews            = 20,
    Dummy                        = 30,
    Fletch                         = 40,
    GabrielCaine                 = 50,
    MetalMan                     = 60,
    PamelaFrost_03             = 70,
    PamelaFrost_04             = 80,
    StumpyPete                  = 90,
    TurboTed                      = 100,
    YoungBuck                    = 110
}

public enum CharacterID {
    ASSASSIN                = 0,
    BEARDY_BUCK           = 10,
    CHUCK_MATTHEWS    = 20,
    GABRIEL_CAINE         = 30,
    METAL_MAN             = 40,
    PAMELA_FROST        = 50,
    SHANIA_FROST        = 60,
    STUMPY_PETE         = 70,
    TURBO_TED            = 80,
    YOUNG_BUCK          = 90,

    //! Enemies
    eMETAL_MAN_ENEMY = 200,
    eFLETCH                  = 210,
    eDUMMY                  = 220,


    NONE = 500
}