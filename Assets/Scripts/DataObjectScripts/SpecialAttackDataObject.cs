﻿using UnityEngine;
using System.Collections;
using System;

 [CreateAssetMenu(menuName = "DataObjects/SpecialAttack")]
public class SpecialAttackDataObject : ScriptableObject {

    public SpecialAttackData data;

}

[Serializable]
public class SpecialAttackData {

    public bool isOffensive = true;
    
    [Space(10)]
    public SpecialAttackType specialAttackType = SpecialAttackType.NONE;

    [Header("Details")]
    public string name = "";
    public string description = "";
    public int storeCost = -816;

    [Space(10)]
    public float knockback = 1.25f;
    public float cooldown = 10;
    [Tooltip("This is multiplied by the characters base spAtkDmg. This can be modified by augments")]
    public int multiplier;

    [Space(10)]
    public Sprite uiImage;
    public Color overlayColor;
    public Color outlineColor;

    //[Space(10)]
    //public AugmentData damageAugmentData;
    //public AugmentData healingAugmentData;
    //public AugmentData effectAugmentData;

    [Space(10)]
    //public ModifierType[] effectTypes;
    public AttackType attackType = AttackType.NONE;
    public ElementalType elementalType = ElementalType.NONE;

    //! I think this is unnecessary, cause two augments can mean two different effect types being applied. 
    //! Left in for now, until I can go refactor all the code to use the character's augments and effect types
    //[Tooltip("Affected by the augment in effect augment slot")]
    //public ModifierType modifierType = ModifierType.NONE;

    //[Space(10)]
    //[Tooltip("Affected by the augment in damage augment slot of entity")]
    //public RangedFloat baseDamage = new RangedFloat();
    //[Tooltip("Affected by the augment in healing augment slot of entity")]
    //public RangedFloat healPower = new RangedFloat(100, 200);
}

// Essentially the "name" of the skill. Each special data object will be assigned via these.
public enum SpecialAttackType {
    KILL,
    BACKSTAB,
    STUN,
    DRAIN,

    LASER,
    CONCUSSION_SHOT,
    GRENADE,

    HEAL,

    HAPPINESS = 98,
    NONE = 99
}

/*
 *  A special attack can have a limited number of augments attached. A special attack will have 2 slots each
 *  (most of them). One is a damage augment, one is an effect augment.
 *  Augments do things like add aoe or bleed or whatever. Characters have special attacks by default, but they can also buy/create
 *  new special attacks. So the customization comes down to customizing special attacks and not 
 *  characters perse. An augment adds an effect. EG. An augment can be an AOE augment with 5 added dmg.
 *  Or an AOE with 10 added dmg. If the special attack has a free augment slot, it can be added to the
 *  special attack.
*/