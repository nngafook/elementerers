﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(menuName = "DataObjects/AugmentDataObject")]
public class AugmentDataObject : ScriptableObject {

    public AugmentData data;
	
}

/// <summary>
/// Because Augments are premade by designer(s) they need unique id's,
/// So the equipped augments can be saved and reloaded via GameProgress
/// An augment can be of two different types. 
/// - Modifier (eg. adds aoe, or row, or all, or overTimeEffect to special attack)
/// - Potency (eg. increases damage/heal power)
/// An Entity can have two augments. One Modifier and one Potency
/// </summary>
[Serializable]
public class AugmentData {

    public string id = "";
    public string name = "";

    public AugmentType augmentType = AugmentType.MODIFIER;

    [Space(10)]
    //[Tooltip("Changes the elemental type of the augment")]
    //public ElementalType elementalType;
    //[Tooltip("Adds an effect to the special attack - eg. bleed, aoe, elemental increase/decrease")]
    public ModifierType effectType = ModifierType.NONE;
    public PotencyType potencyType = PotencyType.NONE;

    [Header("Values that I'm sure we'll need, but have no idea the context yet.")]
    public ElementalType elementalType = ElementalType.NONE;
    public float impact = 0;
    public float duration = 0;
    public PercentageFloat chance = new PercentageFloat(0);

    public AugmentData(AugmentType type) {
        id = "enemyTemp";
        name = "If you see this, your mom wants you to call her";
        augmentType = type;
        effectType = ModifierType.NONE;
        potencyType = PotencyType.NONE;
    }
}

public enum AugmentType {
    MODIFIER,
    POTENCY
}