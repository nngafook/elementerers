﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpecialAttackDataInventory : GameDataObject {

    private static SpecialAttackDataInventory instance;
    public static SpecialAttackDataInventory Instance {
        get {
            if (!instance) {
                if (System.IO.File.Exists(SavedInventoryPath)) {
                    LoadFromJSON();
                }
                else {
                    InitializeFromDefault(Resources.Load<SpecialAttackDataInventory>("Data/SpecialAttackDataInventory"));
                }
            }
            #if UNITY_EDITOR
            if (!instance) {
                if (System.IO.File.Exists(SavedInventoryPath)) {
                    LoadFromJSON();
                }
                else {
                    InitializeFromDefault((SpecialAttackDataInventory)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/SpecialAttackDataInventory.asset", typeof(SpecialAttackDataInventory)));
                }
            }
            #endif
            return instance;
        }
    }

    public static string SavedInventoryPath {
        get {
            return System.IO.Path.Combine(Application.persistentDataPath, "SpecialAttack-Inventory.json");
        }
    }

    public SpecialAttackDataInventory TemplateObject {
        get {
            #if UNITY_EDITOR
                return (SpecialAttackDataInventory)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/SpecialAttackDataInventory.asset", typeof(SpecialAttackDataInventory));
            #else
                return Resources.Load<SpecialAttackDataInventory>("Data/SpecialAttackDataInventory");
            #endif
        }
    }

    public List<SpecialAttackData> items;

    #region SECRET_STUFF

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Window/Special Attack Data Inventory")]
    public static void ShowSpecialAttackInventoy() {
        UnityEditor.Selection.activeObject = UnityEditor.AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/SpecialAttackDataInventory.asset");
        UnityEditor.EditorGUIUtility.PingObject(UnityEditor.Selection.activeObject);
    }
#endif

    public override GameDataObject InitializeFromTemplate() {
        return InitializeFromDefault(TemplateObject);
    }

    public override GameDataObject LoadFromLocalJSON() {
        return LoadFromJSON();
    }

    public override void SaveToLocalJSON() {
        SaveToJSON();
    }

    public static SpecialAttackDataInventory InitializeFromDefault(SpecialAttackDataInventory dataObject) {
        if (instance) {
            //DestroyImmediate(instance);
            instance = null;
        }
        //instance = dataObject;
        instance = Instantiate(dataObject);
        instance.SaveToJSON();
        //instance.hideFlags = HideFlags.HideAndDontSave;
        return instance;
    }

    public static SpecialAttackDataInventory LoadFromJSON() {
        if (instance) {
            //DestroyImmediate(instance);
            instance = null;
        }
        instance = ScriptableObject.CreateInstance<SpecialAttackDataInventory>();
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(SavedInventoryPath), instance);
        //instance.hideFlags = HideFlags.HideAndDontSave;
        return instance;
    }

#if UNITY_EDITOR
    public static void OverwriteFromJSON() {
        instance = Resources.Load<SpecialAttackDataInventory>("Data/SpecialAttackDataInventory");
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(SavedInventoryPath), instance);
        UnityEditor.AssetDatabase.SaveAssets();
    }
#endif

    public void SaveToJSON() {
        Debug.LogFormat(("Saving game progress to {0}").Colored(Color.magenta), SavedInventoryPath);
        System.IO.File.WriteAllText(SavedInventoryPath, JsonUtility.ToJson(this, true));
    }

    public override bool LocalExists() {
        return System.IO.File.Exists(SavedInventoryPath);
    }

    #endregion SECRET_STUFF

    public void AddItem(SpecialAttackData data) {
        items.Add(data);
        SaveToJSON();
    }

    public void Clear() {
        items.Clear();
        SaveToJSON();
    }

    public bool OwnsSpecialAttack(SpecialAttackType type) {
        bool rVal = false;
        for (int i = 0; i < items.Count; i++) {
            if (items[i].specialAttackType == type) {
                rVal = true;
                break;
            }
        }
        return rVal;
    }

}
