﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "DataObjects/Dialogue/DialogueBlock")]
public class DialogueBlockDataObject : ScriptableObject {

    [Space(10)]
    public bool endOfConversation = false;

    [Space(10)]
    public List<DialogueLine> lines = new List<DialogueLine>();

}