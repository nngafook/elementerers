﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "DataObjects/CombatOrderList")]
public class CombatOrderListDataObject : ScriptableObject {

    public CombatOrder fallbackOrder = new CombatOrder();
    [Space(10)]
    public List<CombatOrder> orderList = new List<CombatOrder>();


}
