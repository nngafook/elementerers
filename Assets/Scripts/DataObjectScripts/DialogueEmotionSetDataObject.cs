﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "DataObjects/Dialogue/DialogueEmotionSet")]
public class DialogueEmotionSetDataObject : ScriptableObject {

    /*
     *  [Neutral]    [Smile]     [Happy]
     *   [Frown]    [Angry]
    */

    public string speakerName;
    public Sprite spritesheet;

    private Vector2 GetRowColumnFromEmotionName(EmotionName emotion) {
        Vector2 rVal = Vector2.zero;
        switch (emotion) {
            case EmotionName.NEUTRAL:
                rVal = new Vector2(1, 0);
                break;
            case EmotionName.SMILE:
                rVal = new Vector2(1, 1);
                break;
            case EmotionName.HAPPY:
                rVal = new Vector2(1, 2);
                break;
            case EmotionName.FROWN:
                rVal = new Vector2(0, 0);
                break;
            case EmotionName.ANGRY:
                rVal = new Vector2(0, 1);
                break;
            default:
                break;
        }
        return rVal;
    }

    public Sprite GetSprite(EmotionName emotion) {
        Vector2 rowColumn = GetRowColumnFromEmotionName(emotion);
        Texture2D tex = new Texture2D(256, 256);
        Color[] data = spritesheet.texture.GetPixels((int)rowColumn.y * 256, (int)rowColumn.x * 256, 256, 256);
        tex.SetPixels(data);
        tex.Apply();
        return Sprite.Create(tex, new Rect(0, 0, 256, 256), new Vector2(0.5f, 0.5f));
    }

}

public enum EmotionName {
    NEUTRAL,
    SMILE,
    HAPPY,
    FROWN,
    ANGRY
}