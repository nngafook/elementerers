﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu(menuName = "DataObjects/Dialogue/DialogueConversation")]
public class DialogueConversationDataObject : ScriptableObject {

    public string conversationTitle = "";

    [Space(10)]
    public SpeakerSide firstSpeaker= SpeakerSide.Left;

    [Space(10)]
    public string leftSpeaker = "";
    public string rightSpeaker = "";

    [Space(10)]
    public List<DialogueBlockDataObject> leftSpeakerBlocks = new List<DialogueBlockDataObject>();
    public List<DialogueBlockDataObject> rightSpeakerBlocks = new List<DialogueBlockDataObject>();

}

[Serializable]
public class DialogueLine {

    [TextAreaAttribute(1, 3)]
    [Multiline]
    public string text;

    [Space(10)]
    public EmotionName emotion;
    public DialogueSpeakerAnimation startAnimation = DialogueSpeakerAnimation.None;
    //public DialogueSpeakerAnimation endAnimation = DialogueSpeakerAnimation.None;
    public DialogueLineTransition lineTransition = DialogueLineTransition.None;
    public List<string> transitionLines = new List<string>();

    public DialogueLine(string txt = "") {
        text = txt;
    }
}

public enum SpeakerSide {
    Left,
    Right
}

public enum DialogueSpeakerAnimation {
    SlideIn,
    SlideOut,
    Jump,
    Punch,

    None
}

public enum DialogueLineTransition {
    FadeToBlack,
    FadeToBlackWithText,

    None
}