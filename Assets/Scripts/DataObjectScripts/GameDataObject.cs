﻿using UnityEngine;
using System.Collections;

public abstract class GameDataObject : ScriptableObject {

    public abstract GameDataObject LoadFromLocalJSON();
    public abstract GameDataObject InitializeFromTemplate();
    public abstract void SaveToLocalJSON();
    public abstract bool LocalExists();

}
