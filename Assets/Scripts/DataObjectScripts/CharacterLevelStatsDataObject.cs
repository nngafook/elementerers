﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu(menuName = "DataObjects/CharacterLevelStatsDataObject")]
public class CharacterLevelStatsDataObject : ScriptableObject {
    public List<CharacterLevelStats> characterLevelStats = new List<CharacterLevelStats>();

    public CharacterLevelStats StatsByID(CharacterID id) {
        CharacterLevelStats rVal = null;
        for (int i = 0; i < characterLevelStats.Count; i++) {
            if (characterLevelStats[i].idOwner == id) {
                rVal = characterLevelStats[i];
                break;
            }
        }
        return rVal;
    }
}

[Serializable]
public class CharacterLevelStats {

    public CharacterID idOwner;

    // Determinate == That is the value to be used/is needed
    // Additive == That value is added on to the current base stat value

    [Tooltip("Determinate")]
    public List<int> expRequirements = new List<int>();
    
    [Tooltip("Additive")]
    public List<int> maxHealthLevelValues = new List<int>();
    
    [Tooltip("Determinate")]
    public List<int> combatOrderCapacities = new List<int>();

    [Tooltip("Additive")]
    public List<float> defenseLevelValues = new List<float>();
    
    [Tooltip("Additive")]
    public List<float> damageLevelValues = new List<float>();

    [Tooltip("Additive")]
    public List<float> evasionLevelValues = new List<float>();

    public CharacterLevelStats() { }

    public CharacterLevelStats(CharacterLevelStats o) {
        expRequirements = new List<int>(o.expRequirements);
        maxHealthLevelValues = new List<int>(o.maxHealthLevelValues);
        combatOrderCapacities = new List<int>(o.combatOrderCapacities);
        defenseLevelValues = new List<float>(o.defenseLevelValues);
        damageLevelValues = new List<float>(o.damageLevelValues);
        evasionLevelValues = new List<float>(o.evasionLevelValues);
    }

}