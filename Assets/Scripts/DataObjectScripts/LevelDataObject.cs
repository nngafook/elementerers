﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LevelDataObject : ScriptableObject {

    public LevelData data;

    #if UNITY_EDITOR
    void OnEnable() {
        data.VerifyArenaEventsFallback();
    }
    #endif
}

[Serializable]
public class LevelData {
    public string name;

    public int totalBosses;

    public LevelType levelType = LevelType.Story;

    public List<CharacterID> enemiesToSpawn = new List<CharacterID>();

    //! Arena
    public float timeBetweenDifficultyIncrease = 60f;
    public int arenaCompletionBonus = 1000;
    public int wavesBeforeBoost = 3;
    public List<ArenaWave> arenaWaves = new List<ArenaWave>();
    public List<ArenaEvent> arenaEvents = new List<ArenaEvent>();
    public List<WaveBoost> waveBoostsAvailable = new List<WaveBoost>();

    //! Tutorial
    public List<TutorialLevelEvent> tutorialEvents = new List<TutorialLevelEvent>();

    public LevelRewardsData rewardsData;

    #if UNITY_EDITOR
    public void VerifyArenaEventsFallback() {
        if (levelType == LevelType.Arena && arenaEvents.Count > 0) {
            int fallbacksFound = 0;
            for (int i = 0; i < arenaEvents.Count; i++) {
                if (arenaEvents[i].isFallback) {
                    fallbacksFound++;
                }
            }

            if (fallbacksFound == 0 || fallbacksFound > 1) {
                Debug.LogWarning("Level Data (" + name + ") defaulted the last arena event to be the fallback");
                for (int i = 0; i < arenaEvents.Count; i++) {
                    arenaEvents[i].isFallback = (i == arenaEvents.Count - 1);
                }
            }
        }
    }
    #endif
}

[Serializable]
public class LevelRewardsData {

    public int scrapReward;
    public int expReward;

}

[Serializable]
public class ArenaEvent {

    public ArenaEventType type = ArenaEventType.EnemyAtkDMGIncrease;

    [Tooltip("Percentage chance that this event gets used")]
    public PercentageFloat chance = new PercentageFloat();
    public bool isFallback = false;

    public float eAtkDMGIncrease = 0;
    public float eSPAtkDMGIncrease = 0;
    public float eSPDIncrease = 0;
    public float eRegenAmount = 0;

    public float pDamageDecrease = 0;

}

[Serializable]
public class ArenaWave {
    public int totalEnemiesInWave = 0;
    public List<CharacterID> enemyPool = new List<CharacterID>();
    public LevelRewardsData rewardsData;

    #if UNITY_EDITOR
    public bool expandedInEditor = false;
    #endif
}

[Serializable]
public class TutorialLevelEvent {
    public int killsNeeded;
    public List<string> messages = new List<string>();
}

public enum LevelType {
    Story,
    Arena,
    Tutorial
}

public enum ArenaEventType {
    EnemyAtkDMGIncrease,                // All enemies damage increased by x
    EnemySpAtkDMGIncrease,              // All enemies' spATK increased
    EnemySpeedIncrease,                 // All enemies charge their ATB quicker
    EnemyTimedRegenAmount,        // All enemies get a heal after x seconds

    PlayerDamageDecrease            // Player Unit damage decreased
}

public enum WaveBoost {
    Heal,
    DoubleScrap,
    TempDMG,


    None = 99
}