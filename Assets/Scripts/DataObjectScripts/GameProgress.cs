﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class GameProgress : GameDataObject {

    private static GameProgress instance;
    public static GameProgress Instance {
        get {
            if (!instance) {
                Debug.Log(SavedProgressPath);
                if (System.IO.File.Exists(SavedProgressPath)) {
                    LoadFromJSON();
                }
                else {
                    InitializeFromDefault(Resources.Load<GameProgress>("Data/GameProgress"));
                }
            }
            #if UNITY_EDITOR
            if (!instance) {
                if (System.IO.File.Exists(SavedProgressPath)) {
                    LoadFromJSON();
                }
                else {
                    InitializeFromDefault((GameProgress)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/GameProgress.asset", typeof(GameProgress)));
                }
            }
            #endif
            return instance;
        }
    }

    public static string SavedProgressPath {
        get {
            return System.IO.Path.Combine(Application.persistentDataPath, "Game-Progress.json");
        }
    }

    public GameProgress TemplateObject {
        get {
            #if UNITY_EDITOR
                return (GameProgress)UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Resources/Data/GameProgress.asset", typeof(GameProgress));
            #else
                return Resources.Load<GameProgress>("Data/GameProgress");
            #endif
        }
    }

    public GameProgressData data;

    public List<CharacterID> AvailableCharacters { get { return data.availableCharacters; } }
    public List<CharacterID> CurrentPartyIDs { get { return data.currentPartyIds; } }
    public List<CombatStrategyTargetOrder> OwnedTargetOrders { get { return data.ownedStrategyTargetOrders; } }
    public List<CombatStrategyActionOrder> OwnedActionOrders { get { return data.ownedStrategyActionOrders; } }
    public List<CombatStrategyPercentage> OwnedPercentageOrders { get { return data.ownedStrategyPercentageOrders; } }
    public List<CharacterSaveInfo> CharacterSaveInfos { get { return data.characterSaveInfos; } }
    public int CurrentScrap { get { return data.currentScrap; } }

    #region SECRET_STUFF

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Window/Game Progress")]
    public static void ShowGameProgress() {
        UnityEditor.Selection.activeObject = UnityEditor.AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/GameProgress.asset");
        UnityEditor.EditorGUIUtility.PingObject(UnityEditor.Selection.activeObject);
    }
    #endif

    public override GameDataObject InitializeFromTemplate() {
        return InitializeFromDefault(TemplateObject);
    }

    public override GameDataObject LoadFromLocalJSON() {
        return LoadFromJSON();
    }

    public override void SaveToLocalJSON() {
        SaveToJSON();
    }

    public override bool LocalExists() {
        return System.IO.File.Exists(SavedProgressPath);
    }

    public static GameProgress InitializeFromDefault(GameProgress dataObject) {
        //if (instance) {
        //    DestroyImmediate(instance);
        //}
        instance = null;
        //instance = dataObject;
        instance = Instantiate(dataObject);
        instance.SaveToJSON();
        //instance.hideFlags = HideFlags.HideAndDontSave;
        return instance;
    }

    public static GameProgress LoadFromJSON() {
        if (instance) {
            //DestroyImmediate(instance);
            instance = null;
        }
        instance = ScriptableObject.CreateInstance<GameProgress>();
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(SavedProgressPath), instance);
        //instance.hideFlags = HideFlags.HideAndDontSave;
        return instance;
    }

    #if UNITY_EDITOR
    public static void OverwriteFromJSON() {
        instance = Resources.Load<GameProgress>("Data/GameProgress");
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(SavedProgressPath), instance);
        UnityEditor.AssetDatabase.SaveAssets();
    }
    #endif

    public void SaveToJSON() {
        Debug.LogFormat(("Saving game progress to {0}").Colored(Color.magenta), SavedProgressPath);
        System.IO.File.WriteAllText(SavedProgressPath, JsonUtility.ToJson(this, true));
    }

    #endregion SECRET_STUFF

    public void AddUnlockedCharacter(CharacterID id) {
        data.AddUnlockedCharacter(id);
        SaveToJSON();
    }

    public void SaveCurrentParty(CharacterID[] ids) {
        data.SaveCurrentParty(ids);
        SaveToJSON();
    }

    public void AddToCurrentScrap(int value) {
        int v = CurrentScrap + value;
        SaveCurrentScrap(v);
    }

    public void SaveCurrentScrap(int value) {
        data.SaveCurrentScrap(value);
        SaveToJSON();
    }

    public void SaveCharacterInfo(CharacterSaveInfo info) {
        data.SaveCharacterInfo(info);
        SaveToJSON();
    }

    public CharacterSaveInfo GetCharacterSaveInfo(CharacterID id) {
        return data.GetSaveInfo(id);
    }

    public SpecialAttackType GetEquippedSpecialAttackType(CharacterID id) {
        return data.GetEquippedSpecialAttack(id);
    }

    public void SaveNewEquippedSpecialAttack(CharacterID charID, SpecialAttackType spAtk) {
        data.SaveNewEquippedSpecialAttack(charID, spAtk);
        SaveToJSON();
    }

    public SpecialAttackData EquippedSpecialAttackData(CharacterID charID) {
        SpecialAttackData rVal = DataManager.Instance.SpecialAttackDataByType(data.EquippedSpecialAttackType(charID));
        return rVal;
    }

    public void RemoveCharacterInfo(CharacterID id) {
        data.RemoveCharacterInfo(id);
    }

}

[Serializable]
public class GameProgressData {

    public List<CharacterID> availableCharacters;
    public List<CharacterID> currentPartyIds;
    public List<CharacterSaveInfo> characterSaveInfos;

    public List<CombatStrategyActionOrder> ownedStrategyActionOrders;
    public List<CombatStrategyTargetOrder> ownedStrategyTargetOrders;
    public List<CombatStrategyPercentage> ownedStrategyPercentageOrders;

    public int currentScrap = 100;

    public GameProgressData() {
        availableCharacters = new List<CharacterID>();
        characterSaveInfos = new List<CharacterSaveInfo>();
    }

    public void RecreateSpecialAttackList() {
        if (characterSaveInfos != null && characterSaveInfos.Count > 0) {
            characterSaveInfos.Clear();
        }
    }

    public CharacterSaveInfo GetSaveInfo(CharacterID id) {
        CharacterSaveInfo info = null;

        for (int i = 0; i < characterSaveInfos.Count; i++) {
            if (characterSaveInfos[i].ownerID == id) {
                info = characterSaveInfos[i];
                break;
            }
        }

        return info;
    }

    public SpecialAttackType GetEquippedSpecialAttack(CharacterID id) {
        SpecialAttackType rVal = SpecialAttackType.NONE;
        bool found = false;

        for (int i = 0; i < characterSaveInfos.Count; i++) {
            if (characterSaveInfos[i].ownerID == id) {
                rVal = characterSaveInfos[i].equippedSpecialAttack;
                found = true;
                break;
            }
        }

        if (!found) {
            #if UNITY_EDITOR
                // If this is ever called in editor, and not at runtime, this will fail
                CharacterDataObject[] dataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData/Players");
                for (int i = 0; i < dataObjects.Length; i++)
			    {
                    if (dataObjects[i].data.characterID == id) {
                        rVal = dataObjects[i].data.specialAttackType;
                        break;
                    }
			    }
            #else
                rVal = DataManager.Instance.CharacterDataObjectByID(id).data.specialAttackType;
            #endif
        }

        return rVal;
    }

    #region SAVING
    public void AddUnlockedCharacter(CharacterID id) {
        bool found = false;
        for (int i = 0; i < availableCharacters.Count; i++) {
            if (availableCharacters[i] == id) {
                found = true;
                break;
            }
        }
        if (!found) {
            availableCharacters.Add(id);
            CharacterSaveInfo info = CreateDefaultCharacterInfo(id);
            SaveCharacterInfo(info);
        }
    }

    private CharacterSaveInfo CreateDefaultCharacterInfo(CharacterID id) {
        CharacterSaveInfo info = null;
        SpecialAttackType spAtk = SpecialAttackType.NONE;
        // Get default special attack type from datamanager/assetdatabase
        #if UNITY_EDITOR
            // If this is ever called in editor, and not at runtime, this will fail
            CharacterDataObject[] dataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData/Players");
            for (int i = 0; i < dataObjects.Length; i++) {
                if (dataObjects[i].data.characterID == id) {
                    spAtk = dataObjects[i].data.specialAttackType;
                    break;
                }
            }
        #else
            spAtk = DataManager.Instance.CharacterDataObjectByID(id).data.SpecialAttackType;
            info = new CharacterSaveInfo(id, spAtk);
            characterSaveInfos.Add(info);
        #endif

        info = new CharacterSaveInfo(id, spAtk);
        characterSaveInfos.Add(info);

        return info;
    }

    public void SaveCurrentParty(CharacterID[] ids) {
        currentPartyIds.Clear();
        for (int i = 0; i < ids.Length; i++) {
            currentPartyIds.Add(ids[i]);
        }
    }

    public void SaveCurrentScrap(int value) {
        currentScrap = value;
    }

    public void SaveCharacterInfo(CharacterSaveInfo info) {
        bool found = false;
        for (int i = 0; i < characterSaveInfos.Count; i++) {
            if (characterSaveInfos[i].ownerID == info.ownerID) {
                found = true;
                characterSaveInfos[i] = info;
            }
        }

        if (!found) {
            characterSaveInfos.Add(info);
        }
    }

    public void SaveNewEquippedSpecialAttack(CharacterID charID, SpecialAttackType spAtk) {
        bool found = false;
        for (int i = 0; i < characterSaveInfos.Count; i++) {
            if (characterSaveInfos[i].ownerID == charID) {
                found = true;
                characterSaveInfos[i].equippedSpecialAttack = spAtk;
                break;
            }
        }

        if (!found) {
            characterSaveInfos.Add(new 
                CharacterSaveInfo(charID, spAtk));
        }
    }
    #endregion SAVING

    public void RemoveCharacterInfo(CharacterID id) {
        for (int i = 0; i < characterSaveInfos.Count; i++) {
            if (characterSaveInfos[i].ownerID == id) {
                characterSaveInfos.RemoveAt(i);
                break;
            }
        }
    }

    //!+ If this ever gets called in the editor, it's going to break
    public SpecialAttackType EquippedSpecialAttackType(CharacterID charID) {
        SpecialAttackType rVal = SpecialAttackType.NONE;
        bool found = false;
        for (int i = 0; i < characterSaveInfos.Count; i++) {
            if (characterSaveInfos[i].ownerID == charID) {
                found = true;
                rVal = characterSaveInfos[i].equippedSpecialAttack;
                break;
            }
        }

        if (!found) {
            rVal = DataManager.Instance.CharacterDataObjectByID(charID).data.SpecialAttackType;
            CharacterSaveInfo saveInfo = new CharacterSaveInfo(charID, rVal);
            characterSaveInfos.Add(saveInfo);
            SaveCharacterInfo(saveInfo);
        }
        return rVal;
    }

}

[Serializable]
public class SpecialAttackKeyValue {
    public CharacterID characterID = CharacterID.NONE;
    public SpecialAttackType specialAttackType = SpecialAttackType.NONE;

    public SpecialAttackKeyValue() { }

    public SpecialAttackKeyValue(CharacterID id, SpecialAttackType attack) {
        characterID = id;
        specialAttackType = attack;
    }
}