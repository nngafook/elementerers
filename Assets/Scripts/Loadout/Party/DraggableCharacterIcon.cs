﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

[RequireComponent(typeof(DragPanel))]
public class DraggableCharacterIcon : MonoBehaviour {

    private float snapSpeed = 0.1f;

    private Vector2 dockedPosition;
    private DragPanel dragComponent;
    private RectTransform rectTransform;

    public RectTransform slot;

    void Awake() {
        dragComponent = this.GetComponent<DragPanel>();
        rectTransform = this.GetComponent<RectTransform>();

        dockedPosition = this.transform.position;

        dragComponent.OnDragEndEvent.AddListener(OnDragEnded);
        dragComponent.OnDragStartEvent.AddListener(OnDragStarted);
    }

    private void OnDragStarted() {
        //throw new System.NotImplementedException();
    }

    private void OnDragEnded() {

        if (rectTransform.Overlaps(slot)) {
            transform.DOLocalMove(slot.localPosition, snapSpeed);
        }
        else {
            transform.DOMove(dockedPosition, snapSpeed);
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
