﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ActionButton : MonoBehaviour {

    private string animBool = "isSelected";

    private float targetBoxTweenSpeed = 0.75f;

    private Color normalColor = new Color(0.572549f, 0.572549f, 0.572549f, 1f);
    private Color selectedColor = new Color(0.454902f, 0.7647059f, 0.2470588f, 1f);

    private Vector2 basePosition;
    private Vector2 selectedPosition;
    private Vector2 hiddenPosition;
    private Vector2 targetBoxBigSize = new Vector2(1.5f, 1.5f);

    private bool isVisible = false;
    private bool isSelected = false;
    private bool queuedBoxVisible = false;

    private Animator bubbleAnimator;
    private RectTransform rectTransform;
    private Image bgImage;
    private ActionButtonIcon activeButtonIcon;
    private CanvasGroup queuedTargetBoxCanvasGroup;

    public ActionType actionType;
    public SpecialAttackType specialType;

    public GameObject attackIconObject;
    public GameObject specialAttackIconObject;
    public GameObject healIconObject;
    public GameObject defendIconObject;
    
    public GameObject closeIconObject;
    public RectTransform queuedTargetBox;

    [Space(10)]
    public ActionButtonIcon defaultButtonIcon;

    public bool QueuedBoxVisible { get { return queuedBoxVisible; } }

    void Awake() {
        bgImage = this.GetComponent<Image>();
        rectTransform = this.GetComponent<RectTransform>();
        bubbleAnimator = this.GetComponent<Animator>();
        queuedTargetBoxCanvasGroup = queuedTargetBox.GetComponent<CanvasGroup>();

        basePosition = rectTransform.anchoredPosition;
        selectedPosition = basePosition;
        selectedPosition.y += 10;
        hiddenPosition = basePosition;
        hiddenPosition.y -= 128;

        rectTransform.anchoredPosition = hiddenPosition;
        SetSelectedTargetBox(false);
    }

	// Use this for initialization
	void Start () {
        activeButtonIcon = defaultButtonIcon;
	}

    private void SetIcon() {
        if (activeButtonIcon != null) {
            activeButtonIcon.gameObject.SetActive(false);
        }

        switch (actionType) {
            case ActionType.ATTACK:
                attackIconObject.SetActive(true);
                activeButtonIcon = attackIconObject.GetComponent<ActionButtonIcon>();
                break;
            case ActionType.SPECIAL_ATTACK:
                if (specialType != SpecialAttackType.HEAL) {
                    specialAttackIconObject.SetActive(true);
                    activeButtonIcon = specialAttackIconObject.GetComponent<ActionButtonIcon>();
                }
                else {// if (specialType == SpecialAttackType.HEAL) {
                    healIconObject.SetActive(true);
                    activeButtonIcon = healIconObject.GetComponent<ActionButtonIcon>();
                }
                break;
            case ActionType.DEFEND:
                defendIconObject.SetActive(true);
                //activeButtonIcon = defendIconObject.GetComponent<ActionButtonIcon>();
                break;
            case ActionType.CLOSE:
                closeIconObject.SetActive(true);
                closeIconObject.GetComponent<ActionButtonIcon>().ClearCoolCounter();
                break;
            case ActionType.NONE:
                SetVisible(false);
                break;
            default:
                break;
        }
    }

    public void SetInteractable(bool value) {
        Toggle toggleComponent = this.GetComponent<Toggle>();
        if (toggleComponent) {
            toggleComponent.interactable = value;
        }
    }

    public void ClearCooldown() {
        if (activeButtonIcon != null) {
            activeButtonIcon.ClearCoolCounter();
            SetInteractable(true);
        }
    }

    public void UpdateCooldownCounter(float coolCounter) {
        if (activeButtonIcon != null) {
            activeButtonIcon.UpdateCooldownCounter(coolCounter);
        }
    }

    public void UpdateCooldownOverlay(float coolCounter, float coolMax) {
        if (activeButtonIcon != null) {
            activeButtonIcon.UpdateCooldownOverlay(coolCounter, coolMax);
        }
    }

    public void SetVisible(bool value) {
        isVisible = value;
        rectTransform.DOAnchorPos(((isVisible) ? basePosition : hiddenPosition), 0.5f).SetEase(Ease.OutBounce);
        if (isSelected) {
            Deselect();
        }
    }

    public void SetSelectedTargetBox(bool value) {
        queuedBoxVisible = value;
        if (queuedBoxVisible) {
            queuedTargetBox.localScale = targetBoxBigSize;
            queuedTargetBoxCanvasGroup.DOFade(1, targetBoxTweenSpeed);
            queuedTargetBox.DOScale(1, targetBoxTweenSpeed);
        }
        else {
            queuedTargetBoxCanvasGroup.DOKill();
            queuedTargetBox.DOKill();
            queuedTargetBoxCanvasGroup.alpha = 0;
        }
    }

    public void UpdateType(ActionType newActionType, SpecialAttackType newSpecialType) {
        actionType = newActionType;
        specialType = newSpecialType;
        SetIcon();
        ClearCooldown();
        SetVisible(true);
    }

    public void OnButtonClicked(bool isToggledOn) {
        isSelected = isToggledOn;
        
        if (isSelected) {
            bgImage.color = selectedColor;
            bubbleAnimator.SetBool(animBool, true);
            if (isVisible) {
                rectTransform.DOAnchorPos(selectedPosition, 0.5f).SetEase(Ease.OutBounce);
            }
        }   
        else {
            bgImage.color = normalColor;
            bubbleAnimator.SetBool(animBool, false);
            if (isVisible) {
                rectTransform.DOAnchorPos(basePosition, 0.5f).SetEase(Ease.OutBounce);
            }
        }
        //activeButtonIcon.SetAnimators(isSelected);
    }

    public void Deselect() {
        isSelected = false;
        this.GetComponent<Toggle>().isOn = isSelected;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
