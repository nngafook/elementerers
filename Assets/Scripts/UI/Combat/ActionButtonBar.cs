﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ActionButtonBar : MonoBehaviour {

    private float scaleSpeed = 0.05f;
    private bool isOpen = false;

    private ActionButton queuedButton;
    private CombatEntity entityOwner;
    private RectTransform rectTransform;
    private List<ActionButton> allActionButtons;

    [Space(10)]
    [Header("Buttons")]
    public ActionButton attackButton;
    public ActionButton specialButton;
    public ActionButton defendButton;
    public ActionButton fourthButton;
    public ActionButton closeButton;

    [Space(10)]
    public ToggleGroup buttonToggleGroup;

    public ActionButton SelectedActionButton {
        get {
            ActionButton rVal = null;
            Toggle buttonToggle = buttonToggleGroup.GetActive();
            if (buttonToggle != null) {
                rVal = buttonToggle.GetComponent<ActionButton>();
            }
            return rVal;
        }
    }

    void Awake() {
        rectTransform = this.GetComponent<RectTransform>();

        allActionButtons = new List<ActionButton>();
        allActionButtons.Add(attackButton);
        allActionButtons.Add(specialButton);
        allActionButtons.Add(defendButton);
        allActionButtons.Add(fourthButton);
        allActionButtons.Add(closeButton);

        SetOpen(false);
    }

    // Use this for initialization
    void Start() {

    }

    private void SetOpen(bool value) {
        isOpen = value;
        rectTransform.localScale = (isOpen) ? Vector3.one : Vector3.zero;
    }

    private void RestartSpecialAttackCheck() {
        StopCoroutine("PollEntityOwner");
        StartCoroutine("PollEntityOwner");
    }
	
    private IEnumerator PollEntityOwner() {
        float lastCooldownCounter = entityOwner.SpecialAttackCooldownCounter;
        float newCounter = -1;
        bool firstCheck = false;
        while (isOpen) {
            CheckSpecialAttackCooldown(ref lastCooldownCounter, ref newCounter, ref firstCheck);
            UpdateQueuedButton();
            yield return null;
        }
        yield return null;
    }

    private void CheckSpecialAttackCooldown(ref float lastCooldownCounter, ref float newCounter, ref bool firstCheck) {
        if (entityOwner.SpecialAttackOnCooldown) {
            if (CombatManager.Instance.CurrentCombatState == CombatState.ACTIVE) {
                // Does one time logic when the special is initially used.
                if (!firstCheck) {
                    lastCooldownCounter = entityOwner.SpecialAttackCooldownCounter;
                    specialButton.Deselect();
                    specialButton.SetInteractable(false);
                    specialButton.UpdateCooldownCounter(lastCooldownCounter);
                    specialButton.UpdateCooldownOverlay(lastCooldownCounter, entityOwner.SpecialAttackCooldown);
                    firstCheck = true;
                }

                newCounter = entityOwner.SpecialAttackCooldownCounter;
                specialButton.UpdateCooldownOverlay(newCounter, entityOwner.SpecialAttackCooldown);
                if (Mathf.CeilToInt(lastCooldownCounter) != Mathf.CeilToInt(newCounter)) {
                    lastCooldownCounter = newCounter;
                    specialButton.UpdateCooldownCounter(lastCooldownCounter);
                }
            }
        }
        else if (firstCheck) {
            // One time logic when the cool completes and this window is still open
            specialButton.ClearCooldown();
            firstCheck = false;
        }
    }

    private void UpdateQueuedButton() {
        if (queuedButton == null) {
            if (entityOwner.AttackQueued) {
                queuedButton = attackButton;
                queuedButton.SetSelectedTargetBox(true);
            }
            else if (entityOwner.SpecialAttackQueued) {
                queuedButton = specialButton;
                queuedButton.SetSelectedTargetBox(true);
            }
            else if (entityOwner.DefendQueued || entityOwner.IsDefending) {
                queuedButton = defendButton;
                queuedButton.SetSelectedTargetBox(true);
            }
        }
        else {
            SanityCheckQueuedButtons();
            if (queuedButton != attackButton && entityOwner.AttackQueued) {
                queuedButton.SetSelectedTargetBox(false);
                queuedButton = attackButton;
                queuedButton.SetSelectedTargetBox(true);
            }
            else if (queuedButton != specialButton && entityOwner.SpecialAttackQueued) {
                queuedButton.SetSelectedTargetBox(false);
                queuedButton = specialButton;
                queuedButton.SetSelectedTargetBox(true);
            }
            else if (queuedButton != defendButton && (entityOwner.DefendQueued || entityOwner.IsDefending)) {
                queuedButton.SetSelectedTargetBox(false);
                queuedButton = defendButton;
                queuedButton.SetSelectedTargetBox(true);
            }
        }
    }

    private void SanityCheckQueuedButtons() {
        if (!entityOwner.AttackQueued && attackButton.QueuedBoxVisible) {
            attackButton.SetSelectedTargetBox(false);
        }
        if (!entityOwner.SpecialAttackQueued && specialButton.QueuedBoxVisible) {
            specialButton.SetSelectedTargetBox(false);
        }
        if ((!entityOwner.DefendQueued && !entityOwner.IsDefending) && defendButton.QueuedBoxVisible) {
            defendButton.SetSelectedTargetBox(false);
        }
    }

    private void SetAllQueuedTargetsVisible(bool value) {
        attackButton.SetSelectedTargetBox(value);
        specialButton.SetSelectedTargetBox(value);
        defendButton.SetSelectedTargetBox(value);
    }

    private void OnOpenScaleComplete() {
        SelectAction(attackButton);
    }

    public void CombatStateUpdated() {
        if (isOpen) {
            CombatManager.Instance.CurrentCombatState = GameSettings.Instance.userCombatState;
        }
    }

    public void SetValues(CombatEntity entity) {
        //! This is wrong, cause the data object has default info like special attack data and so forth
        entityOwner = entity;
        SetValues(entityOwner.Data);
    }

    public void SetValues(CharacterData data) {
        queuedButton = null;

        if (data.entityType == EntityType.PLAYER) {
            attackButton.UpdateType(ActionType.ATTACK, SpecialAttackType.NONE);
            specialButton.UpdateType(ActionType.SPECIAL_ATTACK, data.SpecialAttackType);
            defendButton.UpdateType(ActionType.DEFEND, SpecialAttackType.NONE);
        }
        closeButton.UpdateType(ActionType.CLOSE, SpecialAttackType.NONE);
        UpdateQueuedButton();
        if (isOpen) {
            RestartSpecialAttackCheck();
        }
    }

    public void HideActionButtons() {
        attackButton.SetVisible(false);
        specialButton.SetVisible(false);
        defendButton.SetVisible(false);
    }

    public void Open() {
        if (!isOpen) {
            rectTransform.DOScale(1, scaleSpeed).OnComplete(OnOpenScaleComplete);
        }
        else {
            SelectAction(attackButton);
        }

        isOpen = true;
        StartCoroutine("PollEntityOwner");
        if (GameSettings.Instance.userCombatState == CombatState.WAITING) {
            CombatManager.Instance.CurrentCombatState = CombatState.WAITING;
        }
    }

    public void Close() {
        isOpen = false;
        StopCoroutine("PollEntityOwner");
        HideActionButtons();
        rectTransform.DOScale(0, scaleSpeed);

        CombatManager.Instance.CurrentCombatState = CombatState.ACTIVE;
    }

    public void OnCloseActionPressed() {
        EntityManager.Instance.DeselectSelectedEntity();
        Close();
    }

    public void SelectAction(ActionButton btn) {
        DeselectAction();
        Toggle toggle = btn.GetComponent<Toggle>();
        if (toggle != null) {
            toggle.isOn = true;
        }
    }

    public void DeselectAction() {
        if (SelectedActionButton != null) {
            SelectedActionButton.SetSelectedTargetBox(false);
            SelectedActionButton.Deselect();
        }
    }

    public void OnDefendButtonPressed() {
        DeselectAction();
        /*EntityManager.Instance.DefendButtonPressed();*/
    }

    public ActionType GetSelectedActionType() {
        ActionType rVal = ActionType.NONE;
        Toggle buttonToggle = buttonToggleGroup.GetActive();
        if (buttonToggle != null) {
            rVal = buttonToggle.GetComponent<ActionButton>().actionType;
        }
        return rVal;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
