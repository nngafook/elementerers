﻿using UnityEngine;
using System.Collections;

public class QueuedAttackIcon : MonoBehaviour {

    private CanvasGroup mainCG;

    public CanvasGroup attackCG;
    public CanvasGroup specialAttackCG;
    public CanvasGroup healSpecialCG;
    public CanvasGroup defendActionCG;

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();
    }

    // Use this for initialization
    void Start() {
        SetAttackQueued();
    }

    public void Hide() {
        mainCG.alpha = 0;
    }

    public void SetSpecialAttackQueued() {
        if (GameSettings.Instance.showUnitQueuedAction) {
            attackCG.alpha = 0;
            healSpecialCG.alpha = 0;
            defendActionCG.alpha = 0;
            specialAttackCG.alpha = 1;
        }
        else {
            Hide();
        }
    }

    public void SetAttackQueued() {
        if (GameSettings.Instance.showUnitQueuedAction) {
            specialAttackCG.alpha = 0;
            defendActionCG.alpha = 0;
            healSpecialCG.alpha = 0;
            attackCG.alpha = 1;
        }
        else {
            Hide();
        }
    }

    public void SetHealSpecialQueued() {
        if (GameSettings.Instance.showUnitQueuedAction) {
            attackCG.alpha = 0;
            defendActionCG.alpha = 0;
            specialAttackCG.alpha = 0;
            healSpecialCG.alpha = 1;
        }
        else {
            Hide();
        }
    }

    public void SetDefendQueued() {
        if (GameSettings.Instance.showUnitQueuedAction) {
            attackCG.alpha = 0;
            specialAttackCG.alpha = 0;
            healSpecialCG.alpha = 0;
            defendActionCG.alpha = 1;
        }
        else {
            Hide();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
