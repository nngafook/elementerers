﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CombatResultsWindow : GameWindow {

    private int scrapReward;
    private bool isVictory = false;
    private LevelData levelData;

    private Vector2 baseSize = new Vector2(550, 550);

    [Header("Combat Results Window")]
    public Text headerLabel;
    public Text flavorTextLabel;
    public Text totalScrapRewardLabel;
    public Text arenaScrapRewardLabel;

    [Header("Containers")]
    public RectTransform windowContainer;
    public RectTransform okButtonContainer;

    [Header("Canvas Groups")]
    public CanvasGroup victoryCG;
    public CanvasGroup defeatCG;
    public CanvasGroup expCG;

    [Header("BG Sizes")]
    public Vector2 defeatSize;
    public Vector2 expSize;

    [Header("EXP Character Portraits")]
    public List<CharacterExpPortrait> partyMemberPortraits;

    protected override void Awake() {
        base.Awake();

        Close();
    }

    protected override void Start() {
        base.Start();
    }

    private void SetEXPRewardValues() {
        UpdatePartyMembersVisible();
        List<CharacterID> currentParty = new List<CharacterID>(PartyManager.Instance.CurrentPartyIDs);

        for (int i = 0; i < currentParty.Count; i++) {
            partyMemberPortraits[i].SetInfo(currentParty[i]);
        }
    }

    private void UpdatePartyMembersVisible() {
        int currentPartySize = PartyManager.Instance.CurrentPartyIDs.Length;
        for (int i = 0; i < partyMemberPortraits.Count; i++) {
            if (i < currentPartySize) {
                partyMemberPortraits[i].CG().SetAlphaAndBools(true);
            }
            else {
                partyMemberPortraits[i].CG().SetAlphaAndBools(false);
            }
        }
    }

    private void SetScrapReward(int amount) {
        scrapReward = amount;
    }

    private void RollUpScrap() {
        DOTween.To(() => 0, x => scrapReward = x, scrapReward, 1f).SetEase(Ease.Linear).OnUpdate(OnScrapRewardUpdate);
    }

    private void OnScrapRewardUpdate() {
        totalScrapRewardLabel.text = scrapReward.ToString() + "sc";
    }

    private void SetButtonAnimatorEnabled(bool value) {
        okButtonContainer.GetComponent<Animator>().enabled = value;
    }

    private void SizeToBase() {
        windowContainer.DOSizeDelta(baseSize, openSpeed);
    }

    private void SizeToExp() {
        windowContainer.DOSizeDelta(expSize, openSpeed).SetEase(Ease.OutBack).OnComplete(OnSizeToExpComplete);
    }

    private void SizeToDefeat() {
        windowContainer.DOSizeDelta(defeatSize, openSpeed);
    }

    private void OnSizeToExpComplete() {
        contentCG = expCG;
        contentCG.DOFade(1, openSpeed).OnComplete(OnExpPortraitsFadeInComplete);
    }

    private void OnExpPortraitsFadeInComplete() {
        int exp = levelData.rewardsData.expReward;
        for (int i = 0; i < partyMemberPortraits.Count; i++) {
            partyMemberPortraits[i].UpdateInfo(exp);
        }
    }

    #region ANIMATIONS
    private void OnOkButtonScaled() {
        SetButtonAnimatorEnabled(true);
    }
    #endregion ANIMATIONS

    #region PUBLIC_METHODS

    public void SetInfo(ref LevelData data, bool battleWasVictorious) {
        levelData = data;
        int scrapReward = 0;

        isVictory = battleWasVictorious;
        contentCG = (isVictory) ? victoryCG : defeatCG;

        if (levelData.levelType == LevelType.Arena) {
            scrapReward = LevelManager.Instance.TotalArenaScrapEarned + levelData.arenaCompletionBonus;
            arenaScrapRewardLabel.text = levelData.arenaCompletionBonus.ToString();
        }
        else {
            scrapReward = levelData.rewardsData.scrapReward;
            arenaScrapRewardLabel.text = "";
        }

        SetScrapReward(scrapReward);
        GameProgress.Instance.AddToCurrentScrap(scrapReward);
    }

    public override void Open() {
        base.Open();
    }

    public override void Close() {
        base.Close();
        victoryCG.SetAlphaAndBools(false);
        defeatCG.SetAlphaAndBools(false);
    }

    protected override void OnContentFadeInComplete() {
        base.OnContentFadeInComplete();
        okButtonContainer.DOScale(1, openSpeed).SetEase(Ease.OutBounce).OnComplete(OnOkButtonScaled);
        RollUpScrap();
    }

    public void OnOkButtonPressed() {
        if (levelData.levelType == LevelType.Arena || contentCG == expCG) {
            LevelManager.Instance.Reset();
            LoadToScene.Load(SceneName.LOADOUT);
        }
        else {
            contentCG.DOFade(0, openSpeed);
            SetEXPRewardValues();
            SizeToExp();
        }
    }

    #endregion PUBLIC_METHODS

    // Update is called once per frame
	void Update () {

	}
}
