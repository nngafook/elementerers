﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Opencoding.CommandHandlerSystem;

public class CheatMenu : MonoBehaviour {

    private CombatEntity entity;
    private LayerMask unitLayerMask;
    private CanvasGroup cg;

    private List<CombatOrder> combatOrders = new List<CombatOrder>();
    private List<CombatOrder> reorderedCombatOrders = new List<CombatOrder>();

    private bool isOpen = false;

    private int indexToChange = 0;

    public Text nameLabel;

    [Space(5)]
    public Text healthText;
    public Slider healthSlider;

    [Space(5)]
    public Text maxHealthText;
    public Slider maxHealthSlider;

    [Space(5)]
    public Text defenseText;
    public Slider defenseSlider;

    [Space(5)]
    public Text dmgText;
    public Slider dmgSlider;

    [Space(5)]
    public Text spAtkDmgText;
    public Slider spAtkDMGSlider;

    [Space(5)]
    public Toggle activeWaitToggle;
    public Toggle spAtkOnCooldownToggle;

    [Space(5)]
    public Text combatOrderTextZero;
    public Text combatOrderTextOne;
    public Text combatOrderTextTwo;
    public Text combatOrderTextThree;
    public Text combatOrderTextFour;
    
    [Space(5)]
    public Text reordererdCombatOrderZero;
    public Text reordererdCombatOrderOne;
    public Text reordererdCombatOrderTwo;
    public Text reordererdCombatOrderThree;
    public Text reordererdCombatOrderFour;



    void Awake() {
        #if UNITY_EDITOR
            CommandHandlers.RegisterCommandHandlers(this);
            cg = this.GetComponent<CanvasGroup>();
            unitLayerMask = 1 << LayerMask.NameToLayer("UnitEntities");
        #else
            DestroyImmediate(this.gameObject);
        #endif
    }

    // Use this for initialization
    void Start() {
        
    }

    private void AddListeners() {
        RemoveListeners();

        healthSlider.onValueChanged.AddListener(OnHealthSliderChanged);
        maxHealthSlider.onValueChanged.AddListener(OnMaxHealthSliderChanged);
        defenseSlider.onValueChanged.AddListener(OnDefenseSliderChanged);
        dmgSlider.onValueChanged.AddListener(OnDMGSliderChanged);
        //spAtkDMGSlider.onValueChanged.AddListener(OnSPAtkDMGSliderChanged);
        activeWaitToggle.onValueChanged.AddListener(OnActiveWaitToggleValueChanged);
        spAtkOnCooldownToggle.onValueChanged.AddListener(OnSPAtkToggleValueChanged);
    }

    private void RemoveListeners() {
        healthSlider.onValueChanged.RemoveListener(OnHealthSliderChanged);
        maxHealthSlider.onValueChanged.RemoveListener(OnMaxHealthSliderChanged);
        defenseSlider.onValueChanged.RemoveListener(OnDefenseSliderChanged);
        dmgSlider.onValueChanged.RemoveListener(OnDMGSliderChanged);
        //spAtkDMGSlider.onValueChanged.AddListener(OnSPAtkDMGSliderChanged);
        activeWaitToggle.onValueChanged.RemoveListener(OnActiveWaitToggleValueChanged);
    }

    #region LISTENERS

    private void OnHealthSliderChanged(float value) {
        entity.CurrentHealth = value;
        healthText.text = "HP - " + entity.CurrentHealth;
    }

    private void OnMaxHealthSliderChanged(float value) {
        entity.MaxHealth = value;
        healthSlider.maxValue = value;
        maxHealthText.text = "Max HP - " + entity.MaxHealth;
    }

    private void OnDefenseSliderChanged(float value) {
        float difference = value - entity.Defense.max;
        entity.Defense.AddToRange(difference);
        defenseText.text = "Defense (" + entity.Defense.min.ToString() + " - " + entity.Defense.max.ToString() + ")";
    }

    private void OnDMGSliderChanged(float value) {
        float difference = value - entity.BaseAtkDamage.max;
        entity.AddBaseAtkDMG((int)difference);
        dmgText.text = "Damage (" + entity.BaseAtkDamage.min.ToString() + " - " + entity.BaseAtkDamage.max.ToString() + ")";
    }

    private void OnSPAtkDMGSliderChanged(float value) {
        //throw new System.NotImplementedException();
    }

    private void OnActiveWaitToggleValueChanged(bool value) {
        entity.IsDebugPaused = value;
    }

    private void OnSPAtkToggleValueChanged(bool value) {
        entity.SpecialAttackOnCooldown = value;
    }

    public void OnCheatThinkPressed() {
        if (entity != null) {
            entity.DebugThink();
        }
    }

    public void OnResetReorderedCombatOrdersPressed() {
        indexToChange = 0;
        reordererdCombatOrderZero.text = "0";
        reordererdCombatOrderOne.text = "1";
        reordererdCombatOrderTwo.text = "2";
        reordererdCombatOrderThree.text = "3";
        reordererdCombatOrderFour.text = "4";
    }

    public void OnSetReorderedCombatOrdersPressed() {
        CombatOrder fallback = new CombatOrder();
        entity.StrategyController.SetPriorities(ref reorderedCombatOrders, ref fallback);
    }

    public void OnCombatOrderButtonPressed(int index) {
        if (indexToChange < 5 && combatOrders.Count > index) {
            switch (indexToChange) {
                case 0:
                    reordererdCombatOrderZero.text = index.ToString();
                    break;
                case 1:
                    reordererdCombatOrderOne.text = index.ToString();
                    break;
                case 2:
                    reordererdCombatOrderTwo.text = index.ToString();
                    break;
                case 3:
                    reordererdCombatOrderThree.text = index.ToString();
                    break;
                case 4:
                    reordererdCombatOrderFour.text = index.ToString();
                    break;
                default:
                    break;
            }
            reorderedCombatOrders[indexToChange] = combatOrders[index];
            indexToChange++;
        }
    }

    public void OnReorderedCombatOrderButtonPressed(int index) {
        switch (index) {
            case 0:
                reordererdCombatOrderZero.text = combatOrderTextZero.text;
                break;
            case 1:
                reordererdCombatOrderOne.text = combatOrderTextOne.text;
                break;
            case 2:
                reordererdCombatOrderTwo.text = combatOrderTextTwo.text;
                break;
            case 3:
                reordererdCombatOrderThree.text = combatOrderTextThree.text;
                break;
            case 4:
                reordererdCombatOrderFour.text = combatOrderTextFour.text;
                break;
            default:
                break;
        }
        reorderedCombatOrders[index] = combatOrders[index];
    }

    #endregion LISTENERS

    private CombatEntity MouseRaycastForEntity() {
        CombatEntity rVal = null;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 10, unitLayerMask);
        if (hit.collider != null) {
            rVal = hit.collider.GetComponentInParent<CombatEntity>();
        }
        return rVal;
    }
    
    [CommandHandler]
    private void Open() {
        isOpen = true;
        CombatManager.Instance.IsInCheatMode = true;
        CombatManager.Instance.CurrentCombatState = CombatState.WAITING;
        cg.SetAlphaAndBools(true);
    }

    [CommandHandler]
    private void Close() {
        isOpen = false;
        entity = null;
        RemoveListeners();
        CombatManager.Instance.IsInCheatMode = false;
        CombatManager.Instance.CurrentCombatState = CombatState.ACTIVE;
        cg.SetAlphaAndBools(false);
    }

    private void Toggle() {
        isOpen = !isOpen;
        if (isOpen) {
            Open();
        }
        else {
            Close();
        }
    }

    private void SetValues() {
        nameLabel.text = entity.Data.characterName + ": " + entity.UID.Value;

        maxHealthSlider.minValue = 1;
        maxHealthSlider.maxValue = 5000;
        maxHealthSlider.value = entity.MaxHealth;

        healthSlider.minValue = 1;
        healthSlider.maxValue = 5000;
        healthSlider.value = entity.CurrentHealth;

        defenseSlider.minValue = 1;
        defenseSlider.maxValue = 5000;
        defenseSlider.value = entity.Defense.max;

        dmgSlider.minValue = 1;
        dmgSlider.maxValue = 5000;
        dmgSlider.value = entity.BaseAtkDamage.max;

        activeWaitToggle.isOn = entity.IsDebugPaused;
        spAtkOnCooldownToggle.isOn = entity.SpecialAttackOnCooldown;

        healthText.text = "HP: " + entity.CurrentHealth;
        maxHealthText.text = "Max HP: " + entity.MaxHealth;
        defenseText.text = "Defense (" + entity.Defense.min.ToString() + "  -  " + entity.Defense.max.ToString() + ")";
        dmgText.text = "Damage (" + entity.BaseAtkDamage.min.ToString() + "  -  " + entity.BaseAtkDamage.max.ToString() + ")";

        combatOrders = new List<CombatOrder>();
        reorderedCombatOrders = new List<CombatOrder>();
        CharacterSaveInfo saveInfo = GameProgress.Instance.GetCharacterSaveInfo(entity.Data.characterID);
        if (saveInfo != null) {
            for (int i = 0; i < 5; i++) {
                if (saveInfo.combatOrders.Count > i) {
                    combatOrders.Add(saveInfo.combatOrders[i]);
                    reorderedCombatOrders.Add(saveInfo.combatOrders[i]);
                }
            }
        }

        AddListeners();
    }

    // Update is called once per frame
    void Update() {
        bool ctrlHeld = Input.GetKey(KeyCode.LeftControl);
        if (Input.GetKeyUp(KeyCode.Space)) {
            if (ctrlHeld) {
                Toggle();
            }
        }

        if (isOpen) {
            if (Input.GetMouseButtonUp(0) && (EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject())) {
                CombatEntity e = MouseRaycastForEntity();
                if (e != null) {
                    entity = e;
                    SetValues();
                }
            }
        }
    }

}
