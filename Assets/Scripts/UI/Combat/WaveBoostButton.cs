﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WaveBoostButton : MonoBehaviour {

    private const string HEAL_TOOLTIP = "Restore health to entire party";
    private const string DOUBLESCRAP_TOOLTIP = "Double scrap reward for completing the next wave";
    private const string TEMPDMG_TOOLTIP = "Temporary Damage Increase";
    private const string NONE_TOOLTIP = "You have no more boosts available";

    private bool isSelected = false;
    private Color baseButtonColor;

    public Color selectedColor;

    public Toggle toggle;
    public Image buttonBGImage;
    public GameObject healIcon;
    public GameObject doubleScrapIcon;
    public GameObject tempDMGBoostIcon;
    public GameObject noBonusIcon;

    [Space(10)]
    public Text tooltipTextField;

    public bool IsSelected { get { return isSelected; } }

    void Awake() {
        baseButtonColor = buttonBGImage.color;
    }

    void Start() {
        isSelected = toggle.isOn;
        buttonBGImage.color = (isSelected) ? selectedColor : baseButtonColor;
    }

    private void SetAllIcons(bool value) {
        healIcon.SetActive(value);
        doubleScrapIcon.SetActive(value);
        tempDMGBoostIcon.SetActive(value);
        noBonusIcon.SetActive(value);
    }

    public void SetBoost(WaveBoost boost) {
        SetAllIcons(false);
        switch (boost) {
            case WaveBoost.Heal:
                healIcon.SetActive(true);
                tooltipTextField.text = HEAL_TOOLTIP;
                break;
            case WaveBoost.DoubleScrap:
                doubleScrapIcon.SetActive(true);
                tooltipTextField.text = DOUBLESCRAP_TOOLTIP;
                break;
            case WaveBoost.TempDMG:
                tempDMGBoostIcon.SetActive(true);
                tooltipTextField.text = TEMPDMG_TOOLTIP;
                break;
            case WaveBoost.None:
            default:
                noBonusIcon.SetActive(true);
                tooltipTextField.text = NONE_TOOLTIP;
                break;
        }
    }

    public void OnValueChanged(bool value) {
        isSelected = value;
        buttonBGImage.color = (isSelected) ? selectedColor : baseButtonColor;
    }

    // Update is called once per frame
    void Update() {

    }
}
