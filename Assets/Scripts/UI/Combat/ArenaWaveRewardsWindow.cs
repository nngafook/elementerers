﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ArenaWaveRewardsWindow : MonoBehaviour {

    private float scalingSpeed = 0.25f;

    private bool giveBoosts = false;
    private WaveBoost leftBoost = WaveBoost.None;
    private WaveBoost rightBoost = WaveBoost.None;

    private float thinWindowWidth = 10;
    private Vector2 baseWindowWidth;
    private Vector2 hiddenWindowPos;
    private Vector2 shownWindowPos;

    private CanvasGroup mainCG;

    public WaveBoostButton leftBoostButton;
    public WaveBoostButton rightBoostButton;

    public CanvasGroup rewardsCG;
    public CanvasGroup boostsCG;

    [Space(10)]
    public CanvasGroup contentCG;
    public RectTransform windowRT;

    [Space(10)]
    public Text waveTotalText;
    public Text arenaTotalText;

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();

        baseWindowWidth = windowRT.sizeDelta;
        shownWindowPos = windowRT.anchoredPosition;
        hiddenWindowPos = new Vector2(shownWindowPos.x, -1000);

        windowRT.sizeDelta = windowRT.sizeDelta.SetX(thinWindowWidth);
        windowRT.anchoredPosition = hiddenWindowPos;
        contentCG.alpha = 0;
    }

    // Use this for initialization
    void Start() {
        rewardsCG.SetAlphaAndBools(true);
        boostsCG.SetAlphaAndBools(false);

        Close();
    }

    private void SetBoostsValues() {
        if (giveBoosts) {
            WaveBoost[] boosts = Utility.ShuffleArray<WaveBoost>(LevelManager.Instance.CurrentLevel.waveBoostsAvailable.ToArray(), Random.Range(1, 829));

            int boostsAssigned = 0;
            for (int i = 0; i < boosts.Length; i++) {
                if (boostsAssigned < 2) {    
                    if (boosts[i] != WaveBoost.None) {
                        if (boostsAssigned == 0) {
                            leftBoost = boosts[i];
                        }
                        else if (boostsAssigned == 1) {
                            rightBoost = boosts[i];
                        }
                        boostsAssigned++;
                    }
                }
            }

            if (boostsAssigned == 0) {
                leftBoost = WaveBoost.None;
                rightBoost = WaveBoost.None;
            }
            else if (boostsAssigned == 1) {
                rightBoost = WaveBoost.None;
            }

            leftBoostButton.SetBoost(leftBoost);
            rightBoostButton.SetBoost(rightBoost);

        }
    }

    public void SetValues(ref ArenaWave wave) {
        waveTotalText.text = wave.rewardsData.scrapReward.ToString() + "sc";
        arenaTotalText.text = LevelManager.Instance.TotalArenaScrapEarned.ToString() + "sc";

        giveBoosts = LevelManager.Instance.GiveArenaBoosts();
        SetBoostsValues();
    }

    public void Open() {
        windowRT.sizeDelta = windowRT.sizeDelta.SetX(thinWindowWidth);
        windowRT.anchoredPosition = hiddenWindowPos;
        contentCG.alpha = 0;

        boostsCG.SetAlphaAndBools(false);
        rewardsCG.SetAlphaAndBools(true);
        mainCG.SetAlphaAndBools(true);

        windowRT.DOAnchorPos(shownWindowPos, scalingSpeed).OnComplete(OnWindowMoveUpComplete);
    }

    private void OnWindowMoveUpComplete() {
        windowRT.DOSizeDelta(baseWindowWidth, scalingSpeed).OnComplete(OnContentScaleUpComplete);
    }

    private void OnContentScaleUpComplete() {
        contentCG.DOFade(1, scalingSpeed);
    }

    public void Close() {
        giveBoosts = false;
        mainCG.SetAlphaAndBools(false);
    }

    public void OnOnwardButtonPressed() {
        if (giveBoosts) {
            rewardsCG.SetAlphaAndBools(false);
            boostsCG.SetAlphaAndBools(true);
        }
        else {
            Close();
        }
    }

    public void OnQuitButtonPressed() {
        Debug.Log("Fix OnQuitButtonPressed in ArenaWaveRewardsWindow");

        Close();
        LevelManager.Instance.ContinueArena();
    }

    public void OnBoostsOKButtonPressed() {
        WaveBoost boostSelected = (leftBoostButton.IsSelected) ? leftBoost : rightBoost;

        Close();
        LevelManager.Instance.ContinueArena(boostSelected);
    }

    // Update is called once per frame
    void Update() {

    }
}
