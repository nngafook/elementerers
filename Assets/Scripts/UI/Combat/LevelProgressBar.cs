﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class LevelProgressBar : MonoBehaviour {

    private string enemiesRemainingPreText = "Enemies Remaining - ";
    private float scaleSpeed = 0.25f;

    private int currentEnemies;
    private int maxEnemies;

    public RectTransform progressBar;
    public Text enemyCountText;

	// Use this for initialization
	void Start () {
	
	}

    public void InitValues(int current, int max) {
        currentEnemies = current;
        maxEnemies = max;
        progressBar.localScale = Vector3.one;
        UpdateText();
    }

    public void UpdateCount(int value) {
        currentEnemies = value;
        float gotoScale = (float)currentEnemies / (float)maxEnemies;
        progressBar.DOScaleX(gotoScale, scaleSpeed).OnComplete(OnBarScaleComplete);
    }

    private void OnBarScaleComplete() {
        UpdateText();
    }

    private void UpdateText() {
        enemyCountText.text = enemiesRemainingPreText + currentEnemies.ToString();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
