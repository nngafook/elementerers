﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TextFx;

public class TutorialMessagePopup : MonoBehaviour {

    private int indexShowing = 0;
    private bool waitingForIdleEntities = false;
    private bool messageShowing = false;
    private float animSpeed = 0.25f;
    private List<List<string>> messagesToShow = new List<List<string>>();

    private Vector2 showingPosition;
    private Vector2 hiddenPosition;

    public Text messageLabel;
    public TextFxUGUI tapToDismissTextFXLabel;

    void Awake() {
        showingPosition = this.RT().anchoredPosition;
        hiddenPosition = new Vector2(-1000, showingPosition.y);

        SetHidden();
    }

    // Use this for initialization
    void Start() {

    }

    private void SetHidden() {
        this.RT().anchoredPosition = hiddenPosition;

        tapToDismissTextFXLabel.gameObject.SetActive(false);
    }

    private void OnHidingComplete() {
        if (messagesToShow.Count > 0) {
            ProcessShowAnimation();
        }
        else {
            messageShowing = false;
            CombatManager.Instance.CurrentCombatState = CombatState.ACTIVE;
        }
    }

    private void SetText() {
        messageLabel.text = messagesToShow[0][indexShowing];
        indexShowing++;
        if (indexShowing >= messagesToShow[0].Count) {
            messagesToShow.RemoveAt(0);
            indexShowing = 0;
        }
    }

    private void ProcessShowAnimation() {
        messageShowing = true;

        // Reset this here, cause this animates only when a new list of messages are pushed
        indexShowing = 0;
        SetText();

        this.RT().DOAnchorPos(showingPosition, animSpeed).SetEase(Ease.OutBack);

        StopCoroutine("TimeForDismissMessage");
        StartCoroutine("TimeForDismissMessage");
    }

    /// <summary>
    /// Callback from EntityManager when it checks all entities and finds that they're all idle
    /// </summary>
    private void OnAllEntitiesIdle() {
        waitingForIdleEntities = false;
        ProcessShowAnimation();
    }

    private IEnumerator TimeForDismissMessage() {
        float startTime = Time.time;
        float timeToWait = 5f;

        while ((Time.time - startTime) < timeToWait) {
            yield return null;
        }

        tapToDismissTextFXLabel.gameObject.SetActive(true);
        tapToDismissTextFXLabel.AnimationManager.PlayAnimation();

    }

    //public void SetMessage(List<string> messages) {
    //    messagesToShow.Add(messages);
    //    //messageLabel.text = txt;
    //}

    public void PushMessage(List<string> messages) {
        messagesToShow.Add(messages);

        CombatManager.Instance.CurrentCombatState = CombatState.WAITING;
        if (EntityManager.Instance.AllEntitiesIdle()) {
            ProcessShowAnimation();
        }
        else if (!waitingForIdleEntities) {
            waitingForIdleEntities = true;
            EntityManager.Instance.RegisterCallbackForIdleCheck(OnAllEntitiesIdle);
        }
    }

    public void Close() {
        tapToDismissTextFXLabel.gameObject.SetActive(false);
        StopCoroutine("TimeForDismissMessage");

        if (indexShowing != 0) {
            StartCoroutine("TimeForDismissMessage");
            SetText();
        }
        else {
            this.RT().DOAnchorPos(hiddenPosition, animSpeed).SetEase(Ease.InBack).OnComplete(OnHidingComplete);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
