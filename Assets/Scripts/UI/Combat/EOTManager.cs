﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EOTManager : MonoBehaviour {

    public CombatEntity entityOwner;

    [Space(10)]
    public List<EOTIndicator> indicators;

    // Use this for initialization
    void Start() {

    }

    public void AddEOT(float impactValue, bool friendly, float duration) {
        EOTIndicator indicator = null;

        for (int i = 0; i < indicators.Count; i++) {
            if (!indicators[i].IsActive) {
                indicator = indicators[i];
                break;
            }
        }
        indicator.Activate(impactValue, friendly, duration, OnTickCallback);
    }

    public void OnTickCallback(bool isFriendly, float value) {
        if (isFriendly) {
            entityOwner.BeHealed(value, true);
        }
        else {
            entityOwner.TakeEOTDamage(value);
        }
    }

    public void Clear() {
        for (int i = 0; i < indicators.Count; i++) {
            if (indicators[i].IsActive) {
                indicators[i].Deactivate();
            }
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
