﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class TargetPointer : MonoBehaviour {

    public CanvasGroup targetBoxCG;

    [Header("Player Line")]
    public CanvasGroup playerTargetTextCG;
    public Image playerTargetLineMaskImage;

    [Header("Enemy Line")]
    public CanvasGroup enemyTargetTextCG;
    public Image enemyTargetLineMaskImage;

    // Use this for initialization
    void Start() {

    }

    public void Show(EntityType entityType, bool isTarget) {
        Show(Color.green, entityType, isTarget);
    }

    public void Show(Color c, EntityType entityType, bool isTarget) {
        targetBoxCG.GetComponent<Image>().color = c;
        targetBoxCG.alpha = 1;

        if (isTarget) {
            CanvasGroup textCG = (entityType == EntityType.PLAYER) ? playerTargetTextCG : enemyTargetTextCG;
            Image lineMask = (entityType == EntityType.PLAYER) ? playerTargetLineMaskImage : enemyTargetLineMaskImage;
            textCG.alpha = 1;
            lineMask.fillAmount = 0;
            lineMask.DOFillAmount(1, 0.5f).SetEase(Ease.OutBounce);
        }
    }

    public void Hide() {
        targetBoxCG.alpha = 0;

        playerTargetLineMaskImage.DOKill();
        enemyTargetLineMaskImage.DOKill();

        playerTargetTextCG.alpha = 0;
        playerTargetLineMaskImage.fillAmount = 0;

        enemyTargetTextCG.alpha = 0;
        enemyTargetLineMaskImage.fillAmount = 0;
    }

    // Update is called once per frame
    void Update() {

    }
}
