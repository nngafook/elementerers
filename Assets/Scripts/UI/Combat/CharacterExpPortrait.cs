﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TextFx;

public class CharacterExpPortrait : MonoBehaviour {

    private float barFillFactor = 1f;
    private float punchScaleFactor = 0.25f;

    private CharacterData data;
    private CharacterSaveInfo saveInfo;
    private CharacterLevelStats levelStats;

    public CharacterSelectIcon portraitIcon;
    public Image expBarFill;
    public Text expEarnedLabel;
    public Text currentLevelLabel;
    public TextFxUGUI levelUpTextFX;

    // Use this for initialization
    void Start() {
        levelUpTextFX.gameObject.SetActive(false);
    }

    public void SetInfo(CharacterID id) {
        data = DataManager.Instance.CharacterDataObjectByID(id).data;
        saveInfo = GameProgress.Instance.GetCharacterSaveInfo(id);
        levelStats = DataManager.Instance.CharacterLevelStatsObject.StatsByID(id);

        float currentExp = saveInfo.exp;
        float expNeeded = levelStats.expRequirements[saveInfo.level - 1];
        float originalPercentage = currentExp / expNeeded;

        portraitIcon.Init(data, null);

        expBarFill.fillAmount = originalPercentage;
        expEarnedLabel.text = "";
        currentLevelLabel.text = "Level " + saveInfo.level;
    }

    public void UpdateInfo(int exp) {
        saveInfo.exp += exp;
        float currentExp = saveInfo.exp;
        float expNeeded = levelStats.expRequirements[saveInfo.level - 1];
        float endPercentage = currentExp / expNeeded;
        //float overflowFillPercentage = 0;

        if (currentExp >= expNeeded) {
            endPercentage = 1;
            expBarFill.DOFillAmount(endPercentage, barFillFactor).OnComplete(OnExpBarFilledToFull);
        }
        else {
            expBarFill.DOFillAmount(endPercentage, barFillFactor);
            GameProgress.Instance.SaveCharacterInfo(saveInfo);
        }
        
        expEarnedLabel.text = "+" + exp.ToString() + "XP";
        expEarnedLabel.RT().DOPunchScale(new Vector3(punchScaleFactor, punchScaleFactor, punchScaleFactor), punchScaleFactor);
    }

    private void OnExpBarFilledToFull() {
        expBarFill.fillAmount = 0;

        float expNeeded = levelStats.expRequirements[saveInfo.level - 1];
        float currentExp = (int)(saveInfo.exp - expNeeded);
        float endPercentage = currentExp / expNeeded;

        saveInfo.exp = (int)currentExp;
        saveInfo.level++;
        currentLevelLabel.text = "Level " + saveInfo.level;
        currentLevelLabel.RT().DOPunchScale(new Vector3(punchScaleFactor, punchScaleFactor, punchScaleFactor), punchScaleFactor);

        levelUpTextFX.gameObject.SetActive(true);
        levelUpTextFX.AnimationManager.PlayAnimation();

        GameProgress.Instance.SaveCharacterInfo(saveInfo);

        expBarFill.DOFillAmount(endPercentage, barFillFactor);
    }

    // Update is called once per frame
    void Update() {

    }
}
