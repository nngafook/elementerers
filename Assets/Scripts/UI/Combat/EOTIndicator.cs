﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class EOTIndicator : MonoBehaviour {

    public Action<bool, float> OnTickEvent;

    private bool isFriendlyEffect = false;
    private bool isActive = false;
    private float effectValue = 0;
    private float effectDuration = 0;

    public Text label;
    public Image bgCircle;

    [Space(10)]
    public Color buffColor;
    public Color debuffColor;

    public bool IsActive { get { return isActive; } }

    // Use this for initialization
    void Start() {
        SetActive(false);
    }

    private void OnCombatStateChange(CombatState state) {
        if (state == CombatState.WAITING) {
            Pause();
        }
        else {
            Resume();
        }
    }

    private void Pause() {
        bgCircle.DOKill();
    }

    private void Resume() {
        bgCircle.DOFillAmount(0, bgCircle.fillAmount).OnComplete(OnOneSecondTick);
    }

    private void ResetValues() {
        label.text = "";
        bgCircle.fillAmount = 1;
        bgCircle.DOKill();
    }

    private void SetFillColor(bool friendly) {
        isFriendlyEffect = friendly;
        if (isFriendlyEffect) {
            bgCircle.color = buffColor;
        }
        else {
            bgCircle.color = debuffColor;
        }
    }

    private void SetDuration(float duration) {
        effectDuration = duration;
    }

    private void SetLabel() {
        label.text = effectDuration.ToString();
    }

    private void SetActive(bool value) {
        isActive = value;
        gameObject.SetActive(isActive);

        if (isActive) {
            CombatManager.Instance.OnCombatStateChanged.AddListener(OnCombatStateChange);
        }
        else {
            CombatManager.Instance.OnCombatStateChanged.RemoveListener(OnCombatStateChange);
        }
    }

    private void StartOneSecondTick() {
        bgCircle.fillAmount = 1;
        bgCircle.DOFillAmount(0, 1f).OnComplete(OnOneSecondTick);
    }

    private void OnOneSecondTick() {
        effectDuration--;
        
        SetLabel();
        
        if (OnTickEvent != null) {
            OnTickEvent(isFriendlyEffect, effectValue);
        }

        if (effectDuration == 0) {
            OnTickEvent = null;
            SetActive(false);
        }
        else {
            StartOneSecondTick();
        }
    }

    public void Activate(float impactValue, bool friendly, float duration, Action<bool, float> TickCallback) {
        SetActive(true);
        ResetValues();
        SetFillColor(friendly);
        SetDuration(duration);
        SetLabel();

        effectValue = impactValue;
        OnTickEvent = TickCallback;

        StartOneSecondTick();
    }

    public void Deactivate() {
        bgCircle.DOKill();
        OnTickEvent = null;
        SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }
}
