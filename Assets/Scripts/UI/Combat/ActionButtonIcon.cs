﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ActionButtonIcon : MonoBehaviour {

    private bool isTweeningOverlay = false;

    //public List<Animator> iconAnimators;

    [Space(10)]
    public Image cooldownOverlay;
    public Text cooldownText;

	// Use this for initialization
	void Start () {

	}

    private void OnOverlayFillComplete() {
        isTweeningOverlay = false;
        this.GetComponent<RectTransform>().DOScale(1.3f, 0.25f).SetEase(Ease.InCubic).OnComplete(OnPunchComplete);
    }

    private void OnPunchComplete() {
        this.GetComponent<RectTransform>().DOScale(1f, 0.25f);
    }

    public void UpdateCooldownCounter(float counter) {
        cooldownText.text = Mathf.CeilToInt(counter).ToString();
    }

    public void UpdateCooldownOverlay(float counter, float coolMax) {
        if (!isTweeningOverlay) {
            cooldownOverlay.fillAmount = (counter / coolMax);
            if (cooldownOverlay.fillAmount <= 0) {
                OnOverlayFillComplete();
            }
        }
    }

    public void ClearCoolCounter() {
        cooldownOverlay.fillAmount = 0;
        cooldownText.text = "";
        cooldownOverlay.DOKill();
        isTweeningOverlay = false;
    }

    //public void SetAnimators(bool isSelected) {
    //    Animator animator;
    //    for (int i = 0; i < iconAnimators.Count; i++) {
    //        animator = iconAnimators[i];
    //        animator.SetBool("isSelected", isSelected);
    //    }
    //}

}
