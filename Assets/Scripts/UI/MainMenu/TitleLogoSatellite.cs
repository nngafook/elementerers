﻿using UnityEngine;
using System.Collections;

public class TitleLogoSatellite : MonoBehaviour {

    private Animator anim;

    public string clipName;
    [Range(0, 1)]
    public float startFrame = 0;

    void Awake() {
        anim = this.GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
        anim.Play(clipName, 0, startFrame);
        //anim.SetTrigger("Start");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
