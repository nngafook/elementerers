﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.Events;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDropHandler, IPointerUpHandler, IDragHandler {
    
    [HideInInspector]
    public UnityEvent OnMouseDownEvent;
    [HideInInspector]
    public UnityEvent OnDragStartEvent;
    [HideInInspector]
    public UnityEvent OnDragEndEvent;

    private bool dragStartEventFired = false;
    private Vector2 pointerOffset;
    private RectTransform canvasRectTransform;
    
    public RectTransform dragRectTransform;

    void Awake() {
        Canvas canvas = GetComponentInParent<Canvas>();
        if (canvas != null) {
            canvasRectTransform = canvas.transform as RectTransform;
        }
    }

    public void OnPointerDown(PointerEventData data) {
        if (OnMouseDownEvent != null) {
            OnMouseDownEvent.Invoke();
        }
        dragRectTransform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(dragRectTransform, data.position, data.pressEventCamera, out pointerOffset);
    }

    public void OnDrag(PointerEventData data) {
        if (dragRectTransform == null)
            return;

        if ((!dragStartEventFired) && (OnDragStartEvent != null)) {
            OnDragStartEvent.Invoke();
            dragStartEventFired = true;
        }

        Vector3 pointerPostion = ClampToWindow(data);
        //Vector2 localPointerPosition;
        //if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, pointerPostion, data.pressEventCamera, out localPointerPosition)) {
        //    dragRectTransform.localPosition = localPointerPosition - pointerOffset;
        //}

        Vector3 pos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(canvasRectTransform, data.position, data.pressEventCamera, out pointerPostion)) {
            pos = (Vector2)pointerPostion - pointerOffset;
            dragRectTransform.position = pos;
        }

    }

    public void OnPointerUp(PointerEventData data) {
        dragStartEventFired = false;
        if (OnDragEndEvent != null) {
            OnDragEndEvent.Invoke();
        }
    }

    public void OnDrop(PointerEventData data) {
        //dragStartEventFired = false;
        //if (OnDragEndEvent != null) {
        //    OnDragEndEvent.Invoke();
        //}
    }

    Vector2 ClampToWindow(PointerEventData data) {
        Vector2 rawPointerPosition = data.position;

        Vector3[] canvasCorners = new Vector3[4];
        canvasRectTransform.GetWorldCorners(canvasCorners);

        float clampedX = Mathf.Clamp(rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }
}