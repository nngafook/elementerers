﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CanvasGroup))]
public class GameWindow : MonoBehaviour {

    protected float openSpeed = 0.3f;
    protected CanvasGroup mainCG;

    private float thinWidth = 0.01f;
    private Vector2 windowVisiblePosition;
    private Vector2 windowHiddenPosition = new Vector2(0, -1000);

    [Header("Game Window")]
    public RectTransform windowRT;
    public CanvasGroup contentCG;

    protected virtual void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();
        mainCG.SetAlphaAndBools(false);
        windowVisiblePosition = windowRT.anchoredPosition;
    }

    // Use this for initialization
    protected virtual void Start() {

    }

    public virtual void Open() {
        windowRT.anchoredPosition = windowHiddenPosition;
        windowRT.localScale = windowRT.localScale.SetX(thinWidth);
        contentCG.SetAlphaAndBools(false);
        mainCG.SetAlphaAndBools(true);
        windowRT.DOAnchorPos(windowVisiblePosition, openSpeed).OnComplete(OnWindowMoveUpComplete).SetEase(Ease.OutBack);
    }

    private void OnWindowMoveUpComplete() {
        windowRT.DOScaleX(1, openSpeed).OnComplete(OnWindowScaleOpenComplete).SetEase(Ease.OutBack);
    }

    private void OnWindowScaleOpenComplete() {
        contentCG.DOFade(1, openSpeed).OnComplete(OnContentFadeInComplete);
    }

    protected virtual void OnContentFadeInComplete() {
        contentCG.SetAlphaAndBools(true);
    }

    public virtual void Close() {
        contentCG.DOFade(0, openSpeed).OnComplete(OnContentFadeOutComplete);
    }

    private void OnContentFadeOutComplete() {
        contentCG.SetAlphaAndBools(false);
        windowRT.DOScaleX(thinWidth, openSpeed).OnComplete(OnWindowScaleCloseComplete).SetEase(Ease.InBack);
    }

    private void OnWindowScaleCloseComplete() {
        windowRT.DOAnchorPos(windowHiddenPosition, openSpeed).OnComplete(OnWindowMoveDownComplete).SetEase(Ease.InBack);
    }

    private void OnWindowMoveDownComplete() {
        mainCG.SetAlphaAndBools(false);
    }

}
