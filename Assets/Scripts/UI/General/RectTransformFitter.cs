﻿using UnityEngine;
using System.Collections;

public class RectTransformFitter : MonoBehaviour {

    public enum RTProperty {
        WIDTH,
        HEIGHT,
        POSX,
        POSY,
        SCALEX,
        SCALEY,
        SCALEZ
    }

    public RectTransform target;
    public RTProperty propertyToMatch;

    [Space(10)]
    public bool destroyOnAwake = true;
    public bool matchOnAwake = true;
    public bool keepPropertyMatched = false;

    void Awake() {
        if (destroyOnAwake && !matchOnAwake) {
            DestroyImmediate(this);
        }
    }

    void Start() {
        if (matchOnAwake) {
            Match();
        }
        if (destroyOnAwake) {
            DestroyImmediate(this);
        }
        else if (keepPropertyMatched) {

        }
    }

    public void Match() {
        RectTransform rt = this.GetComponent<RectTransform>();
        if (target != null) {
            switch (propertyToMatch) {
                case RTProperty.WIDTH:
                    rt.sizeDelta = new Vector2(target.sizeDelta.x, rt.sizeDelta.y);
                    break;
                case RTProperty.HEIGHT:
                    rt.sizeDelta = new Vector2(rt.sizeDelta.x, target.sizeDelta.y);
                    break;
                case RTProperty.POSX:
                    rt.anchoredPosition = new Vector2(target.anchoredPosition.x, rt.anchoredPosition.y);
                    break;
                case RTProperty.POSY:
                    rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, target.anchoredPosition.y);
                    break;
                case RTProperty.SCALEX:
                    rt.localScale = new Vector3(target.localScale.x, rt.localScale.y, rt.localScale.z);
                    break;
                case RTProperty.SCALEY:
                    rt.localScale = new Vector3(rt.localScale.x, target.localScale.y, rt.localScale.z);
                    break;
                case RTProperty.SCALEZ:
                    rt.localScale = new Vector3(rt.localScale.x, rt.localScale.y, target.localScale.z);
                    break;
            }
        }
    }

    public bool CheckMatch() {
        bool rVal = false;
        if (target != null) {
            RectTransform rt = this.GetComponent<RectTransform>();
            switch (propertyToMatch) {
                case RTProperty.WIDTH:
                    rVal = rt.sizeDelta.x == target.sizeDelta.x;
                    break;
                case RTProperty.HEIGHT:
                    rVal = rt.sizeDelta.y == target.sizeDelta.y;
                    break;
                case RTProperty.POSX:
                    rVal = rt.anchoredPosition.x == target.anchoredPosition.x;
                    break;
                case RTProperty.POSY:
                    rVal = rt.anchoredPosition.y == target.anchoredPosition.y;
                    break;
                case RTProperty.SCALEX:
                    rVal = rt.localScale.x == target.localScale.x;
                    break;
                case RTProperty.SCALEY:
                    rVal = rt.localScale.y == target.localScale.y;
                    break;
                case RTProperty.SCALEZ:
                    rVal = rt.localScale.z == target.localScale.z;
                    break;
            }
        }
        return rVal;
    }
}
