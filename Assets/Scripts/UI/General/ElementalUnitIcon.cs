﻿using UnityEngine;
using System.Collections;

public class ElementalUnitIcon : MonoBehaviour {

    private CanvasGroup cg;

    public ElementalType elementalType;

    [Space(10)]
    public GameObject earthUnitIcon;
    public GameObject fireUnitIcon;
    public GameObject airUnitIcon;
    public GameObject waterUnitIcon;

    void Awake() {
        cg = this.GetComponent<CanvasGroup>();
    }

    // Use this for initialization
    void Start() {
        UpdateIcon();
    }

    private void Show() {
        cg.alpha = 1;
    }

    private void Hide() {
        cg.alpha = 0;
    }

    private void UpdateIcon() {
        SetAllActive(false);
        Show();
        switch (elementalType) {
            case ElementalType.EARTH:
                earthUnitIcon.SetActive(true);
                break;
            case ElementalType.FIRE:
                fireUnitIcon.SetActive(true);
                break;
            case ElementalType.AIR:
                airUnitIcon.SetActive(true);
                break;
            case ElementalType.WATER:
                fireUnitIcon.SetActive(true);
                break;
            case ElementalType.NONE:
            default:
                Hide();
                break;
        }
    }

    private void SetAllActive(bool value) {
        earthUnitIcon.SetActive(value);
        fireUnitIcon.SetActive(value);
        airUnitIcon.SetActive(value);
        waterUnitIcon.SetActive(value);
    }

    public void SetElementalType(ElementalType type) {
        if (GameSettings.Instance.showUnitElementalAffinity) {
            elementalType = type;
            UpdateIcon();
        }
        else {
            Hide();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
