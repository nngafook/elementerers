﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextToggleButton : MonoBehaviour {

    private bool isSelected = false;
    private Text buttonText;
    private Toggle toggle;

    public Color baseColor;
    public Color selectedColor;

    public bool isOn { get { return toggle.isOn; } set { toggle.isOn = value; } }

    void Awake() {
        buttonText = this.GetComponent<Text>();
        toggle = this.GetComponent<Toggle>();

        baseColor = buttonText.color;
        toggle.onValueChanged.AddListener(OnValueChanged);

        isSelected = toggle.isOn;
    }

    void Start() {
        UpdateColor();
    }

    private void UpdateColor() {
        if (isSelected) {
            buttonText.color = selectedColor;
        }
        else {
            buttonText.color = baseColor;
        }
    }

    private void OnValueChanged(bool value) {
        isSelected = value;

        UpdateColor();
    }

}
