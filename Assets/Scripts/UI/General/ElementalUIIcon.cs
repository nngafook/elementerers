﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ElementalUIIcon : MonoBehaviour {

    public ElementalType elementalType;

    [Space(10)]
    public Image earthIcon;
    public Image fireIcon;
    public Image airIcon;
    public Image waterIcon;

	// Use this for initialization
	void Start () {
        
	}

    private void SetIcon() {
        HideAllIcons();
        switch (elementalType) {
            case ElementalType.AIR:
                airIcon.gameObject.SetActive(true);
                break;
            case ElementalType.EARTH:
                earthIcon.gameObject.SetActive(true);
                break;
            case ElementalType.FIRE:
                fireIcon.gameObject.SetActive(true);
                break;
            case ElementalType.WATER:
                waterIcon.gameObject.SetActive(true);
                break;
        }
    }

    private void HideAllIcons() {
        earthIcon.gameObject.SetActive(false);
        fireIcon.gameObject.SetActive(false);
        airIcon.gameObject.SetActive(false);
        waterIcon.gameObject.SetActive(false);
    }

    public void SetType(ElementalType type) {
        elementalType = type;
        SetIcon();
    }

	// Update is called once per frame
	void Update () {
	
	}
}
