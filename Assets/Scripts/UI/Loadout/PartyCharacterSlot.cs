﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using Spine.Unity;
using UnityEngine.Events;
using AnimationState = Spine.AnimationState;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider2D))]
public class PartyCharacterSlot : MonoBehaviour {

    private const string IDLE_ANIMATION_NAME = "idle";
    private const string MELEE_ANIMATION_NAME = "meleeSwing1-fullBody";

    [HideInInspector]
    public IntEvent OnClicked;

    private int fingerID = -1;

    private int listIndex = -1;
    private float scaleSpeed = 0.5f;
    private float characterScaleSpeed = 0.1f;

    private bool isBackline = false;
    private bool isSelected = false;

    private WaitForSeconds waitForAttack;
    public SkeletonAnimation skeletonAnimationObject;

    [Header("Colliders")]
    public BoxCollider2D occupiedCollider;
    public BoxCollider2D unoccupiedCollider;

    public bool IsBackline { get { return isBackline; } }

    void Awake() {
        #if !UNITY_EDITOR
            fingerID = 0;
        #endif
    }

    // Use this for initialization
    void Start() {
        waitForAttack = new WaitForSeconds(4);
    }

    private IEnumerator RandomAttack() {
        yield return waitForAttack;

        skeletonAnimationObject.loop = false;
        skeletonAnimationObject.AnimationName = MELEE_ANIMATION_NAME;
        skeletonAnimationObject.state.Complete += OnMeleeSwingAnimationComplete;
    }

    private void OnMeleeSwingAnimationComplete(AnimationState state, int trackIndex, int loopCount) {
        skeletonAnimationObject.state.Complete -= OnMeleeSwingAnimationComplete;
        skeletonAnimationObject.loop = true;
        skeletonAnimationObject.AnimationName = IDLE_ANIMATION_NAME;
        if (isSelected) {
            StartCoroutine("RandomAttack");
        }
    }

    void OnMouseUpAsButton() {
        if (EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject(fingerID)) {
            if (OnClicked != null) {
                OnClicked.Invoke(listIndex);
            }
        }
    }

    public void SetIndex(int value) {
        listIndex = value;
        UpdateSkin();
    }

    public void UpdateSkin() {
        if (PartyManager.Instance.CurrentPartyIDs.Length > listIndex) {
            CharacterID indexID = PartyManager.Instance.CurrentPartyIDs[listIndex];
            if (indexID != CharacterID.NONE) {
                SetCharacterSkin(DataManager.Instance.CharacterDataObjectByID(indexID).data.SkinName);
            }
            else {
                ClearCharacter();
            }
        }
    }

    public void SetSelected(bool value) {
        isSelected = value;

        if (isSelected) {
            AnimateSelected();
            StartCoroutine("RandomAttack");
        }
        else {
            AnimateDeselected();
            StopCoroutine("RandomAttack");
        }
    }

    public void ScaleCharacterDown() {
        skeletonAnimationObject.transform.DOScale(0, characterScaleSpeed);
    }

    public void ScaleCharacterUp() {
        skeletonAnimationObject.transform.DOScale(1, characterScaleSpeed);
    }

    public void AnimateSelected() {
        transform.DOScale(1.3f, scaleSpeed).SetEase(Ease.OutElastic);
    }

    public void AnimateDeselected() {
        transform.DOScale(1f, scaleSpeed).SetEase(Ease.OutElastic);
    }

    public void ClearCharacter() {
        StopCoroutine("RandomAttack");
        unoccupiedCollider.enabled = true;
        occupiedCollider.enabled = false;
        ScaleCharacterDown();
    }

    public void SetCharacterSkin(string skinName) {
        skeletonAnimationObject.skeleton.SetSkin(skinName);

        skeletonAnimationObject.skeleton.SetAttachment("item_near", "sword_1");
        unoccupiedCollider.enabled = false;
        occupiedCollider.enabled = true;
        ScaleCharacterUp();
    }

    public void ScaleUpForSwap() {
        AnimateSelected();
    }

    public void ScaleDownForSwap() {
        AnimateDeselected();
    }

    // Update is called once per frame
    void Update() {

    }
}