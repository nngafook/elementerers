﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Text;

public class CombatStrategyWindow : GameWindow {

    private enum ViewingPosition {
        Targets,
        Actions,
        Percentages
    }

    private ViewingPosition currentViewingPosition = ViewingPosition.Targets;

    private float arrowScaleSpeed = 0.15f;
    private float targetMoveSpeed = 0.15f;
    private Vector2 availableActionListPosition = Vector2.zero;
    private Vector2 availableTargetListPosition = new Vector2(-230, 0);
    private Vector2 availablePercentageListPosition = new Vector2(-460, 0);

    private CharacterSaveInfo selectedCharacterSaveInfo;
    private CombatOrder selectedCombatOrder;
    private CombatOrderButton selectedCharacterTargetButton;
    private List<CombatOrder> targetingOrders = new List<CombatOrder>();

    private List<DataSelectIcon<CharacterData>> characterIcons = new List<DataSelectIcon<CharacterData>>();
    private List<CombatOrderButton> characterOrderButtons = new List<CombatOrderButton>();
    private List<CombatOrderButton> targetButtons = new List<CombatOrderButton>();
    private List<CombatOrderButton> actionButtons = new List<CombatOrderButton>();
    private List<CombatOrderButton> percentageButtons = new List<CombatOrderButton>();

    private CharacterData selectedCharacterData;

    [Space(10)]
    public bool debugMode = false;

    [Space(10)]
    public Text combatStrategyPathText;

    [Space(10)]
    public RectTransform listsContainer;

    [Space(10)]
    public ToggleGroup characterTargetToggleGroup;
    public ToggleGroup availableTargetToggleGroup;
    public ToggleGroup availableActionToggleGroup;
    public ToggleGroup avaialblePercentageToggleGroup;

    [Space(10)]
    public GameObject characterSelectIconPrefab;
    public GameObject combatOrderButtonPrefab;
    
    [Space(10)]
    public Transform characterListContainer;
    public RectTransform selectedCharacterTargetBox;

    [Space(10)]
    public Transform characterTargetListContainer;
    public Transform availableTargetListContainer;
    public Transform availableActionListContainer;
    public Transform availablePercentageListContainer;

    [Space(10)]
    public ScrollRect characterTargetScrollRect;
    public ScrollRect availableTargetScrollRect;
    public ScrollRect availableActionScrollRect;
    public ScrollRect availablePercentageScrollRect;

    [Space(10)]
    public RectTransform backArrow;
    public RectTransform nextArrow;

    protected override void Awake() {
        base.Awake();
    }

    protected override void Start() {
        base.Start();
        SetRectTransformVisible(selectedCharacterTargetBox, false);
        EmptyAvailableLists();
        EmptyStrategyPath();

        CreateCharacterIcons();
    }

    private void CreateCharacterIcons() {
        characterIcons = new List<DataSelectIcon<CharacterData>>();

        CharacterSelectIcon icon = null;

        CharacterDataObject[] objs = DataManager.Instance.CharacterDataObjects;

        if (debugMode) {
            for (int i = 0; i < objs.Length; i++) {
                if (objs[i].data.entityType == EntityType.PLAYER) {
                    icon = Instantiate(characterSelectIconPrefab).GetComponent<CharacterSelectIcon>();
                    icon.Init(objs[i].data, OnCharacterPressed);
                    icon.transform.SetParent(characterListContainer, false);
                    characterIcons.Add(icon);
                }
            }
        }
        else {
            //AddUnlockedCharacter
            List<CharacterID> availableCharacters = GameProgress.Instance.AvailableCharacters;
            CharacterData data = null;
            for (int i = 0; i < availableCharacters.Count; i++) {
                data = null;
                for (int j = 0; j < objs.Length; j++) {
                    if (objs[j].data.characterID == availableCharacters[i]) {
                        data = objs[j].data;
                        break;
                    }
                }

                if (data != null && data.entityType == EntityType.PLAYER) {
                    icon = Instantiate(characterSelectIconPrefab).GetComponent<CharacterSelectIcon>();
                    icon.Init(data, OnCharacterPressed);
                    icon.transform.SetParent(characterListContainer, false);
                    characterIcons.Add(icon);
                }
            }
        }
    }

    private void SetRectTransformVisible(RectTransform rt, bool value) {
        rt.localScale = (value) ? Vector3.one : Vector3.zero;
    }

    private void SetRectTransformPosition(RectTransform rt, Vector2 pos) {
        rt.anchoredPosition = pos;
    }

    private void AnimateRectTransformPosition(RectTransform rt, Vector2 pos) {
        if (rt.localScale == Vector3.zero) {
            SetRectTransformPosition(rt, pos);
            rt.DOScale(1, targetMoveSpeed).SetEase(Ease.OutBack);
        }
        else {
            rt.DOAnchorPos(pos, targetMoveSpeed).SetEase(Ease.OutBack, 0.75f);
        }
    }

    private CharacterSelectIcon CharacterIconByID(CharacterID id) {
        CharacterSelectIcon rVal = null;
        for (int i = 0; i < characterIcons.Count; i++) {
            if (characterIcons[i].Data.characterID == id) {
                rVal = (CharacterSelectIcon)characterIcons[i];
                break;
            }
        }
        return rVal;
    }

    /// <summary>
    /// On a character is selected, load their combat target orders list
    /// </summary>
    private void LoadCharacterTargetOrders() {
        ClearList(ref characterOrderButtons);
        selectedCharacterTargetButton = null;
        EmptyStrategyPath();

        targetingOrders = new List<CombatOrder>();

        if (debugMode) {
            CombatOrder order = null;
            for (int i = 0; i < 10; i++) {
                order = new CombatOrder();
                targetingOrders.Add(order);
            }
        }
        else {
            List<CharacterSaveInfo> characterSaveInfos = GameProgress.Instance.CharacterSaveInfos;
            for (int i = 0; i < characterSaveInfos.Count; i++) {
                if (characterSaveInfos[i].ownerID == selectedCharacterData.characterID) {
                    selectedCharacterSaveInfo = characterSaveInfos[i];
                    targetingOrders = selectedCharacterSaveInfo.combatOrders;
                    break;
                }
            }
        }

        CombatOrderButton btn = null;
        for (int i = 0; i < targetingOrders.Count; i++) {
            btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
            //btn.SetInfo(targetingOrders[i].targetOrder, i, ref characterTargetToggleGroup, OnCharacterTargetOrderClicked);
            btn.SetInfo(targetingOrders[i], i, ref characterTargetToggleGroup, OnCharacterTargetOrderClicked);
            btn.transform.SetParent(characterTargetListContainer, false);
            characterOrderButtons.Add(btn);
        }
    }

    /// <summary>
    /// Load available Combat target orders
    /// </summary>
    private void LoadCombatTargetOrders() {
        ClearList(ref targetButtons);

        CombatOrderButton btn = null;
        CombatStrategyTargetOrder selectedTargetOrder = selectedCombatOrder.targetOrder;

        if (debugMode) {
            int index = 0;
            foreach (CombatStrategyTargetOrder order in Enum.GetValues(typeof(CombatStrategyTargetOrder))) {
                btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
                btn.SetInfo(order, index, ref availableTargetToggleGroup, OnTargetOrderClicked, TextAnchor.MiddleCenter);
                btn.transform.SetParent(availableTargetListContainer, false);
                targetButtons.Add(btn);
                if (btn.TargetOrder == selectedTargetOrder) {
                    btn.SetSelected(true);
                }
                index++;
            }
        }
        else {
            List<CombatStrategyTargetOrder> ownedTargetOrders = GameProgress.Instance.OwnedTargetOrders;

            for (int i = 0; i < ownedTargetOrders.Count; i++) {
                btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
                btn.SetInfo(ownedTargetOrders[i], i, ref availableTargetToggleGroup, OnTargetOrderClicked, TextAnchor.MiddleCenter);
                btn.transform.SetParent(availableTargetListContainer, false);
                targetButtons.Add(btn);
                if (btn.TargetOrder == selectedTargetOrder) {
                    btn.SetSelected(true);
                }
            }

        }
    }

    /// <summary>
    /// Load available combat action orders
    /// </summary>
    private void LoadCombatActionOrders() {
        ClearList(ref actionButtons);

        int index = 0;
        CombatOrderButton btn = null;
        CombatStrategyActionOrder selectedActionOrder = selectedCombatOrder.actionOrder;

        if (debugMode) {
            foreach (CombatStrategyActionOrder order in Enum.GetValues(typeof(CombatStrategyActionOrder))) {
                btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
                btn.SetInfo(order, index, ref availableActionToggleGroup, OnActionOrderClicked, TextAnchor.MiddleCenter);
                btn.transform.SetParent(availableActionListContainer, false);
                actionButtons.Add(btn);
                if (btn.ActionOrder == selectedActionOrder) {
                    btn.SetSelected(true);
                }
                index++;
            }
        }
        else{
            List<CombatStrategyActionOrder> ownedActionOrders = GameProgress.Instance.OwnedActionOrders;

            for (int i = 0; i < ownedActionOrders.Count; i++) {
                btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
                btn.SetInfo(ownedActionOrders[i], i, ref availableActionToggleGroup, OnActionOrderClicked, TextAnchor.MiddleCenter);
                btn.transform.SetParent(availableActionListContainer, false);
                actionButtons.Add(btn);
                if (btn.ActionOrder == selectedActionOrder) {
                    btn.SetSelected(true);
                }
            }
        }
    }

    /// <summary>
    /// Load available percentage orders
    /// </summary>
    private void LoadPercentageOrders() {
        ClearList(ref percentageButtons);

        int index = 0;
        CombatOrderButton btn = null;
        CombatStrategyPercentage selectedPercentage = selectedCombatOrder.targetPercentage;
        if (debugMode) {
            foreach (CombatStrategyPercentage percentage in Enum.GetValues(typeof(CombatStrategyPercentage))) {
                btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
                btn.SetInfo(percentage, index, ref avaialblePercentageToggleGroup, OnTargetPercentageClicked, TextAnchor.MiddleCenter);
                btn.transform.SetParent(availablePercentageListContainer, false);
                percentageButtons.Add(btn);
                if (btn.TargetPrecentage == selectedPercentage) {
                    btn.SetSelected(true);
                }
                index++;
            }
        }
        else {
            List<CombatStrategyPercentage> ownedPercentageOrders = GameProgress.Instance.OwnedPercentageOrders;

            for (int i = 0; i < ownedPercentageOrders.Count; i++) {
                btn = Instantiate(combatOrderButtonPrefab).GetComponent<CombatOrderButton>();
                btn.SetInfo(ownedPercentageOrders[i], i, ref avaialblePercentageToggleGroup, OnTargetPercentageClicked, TextAnchor.MiddleCenter);
                btn.transform.SetParent(availablePercentageListContainer, false);
                percentageButtons.Add(btn);
                if (btn.TargetPrecentage == selectedPercentage) {
                    btn.SetSelected(true);
                }
            }
        }
    }

    private void ResetViewingToActions() {
        if (currentViewingPosition != ViewingPosition.Actions) {
            currentViewingPosition = ViewingPosition.Actions;
            ScrollListsTo(currentViewingPosition);
        }
    }

    private void ClearList(ref List<CombatOrderButton> list) {
        for (int i = 0; i < list.Count; i++) {
            Destroy(list[i].gameObject);
            list[i] = null;
        }
        list.Clear();
    }

    private void EmptyAvailableLists() {
        ClearList(ref targetButtons);
        ClearList(ref actionButtons);
        ClearList(ref percentageButtons);
        
        SetRectTransformVisible(nextArrow, false);
        SetRectTransformVisible(backArrow, false);

        ResetAvailableScrollListPositions();
    }

    private void ResetAvailableScrollListPositions() {
        availableTargetScrollRect.verticalNormalizedPosition = 1;
        availableActionScrollRect.verticalNormalizedPosition = 1;
        availablePercentageScrollRect.verticalNormalizedPosition = 1;
    }

    private void UpdateStrategyPath() {
        combatStrategyPathText.text = CombatStrategyUtils.GetReadableCombatOrder(selectedCombatOrder);
    }

    private void EmptyStrategyPath() {
        combatStrategyPathText.text = "";
    }

    private void SaveCharacterInfo() {
        if (selectedCharacterSaveInfo != null && !debugMode) {
            GameProgress.Instance.SaveCharacterInfo(selectedCharacterSaveInfo);
        }
    }

    private void RefreshListAvailability() {
        switch (currentViewingPosition) {
            case ViewingPosition.Actions:
                backArrow.DOScale(0, arrowScaleSpeed);
                if ((selectedCharacterTargetButton == null) || (selectedCombatOrder != null && (selectedCombatOrder.actionOrder == CombatStrategyActionOrder.None || selectedCombatOrder.actionOrder == CombatStrategyActionOrder.Defend))) {
                    nextArrow.DOScale(0, arrowScaleSpeed);
                }
                else {
                    nextArrow.DOScale(1, arrowScaleSpeed);
                }
                break;
            case ViewingPosition.Targets:
                backArrow.DOScale(1, arrowScaleSpeed);
                if (selectedCombatOrder.targetOrder != CombatStrategyTargetOrder.TargetHealthAbove && selectedCombatOrder.targetOrder != CombatStrategyTargetOrder.TargetHealthBelow) {
                    nextArrow.DOScale(0, arrowScaleSpeed);
                }
                else {
                    nextArrow.DOScale(1, arrowScaleSpeed);
                }
                break;
            case ViewingPosition.Percentages:
                backArrow.DOScale(1, arrowScaleSpeed);
                nextArrow.DOScale(0, arrowScaleSpeed);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Hides things that need to be hidden and so on.
    /// </summary>
    private void RefreshWindow() {
        if (selectedCharacterData == null) {
            //SetRectTransformVisible(selectedCharacterTargetBox, false);
            selectedCharacterTargetBox.DOScale(0, targetMoveSpeed).SetEase(Ease.InBack);
        }
    }

    private void ScrollListsTo(ViewingPosition position) {
        switch (position) {
            case ViewingPosition.Targets:
                listsContainer.DOAnchorPos(availableTargetListPosition, targetMoveSpeed);
                //SetRectTransformVisible(backArrow, false);
                //SetRectTransformVisible(nextArrow, true);
                break;
            case ViewingPosition.Actions:
                listsContainer.DOAnchorPos(availableActionListPosition, targetMoveSpeed);
                break;
            case ViewingPosition.Percentages:
                listsContainer.DOAnchorPos(availablePercentageListPosition, targetMoveSpeed);
                //SetRectTransformVisible(nextArrow, false);
                //SetRectTransformVisible(backArrow, true);
                break;
            default:
                break;
        }
        RefreshListAvailability();
    }

    
    #region BUTTON_EVENTS

    public void OnCharacterPressed(CharacterData data) {
        if (selectedCharacterData != data) {
            selectedCharacterData = data;
            AnimateRectTransformPosition(selectedCharacterTargetBox, CharacterIconByID(selectedCharacterData.characterID).AnchoredPosition);

            LoadCharacterTargetOrders();

            ResetViewingToActions();

            EmptyAvailableLists();
            characterTargetScrollRect.verticalNormalizedPosition = 1;

            RefreshWindow();
        }
        else {
            selectedCharacterData = null;
            selectedCharacterTargetButton = null;
            EmptyStrategyPath();
            EmptyAvailableLists();
            ClearList(ref characterOrderButtons);
            characterTargetScrollRect.verticalNormalizedPosition = 1;
            RefreshWindow();
        }
    }

    public void OnNextArrowClicked() {
        bool tween = true;
        switch (currentViewingPosition) {
            case ViewingPosition.Actions:
                currentViewingPosition = ViewingPosition.Targets;
                //nextArrow.DOScale(0, arrowScaleSpeed);
                break;
            case ViewingPosition.Targets:
                currentViewingPosition = ViewingPosition.Percentages;
                //backArrow.DOScale(1, arrowScaleSpeed);
                break;
            case ViewingPosition.Percentages:
            default:
                tween = false;
                break;
        }
        if (tween) {
            ScrollListsTo(currentViewingPosition);
        }
    }

    public void OnBackArrowClicked() {
        bool tween = true;
        switch (currentViewingPosition) {
            case ViewingPosition.Targets:
                currentViewingPosition = ViewingPosition.Actions;
                //backArrow.DOScale(0, arrowScaleSpeed);
                break;
            case ViewingPosition.Percentages:
                currentViewingPosition = ViewingPosition.Targets;
                //nextArrow.DOScale(1, arrowScaleSpeed);
                break;
            case ViewingPosition.Actions:
            default:
                tween = false;
                break;
        }
        if (tween) {
            ScrollListsTo(currentViewingPosition);
        }
    }

    public void OnCharacterTargetOrderClicked(CombatOrderButton btn) {
        if (btn.IsSelected) {
            selectedCharacterTargetButton = btn;
            selectedCombatOrder = targetingOrders[btn.ListIndex];

            RefreshListAvailability();

            LoadCombatTargetOrders();
            LoadCombatActionOrders();
            LoadPercentageOrders();

            UpdateStrategyPath();

            ResetViewingToActions();
            //nextArrow.DOScale(1, arrowScaleSpeed);

            SaveCharacterInfo();
        }
        else {
            EmptyStrategyPath();
            selectedCharacterTargetButton = null;
            ResetViewingToActions();
            EmptyAvailableLists();
        }
    }
    
    public void OnTargetOrderClicked(CombatOrderButton btn) {
        selectedCombatOrder.targetOrder = btn.TargetOrder;
        selectedCharacterTargetButton.UpdateOrderLabel(selectedCombatOrder);
        RefreshListAvailability();

        UpdateStrategyPath();

        SaveCharacterInfo();
    }

    public void OnActionOrderClicked(CombatOrderButton btn) {
        SaveCharacterInfo();
        selectedCombatOrder.actionOrder = btn.ActionOrder;
        selectedCharacterTargetButton.UpdateOrderLabel(selectedCombatOrder);
        RefreshListAvailability();

        UpdateStrategyPath();

        SaveCharacterInfo();
    }

    public void OnTargetPercentageClicked(CombatOrderButton btn) {
        SaveCharacterInfo();
        selectedCombatOrder.targetPercentage = btn.TargetPrecentage;
        selectedCharacterTargetButton.UpdateOrderLabel(selectedCombatOrder);
        RefreshListAvailability();

        UpdateStrategyPath();

        SaveCharacterInfo();
    }

    #endregion BUTTON_EVENTS

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyUp(KeyCode.I)) {
            Open();
        }
    }
}
