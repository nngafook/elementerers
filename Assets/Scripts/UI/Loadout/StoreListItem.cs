﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StoreListItem : MonoBehaviour {

    public GameObject rowBG;
    [Space(10)]
    public Button buyButton;
    public Text itemNameText;
    public Text itemCostText;

    private Image btnImage;
    private Color baseColor;
    private Color disabledColor = new Color (0.6f, 0.6f, 0.6f, 1f);
    private Color baseCostColor;
    private StoreManager storeManager;
    private SpecialAttackType attackType;
    private bool availableForPurchase = false;
    private string itemName;
    private int itemCost;

    public SpecialAttackType SpecialAttackType { get { return attackType; } }

	void Awake() {
        btnImage = buyButton.GetComponent<Image>();
        baseColor = btnImage.color;
        baseCostColor = itemCostText.color;
	}

    private void SetAvailableForPurchase(bool value) {
        availableForPurchase = value;
        buyButton.interactable = availableForPurchase;
        btnImage.color = (availableForPurchase) ? baseColor : disabledColor;
        itemCostText.color = (availableForPurchase) ? baseCostColor : CustomColor.GetColor(ColorName.ERROR_RED);
    }

    public void SetData(SpecialAttackData data, StoreManager manager) {
        storeManager = manager;
        itemName = data.name;
        itemCost = data.storeCost;
        attackType = data.specialAttackType;

        RefreshAvailableForPurchase();

        itemNameText.text = itemName;
        itemCostText.text = itemCost.ToString();
    }

    public void SetRowBGVisible(bool value) {
        rowBG.SetActive(value);
    }

    public void OnInfoButtonPressed() {
        storeManager.ShowInfo(attackType);
    }

    public void OnBuyButtonPressed() {
        if (GameProgress.Instance.CurrentScrap >= itemCost) {
            GameProgress.Instance.AddToCurrentScrap(-itemCost);
            SpecialAttackDataInventory.Instance.AddItem(DataManager.Instance.SpecialAttackDataByType(attackType));
            storeManager.ItemPurchased();
        }
    }

    public void RefreshAvailableForPurchase() {
        SetAvailableForPurchase(GameProgress.Instance.CurrentScrap >= itemCost);
    }

    /// <summary>
    /// Called when the item is purchased from the store.
    /// This removes the "view" of the item from the store front.
    /// </summary>
    public void SeeYourselfOut() {
        Destroy(this.gameObject);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
