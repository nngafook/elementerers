﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ClickOutsideCollider : MonoBehaviour {

    public UnityEvent OnClickEvent;

    private bool downWasOverThis = false;

	void Awake() {
	}

    private bool IsRaycastOverThis() {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        return (hit.collider != null && hit.collider.gameObject.name == this.gameObject.name);
    }

    private void ProcessMouseDown() {
        if (Input.GetMouseButtonDown(0)) {
            if (IsRaycastOverThis()) {
                downWasOverThis = true;
            }
        }
    }

    private void ProcessMouseUp() {
        if (Input.GetMouseButtonUp(0)) {
            if (IsRaycastOverThis() && downWasOverThis) {
                if (OnClickEvent != null) {
                    OnClickEvent.Invoke();
                }
            }
            downWasOverThis = false;
        }
    }

	// Update is called once per frame
	void Update () {
        ProcessMouseDown();
        ProcessMouseUp();
	}
}
