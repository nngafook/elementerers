﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CharacterDetailsWindow : MonoBehaviour {

    private float scaleSpeed = 0.05f;
    private bool isOpen = false;

    private RectTransform rectTransform;

    public Text nameValueText;
    public Text healthValueText;
    public Text attackValueText;
    public Text defenseValueText;
    public Text specialAttackValueText;
    public ElementalUIIcon elementalAffinityIcon;


    void Awake() {
        rectTransform = this.GetComponent<RectTransform>();

        SetOpen(false);
    }

	// Use this for initialization
	void Start () {
	
	}

    private void SetOpen(bool value) {
        isOpen = value;
        rectTransform.localScale = (isOpen) ? Vector3.one : Vector3.zero;
    }
    public void SetValues(CombatEntity entity) {
        SetValues(entity.Data);
    }

    public void SetValues(CharacterData data) {
        nameValueText.text = data.characterName;
        healthValueText.text = data.maxHealth.ToString();
        attackValueText.text = data.attackDamage.min.ToString() + " - " + data.attackDamage.max.ToString();
        defenseValueText.text = data.defense.min.ToString() + " - " + data.defense.max.ToString();
        specialAttackValueText.text = Utility.EnumNameToReadable(GameProgress.Instance.GetEquippedSpecialAttackType(data.characterID).ToString());// Utility.CharacterIDToReadable(data.specialAttackType.ToString());
        elementalAffinityIcon.SetType(data.elementalAffinity);
        // skills
    }

    public void Open() {
        if (!isOpen) {
            rectTransform.DOScale(1, scaleSpeed);
        }

        isOpen = true;
    }

    public void Close() {
        isOpen = false;
        rectTransform.DOScale(0, scaleSpeed);
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
