﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class StoreItemInfoWindow : MonoBehaviour {

    private bool isTweening = false;
    private float infoScaleSpeed = 0.35f;

    [Header("Visibility Components")]
    public CanvasGroup infoCG;
    public RectTransform infoWindowBG;

    [Header("Texts")]
    public Text descriptionText;
    public Text primaryEffectText;
    public Text targetsText;
    public Text elementalTypeText;

    [Header("Colors")]
    public Color baseTextColor;
    public Color greenColor;
    public Color blueColor;
    public Color earthTextColor;
    public Color fireTextColor;
    public Color airTextColor;
    public Color waterTextColor;

	// Use this for initialization
	void Start () {
	
	}

    private Color ColorByElementalType(ElementalType type) {
        Color rVal = baseTextColor;
        switch (type) {
            case ElementalType.EARTH:
                rVal = earthTextColor;
                break;
            case ElementalType.FIRE:
                rVal = fireTextColor;
                break;
            case ElementalType.AIR:
                rVal = airTextColor;
                break;
            case ElementalType.WATER:
                rVal = waterTextColor;
                break;
            case ElementalType.NONE:
            default:
                rVal = baseTextColor;
                break;
        }
        return rVal;
    }

    private void SetPrimaryText(SpecialAttackData data) {
        string primarySuffix = (data.specialAttackType == SpecialAttackType.HEAL) ? "Heal Factor: " : "Damage Factor: ";
        Color primaryColor = (data.specialAttackType == SpecialAttackType.HEAL) ? greenColor : fireTextColor;
        primaryEffectText.text = primarySuffix + (data.multiplier.ToString()).Colored(primaryColor);
    }

    private void SetTargetsText(SpecialAttackData data) {
        //string targets = Utility.TargetTypeByEffectType(data.modifierType);
        string targets = "Single";
        targetsText.text = "Target: " + targets.Colored(blueColor);
    }

    private void SetElementalTypeText(SpecialAttackData data) {
        string value = Utility.ToCamelCase(data.elementalType.ToString());
        Color c = ColorByElementalType(data.elementalType);
        elementalTypeText.text = "Elemental: " + value.Colored(c);
    }

    private void SetInfoCGVisible(bool value) {
        infoCG.alpha = value.AsInt();
        infoCG.interactable = value;
        infoCG.blocksRaycasts = value;
    }

    private void OnScaleCloseComplete() {
        isTweening = false;
        SetInfoCGVisible(false);
    }

    private void OnScaleOpenComplete() {
        isTweening = false;
    }

    public void SetInfo(SpecialAttackData data) {
        descriptionText.text = data.description;
        SetPrimaryText(data);
        SetTargetsText(data);
        SetElementalTypeText(data);
    }

    private void Close() {
        infoWindowBG.DOScale(0, infoScaleSpeed).SetEase(Ease.InBack).OnComplete(OnScaleCloseComplete);
    }

    public void Open() {
        if (!isTweening) {
            SetInfoCGVisible(true);
            isTweening = true;

            infoWindowBG.DOScale(1, infoScaleSpeed).SetEase(Ease.OutBounce).OnComplete(OnScaleOpenComplete);
        }
    }

    public void OnOusideInfoWindowClicked() {
        if (!isTweening) {
            isTweening = true;
            //infoCG.interactable = false;
            //infoCG.blocksRaycasts = false;
            Close();
        }
    }

	// Update is called once per frame
	void Update () {
	
	}

}
