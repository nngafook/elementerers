﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class SpecialAttackSelectIcon : DataSelectIcon<SpecialAttackData> {

    public override void Init(SpecialAttackData theData, UnityAction<SpecialAttackData> callback) {
        base.Init(theData, callback);


        enabledColor = data.overlayColor;
        uiIcon.sprite = data.uiImage;
        uiIcon.GetComponent<Outline>().effectColor = data.outlineColor;
        uiIcon.color = enabledColor;
    }

}
