﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterSelectWindow : MonoBehaviour {

    //private int fingerID = -1;
    private int selectedPartySlotIndex = -1;
    private bool isOpen = false;
    private bool isSwapping = false;

    private List<CharacterDataObject> characterDataObjects;
    private List<CharacterSelectIcon> characterIcons;

    public Transform contentContainer;
    public ScrollRect contentScrollRect;

    public CharacterDetailsWindow characterDetailsPanel;
    public GameObject characterSelectIconPrefab;
    [Space(10)]
    public List<PartyCharacterSlot> partySlots;

    void Awake() {

    }

	// Use this for initialization
	void Start () {
        SetCharacterDataObjects();
        CreateIcons();
        AddPartySlotEvents();
        UpdateIconsEnabled();
	}

    private void AddPartySlotEvents() {
        for (int i = 0; i < partySlots.Count; i++) {
            partySlots[i].SetIndex(i);
            partySlots[i].OnClicked.AddListener(OnPartySlotClicked);
        }
    }

    private void SetCharacterDataObjects() {
        CharacterDataObject[] objs = Resources.LoadAll<CharacterDataObject>("Data/CharacterData/Players");
        characterDataObjects = new List<CharacterDataObject>(objs);
    }

    private void CreateIcons() {
        characterIcons = new List<CharacterSelectIcon>();
        CharacterSelectIcon icon = null;
        List<CharacterID> availableCharacters = GameProgress.Instance.AvailableCharacters;

        for (int i = 0; i < characterDataObjects.Count; i++) {
            for (int j = 0; j < availableCharacters.Count; j++) {
                if (availableCharacters[j] == characterDataObjects[i].data.characterID) {
                    icon = Instantiate(characterSelectIconPrefab).GetComponent<CharacterSelectIcon>();
                    icon.Init(characterDataObjects[i].data, OnIconPressed);
                    icon.transform.SetParent(contentContainer, false);
                    characterIcons.Add(icon);
                    break;
                }
            }
        }
    }

    private void UpdateWindowVisible() {
        if (selectedPartySlotIndex == -1) {
            CloseWindow();
        }
        else {
            OpenWindow();
        }
    }

    private void OpenWindow() {
        if (!isOpen) {
            RectTransform contentContainer = contentScrollRect.content.GetComponent<RectTransform>();
            contentContainer.anchoredPosition = contentContainer.anchoredPosition.SetY(0);
            SetScrollingEnabled(false);
            this.GetComponent<RectTransform>().DOScale(1, 0.1f).OnComplete(OnOpenAnimationComplete);
        }
    }

    private void CloseWindow() {
        if (isOpen) {
            SetScrollingEnabled(false);
            isOpen = false;
            isSwapping = false;
            characterDetailsPanel.Close();
            this.GetComponent<RectTransform>().DOScale(0, 0.1f);
        }
    }

    private void OnOpenAnimationComplete() {
        isOpen = true;
        SetScrollingEnabled(true);
    }

    private void SetScrollingEnabled(bool value) {
        contentScrollRect.enabled = value;
    }

    private void OnPartySlotClicked(int index) {
        if (PartyManager.Instance.CurrentPartyIDs.IsIndexValid(index)) {
            if (isSwapping) {
                if (index != selectedPartySlotIndex) {
                    PartyManager.Instance.SwapIDs(index, selectedPartySlotIndex);

                    partySlots[index].UpdateSkin();
                    partySlots[selectedPartySlotIndex].UpdateSkin();

                    partySlots[index].SetSelected(true);
                    partySlots[selectedPartySlotIndex].SetSelected(false);

                    selectedPartySlotIndex = index;
                    UpdateIconsEnabled();
                }
                StopSwapMode();
            }
            else {
                if (index == selectedPartySlotIndex) {
                    partySlots[selectedPartySlotIndex].SetSelected(false);
                    selectedPartySlotIndex = -1;
                    characterDetailsPanel.Close();
                }
                else {
                    if (selectedPartySlotIndex != -1) {
                        partySlots[selectedPartySlotIndex].SetSelected(false);
                    }
                    selectedPartySlotIndex = index;
                    partySlots[selectedPartySlotIndex].SetSelected(true);
                    if (PartyManager.Instance.CurrentPartyIDs[index] != CharacterID.NONE) {
                        characterDetailsPanel.SetValues(DataManager.Instance.CharacterDataObjectByID(PartyManager.Instance.CurrentPartyIDs[index]).data);
                        characterDetailsPanel.Open();
                    }
                    else {
                        characterDetailsPanel.Close();
                    }
                }
                UpdateWindowVisible();
            }
        }
    }

    private void StartSwapMode() {
        SetScrollingEnabled(false);
        CharacterID[] currentPartyIDs = PartyManager.Instance.CurrentPartyIDs;
        for (int i = 0; i < currentPartyIDs.Length; i++) {
            if (selectedPartySlotIndex != i) {
                partySlots[i].ScaleUpForSwap();
            }
        }
        isSwapping = true;
    }

    private void StopSwapMode() {
        CharacterID[] currentPartyIDs = PartyManager.Instance.CurrentPartyIDs;
        for (int i = 0; i < currentPartyIDs.Length; i++) {
            if (selectedPartySlotIndex != i) {
                partySlots[i].ScaleDownForSwap();
            }
        }
        isSwapping = false;
        SetScrollingEnabled(true);
    }

    private void UpdateIconsEnabled() {
        for (int i = 0; i < characterIcons.Count; i++) {
            characterIcons[i].SetEnabled(true);
        }

        CharacterID[] partyIDs = PartyManager.Instance.CurrentPartyIDs;
        for (int i = 0; i < partyIDs.Length; i++) {
            for (int j = 0; j < characterIcons.Count; j++) {
                if (partyIDs[i] == characterIcons[j].Data.characterID) {
                    characterIcons[j].SetEnabled(false);
                }
            }
        }
    }

    public void OnClickOutside() {
        if (isOpen) {
            CloseWindow();
            partySlots[selectedPartySlotIndex].SetSelected(false);
            selectedPartySlotIndex = -1;
        }
    }

    public void OnClearSlotPressed() {
        if (selectedPartySlotIndex != -1) {
            PartyManager.Instance.ClearSlot(selectedPartySlotIndex);
            partySlots[selectedPartySlotIndex].ClearCharacter();
            characterDetailsPanel.Close();
            UpdateIconsEnabled();
        }
    }

    public void OnClearPartyPressed() {
        PartyManager.Instance.ClearParty();
        UpdateIconsEnabled();
        characterDetailsPanel.Close();
        for (int i = 0; i < partySlots.Count; i++) {
            partySlots[i].ClearCharacter();
        }
    }
    
    public void OnIconPressed(CharacterData data) {
        if (selectedPartySlotIndex != -1) {
            partySlots[selectedPartySlotIndex].SetCharacterSkin(data.SkinName);
            characterDetailsPanel.SetValues(data);
            characterDetailsPanel.Open();
        }
        PartyManager.Instance.SetCharacterSlot(selectedPartySlotIndex, data);
        UpdateIconsEnabled();
    }

    public void OnSwapPressed() {
        StartSwapMode();
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
