﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class DataSelectIcon<T> : MonoBehaviour  {

    protected UnityAction<T> OnPressedCallback;

    protected Image uiIcon;
    protected T data;
    //protected SpecialAttackData spAtkData;
    protected Color enabledColor = Color.white;
    protected Color disabledColor = new Color(0.2784314f, 0.2784314f, 0.2784314f, 1f);

    public T Data { get { return data; } }

    public Vector2 AnchoredPosition { get { return this.GetComponent<RectTransform>().anchoredPosition; } }

    void Awake() {
        uiIcon = this.GetComponent<Image>();
    }

    public virtual void Init(T theData, UnityAction<T> callback) {
        data = theData;
        
        OnPressedCallback = callback;
        
    }

    public void SetEnabled(bool value) {
        this.GetComponent<Button>().interactable = value;
        uiIcon.color = (value) ? enabledColor : disabledColor;
    }

    public void SetColor(bool value) {
        uiIcon.color = (value) ? enabledColor : disabledColor;
    }

    public void OnPressed() {
        //selectWindow.OnIconPressed(characterData);
        if (OnPressedCallback != null) {
            OnPressedCallback(data);
        }
    }
}
