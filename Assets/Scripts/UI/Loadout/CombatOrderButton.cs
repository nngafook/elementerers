﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class CombatOrderButton : MonoBehaviour {

    private int listIndex = -1;
    private float replaceSpeed = 0.1f;
    private bool sendCallback = true;

    private Color selectedColor = CustomColor.GetColor(ColorName.RACY_RED);
    private Action<CombatOrderButton> OnButtonClicked;

    private CombatStrategyTargetOrder targetOrder;
    private CombatStrategyActionOrder actionOrder;
    private CombatStrategyPercentage targetPercentage;

    public Text label;
    public Toggle toggleComponent;

    public int ListIndex { get { return listIndex; } }
    public bool IsSelected { get { return toggleComponent.isOn; } }
    public CombatStrategyTargetOrder TargetOrder { get { return targetOrder; } }
    public CombatStrategyActionOrder ActionOrder { get { return actionOrder; } }
    public CombatStrategyPercentage TargetPrecentage { get { return targetPercentage; } }

    void Awake() {

    }

    // Use this for initialization
    void Start() {

    }

    private void OnLabelReplaceScaleDownComplete() {
        //CombatOrder order = new CombatOrder();
        //order.targetOrder = targetOrder;
        //order.actionOrder = actionOrder;
        //order.strategyPercentage = targetPercentage;
        //UpdateOrderLabel(order);
        this.GetComponent<RectTransform>().DOScaleX(1, replaceSpeed);
    }

    private void SetOtherInfo(int index, ref ToggleGroup tg, Action<CombatOrderButton> onClickedCallback, TextAnchor align) {
        OnButtonClicked = onClickedCallback;
        toggleComponent.group = tg;
        listIndex = index;

        label.alignment = align;
    }

    public void UpdateOrderLabel(CombatOrder order) {
        actionOrder = order.actionOrder;
        targetOrder = order.targetOrder;
        targetPercentage = order.targetPercentage;
        label.text = CombatStrategyUtils.GetReadableCombatOrder(order);
        this.GetComponent<RectTransform>().DOScaleX(0, replaceSpeed).OnComplete(OnLabelReplaceScaleDownComplete);
    }

    public void SetInfo(CombatStrategyTargetOrder order, int index, ref ToggleGroup tg, Action<CombatOrderButton> onClickedCallback, TextAnchor align = TextAnchor.MiddleCenter) {
        targetOrder = order;
        label.text = CombatStrategyUtils.GetReadableName(targetOrder);
        SetOtherInfo(index, ref tg, onClickedCallback, align);
    }

    public void SetInfo(CombatStrategyActionOrder action, int index, ref ToggleGroup tg, Action<CombatOrderButton> onClickedCallback, TextAnchor align) {
        actionOrder = action;
        label.text = CombatStrategyUtils.GetReadableName(actionOrder);
        SetOtherInfo(index, ref tg, onClickedCallback, align);
    }

    public void SetInfo(CombatStrategyPercentage percentage, int index, ref ToggleGroup tg, Action<CombatOrderButton> onClickedCallback, TextAnchor align) {
        targetPercentage = percentage;
        label.text = CombatStrategyUtils.GetReadableName(targetPercentage);
        SetOtherInfo(index, ref tg, onClickedCallback, align);
    }

    public void SetInfo(CombatOrder order, int index, ref ToggleGroup tg, Action<CombatOrderButton> onClickedCallback) {
        targetOrder = order.targetOrder;
        actionOrder = order.actionOrder;
        targetPercentage = order.targetPercentage;
        selectedColor = CustomColor.GetColor(ColorName.ROB_BLUE);
        label.text = CombatStrategyUtils.GetReadableCombatOrder(order);
        SetOtherInfo(index, ref tg, onClickedCallback, TextAnchor.MiddleCenter);
    }

    //public void ReplaceTargetOrder(CombatStrategyTargetOrder order) {
    //    targetOrder = order;
    //    this.GetComponent<RectTransform>().DOScaleX(0, replaceSpeed).OnComplete(OnLabelReplaceScaleDownComplete);
    //}

    //public void ReplaceActionOrder(CombatStrategyActionOrder action) {
    //    actionOrder = action;
    //    this.GetComponent<RectTransform>().DOScaleX(0, replaceSpeed).OnComplete(OnLabelReplaceScaleDownComplete);
    //}

    //public void ReplaceTargetPercentage(CombatStrategyPercentage percentage) {
    //    targetPercentage = percentage;
    //    this.GetComponent<RectTransform>().DOScaleX(0, replaceSpeed).OnComplete(OnLabelReplaceScaleDownComplete);
    //}

    public void OnClicked() {
        label.color = (toggleComponent.isOn) ? selectedColor : Color.white;
        if (sendCallback && OnButtonClicked != null) {
            OnButtonClicked(this);
        }
        sendCallback = true;
    }

    public void SetSelected(bool value) {
        sendCallback = false;
        toggleComponent.isOn = value;
        label.color = (toggleComponent.isOn) ? selectedColor : Color.white;
    }

    // Update is called once per frame
    void Update() {

    }
}
