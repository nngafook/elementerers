﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class SettingsWindow : MonoBehaviour {

    private float tweenSpeed = 0.3f;
    private CanvasGroup canvasGroup;

    private Vector2 offScreenPosition;
    private Vector2 onScreenPosition;

    public RectTransform windowRectTransform;

    // Can TextToggleButtons start knowing about their counter part, i.e. the true button know about the false button.
    // So the setting can be sent to just one button, and it'll handle the rest.
    public TextToggleButton activeCombatStateButton;
    public TextToggleButton waitingCombatStateButton;

    public TextToggleButton showElementalAffinityButton;
    public TextToggleButton hideElementalAffinityButton;

    public TextToggleButton showQueuedAttacksButton;
    public TextToggleButton hideQueuedAttacksButton;

    void Awake() {
        canvasGroup = this.GetComponent<CanvasGroup>();

        onScreenPosition = windowRectTransform.anchoredPosition;
        offScreenPosition = new Vector2(windowRectTransform.sizeDelta.x * 2, onScreenPosition.y);

        SetClosed();
    }

    // Use this for initialization
    void Start() {
        UpdateSettings();
    }

    private void SetClosed() {
        windowRectTransform.anchoredPosition = offScreenPosition;
        SetCanvasGroup(false);
    }

    private void SetCanvasGroup(bool value) {
        canvasGroup.alpha = value.AsInt();
        canvasGroup.blocksRaycasts = value;
        canvasGroup.interactable = value;
    }

    private void UpdateSettings() {
        if (GameSettings.Instance.userCombatState == CombatState.ACTIVE) {
            activeCombatStateButton.isOn = true;
        }
        else {
            waitingCombatStateButton.isOn = true;
        }

        showElementalAffinityButton.isOn = GameSettings.Instance.showUnitElementalAffinity;
        hideElementalAffinityButton.isOn = !GameSettings.Instance.showUnitElementalAffinity;

        showQueuedAttacksButton.isOn = GameSettings.Instance.showUnitQueuedAction;
        hideQueuedAttacksButton.isOn = !GameSettings.Instance.showUnitQueuedAction;
    }

    public void OnCombatStateButtonPressed(string state) {
        switch (state) {
            case "active":
                GameSettings.Instance.userCombatState = CombatState.ACTIVE;
                break;
            case "waiting":
                GameSettings.Instance.userCombatState = CombatState.WAITING;
                break;
            default:
                Debug.Log("OnCombatStateButtonPressed() got a default state passed in");
                break;
        }
    }

    public void OnElementalAffinityButtonPressed(bool value) {
        GameSettings.Instance.showUnitElementalAffinity = value;
    }

    public void OnQueuedActionButtonPressed(bool value) {
        GameSettings.Instance.showUnitQueuedAction = value;
    }

    public void OnClosePressed() {
        GameSettings.Instance.SaveToJSON();

        Close();
    }

    public void Open() {
        SetCanvasGroup(true);

        windowRectTransform.anchoredPosition = new Vector2(offScreenPosition.x * -1, offScreenPosition.y);
        windowRectTransform.DOAnchorPos(onScreenPosition, tweenSpeed).SetEase(Ease.OutBack);
    }

    public void Close() {
        windowRectTransform.DOAnchorPos(offScreenPosition, tweenSpeed).SetEase(Ease.InBack).OnComplete(OnCloseTweenComplete);
    }

    private void OnCloseTweenComplete() {
        SetCanvasGroup(false);
    }

    // Update is called once per frame
    void Update() {

    }
}
