﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;

public class EquipWindow : MonoBehaviour {

    private float targetMoveSpeed = 0.5f;
    private float openSpeed = 0.25f;

    private List<DataSelectIcon<CharacterData>> characterIcons;
    private List<DataSelectIcon<SpecialAttackData>> spAtkIcons;

    private CharacterData selectedCharacterData;
    private SpecialAttackData equippedSpAtkData;

    public CanvasGroup blackOverlay;
    public RectTransform contentRectTransform;
    public CanvasGroup characterListCG;
    public CanvasGroup specialAttackListCG;
    public CanvasGroup equippedAttackCG;
    [Space(10)]
    public GameObject characterSelectIconPrefab;
    public GameObject specialAttackSelectIconPrefab;
    [Space(10)]
    public Transform characterListContainer;
    public Transform specialAttackListContainer;
    [Space(10)]
    public Image equippedSpecialAttackImage;
    public Text equippedSpecialAttackNameLabel;
    public RectTransform selectedCharacterTargetBox;
    public RectTransform selectedSpecialAttackTargetBox;

	// Use this for initialization
	void Start () {
        SetRectTransformVisible(selectedCharacterTargetBox, false);
        SetRectTransformVisible(selectedSpecialAttackTargetBox, false);

        CreateCharacterIcons();

        CreateSpecialAttackIcons();
	}

    private void CreateCharacterIcons() {
        characterIcons = new List<DataSelectIcon<CharacterData>>();
        CharacterDataObject[] objs = DataManager.Instance.CharacterDataObjects;

        CharacterSelectIcon icon = null;
        for (int i = 0; i < objs.Length; i++) {
            if (objs[i].data.entityType == EntityType.PLAYER) {
                icon = Instantiate(characterSelectIconPrefab).GetComponent<CharacterSelectIcon>();
                icon.Init(objs[i].data, OnCharacterPressed);
                icon.transform.SetParent(characterListContainer, false);
                characterIcons.Add(icon);
            }
        }
    }

    private void CreateSpecialAttackIcons() {
        spAtkIcons = new List<DataSelectIcon<SpecialAttackData>>();
        SpecialAttackDataObject[] spAtkObjs = DataManager.Instance.SpecialAttackDataObjects;
        DataSelectIcon<SpecialAttackData> spAtkIcon = null;
        for (int i = 0; i < spAtkObjs.Length; i++) {
            spAtkIcon = Instantiate(specialAttackSelectIconPrefab).GetComponent<DataSelectIcon<SpecialAttackData>>();
            spAtkIcon.Init(spAtkObjs[i].data, OnSpecialAttackPressed);
            spAtkIcon.transform.SetParent(specialAttackListContainer, false);
            spAtkIcons.Add(spAtkIcon);
        }
    }

    /// <summary>
    /// Hides things that need to be hidden and so on.
    /// </summary>
    private void RefreshWindow() {
        if (selectedCharacterData == null) {
            SetCGVisible(specialAttackListCG, false);
            SetCGVisible(equippedAttackCG, false);
            SetRectTransformVisible(selectedCharacterTargetBox, false);
            SetRectTransformVisible(selectedSpecialAttackTargetBox, false);
        }
        else {
            SetCGVisible(specialAttackListCG, true);
            RefreshSpecialAttacksEnabled();
        }

        if (equippedSpAtkData == null) {
            SetCGVisible(equippedAttackCG, false);
            SetRectTransformVisible(selectedSpecialAttackTargetBox, false);
            equippedSpecialAttackNameLabel.text = "";
        }
        else {
            equippedSpecialAttackNameLabel.text = equippedSpAtkData.name;
            SetCGVisible(equippedAttackCG, true);
            RefreshSpecialAttacksEnabled();
        }
    }

    private void SetCGVisible(CanvasGroup cg, bool value) {
        cg.alpha = value.AsInt();
        cg.interactable = value;
        cg.blocksRaycasts = value;
    }

    private CharacterSelectIcon CharacterIconByID(CharacterID id) {
        CharacterSelectIcon rVal = null;
        for (int i = 0; i < characterIcons.Count; i++) {
            if (characterIcons[i].Data.characterID == id) {
                rVal = (CharacterSelectIcon)characterIcons[i];
                break;
            }
        }
        return rVal;
    }

    private SpecialAttackSelectIcon SpecialAttackIconByType(SpecialAttackType type) {
        SpecialAttackSelectIcon rVal = null;
        for (int i = 0; i < spAtkIcons.Count; i++) {
            if (spAtkIcons[i].Data.specialAttackType == type) {
                rVal = (SpecialAttackSelectIcon)spAtkIcons[i];
                break;
            }
        }
        return rVal;
    }

    private void SetRectTransformVisible(RectTransform rt, bool value) {
        rt.localScale = (value) ? Vector3.one : Vector3.zero;
    }

    private void SetRectTransformPosition(RectTransform rt, Vector2 pos) {
        rt.anchoredPosition = pos;
    }

    private void AnimateRectTransformPosition(RectTransform rt, Vector2 pos) {
        if (rt.localScale == Vector3.zero) {
            SetRectTransformPosition(rt, pos);
            rt.DOScale(1, targetMoveSpeed).SetEase(Ease.OutBack);
        }
        else {
            rt.DOAnchorPos(pos, targetMoveSpeed).SetEase(Ease.OutBack, 0.75f);
        }
    }

    private void SaveNewEquippedSpecialAttack() {
        GameProgress.Instance.SaveNewEquippedSpecialAttack(selectedCharacterData.characterID, (equippedSpAtkData != null) ? equippedSpAtkData.specialAttackType : SpecialAttackType.NONE);
    }

    private void OnSpecialAttackPressed(SpecialAttackData data) {
        if (equippedSpAtkData != data) {
            SetEquippedSpecialAttackData(data);
            RefreshSpecialAttacksEnabled();
            RefreshWindow();
        }
        else {
            equippedSpAtkData = null;
            SaveNewEquippedSpecialAttack();
            RefreshSpecialAttacksEnabled();
            RefreshWindow();
        }
    }

    private void SetEquippedSpecialAttackData(SpecialAttackData data) {
        if (equippedSpAtkData != data) {
            equippedSpAtkData = data;
            RectTransform equippedSpAtkRectTransform = equippedSpecialAttackImage.GetComponent<RectTransform>();
            equippedSpAtkRectTransform.localScale = Vector3.zero;
            equippedSpecialAttackImage.sprite = equippedSpAtkData.uiImage;
            equippedSpecialAttackImage.color = equippedSpAtkData.overlayColor;
            equippedSpecialAttackImage.GetComponent<Outline>().effectColor = equippedSpAtkData.outlineColor;
            equippedSpecialAttackNameLabel.text = equippedSpAtkData.name;
            equippedSpAtkRectTransform.DOScale(1, targetMoveSpeed).SetEase(Ease.OutBack);
            SaveNewEquippedSpecialAttack();
            AnimateRectTransformPosition(selectedSpecialAttackTargetBox, SpecialAttackIconByType(equippedSpAtkData.specialAttackType).AnchoredPosition);
        }
    }

    private void OnCharacterPressed(CharacterData data) {
        if (selectedCharacterData != data) {
            selectedCharacterData = data;
            AnimateRectTransformPosition(selectedCharacterTargetBox, CharacterIconByID(selectedCharacterData.characterID).AnchoredPosition);

            SpecialAttackData spAtkData = GameProgress.Instance.EquippedSpecialAttackData(selectedCharacterData.characterID);

            if (spAtkData != null) {
                SetEquippedSpecialAttackData(spAtkData);
            }
            else {
                equippedSpAtkData = null;
            }
            RefreshSpecialAttacksEnabled();
            RefreshWindow();
        }
        else {
            selectedCharacterData = null;
            equippedSpAtkData = null;
            RefreshWindow();
        }
    }

    private void RefreshSpecialAttacksEnabled() {
        for (int i = 0; i < spAtkIcons.Count; i++) {
            if (equippedSpAtkData != null && spAtkIcons[i].Data.specialAttackType == equippedSpAtkData.specialAttackType) {
                spAtkIcons[i].SetColor(false);
            }
            else {
                spAtkIcons[i].SetColor(true);
            }
        }
    }

    #region PUBLIC_METHODS

    #if UNITY_EDITOR

    public void EditorOpen() {
        SetCGVisible(blackOverlay, true);
        contentRectTransform.localScale = Vector3.one;
    }

    public void EditorClose() {
        SetCGVisible(blackOverlay, false);
        contentRectTransform.localScale = Vector3.zero;
    }

    #endif

    public void Open() {
        selectedCharacterData = null;
        equippedSpAtkData = null;
        RefreshWindow();
        blackOverlay.blocksRaycasts = true;
        blackOverlay.interactable = true;
        blackOverlay.DOFade(1, openSpeed).OnComplete(OnBlackOverlayFadeInComplete);
    }

    private void OnBlackOverlayFadeInComplete() {
        contentRectTransform.DOScale(1, openSpeed).SetEase(Ease.OutBack);
    }

    public void Close() {
        contentRectTransform.DOScale(0, openSpeed).SetEase(Ease.InBack).OnComplete(OnContentRectScaleDownComplete);
    }

    private void OnContentRectScaleDownComplete() {
        blackOverlay.DOFade(0, openSpeed);
        blackOverlay.blocksRaycasts = false;
        blackOverlay.interactable = false;
    }

    #endregion PUBLIC_METHODS

    // Update is called once per frame
	void Update () {
	
	}
}
