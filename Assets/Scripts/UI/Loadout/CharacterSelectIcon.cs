﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CharacterSelectIcon : DataSelectIcon<CharacterData> {

    public override void Init(CharacterData theData, UnityAction<CharacterData> callback) {
        base.Init(theData, callback);

        uiIcon.sprite = data.uiImage;
        this.gameObject.name = data.SkinName;
    }

}
