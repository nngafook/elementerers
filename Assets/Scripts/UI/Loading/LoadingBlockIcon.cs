﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class LoadingBlockEvent : UnityEvent<LoadingBlockIcon> { }

public class LoadingBlockIcon : MonoBehaviour {

    [HideInInspector]
    public LoadingBlockEvent OnFinishedAnimationEvent = new LoadingBlockEvent();
    [HideInInspector]
    public bool forceStopped = false;

    public bool playOnAwake = false;

    [Space(10)]
    public float playNextDelay = 0;
    
    [Space(10)]
    public LoadingBlockIcon nextIcon;
    public Animator animator;

    void Awake() {
        if (playOnAwake) {
            Play();
        }
    }

    public void Play() {
        if (!forceStopped) {
            animator.SetTrigger("doit");
        }
    }

    public void PlayNextIcon() {
        nextIcon.Invoke("Play", playNextDelay);
    }
    
}
