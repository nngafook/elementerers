﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class LoadingAnimationController : MonoBehaviour {

    public List<LoadingBlockIcon> loadingBlocks;

    void Start() {
    }

    public void Enable() {
        for (int i = 0; i < loadingBlocks.Count; i++) {
            loadingBlocks[i].forceStopped = false;
        }
    }

    public void Disable() {
        for (int i = 0; i < loadingBlocks.Count; i++) {
            loadingBlocks[i].forceStopped = true;
        }
    }

}
