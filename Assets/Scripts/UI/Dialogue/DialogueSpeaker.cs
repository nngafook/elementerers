﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class DialogueSpeaker : MonoBehaviour {

    private UnityAction SlideInCallback;

    private Color baseColor;
    private bool isSelected = true;
    private float slideSpeed = 0.35f;
    private float scaleSpeed = 0.35f;
    private Image image;


    public bool isLeftSpeaker = false;
    public string speakerName;
    
    [Space(10)]
    public RectTransform parentContainer;
    public DialogueEmotionSetDataObject emotionSet;

    [HideInInspector]
    public EmotionName currentEmotion = EmotionName.NEUTRAL;

    public float ImageSize { get { return 256; } }
    //public RectTransform rt { get { return this.GetComponent<RectTransform>(); } }

    void Awake() {
        image = this.GetComponent<Image>();
        baseColor = image.color;
    }

    // Use this for initialization
    void Start() {

    }

    public void Animate(DialogueSpeakerAnimation animation) {
        switch (animation) {
            case DialogueSpeakerAnimation.SlideIn:
                SlideIn();
                break;
            case DialogueSpeakerAnimation.SlideOut:
                SlideOut();
                break;
            case DialogueSpeakerAnimation.Jump:
                Jump();
                break;
            case DialogueSpeakerAnimation.Punch:
                Punch();
                break;
            default:
                break;
        }
    }

    public void SetSelected(bool value) {
        if (value != isSelected) {
            isSelected = value;
            if (isSelected) {
                image.color = baseColor;
                parentContainer.DOScale(1.05f, scaleSpeed);
            }
            else {
                image.color = CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
                parentContainer.DOScale(0.95f, scaleSpeed);
            }
        }
    }

    public void SetEmotion(EmotionName emotion) {
        if (emotionSet != null && emotionSet.speakerName == speakerName && currentEmotion != emotion) {
            currentEmotion = emotion;
            image.sprite = emotionSet.GetSprite(emotion);
        }
    }

    public void Jump() {
        Vector2 endPos = parentContainer.anchoredPosition.AddY(20);
        parentContainer.DOAnchorPos(endPos, slideSpeed).SetEase(Ease.InCirc).SetLoops(2, LoopType.Yoyo);
    }

    public void Punch() {
        Vector3 punch = new Vector3(0.1f, 0.1f, 0.1f);
        parentContainer.DOPunchScale(punch, slideSpeed);//.SetEase(Ease.Linear).SetLoops(1, LoopType.Yoyo);
    }

    /// <summary>
    /// Sets position off screen
    /// </summary>
    public void Hide() {
        float distance = (isLeftSpeaker) ? -ImageSize : ImageSize;
        parentContainer.anchoredPosition = parentContainer.anchoredPosition.AddX(distance);
    }

    /// <summary>
    /// Animates position off screen
    /// </summary>
    private void SlideOut() {
        float distance = (isLeftSpeaker) ? -ImageSize : ImageSize;
        Vector2 endPos = parentContainer.anchoredPosition.AddX(distance);
        parentContainer.DOAnchorPos(endPos, slideSpeed).SetEase(Ease.InBack);
    }

    public void SlideIn(UnityAction onCompleteCallback = null) {
        if (onCompleteCallback != null) {
            SlideInCallback = onCompleteCallback;
        }
        float distance = (isLeftSpeaker) ? ImageSize : -ImageSize;
        Vector2 endPos = parentContainer.anchoredPosition.AddX(distance);
        parentContainer.DOAnchorPos(endPos, slideSpeed).SetEase(Ease.OutBack).OnComplete(OnSlideInComplete);
    }

    private void OnSlideInComplete() {
        if (SlideInCallback != null) {
            SlideInCallback();
            SlideInCallback = null;
        }
    }

}
