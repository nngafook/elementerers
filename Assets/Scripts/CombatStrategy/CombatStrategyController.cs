﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatStrategyController : MonoBehaviour {

    private bool randomTargetAcquiredForTurn = false;

    private CombatEntity entityOwner;
    private CombatEntity potentialTarget;

    private CombatOrder successfulOrder;
    private CombatOrder fallbackOrder;

    public List<CombatOrder> combatOrders = new List<CombatOrder>();

    public bool RandomTargetAcquired { get { return randomTargetAcquiredForTurn; } set { randomTargetAcquiredForTurn = value; } }
    public CombatEntity Target { get { return potentialTarget; } }

    // Use this for initialization
    void Start() {

    }

    private IEnumerator DeepThoughts() {
        potentialTarget = null;
        successfulOrder = null;

        for (int i = 0; i < combatOrders.Count; i++) {
            if (Assess(combatOrders[i])) {
                successfulOrder = combatOrders[i];
                //Debug.Log("Target is UID: " + potentialTarget.UID.Value);
                break;
            }
        }

        if (potentialTarget == null) {
            Debug.Log("Used fallback".Colored(Color.magenta).Sized(12));
            successfulOrder = fallbackOrder;
            // If the entire priority list failed, just default to an attack random
            Assess(fallbackOrder);
        }

        entityOwner.OrderToExecute = successfulOrder;

        if (potentialTarget == null) {
            Debug.Log("FALLBACK COMBAT ORDER FAILED! YOU HAVE FAILED AS A PROGRAMMER/DESIGNER".Colored(Color.red));
        }

        yield return null;
    }

    private bool Assess(CombatOrder order) {
        bool rVal = false;
        //Debug.Log(CombatStrategyUtils.GetReadableCombatOrder(order).Sized(14).Colored(Color.blue));

        // If the weighted chance of the order fails, skip all of the assessment
        if (order.weightedChance.Success) {

            bool actionSuccess = AssessCombatAction(order.actionOrder);
            bool targetSuccess = AssessCombatTarget(order.targetOrder, order.actionOrder);
            bool percentageSuccess = AssessTargetPercentage(order);

            rVal = (actionSuccess && targetSuccess && percentageSuccess);

            if (!rVal) {
                potentialTarget = null;
            }
        }

        //if (rVal) {
        //    Debug.Log((actionSuccess + " " + targetSuccess + " " + percentageSuccess).Colored(Color.green));
        //}
        //else {
        //    Debug.Log((actionSuccess + " " + targetSuccess + " " + percentageSuccess).Colored(Color.red));
        //}

        return rVal;
    }

    private bool AssessCombatAction(CombatStrategyActionOrder actionOrder) {
        bool rVal = false;

        switch (actionOrder) {
            case CombatStrategyActionOrder.Heal:
                rVal = (entityOwner.SpecialAtkData.specialAttackType == SpecialAttackType.HEAL);
                break;
            case CombatStrategyActionOrder.SpecialAttack:
                rVal = !entityOwner.SpecialAttackOnCooldown;
                break;
            case CombatStrategyActionOrder.Attack:
            case CombatStrategyActionOrder.Defend:
                rVal = true;
                break;
            case CombatStrategyActionOrder.None:
            default:
                rVal = false;
                break;
        }

        return rVal;
    }

    private bool AssessCombatTarget(CombatStrategyTargetOrder targetOrder, CombatStrategyActionOrder actionOrder) {
        bool rVal = false;
        List<CombatEntity> entityListToUse = new List<CombatEntity>();
        if (entityOwner.Type == EntityType.PLAYER) {
            if (actionOrder == CombatStrategyActionOrder.Heal) {
                entityListToUse = EntityManager.Instance.ActivePlayerEntities;
            }
            else {
                entityListToUse = EntityManager.Instance.ActiveEnemyEntities;
            }
        }
        else {
            if (actionOrder == CombatStrategyActionOrder.Heal) {
                entityListToUse = EntityManager.Instance.ActiveEnemyEntities;
            }
            else {
                entityListToUse = EntityManager.Instance.ActivePlayerEntities;
            }
        }
        List<CombatEntity> shortList = new List<CombatEntity>();
        EntityType targetType = (entityOwner.Type == EntityType.PLAYER && actionOrder != CombatStrategyActionOrder.Heal) ? EntityType.ENEMY : EntityType.PLAYER;
        CombatEntity e = null;

        switch (targetOrder) {
            case CombatStrategyTargetOrder.TargetRandom:
                e = EntityManager.Instance.GetRandomTarget(targetType);
                rVal = (e != null);
                break;
            case CombatStrategyTargetOrder.TargetHighestHealth:
                float highestHealth = 0;
                for (int i = 0; i < entityListToUse.Count; i++) {
                    if (entityListToUse[i].CurrentHealth > highestHealth) {
                        highestHealth = entityListToUse[i].CurrentHealth;
                        shortList.Clear();
                        shortList.Add(entityListToUse[i]);
                    }
                    else if (entityListToUse[i].CurrentHealth == highestHealth) {
                        shortList.Add(entityListToUse[i]);
                    }
                }

                // The shortlist at this point should only have entities with the same health.
                e = shortList.RandomElement<CombatEntity>();

                rVal = (e != null);

                break;
            case CombatStrategyTargetOrder.TargetLowestHealth:

                float lowestHealth = 10000000;
                for (int i = 0; i < entityListToUse.Count; i++) {
                    if (entityListToUse[i].CurrentHealth < lowestHealth) {
                        lowestHealth = entityListToUse[i].CurrentHealth;
                        shortList.Clear();
                        shortList.Add(entityListToUse[i]);
                    }
                    else if (entityListToUse[i].CurrentHealth == lowestHealth) {
                        shortList.Add(entityListToUse[i]);
                    }
                }

                // The shortlist at this point should only have entities with the same health.
                e = shortList.RandomElement<CombatEntity>();

                rVal = (e != null);

                break;
            case CombatStrategyTargetOrder.TargetHealthAbove:
            case CombatStrategyTargetOrder.TargetHealthBelow:
                rVal = true;
                break;
            default:
                rVal = false;
                break;
        }

        potentialTarget = e;

        return rVal;
    }

    /// <summary>
    /// If potentialTarget is null by the time that this is called, then that means the target order was either targetHealthAbove or targetHealthBelow
    /// </summary>
    /// <param name="targetPercentage">Percentage of health to check for</param>
    /// <param name="targetOrder">Target Order of Command Order</param>
    /// <returns>Returns true if target is found using criteria of command order</returns>
    private bool AssessTargetPercentage(CombatOrder order) {
        // Defaults to true, cause if a targetOrder that is nto hpAbove or hpBelow is in the combatOrder, percentage doesn't matter.
        // So return true so Assess() passes
        bool rVal = true;
        if (order.targetOrder == CombatStrategyTargetOrder.TargetHealthAbove || order.targetOrder == CombatStrategyTargetOrder.TargetHealthBelow) {
            bool targetAbove = (order.targetOrder == CombatStrategyTargetOrder.TargetHealthAbove) ? true : false;     // If this target order is ever NOT HP above or below, this is not going to work as is.
            List<CombatEntity> entityListToUse = new List<CombatEntity>();
            if (entityOwner.Type == EntityType.PLAYER) {
                if (order.actionOrder == CombatStrategyActionOrder.Heal) {
                    entityListToUse = EntityManager.Instance.ActivePlayerEntities;
                }
                else {
                    entityListToUse = EntityManager.Instance.ActiveEnemyEntities;
                }
            }
            else {
                if (order.actionOrder == CombatStrategyActionOrder.Heal) {
                    entityListToUse = EntityManager.Instance.ActiveEnemyEntities;
                }
                else {
                    entityListToUse = EntityManager.Instance.ActivePlayerEntities;
                }
            }
            List<CombatEntity> shortList = new List<CombatEntity>();        // Short list of entities that meet the criteria.
            //EntityType targetType = (entityOwner.EntityType == EntityType.PLAYER && order.actionOrder != CombatStrategyActionOrder.Heal) ? EntityType.ENEMY : EntityType.PLAYER;
            float healthPercentage = 0;
            float valueToCheckAgainst = 1;
            CombatEntity e = null;

            switch (order.targetPercentage) {
                case CombatStrategyPercentage.TwentyFive:
                    valueToCheckAgainst = 0.25f;
                    break;
                case CombatStrategyPercentage.Fifty:
                    valueToCheckAgainst = 0.5f;
                    break;
                case CombatStrategyPercentage.SeventyFive:
                    valueToCheckAgainst = 0.75f;
                    break;
                default:
                    break;
            }

            for (int i = 0; i < entityListToUse.Count; i++) {
                e = entityListToUse[i];
                healthPercentage = e.CurrentHealth / e.MaxHealth;
                if (targetAbove && healthPercentage >= valueToCheckAgainst) {
                    shortList.Add(e);
                }
                else if (!targetAbove && healthPercentage <= valueToCheckAgainst) {
                    shortList.Add(e);
                }
            }

            e = null;

            if (shortList.Count > 0) {
                List<CombatEntity> sameHealthShortList = new List<CombatEntity>();
                //! Go through the short list and find the one with the MOST health. This is semi arbitrary... could make it random, or otherwise.
                float highestHP = 0;
                for (int i = 0; i < shortList.Count; i++) {
                    if (shortList[i].CurrentHealth > highestHP) {
                        highestHP = shortList[i].CurrentHealth;
                        sameHealthShortList.Add(shortList[i]);
                    }
                    else if (shortList[i].CurrentHealth == highestHP) {
                        sameHealthShortList.Add(shortList[i]);
                    }
                }
                // At this point this list should have entities at the same health, or ONE entity who was the highest health in the short list
                e = sameHealthShortList.RandomElement<CombatEntity>();
            }

            potentialTarget = e;

            // At this point, e should be the entity with the highest HP that fit the target percentage criteria
            rVal = (e != null);
        }


        return rVal;
    }

    public void SetOwner(CombatEntity owner) {
        entityOwner = owner;
    }

    public void SetPriorities(ref List<CombatOrder> orders, ref CombatOrder fallback) {
        combatOrders.Clear();
        for (int i = 0; i < orders.Count; i++) {
            combatOrders.Add(new CombatOrder(orders[i]));
        }

        fallbackOrder = fallback;

    }

    /// <summary>
    /// Starts the controller following combat orders
    /// </summary>
    public void StartThinking() {

        StopCoroutine("DeepThoughts");
        StartCoroutine("DeepThoughts");

    }

    // Update is called once per frame
    void Update() {

    }
}
