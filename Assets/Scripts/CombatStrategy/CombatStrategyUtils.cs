﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class CombatStrategyUtils : MonoBehaviour {

    public static string GetReadableName(CombatStrategyTargetOrder order) {
        string rVal = "";
        switch (order) {
            case CombatStrategyTargetOrder.TargetRandom:
                rVal = "Random Target";
                break;
            case CombatStrategyTargetOrder.TargetHighestHealth:
                rVal = "Target Highest HP";
                break;
            case CombatStrategyTargetOrder.TargetLowestHealth:
                rVal = "Target Lowest HP";
                break;
            case CombatStrategyTargetOrder.TargetHealthAbove:
                rVal = "Target HP Above";
                break;
            case CombatStrategyTargetOrder.TargetHealthBelow:
                rVal = "Target HP Below";
                break;
            default:
                break;
        }
        return rVal;
    }

    public static string GetReadableName(CombatStrategyActionOrder order) {
        string rVal = "";
        switch (order) {
            case CombatStrategyActionOrder.Heal:
                rVal = "Heal";
                break;
            case CombatStrategyActionOrder.Attack:
                rVal = "Attack";
                break;
            case CombatStrategyActionOrder.SpecialAttack:
                rVal = "Special Attack";
                break;
            case CombatStrategyActionOrder.Defend:
                rVal = "Defend";
                break;
            case CombatStrategyActionOrder.None:
                rVal = "Empty";
                break;
            default:
                break;
        }
        return rVal;
    }

    public static string GetReadableName(CombatStrategyPercentage order) {
        string rVal = "";
        switch (order) {
            case CombatStrategyPercentage.TwentyFive:
                rVal = "25%";
                break;
            case CombatStrategyPercentage.Fifty:
                rVal = "50%";
                break;
            case CombatStrategyPercentage.SeventyFive:
                rVal = "75%";
                break;
            default:
                break;
        }
        return rVal;
    }

    public static string GetReadableCombatOrder(CombatOrder order) {
        StringBuilder sb = new StringBuilder();
        string target = GetReadableName(order.targetOrder);
        string action = GetReadableName(order.actionOrder);
        string percentage = GetReadableName(order.targetPercentage);

        if (order.actionOrder == CombatStrategyActionOrder.None || order.actionOrder == CombatStrategyActionOrder.Defend) {
            sb.Append(action);
        }
        else if (order.targetOrder != CombatStrategyTargetOrder.TargetHealthAbove && order.targetOrder != CombatStrategyTargetOrder.TargetHealthBelow) {
            sb.Append(action + " " + target);
        }
        else {
            sb.Append(action + " " + target + " " + percentage);
        }

        return sb.ToString();
    }

}

[Serializable]
public class CombatOrder {

    // Is computed with every ai thought, but only enemies have a weight lower than 100. 
    // Helps add some randomness to their actions
    public PercentageFloat weightedChance = new PercentageFloat(100);

    public CombatStrategyActionOrder actionOrder;
    public CombatStrategyTargetOrder targetOrder;

    public CombatStrategyPercentage targetPercentage;

    public CombatOrder() {
        targetOrder = CombatStrategyTargetOrder.TargetRandom;
        actionOrder = CombatStrategyActionOrder.Attack;
        targetPercentage = CombatStrategyPercentage.Fifty;
    }

    public CombatOrder(CombatOrder o) {
        Copy(o);
    }

    public void Copy(CombatOrder o) {
        actionOrder = o.actionOrder;
        targetOrder = o.targetOrder;
        targetPercentage = o.targetPercentage;
        weightedChance = new PercentageFloat(o.weightedChance.baseValue);
    }

}

public enum CombatStrategyTargetOrder {
    TargetRandom,
    TargetHighestHealth,
    TargetLowestHealth,
    TargetHealthAbove,
    TargetHealthBelow,

    TargetEnemyHealer
}

public enum CombatStrategyActionOrder {
    Heal,
    Attack,
    SpecialAttack,
    Defend,

    None = 100
}

public enum CombatStrategyPercentage {
    TwentyFive,
    Fifty,
    SeventyFive
}