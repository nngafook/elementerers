using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

//public class SceneLoadEvent : UnityEvent<string> { };

public class LoadToScene : MonoBehaviour {

    //[HideInInspector]
    //public static SceneLoadEvent sceneLoadEvent = new SceneLoadEvent();

    private static string sceneToLoad = "";
    private WaitForSeconds shortWait = new WaitForSeconds(0.1f);
    private float minLoadTime = 1f;
    private float fadeDuration = 0.5f;
    private bool isFading = false;
    private AsyncOperation async;

    public CanvasGroup mainUICanvasGroup;

	#region PRIVATE_METHODS

    void Awake() {
        
    }

    void Start() {
        if (sceneToLoad != "") {
            mainUICanvasGroup.alpha = 1;
            StartCoroutine(LoadSceneASync());
        }
    }

    private IEnumerator LoadSceneASync() {
        float startTime = Time.time; 
        yield return shortWait;
        async = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
        async.allowSceneActivation = false;

        while (async.progress < 0.9f) {
            yield return null;
        }

        while ((Time.time - startTime) < minLoadTime) {
            yield return null;
        }

        while (!LoadManager.HasNoRoutines) {
            yield return null;
        }

        yield return StartCoroutine(FadeUIOut());

        LoadManager.ClearMessage();

        yield return async;
    }

    private IEnumerator FadeUIOut() {
        isFading = true;
        mainUICanvasGroup.DOFade(0, fadeDuration).OnComplete(OnSceneFadeOutComplete);

        while (isFading) {
            yield return null;
        }

        yield return StartCoroutine(WaitForASyncComplete());

    }

    private void OnSceneFadeOutComplete() {
        isFading = false;
        //StartCoroutine(WaitForASyncComplete());
    }

    private IEnumerator WaitForASyncComplete() {
        async.allowSceneActivation = true;
        while (async.progress < 1) {
            yield return null;
        }

        //if (sceneLoadEvent != null) {
        //    sceneLoadEvent.Invoke(sceneToLoad);
        //}

        LoadManager.ClearMessage();

        SceneManager.UnloadScene(SceneName.LOADING);
        Mothership.Instance.SceneWasLoaded(sceneToLoad);
    }

    private static void OnMothershipFadeToBlackComplete() {
        SceneManager.LoadScene(SceneName.LOADING);
        Mothership.Instance.SceneWasLoaded(SceneName.LOADING, sceneToLoad);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public static void Load(string scene) {
        sceneToLoad = scene;
        Mothership.Instance.AwaitLoadPermission(OnMothershipFadeToBlackComplete);
    }

	#endregion PUBLIC_METHODS

	void Update () {
        
	}
}

public class SceneName {

    public const string SPLASH_SCREEN = "SplashScreen";
    public const string MAIN_MENU = "MainMenu";
    public const string LOADOUT = "Loadout";
    public const string COMBAT = "Combat";
    public const string LOADING = "Loading";
    public const string DIALOGUE = "Dialogue";

}