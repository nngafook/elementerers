﻿using UnityEngine;
using System.Collections;

public class Playground : MonoBehaviour {

    public CharacterDataObject[] characterDataObjects;
    public SpecialAttackDataObject[] specialAttackDataObjects;

    // Use this for initialization
    void Start() {

    }

    private void LoadOne() {
        //StartCoroutine("ResourcesLoad");
        LoadManager.RegisterLoadRoutine(ResourcesLoad(), "Loading Data Objects");
    }

    private void LoadTwo() {

    }

    private IEnumerator ResourcesLoad() {
        Debug.Log("Start time: " + Time.time);
        characterDataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData");
        specialAttackDataObjects = Resources.LoadAll<SpecialAttackDataObject>("Data/SpecialAttackData");
        Debug.Log("End time: " + Time.time);
        yield return null;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyUp(KeyCode.Alpha1)) {
            LoadOne();
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2)) {
            LoadTwo();
        }
    }
}
