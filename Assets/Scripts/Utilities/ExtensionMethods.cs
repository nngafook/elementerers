﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethods {

    #region VECTORS_AND_TRANSFORMS
    public static void AddX(this Transform t, float value) {
        Vector3 temp = t.position;
        temp.x += value;
        t.position = temp;
    }

    public static void AddY(this Transform t, float value) {
        Vector3 temp = t.position;
        temp.y += value;
        t.position = temp;
    }

    public static void AddZ(this Transform t, float value) {
        Vector3 temp = t.position;
        temp.z += value;
        t.position = temp;
    }

    public static void SetX(this Transform t, float value) {
        Vector3 temp = t.position;
        temp.x = value;
        t.position = temp;
    }

    public static void SetY(this Transform t, float value) {
        Vector3 temp = t.position;
        temp.y = value;
        t.position = temp;
    }

    public static void SetZ(this Transform t, float value) {
        Vector3 temp = t.position;
        temp.z = value;
        t.position = temp;
    }

    public static void SetLocalX(this Transform t, float value) {
        Vector3 temp = t.localPosition;
        temp.x = value;
        t.localPosition = temp;
    }

    public static void SetLocalY(this Transform t, float value) {
        Vector3 temp = t.localPosition;
        temp.y = value;
        t.localPosition = temp;
    }

    public static void SetLocalZ(this Transform t, float value) {
        Vector3 temp = t.localPosition;
        temp.z = value;
        t.localPosition = temp;
    }

    public static Vector3 SetX(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.x = value;
        return temp;
    }

    public static Vector3 SetY(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.y = value;
        return temp;
    }

    public static Vector3 SetZ(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.z = value;
        return temp;
    }

    public static Vector2 SetX(this Vector2 v, float value) {
        Vector2 temp = v;
        temp.x = value;
        return temp;
    }

    public static Vector2 SetY(this Vector2 v, float value) {
        Vector2 temp = v;
        temp.y = value;
        return temp;
    }

    public static Vector2 AddX(this Vector2 v, float value) {
        Vector2 temp = v;
        temp.x += value;
        v = temp;
        return v;
    }

    public static Vector2 AddY(this Vector2 v, float value) {
        Vector2 temp = v;
        temp.y += value;
        v = temp;
        return v;
    }

    public static Vector2 SubtractX(this Vector2 v, float value) {
        Vector2 temp = v;
        temp.y -= value;
        v = temp;
        return v;
    }

    public static Vector2 SubtractY(this Vector2 v, float value) {
        Vector2 temp = v;
        temp.y -= value;
        v = temp;
        return v;
    }

    public static Vector3 AddX(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.x += value;
        v = temp;
        return v;
    }

    public static Vector3 AddY(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.y += value;
        v = temp;
        return v;
    }

    public static Vector3 AddZ(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.z += value;
        v = temp;
        return v;
    }

    public static Vector3 Add(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.x += value;
        temp.y += value;
        temp.z += value;
        v = temp;
        return v;
    }

    public static Vector2 AddXY(this Vector2 v, float x, float y) {
        Vector2 temp = v;
        temp.x += x;
        temp.y += y;
        v = temp;
        return v;
    }

    public static Vector3 AddXYZ(this Vector3 v, float x, float y, float z) {
        Vector3 temp = v;
        temp.x += x;
        temp.y += y;
        temp.z += z;
        v = temp;
        return v;
    }

    public static Vector3 Subtract(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.x -= value;
        temp.y -= value;
        temp.z -= value;
        v = temp;
        return v;
    }

    public static Vector3 SubtractY(this Vector3 v, float value) {
        Vector3 temp = v;
        temp.y -= value;
        v = temp;
        return v;
    }

    public static void SetPositionIgnoreY(this Transform t, Vector3 pos) {
        Vector3 temp = pos;
        temp.y = t.position.y;
        t.position = temp;
    }

    public static Vector3 Divide(this Vector3 v, Vector3 value) {
        Vector3 temp = new Vector3(v.x / value.x, v.y / value.y, v.z / value.z);
        return temp;
    }

    public static List<T> Swap<T>(this List<T> list, int indexA, int indexB) {
        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
        return list;
    }
    #endregion VECTORS_AND_TRANSFORMS

    #region STRINGS
    public static string Colored(this string s, Color c) {
        string cHex = ColorUtility.ToHtmlStringRGBA(c);
        return "<color=#" + cHex + ">" + s + "</color>";
    }

    /// <summary>
    /// Sets the color of the text according to the traditional HTML format parameter value.
    /// </summary>
    /// <param craftableName="message">Message</param>
    /// <param craftableName="color">Colored</param>
    public static string Colored(this string message, string colorCode) {
        return "<color=" + colorCode + ">" + message + "</color>";
        //return string.Format("<color={0}>{1}</color>", colorCode, message);
    }

    /// <summary>
    /// Sets the size of the text according to the parameter value, given in pixels.
    /// </summary>
    /// <param craftableName="message">Message.</param>
    /// <param craftableName="size">Size.</param>
    public static string Sized(this string message, int size) {
        return string.Format("<size={0}>{1}</size>", size, message);
    }

    /// <summary>
    /// Renders the text in boldface.
    /// </summary>
    /// <param craftableName="message">Message.</param>
    public static string Bold(this string message) {
        return string.Format("<b>{0}</b>", message);
    }

    /// <summary>
    /// Renders the text in italics.
    /// </summary>
    /// <param craftableName="message">Message.</param>
    public static string Italics(this string message) {
        return string.Format("<i>{0}</i>", message);
    }

    public static string ToCamelCase(this string value) {
        string rVal = "";

        rVal = value.Replace(" ", "");
        rVal = char.ToLower(rVal[0]).ToString() + rVal.Substring(1);

        return rVal;
    }

    #endregion STRINGS

    #region LISTS
    public static void DebugLog<T>(this IEnumerable<T> list, params string[] properties) {
        foreach (T value in list) {
            if (properties.Length > 0) {
                for (int i = 0; i < properties.Length; i++) {
                    object o = GetPropertyValue(value, properties[i]);
                    Debug.Log(o);
                }
            }
            else {
                Debug.Log(value);
            }
        }
    }

    public static bool IsNullOrEmpty<T>(this List<T> list) {
        return (list == null || list.Count == 0);
    }

    public static bool IsIndexValid<T>(this List<T> list, int index) {
        return (list.Count > index && list[index] != null);
    }

    public static bool IsIndexValid<T>(this T[] array, int index) {
        return (array.Length > index && array[index] != null);
    }

    public static T RandomElement<T>(this IEnumerable<T> list) {
        int randomIndex = Random.Range(0, list.Count());
        return list.ElementAt(randomIndex);
    }

    public static T RandomElement<T>(this IEnumerable<T> list, bool condition, params string[] boolProperties) where T : class {
        int randomIndex = Random.Range(0, list.Count());
        T element = list.ElementAt(randomIndex);
        object o = null;
        if (boolProperties.Length > 0) {
            for (int i = 0; i < boolProperties.Length; i++) {
                o = GetPropertyValue(element, boolProperties[i]);
            }
        }
        //if (!(bool)o) {
        //    Debug.Log("The codition passed to RandomElement() has failed");
        //}
        return ((bool)o == condition) ? list.ElementAt(randomIndex) : null;
    }

    private static object GetPropertyValue(object obj, string propertyName) {
        string[] propertyNames = propertyName.Split('.');
        for (var i = 0; i < propertyNames.Length; i++) {
            if (obj != null) {
                System.Reflection.PropertyInfo propInfo = obj.GetType().GetProperty(propertyNames[i]);
                if (propInfo != null) {
                    obj = propInfo.GetValue(obj, null);
                }
                else {
                    obj = null;
                }
            }
        }
        return obj;
    }
    #endregion LISTS

    #region RECT_TRANSFORMS

    public static bool Overlaps(this RectTransform rectOne, RectTransform other) {
        Vector2 sizeOne = rectOne.sizeDelta;
        Vector2 sizeTwo = other.sizeDelta;

        float leftOne = rectOne.position.x - (sizeOne.x / 2);
        float leftTwo = other.position.x -(sizeTwo.x / 2);

        float rightOne = rectOne.position.x + (sizeOne.x / 2);
        float rightTwo = other.position.x + (sizeTwo.x / 2);

        float topOne = rectOne.position.y + (sizeTwo.y / 2);
        float topTwo = other.position.y + (sizeTwo.y / 2);

        float bottomOne = rectOne.position.y - (sizeOne.y / 2);
        float bottomTwo = other.position.y - (sizeTwo.y / 2);

        bool xIn = ((leftOne <= rightTwo) && (leftOne >= leftTwo))
            || ((rightOne >= leftOne) && (rightOne <= rightTwo));
        bool yIn = (((topOne <= topTwo) && (topOne >= bottomTwo))
            || ((bottomOne >= bottomTwo) && (bottomOne <= topTwo)));

        return (xIn && yIn);
    }

    #endregion RECT_TRANSFORMS

    public static RectTransform RT(this Component comp) {
        return comp.GetComponent<RectTransform>();
    }

    public static CanvasGroup CG(this Component comp) {
        return comp.GetComponent<CanvasGroup>();
    }

    public static bool AsBool(this int value) {
        return (value > 0);
    }

    public static int AsInt(this bool value) {
        return (value) ? 1 : 0;
    }

    public static void SetAlphaAndBools(this CanvasGroup cg, bool value) {
        cg.alpha = value.AsInt();
        cg.interactable = value;
        cg.blocksRaycasts = value;
    }

    /// <summary>
    /// This is using LINQ. Be warned
    /// </summary>
    /// <param craftableName="group"></param>
    /// <returns></returns>
    public static Toggle GetActive(this ToggleGroup group) {
        return group.ActiveToggles().FirstOrDefault();
    }

}