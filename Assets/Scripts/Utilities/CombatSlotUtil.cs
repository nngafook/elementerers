﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatSlotUtil : MonoBehaviour {

	private CombatManager combatManager;

	public List<Transform> playerSlots;
	public List<Transform> enemySlots;

    public float xAdjustValue = 0;
    public float yAdjustValue = 0;

	public void SavePositions() {
        combatManager = this.GetComponent<CombatManager>();

		combatManager.playerCombatPositions = new List<Vector3>();
		combatManager.enemyCombatPositions = new List<Vector3>();

		for(int i = 0; i < playerSlots.Count; i++ ){
			combatManager.playerCombatPositions.Add(playerSlots[i].position);
		}

		for(int i = 0; i < enemySlots.Count; i++ ){
			combatManager.enemyCombatPositions.Add(enemySlots[i].position);
		}
		DestroyImmediate(this);
	}

    public void AdjustXValues() {
        if (xAdjustValue != 0) {
            combatManager = this.GetComponent<CombatManager>();
            Vector3 pos;
            for (int i = 0; i < combatManager.playerCombatPositions.Count; i++) {
                pos = combatManager.playerCombatPositions[i];
                pos.x += xAdjustValue;
                combatManager.playerCombatPositions[i] = pos;
            }

            for (int i = 0; i < combatManager.enemyCombatPositions.Count; i++) {
                pos = combatManager.enemyCombatPositions[i];
                pos.x += xAdjustValue;
                combatManager.enemyCombatPositions[i] = pos;
            }
            DestroyImmediate(this);
        }
    }

    public void AdjustYValues() {
        combatManager = this.GetComponent<CombatManager>();
        if (yAdjustValue != 0) {
            Vector3 pos;
            for (int i = 0; i < combatManager.playerCombatPositions.Count; i++) {
                pos = combatManager.playerCombatPositions[i];
                pos.y += yAdjustValue;
                combatManager.playerCombatPositions[i] = pos;
            }

            for (int i = 0; i < combatManager.enemyCombatPositions.Count; i++) {
                pos = combatManager.enemyCombatPositions[i];
                pos.y += yAdjustValue;
                combatManager.enemyCombatPositions[i] = pos;
            }
            DestroyImmediate(this);
        }
    }

    public void CreateSlotTransforms(Transform slotsContainer) {
        combatManager = this.GetComponent<CombatManager>();

        Transform newTransform = null;
        for (int i = 0; i < combatManager.playerCombatPositions.Count; i++) {
            newTransform = new GameObject().transform;
            newTransform.SetParent(slotsContainer, true);
            newTransform.position = combatManager.playerCombatPositions[i];
            newTransform.name = "Player Slot " + i;
        }

        for (int i = 0; i < combatManager.enemyCombatPositions.Count; i++) {
            newTransform = new GameObject().transform;
            newTransform.SetParent(slotsContainer, true);
            newTransform.position = combatManager.enemyCombatPositions[i];
            newTransform.name = "Enemy Slot " + i;
        }
        DestroyImmediate(this);
    }

}
