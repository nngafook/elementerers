﻿using System;
using System.Collections;

public class EventChanges {

    //public float enemyTotalAtkDMGChanged = 0;
    //public float playerTotalAtkDMGChanged = 0;
    //public float enemyTotaSPAtkDMGChanged = 0;
    //public float playerTotalSPAtkDMGChanged = 0;
    //public float enemyTotalSPDChanged = 0;
    //public float playerTotalSPDChanged = 0;

    public float playerAtkDMGChanged = 0;
    public float enemyAtkDMGChanged = 0;
    public float playerSPAtkDMGChanged = 0;
    public float enemySPAtkDMGChanged = 0;
    public float playerSPDChanged = 0;
    public float enemySPDChanged = 0;
    public float playerRegenChanged = 0;
    public float enemyRegenChanged = 0;

    //#region TOTALS

    //public void AddTotalEnemyAtkDMG(float value) {
    //    enemyTotalAtkDMGChanged += value;
    //}

    //public void AddTotalPlayerAtkDMG(float value) {
    //    playerTotalAtkDMGChanged += value;
    //}

    //public void AddTotalEnemySPAtkDMG(float value) {
    //    enemyTotaSPAtkDMGChanged += value;
    //}

    //public void AddTotalPlayerSPAtkDMG(float value) {
    //    playerTotalSPAtkDMGChanged += value;
    //}

    //public void AddTotalEnemySPD(float value) {
    //    enemyTotalSPDChanged += value;
    //}

    //public void AddTotalPlayerSPD(float value) {
    //    playerTotalSPDChanged += value;
    //}

    //#endregion TOTALS

    #region GETTERS
    public float AtkDMGChanged(EntityType type) { 
        float rVal = enemyAtkDMGChanged; 
        if (type == EntityType.PLAYER) { 
            rVal = playerAtkDMGChanged; 
        } 
        return rVal; 
    }

    public float SPAtkDMGChanged(EntityType type) { 
        float rVal = enemySPAtkDMGChanged; 
        if (type == EntityType.PLAYER) { 
            rVal = playerSPAtkDMGChanged; 
        } 
        return rVal; 
    }

    public float SPDChanged(EntityType type) { 
        float rVal = enemySPDChanged; 
        if (type == EntityType.PLAYER) { 
            rVal = playerSPDChanged; 
        } 
        return rVal;
    }

    public float RegenChanged(EntityType type) {
        float rVal = enemyRegenChanged;
        if (type == EntityType.PLAYER) {
            rVal = playerRegenChanged;
        }
        return rVal;
    }
    #endregion GETTERS

}