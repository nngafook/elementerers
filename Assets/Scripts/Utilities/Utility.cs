﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

#pragma warning disable 0660
#pragma warning disable 0661
public static class Utility {

    public static T[] ShuffleArray<T>(T[] array, int seed) {
        System.Random prng = new System.Random(seed);

        for (int i = 0; i < array.Length - 1; i++) {
            int randomIndex = prng.Next(i, array.Length);
            T tempItem = array[randomIndex];
            array[randomIndex] = array[i];
            array[i] = tempItem;
        }

        return array;
    }

    public static bool IsWithinGrid(int coordColumn, int coordRow, int gridWidth, int gridHeight) {
        return (((coordColumn >= 0) && (coordColumn < gridWidth)) && ((coordRow >= 0) && (coordRow < gridHeight)));
    }

    public static Vector2 MouseWorldPosition() {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public static void WriteData(string data, string folderPath) {
        if (!string.IsNullOrEmpty(data)) {
            string path = Application.persistentDataPath + folderPath;
            if (!string.IsNullOrEmpty(path)) {
                using (FileStream fs = new FileStream(path, FileMode.Create)) {
                    using (StreamWriter writer = new StreamWriter(fs)) {
                        writer.Write(data);
                    }
                }
                //Debug.Log("Export Complete".Bold().Colored(Color.yellow));
            }
        }
    }

    public static string IDToName(string id, string suffixIDTag = "_id") {
        char c;
        List<char> chars = new List<char>();
        chars.Add(char.ToUpper(id[0]));
        for (int i = 1; i < id.Length - suffixIDTag.Length; i++) {
            c = id[i];
            if (char.IsUpper(c)) {
                chars.Add(' ');
                chars.Add(char.ToUpper(c));
            }
            else
                chars.Add(c);
        }

        return new string(chars.ToArray());
    }

    public static string EnumNameToReadable(string id) {
        string rVal = id.Replace('_', ' ');
        return ToCamelCase(rVal);
    }

    public static string ToCamelCase(string value) {
        value = value.ToLower();
        char[] array = value.ToCharArray();
        // Handle the first letter in the string.
        if (array.Length >= 1) {
            if (char.IsLower(array[0])) {
                array[0] = char.ToUpper(array[0]);
            }
        }
        // Scan through the letters, checking for spaces.
        // ... Uppercase the lowercase letters following spaces.
        for (int i = 1; i < array.Length; i++) {
            if (array[i - 1] == ' ') {
                if (char.IsLower(array[i])) {
                    array[i] = char.ToUpper(array[i]);
                }
            }
        }
        return new string(array);
    }

    public static string CamelCaseToReadable(string value) {
        StringBuilder sb = new StringBuilder(value.Length * 2);
        char[] charArray = value.ToCharArray();
        // Uppercase first letter
        sb.Append(char.ToUpper(charArray[0]));
        for (int i = 1; i < charArray.Length; i++) {
            if (char.IsUpper(charArray[i])) {
                if (char.IsLower(charArray[i - 1])) {
                    sb.Append(' ');
                }
            }
            sb.Append(charArray[i]);
        }

        return sb.ToString();
    }

    public static string CharacterIDToReadable(string value) {
        if (value[0] == 'e') {
            value = value.Substring(1);
        }
        return EnumNameToReadable(value);
    }

    public static Texture2D GenerateTextureFromSprite(Sprite aSprite) {
        Rect rect = aSprite.rect;
        Texture2D tex = new Texture2D((int)rect.width, (int)rect.height);
        Color[] data = aSprite.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
        tex.SetPixels(data);
        tex.Apply(true);
        return tex;
    }

    public static string GenerateUniqueStringID() {
        string rVal = "";
        int seed = Environment.TickCount;
        System.Random prng = new System.Random(seed);
        int idLength = 18;
        StringBuilder sb = new StringBuilder(idLength);

        // Generate letters
        for (int i = 0; i < idLength; i++) {
            if (i % 2 == 0) {
                // Letters
                sb.Append((char)prng.Next(65, 91));
            }
            else {      
                // Numbers
                int previousChar = sb[i - 1];
                int appendValue = 0;
                while (previousChar != 0) {
                    appendValue += previousChar % 10;
                    previousChar /= 10;
                    if (previousChar == 0 && appendValue > 9) {
                        previousChar = appendValue;
                        appendValue = 0;
                    }
                }

                appendValue += prng.Next(-appendValue, 10 - appendValue);
                sb.Append(appendValue);
            }
        }

        rVal = sb.ToString();
        return rVal;
    }

    public static string RandomFakeLoadingMessage() {
        int value = UnityEngine.Random.Range(0, 10);
        string rVal = "";
        switch (value) {
            case 0:
                rVal = "Charging Lazorz";
                break;
            case 1:
                rVal = "Calculating Lethal";
                break;
            case 2:
                rVal = "Denying Creeps";
                break;
            case 3:
                rVal = "Feeding the Tamagotchis";
                break;
            case 4:
                rVal = "Catching them all";
                break;
            case 5:
                rVal = "Holding the door";
                break;
            case 6:
                rVal = "Diggy Digging holes";
                break;
            case 7:

                //break;
            case 8:

                //break;
            case 9:

                //break;
            default:
                rVal = "Searching for Will";
                break;
        }
        return rVal;
    }


    public static string TargetTypeByEffectType(ModifierType effectType) {
        string rVal = "Single";
        switch (effectType) {
            case ModifierType.TARGET_ALL:
                rVal = "All";
                break;
            case ModifierType.TARGET_ROW:
                rVal = "Row";
                break;
            case ModifierType.AOE:
                rVal = "AOE";
                break;
            case ModifierType.NONE:
                rVal = "Single";
                break;
            default:
                rVal = "Target Type default return from Utility.TargetTypeByEffectType";
                break;
        }
        return rVal;
    }

}

#region CLASSES_AND_STRUCTS
[Serializable]
public class IntEvent : UnityEvent<int> { }

[Serializable]
public class StringEvent : UnityEvent<string> { }

//[Serializable]
public class UID : IComparable<UID> {
    private static int factoryValue = 1;
    //[SerializeField]
    private int value = 0;

    public int Value { get { return this.value; } }

    public UID() {
        value = factoryValue;
        factoryValue++;
    }

    public int CompareTo(UID other) {
        return other.value.CompareTo(this.value);
    }
}


[Serializable]
public class RangedFloat {
    public float min = 0;
    public float max = 1;

    public RangedFloat() {
        min = 0;
        max = 1;
    }

    public RangedFloat(float minValue, float maxValue) {
        min = minValue;
        max = maxValue;
    }

    public void AddToRange(float value) {
        min += value;
        max += value;
    }

    public float Random {
        get {
            return UnityEngine.Random.Range(min, max);
        }
    }

    public int RandomInt {
        get {
            return Mathf.RoundToInt(UnityEngine.Random.Range(min, max));
        }
    }
}

/// <summary>
/// A float used in percentage calculations.
/// </summary>
[Serializable]
public class PercentageFloat {
    [Range(0, 100)]
    public float baseValue;

    public PercentageFloat() {
        baseValue = 50;
    }

    public PercentageFloat(float v) {
        baseValue = v;
    }

    public float BaseValue {
        get {
            return baseValue;
        }
    }

    public void AddToBase(float value) {
        baseValue += value;
    }

    public bool Success {
        get {
            float randomValue = UnityEngine.Random.Range(0, 100);
            return (randomValue <= baseValue);
        }
    }
}

public class CustomColor {

    private static Color soniPoop = new Color(0.6f, 0.5f, 0.1f, 1f);
    private static Color errorRed = new Color(1f, 0.1803922f, 0.1803922f, 1f);
    private static Color submitGreen = new Color(0.3f, 0.9f, 0.4f, 1f);
    private static Color robBlue = new Color(0.3f, 0.6f, 0.9f, 1f);
    private static Color purplePlum = new Color(0.5921569f, 0.1568628f, 0.4313726f, 1f);
    private static Color warningYellow = new Color(0.8196079f, 0.8823529f, 0.427451f, 1f);
    private static Color yallow = new Color(0.8313726f, 0.7411765f, 0.3647059f, 1f);
    private static Color buttonDisabledGray = new Color ( 0.2313726f, 0.2313726f, 0.2313726f, 1f );
    private static Color urgentUrange = new Color(1f, 0.7372549f, 0.2705882f, 1f);
    private static Color racyRed = new Color(0.945098f, 0.1921569f, 0.1921569f, 1f);
    private static Color prsPurple = new Color(0.4392157f, 0.4235294f, 0.8352941f, 1f);
    private static Color skyBlue = new Color(0.4235294f, 0.7294118f, 0.8352941f, 1f);
    private static Color dullGray = new Color(0.6039216f, 0.6666667f, 0.6901961f, 1f);

    public static Color GetColor(ColorName color) {
        Color rVal = new Color();

        switch (color) {
            case ColorName.SONI_POOP:
                rVal = soniPoop;
                break;
            case ColorName.ERROR_RED:
                rVal = errorRed;
                break;
            case ColorName.SUBMIT_GREEN:
                rVal = submitGreen;
                break;
            case ColorName.ROB_BLUE:
                rVal = robBlue;
                break;
            case ColorName.PURPLE_PLUM:
                rVal = purplePlum;
                break;
            case ColorName.URGENT_URANGE:
                rVal = urgentUrange;
                break;
            case ColorName.WARNING_YELLOW:
                rVal = warningYellow;
                break;
            case ColorName.YALLOW:
                rVal = yallow;
                break;
            case ColorName.RACY_RED:
                rVal = racyRed;
                break;
            case ColorName.PRS_PURPLE:
                rVal = prsPurple;
                break;
            case ColorName.SKY_BLUE:
                rVal = skyBlue;
                break;
            case ColorName.DULL_GRAY:
                rVal = dullGray;
                break;
            case ColorName.BUTTON_DISABLED_GRAY:
                rVal = buttonDisabledGray;
                break;
        }

        return rVal;
    }
}

[Serializable]
public class StringStringKeyValue {

    public string key;
    public string value;

    public StringStringKeyValue(string k, string v) {
        key = k;
        value = v;
    }

}

[Serializable]
public class StringFloatKeyValue {

    public string key;
    public float value;

    public StringFloatKeyValue(string k, float v) {
        key = k;
        value = v;
    }

}


#endregion CLASSES_AND_STRUCTS

[Serializable]
public enum SFXName {
    ENTITY_BUTTON_CLICK
}

public enum ColorName {
    SONI_POOP,
    ERROR_RED,
    SUBMIT_GREEN,
    ROB_BLUE,
    PURPLE_PLUM,
    WARNING_YELLOW,
    YALLOW,
    URGENT_URANGE,
    RACY_RED,
    PRS_PURPLE,
    SKY_BLUE,
    DULL_GRAY,

    BUTTON_DISABLED_GRAY = 999
}

public enum CheatCommand {
    FLARE,
    RESET_COOLDOWNS,
    TOGGLE_FOG,
    GIVE_COMPONENT,
    WTF_MODE,

    NONE = 99
}

public enum CommandType {
    MOVE,
    ATTACK,
    SKILL,

    ATTACK_MISS = 96,
    BACK = 97,
    END = 98,
    NONE = 99
}

public enum EntityType {
    PLAYER,
    ENEMY,
    BOSS,
    NEUTRAL,
    PROP,
    INTERACTIVE,
    BUILDING,

    NONE = 99
}

public enum EntityState {
    IDLE,
    MOVING,
    ATTACKING,

    CLOSE = 90,
    END = 99
}

public enum VFXId {
    HEAL_VFX = 0
}

public enum GamePhase {
    PLAYER,
    ENEMY,
    BOSS,
    NEUTRAL,

    NONE = 99
}

public enum GameState {
    FREE_ROAM,
    ENTITY_MENU,
    ENTITY_MOVE,
    ENTITY_ATTACK,

    NONE = 99
}

public enum AttackVFXType {
    FIREBALL = 1,
    EXPLOSION = 2,
    MELEE = 3,

    NONE
}

public enum AttackType {
    RANGED,
    MELEE,
    TARGETED,

    NONE
}

public enum DamageType {
    PHYSICAL,
    MAGICAL,

    NONE
}

public enum ElementalType {
    EARTH,
    FIRE,
    AIR,
    WATER,
    NONE = 10
}

public enum ActionType {
    ATTACK,
    SPECIAL_ATTACK,
    DEFEND,

    CLOSE = 98,
    NONE = 99
}

public enum ModifierType {
    // At time of targeting
    //! Modifiers
    TARGET_ALL,                        // Targets all opposing entities
    TARGET_ROW,                     // Targets everyone in the same row (front or back)
    AOE,                                // Hits the two entities on either side, regardless of row position

    NONE = 99
}

public enum PotencyType {
    // At time of calculating damage/heal
    //! Potency
    INCREASE_MULTIPLIER,         // Increases the multiplier of equipped Special Attack
    ADD_EOT,                           // Adds a DoT/HoT of 10% of the damage done (pre-reduction)
    IGNORE_ELEMENT,                // Ignores the target's elemental affinity.
    INCREASE_KNOCKBACK,         // Increases how much knockback is applied on hit

    NONE = 99
}


//public enum ModifierSubType {
//    TARGETING       // ex. TARGET_ALL
//}

//public enum PotencySubType {
//    BUFF,                  // ex. ADD_OVER_TIME_EFFECT
//    DEBUFF,              // ex. ADD_OVER_TIME_EFFECT
//    OFFENSIVE,        // Only affects Special Attacks targeting hostile entities
//    DEFFENSIVE,      // Only affects Special Attacks targeting friendly entities
//    UNIVERSAL       // Can affect either friendly or hostile
//}

// Will probably end up becoming a class
public enum Buff {
    REGEN
}

// Will probably end up becoming a class
public enum Debuff {
    BLEED
}

public enum EquippableItemType {
    HELM,
    CHESTPIECE,
    LEG,
    MAIN_HAND_WEAPON,
    OFF_HAND_WEAPON,
    TWO_HAND_WEAPON,


    NONE = 99
}

public enum CharacterClass {
    MAGE,
    SOLDIER,


    NONE = 99
}