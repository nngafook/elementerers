﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using TextFx;
using System.Collections.Generic;

public class CombatUIManager : MonoBehaviour {

    private static CombatUIManager instance = null;
    public static CombatUIManager Instance { get { return instance; } }

    private StringBuilder arenaStringBuilder = new StringBuilder();
    private string arenaTimerTextPreLabel = "Time Left: ";

    public Text combatStateButtonLabel;
    public Text arenaTimerLabel;
    public Image arenaTimerCircleFill;
    public TextFxUGUI combatMessageText;
    //public ActionButtonBar actionButtonBar;
    public LevelProgressBar levelProgressBar;
    public CombatResultsWindow combatResultsWindow;
    public ArenaWaveRewardsWindow waveRewardsWindow;
    public TutorialMessagePopup tutorialMessagePopup;

    void Awake() {
        instance = this;
    }

	// Use this for initialization
	void Start () {
        
	}

    public void InitLevelProgress(int current, int max) {
        levelProgressBar.InitValues(current, max);
        InitArenaProperties();
    }

    private void InitArenaProperties() {
        if (LevelManager.Instance.CurrentLevelType == LevelType.Arena) {
            arenaTimerLabel.text = arenaTimerTextPreLabel + ((int)LevelManager.Instance.CurrentLevel.timeBetweenDifficultyIncrease).ToString();
            arenaTimerCircleFill.fillAmount = 1;
        }
        else {
            arenaTimerLabel.text = "";
            arenaTimerCircleFill.fillAmount = 0;
        }
    }

    public void  UpdateArenaInfo(int timeLeft, int max) {
        arenaStringBuilder.Length = 0;
        arenaStringBuilder.Capacity = 0;
        arenaStringBuilder.Append(arenaTimerTextPreLabel + (timeLeft).ToString());
        arenaTimerLabel.text = arenaStringBuilder.ToString();
        arenaTimerCircleFill.DOFillAmount(((float)timeLeft / (float)max), 0.8f).SetEase(Ease.Linear);
        Vector3 punch = new Vector3(0.1f, 0.1f, 0.1f);
        if (timeLeft <= 5) {
            arenaTimerLabel.color = Color.red;
        }
        arenaTimerLabel.GetComponent<RectTransform>().DOPunchScale(punch, 0.35f, elasticity: 0).OnComplete(OnArenaTimerPunchComplete);
    }

    private void OnArenaTimerPunchComplete() {
        arenaTimerLabel.color = Color.white;
    }

    public void PushArenaEventMessage(ArenaEventType type) {
        string message = "";
        switch (type) {
            case ArenaEventType.EnemyAtkDMGIncrease:
                message = "Enemy Atk DMG Increased";
                break;
            case ArenaEventType.EnemySpAtkDMGIncrease:
                message = "Enemy SP Atk DMG Increased";
                break;
            case ArenaEventType.EnemySpeedIncrease:
                message = "Enemy Speed Increased";
                break;
            case ArenaEventType.EnemyTimedRegenAmount:
                message = "Enemy Regen Increased";
                break;
            case ArenaEventType.PlayerDamageDecrease:
                message = "Player Damage Decreased";
                break;
            default:
                message = "This is an Arena Event Message";
                break;
        }
        PushCombatMessage(message);
    }

    public void PushCombatMessage(string message) {
        combatMessageText.text = message;
        combatMessageText.AnimationManager.PlayAnimation();
    }

    public void PushTutorialMessage(List<string> messages) {
        tutorialMessagePopup.PushMessage(messages);
        //tutorialMessagePopup.PushMessage();
    }

    public void UpdateLevelProgress(int current) {
        levelProgressBar.UpdateCount(current);
    }

    //public void SetCharacterDetails(CombatEntity entity) {
    //    actionButtonBar.SetValues(entity);
    //}

    //public void OpenCharacterDetails() {
    //    actionButtonBar.Open();
    //}

    //public void CloseCharacterDetails() {
    //    actionButtonBar.Close();
    //}

    public void OpenCombatResults(ref LevelData data, bool battleWasVictorious) {
        combatResultsWindow.SetInfo(ref data, battleWasVictorious);
        combatResultsWindow.Open();
    }

    public void OpenWaveRewardsWindow(ref ArenaWave wave) {
        waveRewardsWindow.SetValues(ref wave);
        waveRewardsWindow.Open();
    }

    //public ActionType SelectedAction() {
    //    return actionButtonBar.GetSelectedActionType();
    //}

    //public void DeselectAction() {
    //    actionButtonBar.DeselectAction();
    //}

    #region BUTTON_CALLBACKS

    public void OpenLoadOutScene() {
        LevelManager.Instance.Reset();
        LoadToScene.Load(SceneName.LOADOUT);
    }

    public void CombatStatePressed() {
        if (GameSettings.Instance.userCombatState == CombatState.ACTIVE) {
            GameSettings.Instance.userCombatState = CombatState.WAITING;
            combatStateButtonLabel.text = "Waiting";
        }
        else {
            GameSettings.Instance.userCombatState = CombatState.ACTIVE;
            combatStateButtonLabel.text = "Active";
        }
        //actionButtonBar.CombatStateUpdated();
    }

    #endregion BUTTON_CALLBACKS

    // Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            CombatStatePressed();
        }
	}
}
