﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using TextFx;

public class LoadManager : MonoBehaviour {

    private static LoadManager instance;

    private static WaitForSeconds minDelay = new WaitForSeconds(1f);

    private static RectTransform loadingMessageRectTransform;
    private static List<IEnumerator> queuedRoutines = new List<IEnumerator>();
    private static List<string> queuedMessages = new List<string>();
    private static bool routineRunning = false;
    private static bool jobDoneShowing = false;
    //private static bool waitingForLastBlockAnimation = false;

    private static float messageTweenSpeed = 0.2f;
    
    private Vector2 upperTextPosition = new Vector2(0, 40);
    private Vector2 lowerTextPosition = new Vector2(0, -40);

    public bool logMessages = false;
    
    [Space(10)]
    public Text loadingMessage;
    public LoadingAnimationController loadingAnimationController;

    public static bool HasNoRoutines { get { return (!routineRunning && queuedRoutines.Count == 0 && jobDoneShowing); } }

    void Awake() {
        instance = this;
        loadingMessageRectTransform = instance.loadingMessage.GetComponent<RectTransform>();
    }

	// Use this for initialization
	void Start () {
        jobDoneShowing = false;
        ClearMessage();
        loadingMessageRectTransform.anchoredPosition = instance.lowerTextPosition;
        instance.loadingAnimationController.Enable();
        
        // Will hopefully buy enough time for registers that happen after the loading scene is loaded.
        //if (queuedRoutines.Count == 0) {
            RegisterLoadRoutine(LoadFakeInitialRoutine(), RandomFakeLoadingMessage());
        //}
        StartNextRoutine();
	}

    private static string RandomFakeLoadingMessage() {
        return Utility.RandomFakeLoadingMessage();
    }

    private static IEnumerator LoadFakeInitialRoutine() {
        yield return null;
    }

    private static IEnumerator RandomWait() {
        yield return new WaitForSeconds(Random.Range(0, 0.5f));
    }

    private static void StartNextRoutine() {
        if (!routineRunning && queuedRoutines.Count > 0) {
            routineRunning = true;
            HideMessage();
        }
        else if (queuedRoutines.Count == 0) {
            HideMessageForJobDone();
        }
    }

    //private static void OnLastBlockAnimationComplete() {
    //    instance.loadingAnimationController.OnLastBlockFinished.RemoveListener(OnLastBlockAnimationComplete);
    //    waitingForLastBlockAnimation = false;
    //}

    private static void HideMessageForJobDone() {
        loadingMessageRectTransform.DOAnchorPos(instance.upperTextPosition, messageTweenSpeed).OnComplete(OnMessageHiddenForJobDoneComplete);
    }

    private static void OnMessageHiddenForJobDoneComplete() {
        StopTextAnimation();
        instance.loadingMessage.text = "Job Done!";
        loadingMessageRectTransform.anchoredPosition = instance.lowerTextPosition;
        loadingMessageRectTransform.DOAnchorPos(Vector2.zero, messageTweenSpeed).OnComplete(OnMessageShowForJobDoneComplete);
    }

    private static void OnMessageShowForJobDoneComplete() {
        PlayTextAnimation();

        instance.StartCoroutine(RandomWaitAfterJobDone());
    }

    private static IEnumerator RandomWaitAfterJobDone() {
        yield return new WaitForSeconds(Random.Range(0, 0.5f));
        jobDoneShowing = true;
        Resources.UnloadUnusedAssets();
    }

    private static void ShowMessage() {
        loadingMessageRectTransform.anchoredPosition = instance.lowerTextPosition;
        loadingMessageRectTransform.DOAnchorPos(Vector2.zero, messageTweenSpeed).OnComplete(OnMessageShowComplete);
    }

    private static void HideMessage() {
        loadingMessageRectTransform.DOAnchorPos(instance.upperTextPosition, messageTweenSpeed).OnComplete(OnMessageHideComplete);
    }

    private static void OnMessageShowComplete() {
        if (instance.logMessages) {
            Debug.Log("Starting: " + queuedMessages[0]);
        }
        PlayTextAnimation();
        instance.StartCoroutine(WaitForRoutine(queuedRoutines[0]));
    }

    private static void OnMessageHideComplete() {
        StopTextAnimation();
        instance.loadingMessage.text = queuedMessages[0];
        ShowMessage();
    }

    private static void PlayTextAnimation() {
        instance.loadingMessage.GetComponent<TextFxUGUI>().AnimationManager.PlayAnimation();
    }

    private static void StopTextAnimation() {
        instance.loadingMessage.GetComponent<TextFxUGUI>().AnimationManager.ResetAnimation();
    }

    public static void RegisterLoadRoutine(IEnumerator routine, string message) {
        queuedRoutines.Add(routine);
        queuedMessages.Add(message);

        if (instance != null && instance.logMessages) {
            Debug.Log("Registered: " + message);
        }

        if (instance != null) {
            StartNextRoutine();
        }
    }

    public static IEnumerator WaitForRoutine(IEnumerator routine) {
        yield return minDelay;

        yield return routine;

        yield return instance.StartCoroutine(RandomWait());

        if (instance.logMessages) {
            Debug.Log("Completed: " + queuedMessages[0]);
        }

        routineRunning = false;
        jobDoneShowing = false;
        
        queuedRoutines.RemoveAt(0);
        queuedMessages.RemoveAt(0);

        StartNextRoutine();
    }

    //private static IEnumerator WaitForLastBlockAnimation() {
    //    waitingForLastBlockAnimation = true;

    //    instance.loadingAnimationController.OnLastBlockFinished.AddListener(OnLastBlockAnimationComplete);
        
    //    while (waitingForLastBlockAnimation) {
    //        yield return null;
    //    }
    //}

    public static void ClearMessage() {
        instance.loadingMessage.text = "";
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(queuedRoutines.Count);
	}
}
