﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

    #region SINGLETON
    private static LevelManager instance = null;
    private static bool isShuttingDown;
    #endregion SINGLETON

    public static LevelManager Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<LevelManager>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/LevelManager"));
                    singleton.name = "LevelManager";
                    instance = singleton.GetComponent<LevelManager>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    private int enemiesKilled = 0;
    private int currentEnemyIndex = 0;
    private int currentArenaWaveIndex = 0;
    private int totalArenaWaveEnemiesSpawned = 0;
    private int totalArenaScrapEarned = 0;
    private float arenaTimerCounter = 0;
    private EventChanges eventChanges = new EventChanges();
    private LevelData currentLevel;
    private ArenaWave currentArenaWave;

    public int levelIndexToForceLoad = 0;

    [Space(10)]
    public LevelDataObject[] allLevelDatas;

    public EventChanges CurrentEventChanges { get { return eventChanges; } }
    public LevelType CurrentLevelType { get { return currentLevel.levelType; } }
    public LevelData CurrentLevel { get { return currentLevel; } }
    public int CurrentArenaWaveIndex { get { return currentArenaWaveIndex; } }
    public int TotalArenaScrapEarned { get { return totalArenaScrapEarned; } }
    public int TotalEnemiesInLevel {
        get {
            if (CurrentLevelType == LevelType.Arena) {
                return currentArenaWave.totalEnemiesInWave;
            }
            else {
                return currentLevel.enemiesToSpawn.Count;
            }
        }
    }
    public int EnemiesRemaining { 
        get {
            if (CurrentLevelType == LevelType.Arena) {
                return currentArenaWave.totalEnemiesInWave - enemiesKilled;
            }
            else {
                return TotalEnemiesInLevel - enemiesKilled;
            }
        } 
    }

    public bool EnemiesLeftInLevel { 
        get {
            if (CurrentLevelType == LevelType.Arena) {
                return (totalArenaWaveEnemiesSpawned < currentArenaWave.totalEnemiesInWave);
            }
            else {
                return (currentEnemyIndex < TotalEnemiesInLevel);
            }
        } 
    }

    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }
        LoadAllData();
    }

    private void LoadAllData() {
        allLevelDatas = Resources.LoadAll<LevelDataObject>("Data/LevelData");
    }

    // Use this for initialization
    void Start() {
        
    }

    //private void OnCombatSceneLoaded(string sceneName) {
    //    if (sceneName == SceneName.COMBAT) {
    //        BeginLevel();
    //    }
    //}

    #region PRIVATE_METHODS

    private IEnumerator ProcessArenaTimer() {
        CombatManager cm = CombatManager.Instance;
        CombatUIManager ui = CombatUIManager.Instance;
        float maxTime = currentLevel.timeBetweenDifficultyIncrease;
        int lastTimeTicked = (int)maxTime;
        arenaTimerCounter = maxTime;
        while (cm.BattleOngoing) {

            if (cm.CurrentCombatState == CombatState.ACTIVE) {
                arenaTimerCounter -= Time.deltaTime;
                if (lastTimeTicked - Mathf.CeilToInt(arenaTimerCounter) == 1) {
                    lastTimeTicked = Mathf.CeilToInt(arenaTimerCounter);
                    if (lastTimeTicked <= 0) {
                        lastTimeTicked = (int)maxTime;
                        arenaTimerCounter = maxTime;
                        ProcessArenaEvent();
                    }
                    ui.UpdateArenaInfo(lastTimeTicked, (int)maxTime);
                }
            }

            yield return null;
        }
    }

    private void UpdateLevelProgress() {
        CombatUIManager.Instance.UpdateLevelProgress(EnemiesRemaining);
    }

    private void ProcessArenaEvent() {
        ArenaEvent arenaEvent = null;
        for (int i = 0; i < currentLevel.arenaEvents.Count; i++) {
            if (arenaEvent == null) {
                if (currentLevel.arenaEvents[i].isFallback || currentLevel.arenaEvents[i].chance.Success) {
                    arenaEvent = currentLevel.arenaEvents[i];
                    break;
                }
            }
        }

        switch (arenaEvent.type) {
            case ArenaEventType.EnemyAtkDMGIncrease:
                eventChanges.enemyAtkDMGChanged += arenaEvent.eAtkDMGIncrease;
                EntityManager.Instance.AddBaseAtkDMGToEntities(EntityType.ENEMY, arenaEvent.eAtkDMGIncrease);
                break;
            case ArenaEventType.EnemySpAtkDMGIncrease:
                eventChanges.enemySPAtkDMGChanged += arenaEvent.eSPAtkDMGIncrease;
                EntityManager.Instance.AddBaseSPAtkDamageToEntities(EntityType.ENEMY, arenaEvent.eSPAtkDMGIncrease);
                break;
            case ArenaEventType.EnemySpeedIncrease:
                eventChanges.enemySPDChanged += arenaEvent.eSPDIncrease;
                EntityManager.Instance.AddBaseSPDToEntities(EntityType.ENEMY, arenaEvent.eSPDIncrease);
                break;
            case ArenaEventType.EnemyTimedRegenAmount:
                EntityManager.Instance.HealEntities(EntityType.ENEMY, arenaEvent.eRegenAmount);
                break;
            case ArenaEventType.PlayerDamageDecrease:
                break;
            default:
                break;
        }

        CombatUIManager.Instance.PushArenaEventMessage(arenaEvent.type);
    }

    private void EndArenaWave() {
        CombatManager.Instance.EndWave(ref currentArenaWave);
    }

    private void ApplyWaveBoost(WaveBoost boost) {
        switch (boost) {
            case WaveBoost.Heal:
                EntityManager.Instance.HealEntities(EntityType.PLAYER, 999);
                break;
            case WaveBoost.DoubleScrap:
                break;
            case WaveBoost.TempDMG:
                break;
            case WaveBoost.None:
                break;
            default:
                break;
        }
    }

    private void CheckForTutorialMessage() {
        if (CurrentLevelType == LevelType.Tutorial) {
            for (int i = 0; i < currentLevel.tutorialEvents.Count; i++) {
                if (currentLevel.tutorialEvents[i].killsNeeded == enemiesKilled) {
                    CombatUIManager.Instance.PushTutorialMessage(currentLevel.tutorialEvents[i].messages);
                }
            }
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void ContinueArena(WaveBoost boost = WaveBoost.None) {
        totalArenaWaveEnemiesSpawned = 0;
        enemiesKilled = 0;
        currentArenaWaveIndex++;
        if (currentArenaWaveIndex == currentLevel.arenaWaves.Count) {
            currentArenaWaveIndex = 0;
            CombatManager.Instance.EndCombat(ref currentLevel, true);
        }
        else {
            currentArenaWave = currentLevel.arenaWaves[currentArenaWaveIndex];

            CombatManager.Instance.BeginWave(EnemiesRemaining, TotalEnemiesInLevel);
            EntityManager.Instance.ResetEnemiesForNewWave();
            //EntityManager.Instance.RunEnemyEntitiesOnScreen();

            ApplyWaveBoost(boost);
        }
    }

    public void LoadLevel() {
        //!+ This has to be fixed
        currentLevel = allLevelDatas[levelIndexToForceLoad].data;
        if (CurrentLevelType == LevelType.Arena) {
            currentArenaWave = currentLevel.arenaWaves[currentArenaWaveIndex];
        }
        //CombatUIManager.Instance.InitLevelProgress(EnemiesRemaining, currentLevel.TotalEnemies);
    }

    public void Reset() {
        currentEnemyIndex = 0;
        enemiesKilled = 0;
        currentArenaWaveIndex = 0;
        totalArenaWaveEnemiesSpawned = 0;
        totalArenaScrapEarned = 0;
        EntityManager.Instance.Reset();
    }

    public void BeginLevel() {
        EntityManager.Instance.RunEntitiesOnScreen();
        if (CurrentLevelType == LevelType.Arena) {
            StartCoroutine(ProcessArenaTimer());
        }
        CheckForTutorialMessage();
    }

    public void AllPlayersDied() {
        CombatManager.Instance.EndCombat(ref currentLevel, false);
    }

    public void EnemyKilled() {
        enemiesKilled++;
        UpdateLevelProgress();

        CheckForTutorialMessage();

        if (EnemiesRemaining == 0) {
            if (CurrentLevelType == LevelType.Arena) {
                totalArenaScrapEarned += currentArenaWave.rewardsData.scrapReward;
            }

            if (CurrentLevelType == LevelType.Arena && currentArenaWaveIndex < (currentLevel.arenaWaves.Count - 1)) {
                EndArenaWave();
            }
            else {
                CombatManager.Instance.EndCombat(ref currentLevel, true);
            }
        }
    }

    public CharacterID GetNextEnemy() {
        CharacterID rVal = CharacterID.NONE;
        if (CurrentLevelType == LevelType.Arena) {
            rVal = GetNextArenaEnemy();
        }
        else if (EnemiesLeftInLevel) {
            rVal = currentLevel.enemiesToSpawn[currentEnemyIndex];
            if (rVal != CharacterID.NONE) {
                currentEnemyIndex++;
            }
        }
        return rVal;
    }

    public CharacterID GetNextArenaEnemy() {
        CharacterID rVal = CharacterID.NONE;
        if (EnemiesLeftInLevel) {
            rVal = currentArenaWave.enemyPool.RandomElement<CharacterID>();
            if (rVal != CharacterID.NONE) {
                totalArenaWaveEnemiesSpawned++;
            }
        }
        return rVal;
    }

    public bool GiveArenaBoosts() {
        Debug.Log("Change the wavesBeforeBoost in the Arena Level Object back to 3".Colored(Color.magenta).Sized(16));
        return (currentArenaWaveIndex % currentLevel.wavesBeforeBoost == 0);
    }

    public float GetLevelAtkDMGChanged(EntityType type) {
        float rVal = 0;
        if (type == EntityType.PLAYER) {
            rVal = eventChanges.playerAtkDMGChanged;
        }
        else {
            rVal = eventChanges.enemyAtkDMGChanged;
        }
        return rVal;
    }

    public float GetLevelSPAtkDamageChanged(EntityType type) {
        float rVal = 0;
        if (type == EntityType.PLAYER) {
            rVal = eventChanges.playerSPAtkDMGChanged;
        }
        else {
            rVal = eventChanges.enemySPAtkDMGChanged;
        }
        return rVal;
    }

    // Update is called once per frame
    void Update() {

    }

    #endregion PUBLIC_METHODS
}