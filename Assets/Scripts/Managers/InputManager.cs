﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour {

    private struct MouseProperties {
        public Vector2 downScreenPosition;
        public Vector2 upScreenPosition;
        public Vector2 dragBeginScreenPosition;

        public Vector2 DragBeginPosition { get { return dragBeginScreenPosition; } }
        public Vector2 DragDelta { get { return (Vector2)Input.mousePosition - downScreenPosition; } }
        public float DragDistance { get { return Vector2.Distance((Vector2)Input.mousePosition, downScreenPosition); } }
        public bool DragVertical { get { return (DragDelta.x < DragDelta.y); } }
        public bool DragHorizontal { get { return (DragDelta.x >= DragDelta.y); } }
    }

    private enum MouseState {
        DOWN,
        DRAGGING,
        UP,

        NONE = 99
    }

    private static InputManager instance = null;
    public static InputManager Instance { get { return instance; } }

    private MouseState currentMouseState = MouseState.NONE;
    private MouseProperties mouseProperties = new MouseProperties();

    private int fingerID = -1;
    //private float lastMouseUpTime = 0;
    //private float doubleClickThreshold = 0.4f;
    //private float dragValidThreshold = 10;
    //private bool dragStartEventFired = false;

    public Vector2 MouseDownPosition { get { return mouseProperties.downScreenPosition; } }

    #region PRIVATE_METHODS
    void Awake() {
        #if !UNITY_EDITOR
            fingerID = 0;
        #endif
        instance = this;
    }

    private void ProcessMouseDown() {
        
        //Debug.Log(currentMouseState + " || " + EventSystem.current + " || " + EventSystem.current.IsPointerOverGameObject(fingerID));
        if ((currentMouseState == MouseState.NONE) && (Input.GetMouseButtonDown(0)) && ((EventSystem.current != null) && (!EventSystem.current.IsPointerOverGameObject(fingerID)))) {
            /*currentMouseState = MouseState.DOWN;
            mouseProperties.downScreenPosition = Input.mousePosition;
            EntityManager.Instance.MouseDown();*/
        }
        
    }

    //private void ProcessMouseDrag() {
    //    if ((currentMouseState == MouseState.DOWN) && (mouseProperties.DragDistance > dragValidThreshold)) {
    //        currentMouseState = MouseState.DRAGGING;
    //        mouseProperties.dragBeginScreenPosition = (Vector2)Input.mousePosition;
    //        //CameraManager.Instance.BeginDrag();
    //        if (!dragStartEventFired) {
    //            EntityManager.Instance.MouseDragStart();
    //            dragStartEventFired = true;
    //        }
    //    }
    //}

    //private void ProcessMouseDragUp() {
    //    if ((currentMouseState == MouseState.DRAGGING) && (Input.GetMouseButtonUp(0))) {
    //        //CameraManager.Instance.EndDrag();
    //        //Debug.Log("Drag Up");
    //        EntityManager.Instance.MouseDragUp();
    //    }
    //}

    private void ProcessMouseUp() {
        if (Input.GetMouseButtonUp(0)) {
            if (((EventSystem.current != null) && (!EventSystem.current.IsPointerOverGameObject(fingerID))) && (currentMouseState != MouseState.DRAGGING)) {
                //Debug.Log("Button Up");
                /*EntityManager.Instance.MouseUp();*/
            }
            //dragStartEventFired = false;
            mouseProperties.upScreenPosition = Input.mousePosition;
            currentMouseState = MouseState.NONE;
        }
    }

    private void DoubleClicked() {
        
    }

    private void ProcessRightClick() {
        if (Input.GetMouseButtonUp(1)) {
            LayerMask unitLayerMask = 1 << LayerMask.NameToLayer("UnitEntities");
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 10, unitLayerMask);
            if (hit.collider != null) {
                //Debug.Log(hit.collider.gameObject.name);
            }
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void EntityClickedAsButton(Entity entity) {
        if ((!EventSystem.current.IsPointerOverGameObject(fingerID)) && (currentMouseState != MouseState.DRAGGING)) {

        }
    }

    #endregion PUBLIC_METHODS
    
    /*void Update() {
        ProcessMouseDown();
        //ProcessMouseDrag();
        //ProcessMouseDragUp();
        ProcessMouseUp();

        ProcessRightClick();

        if (Input.GetKeyUp(KeyCode.I)) {
            for (int i = 0; i < PartyManager.Instance.CurrentPartyIDs.Length; i++) {
                Debug.Log(PartyManager.Instance.CurrentPartyIDs[i]);
            }
        }
    }*/
}
