﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatStateChangeEvent : UnityEngine.Events.UnityEvent<CombatState> { }

public class CombatManager : MonoBehaviour {

    private static CombatManager instance = null;
    private static bool isShuttingDown;
    public static CombatManager Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<CombatManager>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/CombatManager"));
                    singleton.name = "CombatManager";
                    instance = singleton.GetComponent<CombatManager>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    private bool isInCheatMode = false;
    private bool battleOngoing = false;
    private bool battleWasVictorious = false;
    private ActionType selectedActionType;
    private WaitForSeconds shortWait = new WaitForSeconds(0.15f);

    private List<CombatEntity> turnQueue = new List<CombatEntity>();

    private CombatState currentCombatState = CombatState.ACTIVE;
    public CombatState CurrentCombatState { get { return currentCombatState; } set { currentCombatState = value; if (OnCombatStateChanged != null) { OnCombatStateChanged.Invoke(currentCombatState); } } }

    public bool IsInCheatMode { get { return isInCheatMode; } set { isInCheatMode = value; } }

    [HideInInspector]
    public CombatStateChangeEvent OnCombatStateChanged = new CombatStateChangeEvent();

    public GameObject deathSkullParticleObject;

	[Header("Combat Positions")]
    public List<Vector3> playerCombatPositions;
    public List<Vector3> enemyCombatPositions;

    public bool BattleOngoing { get { return battleOngoing; } set { battleOngoing = value; } }
    public ActionType SelectedActionType { get { return selectedActionType; } set { selectedActionType = value; } }

    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }
    }

	// Use this for initialization
    void Start() {
        
    }

    private IEnumerator LoadLevelAndUnits() {
        // Called by Init(), done when LoadToScene.Load(COMBAT) is called
        LevelManager.Instance.LoadLevel();
        EntityManager.Instance.CreateUnits();
        StartCoroutine("ProcessTurns");
        yield return null;
    }

    private IEnumerator ProcessTurns() {
        battleOngoing = true;
        while (battleOngoing) {
            if (turnQueue.Count > 0 && currentCombatState != CombatState.WAITING) {
                CombatEntity entity = null;
                entity = turnQueue[0];
                if (entity.CanAttack) {
                    entity.Attack();
                    turnQueue.RemoveAt(0);
                }
            }
            yield return shortWait;
        }
    }

    #region PUBLIC_METHODS

    public void Init() {
        LoadManager.RegisterLoadRoutine(LoadLevelAndUnits(), "Loading Level");
    }

    public void ForceInit() {
        StartCoroutine(LoadLevelAndUnits());
    }

    public void EndCombat(ref LevelData currentLevel, bool victorious) {
        battleOngoing = false;
        battleWasVictorious = victorious;
        CombatManager.Instance.CurrentCombatState = CombatState.WAITING;
        CombatUIManager.Instance.OpenCombatResults(ref currentLevel, battleWasVictorious);
    }

    public void EndWave(ref ArenaWave wave) {
        CombatManager.Instance.CurrentCombatState = CombatState.WAITING;
        CombatUIManager.Instance.OpenWaveRewardsWindow(ref wave);
    }

    public void BeginWave(int enemiesRemaining, int totalEnemies) {
        CombatManager.Instance.CurrentCombatState = CombatState.ACTIVE;
        CombatUIManager.Instance.InitLevelProgress(enemiesRemaining, totalEnemies);
    }

    #region VFX
    public void FireDeathSkullVFX(Vector2 pos) {
        deathSkullParticleObject.transform.position = pos;
        deathSkullParticleObject.SetActive(true);
    }
    #endregion VFX

    public void AddEntityTurn(CombatEntity entity) {
        turnQueue.Add(entity);
    }

    public void RemoveFromTurnQueue(CombatEntity entity) {
        if (turnQueue.Contains(entity)) {
            turnQueue.Remove(entity);
        }
    }

    #endregion PUBLIC_METHODS

    // Update is called once per frame
	void Update () {
	
	}
}

public enum CombatState {
    ACTIVE,
    WAITING
}