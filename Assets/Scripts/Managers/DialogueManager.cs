﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;

public class DialogueManager : MonoBehaviour {

    private enum DialogueState {
        DialogueLines,
        TransitionLines,
        Fading,
        TextTyping
    }

    private bool keepTyping = true;
    private bool conversationOver = false;

    private float fadeTransitionSpeed = 1.0f;

    private int leftBlockIndex = 0;
    private int rightBlockIndex = 0;
    private int leftLineIndex = 0;
    private int rightLineIndex = 0;
    private int nextTransitionLineIndex = 0;

    private WaitForSeconds fadeOverlayWait = new WaitForSeconds(1);
    private WaitForSeconds shortWait = new WaitForSeconds(0.5f);
    private RectTransform continueButtonRT;
    private List<string> queuedTransitionLines = new List<string>();
    private DialogueLineTransition queuedLineTransition = DialogueLineTransition.None;
    private SpeakerSide currentSpeakerSide = SpeakerSide.Left;
    private DialogueState previousDialogueState = DialogueState.DialogueLines;
    private DialogueState dialogueState = DialogueState.DialogueLines;

    [Header("This should be dynamic I think")]
    public DialogueSpeaker leftSpeaker;
    public DialogueSpeaker rightSpeaker;

    [Space(10)]
    public DialogueConversationDataObject conversation;

    [Space(10)]
    public Text speakerNameTextField;
    public Text dialogueTextField;
    public CanvasGroup continueButtonCG;

    [Space(10)]
    [Header("Transition")]
    public Text lineTransitionText;
    public CanvasGroup fadeToBlackOverlayCG;
    public CanvasGroup lineTransitionCG;

    void Awake() {
        continueButtonRT = continueButtonCG.GetComponent<RectTransform>();

        continueButtonCG.alpha = 0;
        continueButtonCG.blocksRaycasts = false;
        continueButtonCG.interactable = false;
    }

	// Use this for initialization
	IEnumerator Start () {
        currentSpeakerSide = conversation.firstSpeaker;

        leftSpeaker.Hide();
        rightSpeaker.Hide();

        yield return shortWait;

        leftSpeaker.SlideIn();
        rightSpeaker.SlideIn(OnSpeakersSlideInComplete);
	}

    /// <summary>
    /// Called on initial load of the Dialogue scene, and the characters are done sliding in.
    /// </summary>
    private void OnSpeakersSlideInComplete() {
        NextLine();
    }

    /// <summary>
    /// Sets both speakers selected. Mainly used to end the dialogue
    /// </summary>
    public void SetBothSelected() {
        leftSpeaker.SetSelected(true);
        rightSpeaker.SetSelected(true);
    }

    /// <summary>
    /// Refreshes the speakers avatars to be selected or not.
    /// </summary>
    private void RefreshSpeakersSelected() {
        if (currentSpeakerSide == SpeakerSide.Left) {
            leftSpeaker.SetSelected(true);
            rightSpeaker.SetSelected(false);
        }
        else {
            leftSpeaker.SetSelected(false);
            rightSpeaker.SetSelected(true);
        }
    }

    /// <summary>
    /// Attempts to process the next dialogue line. If the dialogue is over, or there's a transition, this processes that instead
    /// </summary>
    private void NextLine() {
        if (queuedLineTransition != DialogueLineTransition.None) {
            PlayQueuedTransition();
        }
        else if (!conversationOver) {
            RefreshSpeakersSelected();
            ProcessNextLine();
        }
        else {
            EndConversation();
        }
    }

    /// <summary>
    /// Ends the conversation. 
    /// </summary>
    private void EndConversation() {
        dialogueTextField.text = "";
        speakerNameTextField.text = "";
        SetBothSelected();
        continueButtonCG.DOFade(1, 0.5f).OnComplete(OnContinueButtonFadeInComplete);
    }

    /// <summary>
    /// Processes next line in the current speakers dialogue block
    /// </summary>
    private void ProcessNextLine() {
        dialogueTextField.text = "";

        //dialogueTextField.alignment = (currentSpeakerSide == SpeakerSide.Left) ? TextAnchor.UpperLeft : TextAnchor.UpperRight;
        speakerNameTextField.alignment = (currentSpeakerSide == SpeakerSide.Left) ? TextAnchor.UpperLeft : TextAnchor.UpperRight;

        DialogueLine line = (currentSpeakerSide == SpeakerSide.Left) ? conversation.leftSpeakerBlocks[leftBlockIndex].lines[leftLineIndex] : conversation.rightSpeakerBlocks[rightBlockIndex].lines[rightLineIndex];
        queuedLineTransition = line.lineTransition;
        queuedTransitionLines = line.transitionLines;

        //dialogueTextField.text = line.text;
        StartCoroutine("TypewriteThatShit", line.text);
        speakerNameTextField.text = (currentSpeakerSide == SpeakerSide.Left) ? conversation.leftSpeaker : conversation.rightSpeaker;

        DialogueSpeaker speaker = (currentSpeakerSide == SpeakerSide.Left) ? leftSpeaker : rightSpeaker;

        // Animation
        if (line.startAnimation != DialogueSpeakerAnimation.None) {
            speaker.Animate(line.startAnimation);
        }

        // Emotion
        if (line.emotion != speaker.currentEmotion) {
            speaker.SetEmotion(line.emotion);
        }

        int lineIndex = 0;
        int blockIndex = 0;
        List<DialogueBlockDataObject> speakerBlocks = new List<DialogueBlockDataObject>();
        if (currentSpeakerSide == SpeakerSide.Left) {
            leftLineIndex++;
            lineIndex = leftLineIndex;
            blockIndex = leftBlockIndex;
            speakerBlocks = conversation.leftSpeakerBlocks;
        }
        else {
            rightLineIndex++;
            lineIndex = rightLineIndex;
            blockIndex = rightBlockIndex;
            speakerBlocks = conversation.rightSpeakerBlocks;
        }

        if (!conversationOver && lineIndex == speakerBlocks[blockIndex].lines.Count) {
            conversationOver = speakerBlocks[blockIndex].endOfConversation;
            if (currentSpeakerSide == SpeakerSide.Left) {
                leftBlockIndex++;
                leftLineIndex = 0;
            }
            else {
                rightBlockIndex++;
                rightLineIndex = 0;
            }
            currentSpeakerSide = (currentSpeakerSide == SpeakerSide.Left) ? SpeakerSide.Right : SpeakerSide.Left;
        }
    }

    #region LINE_TRANSITIONS

    /// <summary>
    /// Begin the Line Transition
    /// </summary>
    private void PlayQueuedTransition() {
        // Set to fading by default. If there's text, it'll change to transitionText state 
        dialogueState = DialogueState.Fading;
        switch (queuedLineTransition) {
            case DialogueLineTransition.FadeToBlack:
            case DialogueLineTransition.FadeToBlackWithText:
                fadeToBlackOverlayCG.DOFade(1, fadeTransitionSpeed).OnComplete(OnFadeOverlayFadeInComplete);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// After the transition fade to black overlay is faded in
    /// </summary>
    private void OnFadeOverlayFadeInComplete() {
        dialogueTextField.text = "";
        speakerNameTextField.text = "";
        switch (queuedLineTransition) {
            case DialogueLineTransition.FadeToBlack:
                if (conversationOver) {
                    EndConversation();
                }
                else {
                    StartCoroutine(TransitionFadeDelay());
                }
                break;
            case DialogueLineTransition.FadeToBlackWithText:
                dialogueState = DialogueState.TransitionLines;
                lineTransitionText.text = "";
                lineTransitionCG.alpha = 1;
                NextTransitionLine();
                break;
        }
    }

    /// <summary>
    /// Pauses with the black overlay transition on screen for fadeOverlayWait
    /// </summary>
    /// <returns></returns>
    private IEnumerator TransitionFadeDelay() {
        yield return fadeOverlayWait;

        FadeTransitionBlackOverlayOut();
    }

    /// <summary>
    /// Prints the next transition line text
    /// When it's complete, fades the black overlay out
    /// </summary>
    private void NextTransitionLine() {
        if (nextTransitionLineIndex < queuedTransitionLines.Count) {
            lineTransitionText.text = queuedTransitionLines[nextTransitionLineIndex];
            nextTransitionLineIndex++;
        }
        else {
            dialogueState = DialogueState.Fading;
            lineTransitionCG.DOFade(0, fadeTransitionSpeed).OnComplete(OnTransitionLineFadeOutComplete);
            nextTransitionLineIndex = 0;
        }
    }

    /// <summary>
    /// After fading out after the last transition line text
    /// </summary>
    private void OnTransitionLineFadeOutComplete() {
        if (conversationOver) {
            EndConversation();
        }
        else {
            FadeTransitionBlackOverlayOut();
        }
    }

    /// <summary>
    /// Fades the transition black overlay out
    /// </summary>
    private void FadeTransitionBlackOverlayOut() {
        fadeToBlackOverlayCG.DOFade(0, fadeTransitionSpeed).OnComplete(LineTransitionsComplete);
    }

    /// <summary>
    /// Ends the transition state and resumes the dialogue lines
    /// </summary>
    private void LineTransitionsComplete() {
        queuedLineTransition = DialogueLineTransition.None;
        dialogueState = DialogueState.DialogueLines;
        NextLine();
    }

    #endregion LINE_TRANSITIONS

    private void OnMouseClicked() {
        switch (dialogueState) {
            case DialogueState.DialogueLines:
                NextLine();
                break;
            case DialogueState.TransitionLines:
                NextTransitionLine();
                break;
            case DialogueState.TextTyping:
                EndTyping();
                break;
            default:
                break;
        }
    }

    private void OnContinueButtonFadeInComplete() {
        continueButtonCG.blocksRaycasts = true;
        continueButtonCG.interactable = true;
        Vector2 endPos = continueButtonRT.anchoredPosition.AddX(6);
        continueButtonCG.GetComponent<RectTransform>().DOAnchorPos(endPos, 0.35f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }

    private void EndTyping() {
        keepTyping = false;
    }

    private IEnumerator TypewriteThatShit(string txt) {
        previousDialogueState = dialogueState;
        dialogueState = DialogueState.TextTyping;
        dialogueTextField.text = "";
        StringBuilder sb = new StringBuilder(txt.Length);
        int i = 0;
        while (i < txt.Length && keepTyping) {
            if (txt[i] == '<' && txt[i + 1] != '/') {
                int closeTagStartIndex = txt.IndexOf("</", i);
                int closeTagEndIndex = txt.IndexOf('>', closeTagStartIndex) + 1;

                string sub = txt.Substring(i, (closeTagEndIndex - i));
                sb.Append(sub);
                i = closeTagEndIndex;
            }
            else {
                sb.Append(txt[i]);
                i++;
            }
            yield return null;
            dialogueTextField.text = sb.ToString();
        }

        if (!keepTyping) {
            dialogueTextField.text = txt;
            keepTyping = true;
        }
        dialogueState = previousDialogueState;
        previousDialogueState = DialogueState.TextTyping;
    }

    public void OnContinuePressed() {
        LoadToScene.Load(SceneName.LOADOUT);
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)) {
            OnMouseClicked();
        }
	}
}



//const int sLen=30, Loops=5000;
//DateTime sTime, eTime;
//int i;
//string sSource = new String('X', sLen);
//string sDest = "";
//// 
//// Time string concatenation.
//// 
//sTime = DateTime.Now;
//for(i=0;i<Loops;i++) sDest += sSource;
//eTime = DateTime.Now;
//Console.WriteLine("Concatenation took " + (eTime - sTime).TotalSeconds + " seconds.");
//// 
//// Time StringBuilder.
//// 
//sTime = DateTime.Now;
//System.Text.StringBuilder sb = new System.Text.StringBuilder((int)(sLen * Loops * 1.1));
//for(i=0;i<Loops;i++) sb.Append(sSource);
//sDest = sb.ToString();
//eTime = DateTime.Now;
//Console.WriteLine("String Builder took " + (eTime - sTime).TotalSeconds + " seconds.");
//// 
//// Make the console window stay open
//// so that you can see the results when running from the IDE.
//// 
//Console.WriteLine();
//Console.Write("Press Enter to finish ... ");
//Console.Read();
