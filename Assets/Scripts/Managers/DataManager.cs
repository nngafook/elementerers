﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DataManager : MonoBehaviour {

    #region SINGLETON
    private static DataManager instance = null;
    private static bool isShuttingDown;
    #endregion SINGLETON

    public static DataManager Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<DataManager>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/DataManager"));
                    singleton.name = "DataManager";
                    instance = singleton.GetComponent<DataManager>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    //private string pw = "SoNiSauRuS";
    //private string salt = "oTocPi6425";
    //private string scKey = "Soft Currency";

    //private static bool dataObjectsLoaded = false;

    private CharacterDataObject[] characterDataObjects;
    private SpecialAttackDataObject[] specialAttackDataObjects;
    private AugmentDataObject[] augmentDataObjects;
    private CharacterLevelStatsDataObject characterLevelStatsDataObject;

    public CharacterDataObject[] CharacterDataObjects { get { return characterDataObjects; } }
    public SpecialAttackDataObject[] SpecialAttackDataObjects { get { return specialAttackDataObjects; } }
    public AugmentDataObject[] AugmentDataObjects { get { return augmentDataObjects; } }
    public CharacterLevelStatsDataObject CharacterLevelStatsObject { get { return characterLevelStatsDataObject; } }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }
    }

    private IEnumerator LoadAllData() {
        //Debug.Log("called");
        if (characterDataObjects != null) {
            Array.Clear(characterDataObjects, 0, characterDataObjects.Length);
        }
        if (specialAttackDataObjects != null) {
            Array.Clear(specialAttackDataObjects, 0, specialAttackDataObjects.Length);
        }
        if (characterLevelStatsDataObject != null) {
            characterLevelStatsDataObject = null;
        }
        if (augmentDataObjects != null) {
            Array.Clear(augmentDataObjects, 0, augmentDataObjects.Length);
        }
        //! Is there any lag after this is loaded, or during its loading?
        characterDataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData");
        specialAttackDataObjects = Resources.LoadAll<SpecialAttackDataObject>("Data/SpecialAttackData");
        augmentDataObjects = Resources.LoadAll<AugmentDataObject>("Data/AugmentData");
        characterLevelStatsDataObject = Resources.Load<CharacterLevelStatsDataObject>("Data/CharacterLevelStatsData/CharacterLevelStats");
        //dataObjectsLoaded = true;
        yield return null;
    }

    public void RegisterLoadDataObjects() {
        LoadManager.RegisterLoadRoutine(LoadAllData(), "Loading Data Objects");
        //PartyManager.Instance.Init();
    }

    /// <summary>
    /// Editor only method... I hope.
    /// </summary>
    public void ForceLoadDataObjects() {
        if (characterDataObjects != null) {
            Array.Clear(characterDataObjects, 0, characterDataObjects.Length);
        }
        if (specialAttackDataObjects != null) {
            Array.Clear(specialAttackDataObjects, 0, specialAttackDataObjects.Length);
        }
        if (characterLevelStatsDataObject != null) {
            characterLevelStatsDataObject = null;
        }
        if (augmentDataObjects != null) {
            Array.Clear(augmentDataObjects, 0, augmentDataObjects.Length);
        }
        characterDataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData");
        specialAttackDataObjects = Resources.LoadAll<SpecialAttackDataObject>("Data/SpecialAttackData");
        augmentDataObjects = Resources.LoadAll<AugmentDataObject>("Data/AugmentData");
        characterLevelStatsDataObject = Resources.Load<CharacterLevelStatsDataObject>("Data/CharacterLevelStatsData/CharacterLevelStats");

    }

    public CharacterDataObject CharacterDataObjectByID(CharacterID id) {
        CharacterDataObject rVal = null;
        for (int i = 0; i < characterDataObjects.Length; i++) {
            if (characterDataObjects[i].data.characterID == id) {
                rVal = characterDataObjects[i];
                break;
            }
        }
        return rVal;
    }

    public SpecialAttackData SpecialAttackDataByType(SpecialAttackType type) {
        SpecialAttackData rVal = null;
        for (int i = 0; i < specialAttackDataObjects.Length; i++) {
            if (specialAttackDataObjects[i].data.specialAttackType == type) {
                rVal = specialAttackDataObjects[i].data;
                break;
            }
        }
        return rVal;
    }

    public AugmentData AugmentDataByID(string id) {
        AugmentData rVal = null;

        for (int i = 0; i < augmentDataObjects.Length; i++) {
            if (augmentDataObjects[i].data.id == id) {
                rVal = augmentDataObjects[i].data;
                break;
            }
        }

        return rVal;
    }

    //public int GetSoftCurrency() {
    //    ZPlayerPrefs.Initialize(pw, salt);
    //    return (ZPlayerPrefs.HasKey(scKey)) ? ZPlayerPrefs.GetInt(scKey) : 0;
    //}

    //public void AddSoftCurrency(int value) {
    //    ZPlayerPrefs.Initialize(pw, salt);
    //    int current = (ZPlayerPrefs.HasKey(scKey)) ? ZPlayerPrefs.GetInt(scKey) : 0;
    //    int total = current + value;
    //    ZPlayerPrefs.SetInt(scKey, total);
    //    ZPlayerPrefs.Save();
    //}

    void Update() {
        if (Input.GetKeyUp(KeyCode.Keypad5)) {
            //Debug.Log(GetSoftCurrency());
        }
    }
}
