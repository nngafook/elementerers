﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class EntityManager : MonoBehaviour {

    private static EntityManager instance = null;
    private static bool isShuttingDown;
    public static EntityManager Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<EntityManager>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/EntityManager"));
                    singleton.name = "DataManager";
                    instance = singleton.GetComponent<EntityManager>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    public UnityAction OnEntityIdleCheckComplete;
    private WaitForSeconds idleCheckWait = new WaitForSeconds(0.2f);

    /*private CombatEntity entityDownedOn;
    private CombatEntity entityUppedOn;*/
    private CombatEntity selectedEntity;

    private LayerMask unitLayerMask;

    [Header("Entity Lists")]
    private List<CombatEntity> activePlayerEntities = new List<CombatEntity>();
    private List<CombatEntity> activeEnemyEntities = new List<CombatEntity>();
    private List<CombatEntity> allPlayerEntities = new List<CombatEntity>();        //! I think this is for NOTHING
    private List<CombatEntity> allEnemyEntities = new List<CombatEntity>();
    private List<CombatEntity> allActiveEntities = new List<CombatEntity>();                  //! I think this only contains active entities (Used to be called allEntities)

    [Space(20)]
    public CombatEntity playerUnitPrefab;
    public CombatEntity enemyUnitPrefab;

    public List<CombatEntity> ActivePlayerEntities { get { return activePlayerEntities; } }
    public List<CombatEntity> ActiveEnemyEntities { get { return activeEnemyEntities; } }

    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }
        unitLayerMask = 1 << LayerMask.NameToLayer("UnitEntities");
    }

	// Use this for initialization
	void Start () {
        
	}

    private CombatEntity MouseRaycastForEntity() {
        CombatEntity rVal = null;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 10, unitLayerMask);
        if (hit.collider != null) {
            rVal = hit.collider.GetComponentInParent<CombatEntity>();
        }
        return rVal;
    }

    private CombatEntity EntityByListIndex(EntityType type, int listIndex) {
        CombatEntity rVal = null;
        if (type == EntityType.PLAYER) {
            for (int i = 0; i < activePlayerEntities.Count; i++) {
                if (activePlayerEntities[i].ListIndex == listIndex) {
                    rVal = activePlayerEntities[i];
                    break;
                }
            }
        }
        else {
            for (int i = 0; i < activeEnemyEntities.Count; i++) {
                if (activeEnemyEntities[i].ListIndex == listIndex) {
                    rVal = activeEnemyEntities[i];
                    break;
                }
            }
        }
        return rVal;
    }

    private void AddToAllEntityList(CombatEntity entity) {
        bool entityFound = false;
        if (entity.Type == EntityType.PLAYER) {
            for (int i = 0; i < allPlayerEntities.Count; i++) {
                if (allPlayerEntities[i].UID == entity.UID) {
                    entityFound = true;
                    break;
                }
            }
            if (!entityFound) {
                allPlayerEntities.Add(entity);
            }
        }
        else if (entity.Type == EntityType.ENEMY) {
            for (int i = 0; i < allEnemyEntities.Count; i++) {
                if (allEnemyEntities[i].UID == entity.UID) {
                    entityFound = true;
                    break;
                }
            }
            if (!entityFound) {
                allEnemyEntities.Add(entity);
            }
        }
    }

    private bool IsBackRow(int index) {
        return (index == 0 || index == 2 || index == 4);
    }

    private IEnumerator ProcessCheckForAllIdle() {

        while (!AllEntitiesIdle()) {
            yield return idleCheckWait;
        }

        if (OnEntityIdleCheckComplete != null) {
            OnEntityIdleCheckComplete.Invoke();
            OnEntityIdleCheckComplete = null;
        }

    }

    /*
    private void SelectEntityUppedOn() {
        if (entityUppedOn.EntityType == EntityType.PLAYER) {
            CombatEntity tempEntity = selectedEntity;

            DeselectSelectedEntity();

            if (tempEntity == null || tempEntity.UID != entityUppedOn.UID) {
                selectedEntity = entityUppedOn;
                selectedEntity.IsSelected = true;
                selectedEntity.ShowBoxOnTarget();
            }

            if (selectedEntity != null) {
                CombatUIManager.Instance.SetCharacterDetails(selectedEntity);
                CombatUIManager.Instance.OpenCharacterDetails();
            }
            else {
                CombatUIManager.Instance.CloseCharacterDetails();
            }
        }
    }
    */
    #region PUBLIC_METHODS

    public void CreateUnits() {
        CreatePlayerUnits();
        CreateEnemyUnits();
    }

    public void CreatePlayerUnits() {
        CombatEntity entity = null;
        List<Vector3> playerCombatPositions = CombatManager.Instance.playerCombatPositions;

        CharacterID[] partyIDs = PartyManager.Instance.CurrentPartyIDs;
        for (int i = 0; i < partyIDs.Length; i++) {
            if (partyIDs[i] != CharacterID.NONE) {
                entity = Instantiate(playerUnitPrefab);
                entity.SetDataObject(partyIDs[i]);
                entity.SetListIndex(i);
                entity.transform.position = playerCombatPositions[i];
                entity.SetBasePosition(playerCombatPositions[i]);
                entity.SetOffScreen();
                SceneManager.MoveGameObjectToScene(entity.gameObject, SceneManager.GetSceneByName(SceneName.COMBAT));
            }
        }
    }

    public void CreateEnemyUnits() {
        CombatEntity entity = null;
        List<Vector3> enemyCombatPositions = CombatManager.Instance.enemyCombatPositions;

        int totalEnemyPositions = enemyCombatPositions.Count;
        CharacterID enemyID = CharacterID.NONE;
        for (int i = 0; i < totalEnemyPositions; i++) {
            if (allEnemyEntities.Count > i && allEnemyEntities[i] != null) {
                allEnemyEntities[i].ResetData();
            }
            else {
                enemyID = LevelManager.Instance.GetNextEnemy();
                if (enemyID != CharacterID.NONE) {
                    entity = Instantiate(enemyUnitPrefab);
                    entity.SetDataObject(enemyID);
                    entity.SetListIndex(i);
                    entity.transform.position = enemyCombatPositions[i];
                    entity.SetBasePosition(enemyCombatPositions[i]);
                    entity.SetOffScreen();
                    SceneManager.MoveGameObjectToScene(entity.gameObject, SceneManager.GetSceneByName(SceneName.COMBAT));
                }
            }
        }
    }

    public void Reset() {
        activePlayerEntities.Clear();
        activeEnemyEntities.Clear();
        allActiveEntities.Clear();
    }

    public void ResetEnemiesForNewWave() {
        CreateEnemyUnits();
        RunEnemyEntitiesOnScreen();
    }

    public void RegisterCombatEntity(CombatEntity entity) {
        if (entity.Type == EntityType.PLAYER) {
            activePlayerEntities.Add(entity);
        }
        else if (entity.Type == EntityType.ENEMY) {
            activeEnemyEntities.Add(entity);
        }
        AddToAllEntityList(entity);
        allActiveEntities.Add(entity);
    }

    public void UnregisterCombatEntity(CombatEntity entity) {
        if (entity.Type == EntityType.PLAYER) {
            activePlayerEntities.Remove(entity);
            if (activePlayerEntities.Count == 0) {
                LevelManager.Instance.AllPlayersDied();
            }
        }
        else if (entity.Type == EntityType.ENEMY) {
            activeEnemyEntities.Remove(entity);
            LevelManager.Instance.EnemyKilled();
        }
        allActiveEntities.Remove(entity);
    }

    public CombatEntity GetRandomTarget(EntityType targetType) {
        CombatEntity rVal = null;
        if (targetType == EntityType.PLAYER) {
            if (activePlayerEntities.Count > 0) {
                rVal = activePlayerEntities.RandomElement();
            }
        }
        else if (targetType == EntityType.ENEMY) {
            if (activeEnemyEntities.Count > 0) {
                rVal = activeEnemyEntities.RandomElement(true, "IsAlive");
            }
            else {
                Debug.Log("Trying to get an enemy from the enemy entities list when it's empty");
            }
        }
        return rVal;
    }

    /// <summary>
    /// Returns a list of targets based on the attack type, and the initial target is always the first in the list
    /// </summary>
    /// <param name="target"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<CombatEntity> GetTargetsBasedOnEffectType(CombatEntity target, AugmentData augmentData) {
        List<CombatEntity> rVal = new List<CombatEntity>();

        int targetIndex = target.ListIndex;
        EntityType targetType = target.Type;

        rVal.Add(target);

        List<CombatEntity> listToUse = (targetType == EntityType.PLAYER) ? activePlayerEntities : activeEnemyEntities;

        CombatEntity entityInQuestion = null;

        if (augmentData != null) {
            switch (augmentData.effectType) {
                case ModifierType.TARGET_ALL:
                    for (int i = 0; i < listToUse.Count; i++) {
                        if (listToUse[i] != null && listToUse[i].IsAlive && listToUse[i] != target) {
                            rVal.Add(listToUse[i]);
                        }
                    }
                    break;
                case ModifierType.TARGET_ROW:
                    if (IsBackRow(targetIndex)) {
                        if (targetIndex != 0) {
                            entityInQuestion = EntityByListIndex(targetType, 0);
                            if (entityInQuestion != null && entityInQuestion.IsAlive) {
                                rVal.Add(entityInQuestion);
                            }
                        }
                        if (targetIndex != 2) {
                            entityInQuestion = EntityByListIndex(targetType, 2);
                            if (entityInQuestion != null && entityInQuestion.IsAlive) {
                                rVal.Add(entityInQuestion);
                            }
                        }
                        if (targetIndex != 4) {
                            entityInQuestion = EntityByListIndex(targetType, 4);
                            if (entityInQuestion != null && entityInQuestion.IsAlive) {
                                rVal.Add(entityInQuestion);
                            }
                        }
                    }
                    else {
                        if (targetIndex != 1) {
                            entityInQuestion = EntityByListIndex(targetType, 1);
                            if (entityInQuestion != null && entityInQuestion.IsAlive) {
                                rVal.Add(entityInQuestion);
                            }
                        }
                        if (targetIndex != 3) {
                            entityInQuestion = EntityByListIndex(targetType, 3);
                            if (entityInQuestion != null && entityInQuestion.IsAlive) {
                                rVal.Add(entityInQuestion);
                            }
                        }
                    }
                    break;
                case ModifierType.AOE:
                    int index = targetIndex - 1;
                    if (index >= 0) {
                        entityInQuestion = EntityByListIndex(targetType, index);
                        if (entityInQuestion != null && entityInQuestion.IsAlive) {
                            rVal.Add(entityInQuestion);
                        }
                    }
                    index = targetIndex + 1;
                    if (index < listToUse.Count) {
                        entityInQuestion = EntityByListIndex(targetType, index);
                        if (entityInQuestion != null && entityInQuestion.IsAlive) {
                            rVal.Add(entityInQuestion);
                        }
                    }
                    break;
                case ModifierType.NONE:
                    // No effect - Single Target
                    break;
                default:
                    Debug.LogWarning("An EffectType that is not useable on targets was passed to EntityManager.GetTargetsBasedOnEffectType()");
                    break;
            }
        }

        return rVal;
    }

    public void RunEntitiesOnScreen() {
        RunPlayerEntitiesOnScreen();
        RunEnemyEntitiesOnScreen();
    }

    public void RunPlayerEntitiesOnScreen() {
        for (int i = 0; i < activePlayerEntities.Count; i++) {
            activePlayerEntities[i].BeginCombat();
        }
    }

    public void RunEnemyEntitiesOnScreen() {
        for (int i = 0; i < activeEnemyEntities.Count; i++) {
            activeEnemyEntities[i].BeginCombat();
        }
    }

    public void AddBaseAtkDMGToEntities(EntityType entityType, float amount) {
        if (entityType != EntityType.PLAYER) {
            for (int i = 0; i < activeEnemyEntities.Count; i++) {
                activeEnemyEntities[i].AddBaseAtkDMG(amount);
            }
        }
    }

    public void AddBaseSPAtkDamageToEntities(EntityType entityType, float amount) {
        if (entityType != EntityType.PLAYER) {
            for (int i = 0; i < activeEnemyEntities.Count; i++) {
                activeEnemyEntities[i].AddBaseSPAtkDMG(amount);
            }
        }
    }

    public void AddBaseSPDToEntities(EntityType entityType, float amount) {
        if (entityType != EntityType.PLAYER) {
            for (int i = 0; i < activeEnemyEntities.Count; i++) {
                activeEnemyEntities[i].AddBaseSPD(amount);
            }
        }
    }

    public void HealEntities(EntityType entityType, float amount) {
        if (entityType != EntityType.PLAYER) {
            for (int i = 0; i < activeEnemyEntities.Count; i++) {
                activeEnemyEntities[i].BeHealed(amount, false);
            }
        }
        else {
            for (int i = 0; i < activePlayerEntities.Count; i++) {
                activePlayerEntities[i].BeHealed(amount, false);
            }
        }
    }

    public bool AllEntitiesIdle() {
        bool rVal = true;

        for (int i = 0; i < allActiveEntities.Count; i++) {
            if (!allActiveEntities[i].IsIdle) {
                rVal = false;
                break;
            }
        }

        return rVal;
    }

    public void RegisterCallbackForIdleCheck(UnityAction callback) {
        OnEntityIdleCheckComplete = callback;
        
        StopCoroutine("ProcessCheckForAllIdle");
        StartCoroutine("ProcessCheckForAllIdle");
    }

    #region EVENTS

    /*

    public void MouseDown() {
        CombatEntity entity = MouseRaycastForEntity();
        if (entity != null) {
            entityDownedOn = entity;
        }
    }

    public void MouseUp() {
        if (entityDownedOn != null) {
            entityUppedOn = MouseRaycastForEntity();
			if (entityUppedOn != null && entityUppedOn.UID == entityDownedOn.UID) {
                ActionType actionType = CombatUIManager.Instance.SelectedAction();
                if (actionType != ActionType.NONE) {
                    if (selectedEntity != null && selectedEntity.EntityType == EntityType.PLAYER) {
                        if (entityUppedOn.EntityType == EntityType.PLAYER) {
                            // Player targetting player
                            if (actionType == ActionType.SPECIAL_ATTACK && selectedEntity.Data.SpecialAttackType == SpecialAttackType.HEAL) {
                                selectedEntity.ExecuteHealAction(entityUppedOn);
                            }
                            else {
                                SelectEntityUppedOn();
                            }
                        }
                        else {
                            // Selected enemy
                            entityDownedOn = null;
                            // if the selected action is NOT heal
                            if (!(actionType == ActionType.SPECIAL_ATTACK && selectedEntity.Data.SpecialAttackType == SpecialAttackType.HEAL)) {
                                selectedEntity.ExecuteSelectedAction(actionType, entityUppedOn);
                            }
                        }
                    }
                }
                else {
                    SelectEntityUppedOn();
                }
            }
            entityDownedOn = null;
        }
    }

    public void DefendButtonPressed() {
        if (selectedEntity != null) {
            selectedEntity.ExecuteDefendAction();
        }
    }

    */

    public void DeselectSelectedEntity() {
        if (selectedEntity != null) {
            selectedEntity.IsSelected = false;
            selectedEntity.HideBoxOnTarget();
            selectedEntity = null;
        }
    }

    //public void MouseDragStart() {
    //    if ((entityDownedOn != null) && (entityDownedOn.IsIdle)) {
    //        playerAttackDragStarted = true;
    //    }
    //}

    //public void MouseDragUp() {
    //    StopCoroutine("ProcessDelayForTargetHold");
    //    TapHoldIndicator.Instance.StopProgress();
    //    if (entityDownedOn != null) {
    //        entityUppedOn = MouseRaycastForEntity();
    //        if ((entityUppedOn != null) && (entityUppedOn.IsIdle)) {
    //            if (entityUppedOn.EntityType == entityDownedOn.EntityType) {
    //                if (entityDownedOn.Data.canHeal) {
    //                    entityDownedOn.SetTarget(entityUppedOn);
    //                }
    //                else {
    //                    entityDownedOn.SetLineRendererToTarget();
    //                }
    //            }
    //            else {
    //                entityDownedOn.SetTarget(entityUppedOn);
    //            }
    //        }
    //        else {
    //            entityDownedOn.SetTarget(null);
    //        }
    //    }
    //    //activeLineRenderer = null;
    //    entityDownedOn = null;
    //    playerAttackDragStarted = false;
    //}
    #endregion EVENTS

    #endregion PUBLIC_METHODS

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonUp(1)) {
            CombatEntity entity = MouseRaycastForEntity();
            if (Input.GetKey(KeyCode.LeftShift)) {
                if (entity != null) {
                    AugmentData tempPotency = new AugmentData(AugmentType.POTENCY);
                    entity.TakeDamage(100, false, tempPotency, entity.Data.elementalAffinity, 0);
                }
            }
            else if (Input.GetKey(KeyCode.RightShift)) {
                if (entity != null) {
                    AugmentData tempPotency = new AugmentData(AugmentType.POTENCY);
                    entity.TakeDamage(100, false, tempPotency, entity.Data.elementalAffinity, 0);
                }
            }
            else if (Input.GetKey(KeyCode.LeftControl)) {
                if (entity != null) {
                    AugmentData tempPotency = new AugmentData(AugmentType.POTENCY);
                    entity.TakeDamage(entity.CurrentHealth - 1, false, tempPotency, ElementalType.NONE, 0);
                }
            }
            else {
                if (entity != null) {
                    //if (entity.IsIdle) {
                        entity.Die();
                    //}
                    //else {
                    //    Debug.Log("Don't right click entities to kill them if they're not idle!".Sized(16).Colored(CustomColor.GetColor(ColorName.ERROR_RED)));
                    //}
                }
            }
        }
    }

}
