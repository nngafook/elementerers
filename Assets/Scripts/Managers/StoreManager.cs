﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class StoreManager : MonoBehaviour {

    private List<StoreListItem> allStoreListItems;

    public GameObject storeListItemPrefab;
    public Transform storeListItemContainer;
    public Text currentScrapDisplayText;
    public CanvasGroup soldOutTextCG;

    [Header("Info Window")]
    public StoreItemInfoWindow infoWindow;

    void Awake() {
        UpdateCurrentScrapText();
    }

	// Use this for initialization
	void Start () {
        CreateStoreItems();
	}

    private void UpdateCurrentScrapText() {
        currentScrapDisplayText.text = GameProgress.Instance.CurrentScrap.ToString() + " sc";
    }

    private void CheckIfSoldOut() {
        soldOutTextCG.alpha = (allStoreListItems.Count == 0).AsInt();
    }

    private void ClearAllItems() {
        if (allStoreListItems != null) {
            for (int i = allStoreListItems.Count - 1; i >= 0; i--) {
                allStoreListItems[i].SeeYourselfOut();
                allStoreListItems[i] = null;
            }
        }
    }

    private void CreateStoreItems() {
        ClearAllItems();
        allStoreListItems = new List<StoreListItem>();
        SpecialAttackDataObject[] specialAttackDataObjects = DataManager.Instance.SpecialAttackDataObjects.OrderBy(dataObject => dataObject.data.storeCost).ToArray();
        StoreListItem item = null;
        SpecialAttackData data = null;
        for (int i = 0; i < specialAttackDataObjects.Length; i++) {
            data = specialAttackDataObjects[i].data;
            if (!SpecialAttackDataInventory.Instance.OwnsSpecialAttack(data.specialAttackType)) {
                item = Instantiate(storeListItemPrefab).GetComponent<StoreListItem>();
                item.SetRowBGVisible((i % 2 == 0));
                item.transform.SetParent(storeListItemContainer, false);
                allStoreListItems.Add(item);
                item.SetData(data, this);
            }
        }
        CheckIfSoldOut();
    }

    private void RefreshStoreItems() {
        SpecialAttackType type = SpecialAttackType.NONE;
        for (int i = 0; i < allStoreListItems.Count; i++) {
            type = allStoreListItems[i].SpecialAttackType;
            if (SpecialAttackDataInventory.Instance.OwnsSpecialAttack(type)) {
                allStoreListItems[i].SeeYourselfOut();
                allStoreListItems.RemoveAt(i);
                i = -1;
            }
            else {
                allStoreListItems[i].RefreshAvailableForPurchase();
                allStoreListItems[i].SetRowBGVisible((i % 2 == 0));
            }
        }
        CheckIfSoldOut();
    }

    public void ItemPurchased() {
        UpdateCurrentScrapText();
        RefreshStoreItems();
    }

    public void ShowInfo(SpecialAttackType type) {
        infoWindow.SetInfo(DataManager.Instance.SpecialAttackDataByType(type));
        infoWindow.Open();
    }

    public void DebugAddScrap() {
        GameProgress.Instance.AddToCurrentScrap(1000);
        ItemPurchased();
    }

    public void DebugRemoveScrap() {
        GameProgress.Instance.SaveCurrentScrap(0);
        ItemPurchased();
    }

    public void DebugClearInventory() {
        SpecialAttackDataInventory.Instance.Clear();
        CreateStoreItems();
    }

	// Update is called once per frame
	void Update () {
	
	}
}
