﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System;
using System.Collections.Generic;

public class LoadoutCameraManager : MonoBehaviour {

    private float slideSpeed = 0.35f;

    public Camera cam;
    public LoadoutCameraPosition currentCameraPosition;
    public LoadoutCameraPositionName currentCameraPositionName;

    public List<LoadoutCameraPosition> cameraPositions;

	// Use this for initialization
	void Start () {
	
	}

    public void OpenCombatScene() {
        LoadToScene.Load(SceneName.COMBAT);
    }

    public void OpenMainMenuScene() {
        LoadToScene.Load(SceneName.MAIN_MENU);
    }

    public void SetToMenu() {
        Goto(LoadoutCameraPositionName.MENU);
    }

    public void SetToParty() {
        Goto(LoadoutCameraPositionName.PARTY);
    }

    public void SetToStore() {
        Goto(LoadoutCameraPositionName.STORE);
    }

    public void SetToCraft() {
        Goto(LoadoutCameraPositionName.CRAFT);
    }

    private LoadoutCameraPosition GetCameraPosition(LoadoutCameraPositionName name) {
        LoadoutCameraPosition rVal = null;

        for (int i = 0; i < cameraPositions.Count; i++) {
            if (cameraPositions[i].positionName == name) {
                rVal = cameraPositions[i];
                break;
            }
        }

        return rVal;
    }

    public void EditorGoToCurrent() {
        LoadoutCameraPosition camPos = GetCameraPosition(currentCameraPositionName);
        cam.transform.position = camPos.cameraPosition;
    }

    public void Goto(LoadoutCameraPositionName positionName) {
        currentCameraPositionName = positionName;
        LoadoutCameraPosition camPos = GetCameraPosition(positionName);
        cam.transform.DOMove(camPos.cameraPosition, slideSpeed).SetEase(Ease.InQuad);
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.M)) {
            Goto(LoadoutCameraPositionName.MENU);
        }
	}
}

[Serializable]
public class LoadoutCameraPosition {
    public LoadoutCameraPositionName positionName;
    public Vector3 cameraPosition;
}

public enum LoadoutCameraPositionName {
    MENU,
    PARTY,
    STORE,
    CRAFT
}