﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
//using System.Linq;

public class PartyManager {

    private static PartyManager instance = null;
    public static PartyManager Instance { get { if (instance == null) { instance = new PartyManager(); } return instance; } }

    //private string textFilePath = "/CurrentParty.txt";
    //private string encodedFilePath = "/EncodedParty.txt";

    private int maxPartyMembers = 5;
    private CharacterID[] currentPartyIDs;

    public CharacterID[] CurrentPartyIDs { get { return currentPartyIDs; } }

    public PartyManager() {
        
    }

    private IEnumerator LoadSavedParty() {
        //ForceImport();
        LoadPartyFromProgress();
        yield return null;
    }

    private void LoadPartyFromProgress() {
        //currentPartyIDs = new CharacterID[maxPartyMembers];
        //CurrentPartyJsonObject jsonObj = JsonUtility.FromJson<CurrentPartyJsonObject>(json);
        List<CharacterID> partyIDs = GameProgress.Instance.CurrentPartyIDs;
        currentPartyIDs = partyIDs.ToArray();
        //for (int i = 0; i < partyIDs.Count; i++) {
        //    currentPartyIDs[i] = partyIDs[i];
        //}
    }

    public void RegisterLoadSavedParty() {
        //currentPartyIDs = new CharacterID[maxPartyMembers];
        // TODO: Make sure that this load at instantiation isn't interferring with the save or vice versa
        //LoadSavedParty();
        LoadManager.RegisterLoadRoutine(LoadSavedParty(), "Loading Party");
    }

    /// <summary>
    /// Editor only method... I hope
    /// </summary>
    public void ForceLoadSavedParty() {
        //! There is probably some lag/delay while this is fetched... good luck hiding it
        //ForceImport();
        LoadPartyFromProgress();
    }

    public void ClearSlot(int slot) {
        currentPartyIDs[slot] = CharacterID.NONE;
        //Export();
        GameProgress.Instance.SaveCurrentParty(currentPartyIDs);
    }

    public void ClearParty() {
        currentPartyIDs = new CharacterID[maxPartyMembers];
        for (int i = 0; i < maxPartyMembers; i++) {
            currentPartyIDs[i] = CharacterID.NONE;
        }
        //Export();
        GameProgress.Instance.SaveCurrentParty(currentPartyIDs);
    }

    //public CharacterData GetCharacterInSlot(int slot) {
    //    CharacterData rVal = null;

    //    return rVal;
    //}

    public void SetCharacterSlot(CharacterData data) {
        for (int i = 0; i < currentPartyIDs.Length; i++) {
            if (currentPartyIDs[i] == CharacterID.NONE) {
                currentPartyIDs[i] = data.characterID;
                break;
            }
        }

        //Export();
        GameProgress.Instance.SaveCurrentParty(currentPartyIDs);
    }

    public void SetCharacterSlot(int slot, CharacterData data) {
        currentPartyIDs[slot] = data.characterID;

        //Export();
        GameProgress.Instance.SaveCurrentParty(currentPartyIDs);
    }

    public void SwapIDs(int indexOne, int indexTwo) {
        CharacterID temp = currentPartyIDs[indexOne];

        currentPartyIDs[indexOne] = currentPartyIDs[indexTwo];
        currentPartyIDs[indexTwo] = temp;

        //Export();
        GameProgress.Instance.SaveCurrentParty(currentPartyIDs);
    }

    #region SAVING
    //public void Export() {
    //    CurrentPartyJsonObject jsonObject = new CurrentPartyJsonObject();
    //    jsonObject.CharacterDataList = new List<CharacterID>(currentPartyIDs);

    //    string propertiesJSON = JsonUtility.ToJson(jsonObject);
    //    Utility.WriteData(propertiesJSON, textFilePath);



    //    byte[] bytesToEncode = Encoding.UTF8.GetBytes(propertiesJSON);
    //    string encodedText = Convert.ToBase64String(bytesToEncode);

    //    Utility.WriteData(encodedText, encodedFilePath);

    //    Debug.Log(("Encrypted: " + encodedText).Bold().Colored(CustomColor.GetColor(ColorName.ERROR_RED)));
    //}

    //public void ForceImport() {
    //    string path = Application.persistentDataPath + textFilePath;
    //    WWW reader = new WWW("file:///" + path);
    //    while (!reader.isDone) {

    //    }
    //    Import(reader.text);

    //    path = Application.persistentDataPath + encodedFilePath;
    //    reader = new WWW("file:///" + path);
    //    while (!reader.isDone) {

    //    }

    //    if (!string.IsNullOrEmpty(reader.text)) {
    //        byte[] decodedBytes = Convert.FromBase64String(reader.text);
    //        string decodedText = Encoding.UTF8.GetString(decodedBytes);
    //        Debug.Log(("Decrypted: " + decodedText).Bold().Colored(CustomColor.GetColor(ColorName.ERROR_RED)));
    //    }
    //}

    //public void Import(string json) {
    //    if (!string.IsNullOrEmpty(json)) {
    //        currentPartyIDs = new CharacterID[maxPartyMembers];
    //        CurrentPartyJsonObject jsonObj = JsonUtility.FromJson<CurrentPartyJsonObject>(json);
    //        for (int i = 0; i < jsonObj.CharacterDataList.Count; i++) {
    //            currentPartyIDs[i] = jsonObj.CharacterDataList[i];
    //        }
    //        Debug.Log("Import Complete".Bold().Colored(CustomColor.GetColor(ColorName.ROB_BLUE)));
    //    }
    //}
    #endregion SAVING

}

//[Serializable]
//class CurrentPartyJsonObject {
//    public List<CharacterID> CharacterDataList;

//    public CurrentPartyJsonObject() {
//        CharacterDataList = new List<CharacterID>();
//    }
//}