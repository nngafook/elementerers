﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Mothership : MonoBehaviour {

    #region SINGLETON
    private static Mothership instance = null;
    private static bool isShuttingDown;
    public static Mothership Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<Mothership>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/Mothership"));
                    singleton.name = "Mothership";
                    instance = singleton.GetComponent<Mothership>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }
    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    #endregion SINGLETON

    #region SCENE_RELATED
    private WaitForSeconds shortWait = new WaitForSeconds(0.1f);
    private string currentScene = SceneName.MAIN_MENU;
    // Is used when the current scene becomes LOADING. destinationScene will be the scene
    // that is going to be loaded AFTER the LOADING scene is done doing its thing.
    private string destinationScene = SceneName.LOADOUT;
    private float fadeSpeed = 0.25f;
    // Is set to true the very first time awake is called. This is to make sure that the mothership is
    // in sync with the game, eg. if the game is run in the editor and starts in an arbitrary scene.
    private static bool firstAwakeOccurred = false;
    //- Stores a method to call when the fade to a scene is complete. This should ever only be one method.
    //- i.e. one method to kick off any actions for a scene.
    // TODO: variable description: MAYBE?
    private UnityAction sceneLoadCallback;
    #endregion SCENE_RELATED

    #region DATA_TRACKING
    private static bool dataObjectsLoaded = false;
    private static bool partyLoaded = false;
    #endregion DATA_TRACKING

    #region COMPONENTS
    public CanvasGroup sceneOverlayImageCG;
    #endregion COMPONENTS

    #region DEBUG
    [Space(10)]
    public bool logMessages = false;
    #endregion DEBUG

    #region PRIVATE_METHODS

    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }

        if (!firstAwakeOccurred) {
            firstAwakeOccurred = true;
            currentScene = SceneManager.GetActiveScene().name;
            destinationScene = currentScene;
            Signal("First Awake has occurred. CurrentScene is now: " + currentScene);
            EditorFirstAwakeLoad();
        }
    }

    #region UTILITY_METHODS
    private void Signal(string message) {
        if (logMessages) {
            Debug.Log(("~ MOTHERSHIP: " + message + " ~").Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
        }
    }
    private void SetSceneOverlayCGVisible(bool value) {
        sceneOverlayImageCG.alpha = value.AsInt();
        sceneOverlayImageCG.blocksRaycasts = value;
        sceneOverlayImageCG.interactable = value;
    }
    private void FadeSceneOverlay(bool value) {
        if (value) {    // if fade IN
            sceneOverlayImageCG.DOFade(1, fadeSpeed).OnComplete(OnSceneOverlayFadeInComplete);
        }
        else {          // if fade OUT
            sceneOverlayImageCG.DOFade(0, fadeSpeed).OnComplete(OnSceneOverlayFadeOutComplete);
        }
    }
    #endregion UTILITY_METHODS

    private void EditorFirstAwakeLoad() {
        if (currentScene != SceneName.MAIN_MENU) {
            SetSceneOverlayCGVisible(true);
            ForceLoadingProcedures();
            if (currentScene == SceneName.COMBAT) {
                CombatManager.Instance.ForceInit();
            }
            FadeSceneOverlay(false);
        }
    }

    private void OnSceneOverlayFadeInComplete() {
        //! Assuming that this is the entry point BEFORE the LOADING scene becomes active...
        // Everytime scene becomes LOADING, go through a list of things that need to be loaded
        // regardless of where we're going
        ProcessLoadingProcedures();

        // Fades to visible/black
        SetSceneOverlayCGVisible(true);
        if (sceneLoadCallback != null) {
            sceneLoadCallback.Invoke();
            sceneLoadCallback = null;
        }
    }

    private void OnSceneOverlayFadeOutComplete() {
        // Fades to invisible
        SetSceneOverlayCGVisible(false);

        switch (currentScene) {
            case SceneName.MAIN_MENU:

                break;
            case SceneName.LOADING:
                
                break;
            case SceneName.LOADOUT:
                
                break;
            case SceneName.COMBAT:
                LevelManager.Instance.BeginLevel();
                CombatUIManager.Instance.InitLevelProgress(LevelManager.Instance.EnemiesRemaining, LevelManager.Instance.TotalEnemiesInLevel);
                break;
            default:
                Signal("A scene was loaded that is not in the switch in OnSceneOverlayFadeoutComplete()");
                break;
        }
    }

    private IEnumerator ProcessSceneChange(string scene, string secondScene) {
        yield return shortWait;
        //! This can probably use default to do what happens when loadout is loaded. i.e. mothership should always fade black out
        currentScene = scene;
        destinationScene = secondScene;
        switch (currentScene) {
            case SceneName.MAIN_MENU:
                FadeSceneOverlay(false);
                break;
            case SceneName.LOADING:
                //SetCanvasGroupVisible(false, sceneOverlayImageCG);
                if (!string.IsNullOrEmpty(destinationScene) && destinationScene == SceneName.COMBAT) {
                    Signal("Combat Manager Init called cause destination scene is: " + destinationScene);
                    CombatManager.Instance.Init();
                }
                break;
            case SceneName.LOADOUT:
                SetSceneOverlayCGVisible(true);
                FadeSceneOverlay(false);
                break;
            case SceneName.DIALOGUE:
                SetSceneOverlayCGVisible(true);
                FadeSceneOverlay(false);
                break;
            case SceneName.COMBAT:
                SetSceneOverlayCGVisible(true);
                FadeSceneOverlay(false);
                break;
            default:
                Signal("A scene was loaded that is not in the switch in SceneWasLoaded()");
                break;
        }
    }

    private void ProcessLoadingProcedures() {
        if (!dataObjectsLoaded) {
            DataManager.Instance.RegisterLoadDataObjects();
            dataObjectsLoaded = true;
        }

        if (!partyLoaded) {
            PartyManager.Instance.RegisterLoadSavedParty();
            partyLoaded = true;
        }
    }

    /// <summary>
    /// Should only be used in the editor. For when the game is started NOT at the first scene.
    /// </summary>
    private void ForceLoadingProcedures() {
        if (!dataObjectsLoaded) {
            Signal("Forcing data objects load");
            DataManager.Instance.ForceLoadDataObjects();
            dataObjectsLoaded = true;
        }

        if (!partyLoaded) {
            Signal("Forcing party load");
            PartyManager.Instance.ForceLoadSavedParty();
            partyLoaded = true;
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void SceneWasLoaded(string scene, string secondScene = "") {
        // TODO: This may need a delay, as I think maybe scenes don't actually get loaded in the same frame
        // It did...
        StartCoroutine(ProcessSceneChange(scene, secondScene));
    }

    /// <summary>
    /// Is called when changing a scene. This should fade the overlay IN (i.e. fade to black)
    /// and only when the fade is complete, will the LoadToScene load the loading scene in.
    /// </summary>
    /// <param name="callback"></param>
    public void AwaitLoadPermission(UnityAction callback) {
        sceneLoadCallback = callback;
        FadeSceneOverlay(true);
    }

    #endregion PUBLIC_METHODS

}
