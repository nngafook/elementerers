﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public CanvasGroup mainMenuCG;

    public Text dataClearedLabel;

	void Awake() {
        mainMenuCG.alpha = 0;
        mainMenuCG.interactable = false;
        mainMenuCG.blocksRaycasts = false;
	}

    public void ShowButtons() {
        mainMenuCG.DOFade(1, 1f).OnComplete(OnCGFadeComplete);
    }

    private void OnCGFadeComplete() {
        mainMenuCG.interactable = true;
        mainMenuCG.blocksRaycasts = true;
    }

    public void ClearSaveData() {
        string path = System.IO.Path.Combine(Application.persistentDataPath, "Game-Progress.json");
        if (System.IO.File.Exists(path)) {
            System.IO.File.Delete(path);
            dataClearedLabel.DOKill();
            dataClearedLabel.RT().localScale = Vector2.zero;
            dataClearedLabel.color = Color.white;
            dataClearedLabel.text = "Game Progress Cleared";
            dataClearedLabel.RT().DOScale(1, 0.25f).SetEase(Ease.OutBack).OnComplete(OnDataClearedLabelFadeInComplete);
        }
        else {
            dataClearedLabel.DOKill();
            dataClearedLabel.RT().localScale = Vector2.zero;
            dataClearedLabel.color = Color.white;
            dataClearedLabel.text = "No Game Progress Found";
            dataClearedLabel.RT().DOScale(1, 0.25f).SetEase(Ease.OutBack).OnComplete(OnDataClearedLabelFadeInComplete);
        }
    }

    private void OnDataClearedLabelFadeInComplete() {
        dataClearedLabel.DOFade(0, 0.5f).SetDelay(1f);
    }

    public void LoadLoadout() {
        LoadToScene.Load(SceneName.LOADOUT);
    }

    public void LoadCombat() {
        LoadToScene.Load(SceneName.COMBAT);
    }

    public void LoadDialogue() {
        LoadToScene.Load(SceneName.DIALOGUE);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
