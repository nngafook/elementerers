﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class SplashScreen : MonoBehaviour {

    private bool scalingOut = false;
    private float scaleSpeed = 1f;
    private RectTransform maskRectTransform;
    private Vector2 fullMaskSize;
    private WaitForSeconds initialWait = new WaitForSeconds(1f);
    private WaitForSeconds waitBeforeClose = new WaitForSeconds(2f);

    public CanvasGroup splashCG;
    public MainMenu mainMenu;

    void Awake() {
        maskRectTransform = this.GetComponent<RectTransform>();
        fullMaskSize = maskRectTransform.sizeDelta;
        maskRectTransform.sizeDelta = Vector2.zero;
        
        splashCG.alpha = 1;
        splashCG.interactable = true;
        splashCG.blocksRaycasts = true;
    }

	// Use this for initialization
	IEnumerator Start () {
        yield return initialWait;
        maskRectTransform.DOSizeDelta(fullMaskSize, scaleSpeed).OnComplete(OnScaleUpComplete);
	}

    private void OnScaleUpComplete() {
        StartCoroutine(WaitToClose());
    }

    private IEnumerator WaitToClose() {
        yield return waitBeforeClose;
        if (!scalingOut) {
            maskRectTransform.DOSizeDelta(Vector2.zero, scaleSpeed).OnComplete(OnScaleDownComplete);
        }
    }

    private void OnScaleDownComplete() {
        StartCoroutine(WaitToRemoveBlackOverlay());
    }

    private IEnumerator WaitToRemoveBlackOverlay() {
        yield return initialWait;
        if (!scalingOut) {
            scalingOut = true;
            splashCG.DOFade(0, scaleSpeed).OnComplete(OnBlackOverlayRemoved);
        }
    }

    private void OnBlackOverlayRemoved() {
        scalingOut = true;
        splashCG.interactable = false;
        splashCG.blocksRaycasts = false;
        mainMenu.ShowButtons();
    }

    public void OnSplashScreenTapped() {
        if (!scalingOut) {
            scalingOut = true;
            splashCG.interactable = false;
            splashCG.blocksRaycasts = false;
            maskRectTransform.DOKill();
            maskRectTransform.DOSizeDelta(Vector2.zero, scaleSpeed).OnComplete(OnForcedScaleOutComplete);
            
        }
    }

    private void OnForcedScaleOutComplete() {
        splashCG.DOFade(0, scaleSpeed).OnComplete(OnBlackOverlayRemoved);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
