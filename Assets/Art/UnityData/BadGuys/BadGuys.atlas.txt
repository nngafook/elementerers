
BadGuys.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Tritch/FarEyebrow
  rotate: false
  xy: 680, 24
  size: 95, 82
  orig: 95, 82
  offset: 0, 0
  index: -1
Tritch/FarFoot
  rotate: false
  xy: 519, 158
  size: 189, 96
  orig: 189, 96
  offset: 0, 0
  index: -1
Tritch/FarHand
  rotate: false
  xy: 710, 108
  size: 167, 146
  orig: 167, 146
  offset: 0, 0
  index: -1
Tritch/Head
  rotate: false
  xy: 519, 2
  size: 159, 154
  orig: 159, 154
  offset: 0, 0
  index: -1
Tritch/NearEyebrow
  rotate: true
  xy: 879, 105
  size: 149, 87
  orig: 149, 87
  offset: 0, 0
  index: -1
Tritch/NearFoot
  rotate: false
  xy: 291, 62
  size: 195, 92
  orig: 195, 92
  offset: 0, 0
  index: -1
Tritch/NearHand
  rotate: false
  xy: 291, 156
  size: 226, 98
  orig: 226, 98
  offset: 0, 0
  index: -1
Tritch/Torso
  rotate: true
  xy: 2, 43
  size: 211, 287
  orig: 211, 287
  offset: 0, 0
  index: -1
