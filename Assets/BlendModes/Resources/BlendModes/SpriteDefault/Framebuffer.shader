Shader "BlendModes/SpriteDefault/Framebuffer"
{
	Properties
	{
		[PerRendererData] 
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		[MaterialToggle] 
		PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue" = "Transparent" 
			"IgnoreProjector" = "True" 
			"RenderType" = "Transparent" 
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass
		{
			CGPROGRAM
			
			#include "UnityCG.cginc"
			#include "../BlendModes.cginc"

			#pragma only_renderers framebufferfetch
			#pragma target 3.0			
			#pragma multi_compile BmDarken BmMultiply BmColorBurn BmLinearBurn BmDarkerColor BmLighten BmScreen BmColorDodge BmLinearDodge BmLighterColor BmOverlay BmSoftLight BmHardLight BmVividLight BmLinearLight BmPinLight BmHardMix BmDifference BmExclusion BmSubtract BmDivide BmHue BmSaturation BmColor BmLuminosity
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#pragma vertex ComputeVertex
			#pragma fragment ComputeFragment
			
			sampler2D _MainTex;
			fixed4 _Color;
			
			struct VertexInput
			{
				float4 Vertex : POSITION;
				float2 TexCoord : TEXCOORD0;
			};

			struct VertexOutput
			{
				float4 Vertex : SV_POSITION;
				float2 TexCoord : TEXCOORD0;
			};
			
			VertexOutput ComputeVertex (VertexInput vertexInput)
			{
				VertexOutput vertexOutput;
				
				vertexOutput.Vertex = mul(UNITY_MATRIX_MVP, vertexInput.Vertex);
				vertexOutput.TexCoord = vertexInput.TexCoord;
				#ifdef PIXELSNAP_ON
				vertexOutput.Vertex = UnityPixelSnap(vertexOutput.Vertex);
				#endif
							
				return vertexOutput;
			}
			
			void ComputeFragment (VertexOutput vertexOutput, inout fixed4 bufferColor : SV_Target)
			{
			    fixed4 texColor = tex2D(_MainTex, vertexOutput.TexCoord) * _Color;
				fixed4 grabColor = bufferColor;
				
				#include "../BlendOps.cginc"

				bufferColor = blendResult;
			} 
			
			ENDCG
		}
	}
	
	Fallback "Sprites/Default"
	CustomEditor "BmMaterialEditor"
}
