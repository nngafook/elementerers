﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;

public class DialogueConversationSelectWindow : ScriptableObject {

    private float padding = 10;
    private string dataObjectsPath = "Data/DialogueData";

    private Action<DialogueConversationDataObject> OnCloseCallback;

    private DialogueConversationDataObject selectedDataObject;
    private Vector2 buttonSize = new Vector2(80, 40);
    private DialogueConversationDataObject[] dataObjects;
    private Vector2 scrollPosition;

    public Rect windowRect;
    public string windowTitle = "Open Conversation";
    public Vector2 minSize = new Vector2(420, 500);

    private void CloseWindow() {
        if (OnCloseCallback != null) {
            OnCloseCallback(selectedDataObject);
        }
    }

    public void Init(ref DialogueConversationDataObject obj) {
        if (obj != null) {
            selectedDataObject = obj;
        }
        dataObjects = Resources.LoadAll<DialogueConversationDataObject>(dataObjectsPath);
    }

    public void SetCallbacks(Action<DialogueConversationDataObject> closeCallback) {
        OnCloseCallback = closeCallback;
    }

    private void DrawConversationButtons() {
        int column = 0;
        int totalColumns = 3;
        string title = "";

        GUILayout.BeginVertical();

        for (int i = 0; i < dataObjects.Length; i++) {
            if (column == 0) {
                GUILayout.BeginHorizontal();
            }

            title = dataObjects[i].conversationTitle;
            GUI.color = (selectedDataObject != null && selectedDataObject == dataObjects[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(title, GUILayout.Width(buttonSize.x), GUILayout.Height(buttonSize.y))) {
                if (selectedDataObject == dataObjects[i]) {
                    selectedDataObject = null;
                }
                else {
                    selectedDataObject = dataObjects[i];
                }
            }

            column++;
            if (column == totalColumns) {
                GUILayout.EndHorizontal();
                column = 0;
            }
        }
        if (column < totalColumns && column != 0) {
            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();
    }
    

    public void DrawWindow() {
        //shiftHeld = Event.current.shift;

        GUILayout.BeginVertical();
        GUILayout.Space(padding);

        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        DrawConversationButtons();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();

        //GUILayout.BeginHorizontal();
        //GUILayout.FlexibleSpace();
        //GUILayout.Label(Utility.CharacterIDToReadable(characterIDOptions[selectedBtnIndex].ToString()));
        //GUILayout.FlexibleSpace();
        //GUILayout.EndHorizontal();

        //! OK BUTTON
        GUILayout.Space(padding);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button((selectedDataObject != null) ? "Load " + selectedDataObject.conversationTitle : "Close", GUILayout.Width(100))) {
            CloseWindow();
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();



        GUILayout.EndVertical();
    }
    
}
