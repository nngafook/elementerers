﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TextFieldReturnWindow : ScriptableObject {

    private float padding = 10;
    private string defaultText = "";
    private string inputText = "";

    private Action<string> OnCloseCallback;

    public Rect windowRect;
    public string windowTitle = "Confirm Text";
    public Vector2 minSize = new Vector2(400, 100);

    private void CloseWindow() {
        if (OnCloseCallback != null) {
            OnCloseCallback(inputText);
        }
    }

    public void Init(string txt) {
        defaultText = txt;
        inputText = defaultText;
    }

    public void SetCallbacks(Action<string> closeCallback) {
        OnCloseCallback = closeCallback;
    }

    public void DrawWindow() {
        //shiftHeld = Event.current.shift;

        GUILayout.BeginVertical();
        GUILayout.Space(padding);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        inputText = GUILayout.TextField(inputText, GUILayout.Width(200));

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        //! OK BUTTON
        GUILayout.Space(padding);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            inputText = defaultText;
            CloseWindow();
        }
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("Confirm", GUILayout.Width(100))) {
            CloseWindow();
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }
}
