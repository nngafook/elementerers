﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using System;
using System.IO;

public class DialogueEditorWindow : EditorWindow {

    private static DialogueConversationDataObject loadedDataObject;
    private Action<string> OnTextFieldReturned;

    private static string prefsLastOpenKey = "lastOpenedConversation";

    private float padding = 5;
    private float lineHeight = 30;
    private float contentLabelWidth = 150;
    private float contentButtonWidth = 100;
    private float contentTextFieldWidth = 205;
    private float toolbarButtonWidth = 100;
    private float toolbarButtonHeight = 30;
    private bool viewingLeftBlocks = false;
    private bool viewingRightBlocks = false;
    private bool inputDisabled = false;
    private string assetPathToRename = "";

    private Color insertColorTagColor = Color.white;
    private GUISkin myGUISkin;
    private GUISkin baseGUISkin;
    private Vector2 linesScrollPosition;
    private Vector2 dialogueTextScrollPosition;
    private Texture2D windowBGTexture;
    private DialogueBlockDataObject selectedBlock;
    private TextFieldReturnWindow textFieldReturnWindow;
    private List<DialogueBlockDataObject> selectedBlockList;
    private DialogueConversationSelectWindow conversationSelectWindow;

    private float ContentLeftWidth { get { return position.width / 3; } }
    private float ContentRightWidth { get { return position.width - ContentLeftWidth; } }
    private float WindowWidth { get { return position.width; } }
    private float WindowHeight { get { return position.height; } }
    private float ToolbarHeight { get { return 50; } }

    void Awake() {
        this.minSize = new Vector2(1400, 800);
        InitTextures();
        GetGUISkin();
        linesScrollPosition = Vector2.zero;
        Undo.undoRedoPerformed -= OnUndoRedoPerformed;
        Undo.undoRedoPerformed += OnUndoRedoPerformed;

        if (EditorPrefs.HasKey(prefsLastOpenKey)) {
            loadedDataObject = AssetDatabase.LoadAssetAtPath<DialogueConversationDataObject>(EditorPrefs.GetString(prefsLastOpenKey));
        }
    }

    void OnEnable() {
        InitTextures();
        GetGUISkin();
        linesScrollPosition = Vector2.zero;
        Undo.undoRedoPerformed -= OnUndoRedoPerformed;
        Undo.undoRedoPerformed += OnUndoRedoPerformed;

        if (EditorPrefs.HasKey(prefsLastOpenKey)) {
            loadedDataObject = AssetDatabase.LoadAssetAtPath < DialogueConversationDataObject>(EditorPrefs.GetString(prefsLastOpenKey));
        }
    }

    private void OnUndoRedoPerformed() {
        Repaint();
    }

    void OnDestroy() {
        if (conversationSelectWindow != null) {
            DestroyImmediate(conversationSelectWindow);
            conversationSelectWindow = null;
            inputDisabled = false;
        }
    }

    private void InitTextures() {
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.6039216f, 0.6666667f, 0.6901961f, 1f));
        windowBGTexture.Apply();
    }

    private void GetGUISkin() {
        myGUISkin = (GUISkin)AssetDatabase.LoadAssetAtPath("Assets/Resources/GameDataEditorGUISkin.guiskin", typeof(GUISkin));
    }

    // ================================================ //

    #region EDITOR_MENU_METHODS
    [MenuItem("Game Tools/Dialogue Editor %&d")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        DialogueEditorWindow window = (DialogueEditorWindow)EditorWindow.GetWindow(typeof(DialogueEditorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    // ================================================ //

    #region MISC_METHODS

    private void OpenConversationSelectWindow() {
        if (conversationSelectWindow == null) {
            conversationSelectWindow = CreateInstance<DialogueConversationSelectWindow>();

            conversationSelectWindow.Init(ref loadedDataObject);
            conversationSelectWindow.SetCallbacks(OnConversationSelectWindowClosed);
            conversationSelectWindow.windowRect = new Rect((Screen.width / 2) - (conversationSelectWindow.minSize.x / 2), (Screen.height / 2) - (conversationSelectWindow.minSize.y / 2), conversationSelectWindow.minSize.x, conversationSelectWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OpenTextFieldReturnWindow(string txt) {
        if (textFieldReturnWindow == null) {
            textFieldReturnWindow = CreateInstance<TextFieldReturnWindow>();

            textFieldReturnWindow.Init(txt);
            textFieldReturnWindow.SetCallbacks(OnTextFieldReturnWindowClosed);
            textFieldReturnWindow.windowRect = new Rect((Screen.width / 2) - (textFieldReturnWindow.minSize.x / 2), (Screen.height / 2) - (textFieldReturnWindow.minSize.y / 2), textFieldReturnWindow.minSize.x, textFieldReturnWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnConversationSelectWindowClosed(DialogueConversationDataObject dataObject) {
        conversationSelectWindow = null;
        inputDisabled = false;
        linesScrollPosition = Vector2.zero;
        viewingLeftBlocks = false;
        viewingRightBlocks = false;
        selectedBlock = null;
        selectedBlockList = null;
        if (dataObject != null) {
            UnloadDataObject();
            loadedDataObject = dataObject;
            EditorPrefs.SetString(prefsLastOpenKey, AssetDatabase.GetAssetPath(loadedDataObject));
        }
    }

    private void OnTextFieldReturnWindowClosed(string txt) {
        textFieldReturnWindow = null;
        inputDisabled = false;

        if (OnTextFieldReturned != null) {
            OnTextFieldReturned(txt);
        }
    }

    private void AddNewDialogueBlock(ref List<DialogueBlockDataObject> list) {
        string loadedAssetPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(loadedDataObject));

        DialogueBlockDataObject newBlock = ScriptableObject.CreateInstance<DialogueBlockDataObject>();
        AssetDatabase.CreateAsset(newBlock, loadedAssetPath + "/NewTestConversation_" + UnityEngine.Random.Range(0, 100) + ".asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        list.Add(newBlock);

        selectedBlock = newBlock;
    }

    private void RenameBlock(DialogueBlockDataObject dataObject) {
        OpenTextFieldReturnWindow(dataObject.name);

        assetPathToRename = AssetDatabase.GetAssetPath(dataObject);

        OnTextFieldReturned = OnTextFieldReturnRenameBlock;
    }

    private void OnTextFieldReturnRenameBlock(string txt) {
        AssetDatabase.RenameAsset(assetPathToRename, txt);
        assetPathToRename = "";
        OnTextFieldReturned = null;

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }

    private void UnloadDataObject() {
        //!+ prompt for save

    }

    #endregion MISC_METHODS

    // ================================================ //

    #region DRAW_FIELD_METHODS
    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }
    #endregion DRAW_FIELD_METHODS

    // ================================================ //

    #region DRAW_METHODS

    private void DrawSelectWindow(int id) {
        if (conversationSelectWindow != null) {
            conversationSelectWindow.DrawWindow();
        }
        if (textFieldReturnWindow != null) {
            textFieldReturnWindow.DrawWindow();
        }
        GUI.DragWindow();
    }

    private void DrawToolbar() {
        GUILayout.BeginArea(new Rect(0, 0, WindowWidth, ToolbarHeight));
        GUILayout.BeginArea(new Rect(padding * 2, padding * 2, WindowWidth - (padding * 4), ToolbarHeight));
        
        GUILayout.BeginHorizontal();

        GUI.color = CustomColor.GetColor(ColorName.WARNING_YELLOW);
        if (GUILayout.Button("Open Conversation", GUILayout.Width(toolbarButtonWidth), GUILayout.Height(toolbarButtonHeight))) {
            OpenConversationSelectWindow();
        }

        GUI.color = Color.white;

        GUILayout.EndHorizontal();
        
        GUILayout.EndArea();
        GUILayout.EndArea();
    }

    private void DrawConversationInfo() {
        if (loadedDataObject) {
            Undo.RecordObject(loadedDataObject, "Dialogue Conversation");

            GUILayout.BeginArea(new Rect(0, ToolbarHeight, WindowWidth, WindowHeight - ToolbarHeight));
            GUILayout.BeginArea(new Rect(padding, padding, WindowWidth, WindowHeight - ToolbarHeight));

            GUILayout.BeginVertical();

            GUILayout.BeginArea(new Rect(padding, 0, ContentLeftWidth - (padding * 2), WindowHeight - ToolbarHeight - (padding * 2)));
            DrawConversationTitle();
            DrawSpeakerSideOptions();
            DrawSpeakerNames();
            DrawDialogueSelectBlockOptions();
            DrawDialogueBlockList();
            GUILayout.EndArea();

            GUILayout.BeginArea(new Rect(ContentLeftWidth, 0, ContentRightWidth - (padding * 2), WindowHeight - ToolbarHeight - (padding * 2)));
            DrawBlockInfo();
            DrawBlockLines();
            GUILayout.EndArea();

            GUILayout.EndVertical();

            GUILayout.EndArea();
            GUILayout.EndArea();
        }
    }

    private void DrawConversationTitle() {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Conversation Title", GUILayout.Width(contentLabelWidth));
        loadedDataObject.conversationTitle = GUILayout.TextField(loadedDataObject.conversationTitle, "LeftTextField", GUILayout.Width(contentTextFieldWidth), GUILayout.Height(lineHeight));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    private void DrawSpeakerSideOptions() {
        GUILayout.BeginHorizontal();
        GUILayout.Label("First Speaker", GUILayout.Width(contentLabelWidth));
        foreach (SpeakerSide side in Enum.GetValues(typeof(SpeakerSide))) {
            GUI.color = (side == loadedDataObject.firstSpeaker) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(Utility.EnumNameToReadable(side.ToString()), GUILayout.Width(contentButtonWidth))) {
                loadedDataObject.firstSpeaker = side;
            }
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUI.color = Color.white;
    }

    private void DrawSpeakerNames() {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Left Speaker", GUILayout.Width(contentLabelWidth));
        loadedDataObject.leftSpeaker = GUILayout.TextField(loadedDataObject.leftSpeaker, "LeftTextField", GUILayout.Width(contentTextFieldWidth), GUILayout.Height(lineHeight));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Right Speaker", GUILayout.Width(contentLabelWidth));
        loadedDataObject.rightSpeaker = GUILayout.TextField(loadedDataObject.rightSpeaker, "LeftTextField", GUILayout.Width(contentTextFieldWidth), GUILayout.Height(lineHeight));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    private void DrawDialogueSelectBlockOptions() {
        GUILayout.Space(padding * 2);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUI.color = (viewingLeftBlocks) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("View " + loadedDataObject.leftSpeaker + " Blocks", GUILayout.Width(contentLabelWidth))) {
            selectedBlock = null;
            viewingRightBlocks = false;
            if (selectedBlockList == loadedDataObject.leftSpeakerBlocks) {
                selectedBlockList = null;
                viewingLeftBlocks = false;
            }
            else {
                selectedBlockList = loadedDataObject.leftSpeakerBlocks;
                viewingLeftBlocks = true;
            }
        }
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("Add Left Dialogue Block", GUILayout.Width(contentLabelWidth))) {
            AddNewDialogueBlock(ref loadedDataObject.leftSpeakerBlocks);
            viewingLeftBlocks = true;
            viewingRightBlocks = false;
            selectedBlockList = loadedDataObject.leftSpeakerBlocks;
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUI.color = (viewingRightBlocks) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("View " + loadedDataObject.rightSpeaker + " Blocks", GUILayout.Width(contentLabelWidth))) {
            selectedBlock = null;
            viewingLeftBlocks = false;
            if (selectedBlockList == loadedDataObject.rightSpeakerBlocks) {
                selectedBlockList = null;
                viewingRightBlocks = false;
            }
            else {
                selectedBlockList = loadedDataObject.rightSpeakerBlocks;
                viewingRightBlocks = true;
            }
        }
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("Add Right Dialogue Block", GUILayout.Width(contentLabelWidth))) {
            AddNewDialogueBlock(ref loadedDataObject.rightSpeakerBlocks);
            viewingLeftBlocks = false;
            viewingRightBlocks = true;
            selectedBlockList = loadedDataObject.rightSpeakerBlocks;
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        GUI.color = Color.white;
    }

    private void DrawDialogueBlockList() {
        if (selectedBlockList != null) {
            GUILayout.Space(padding * 4);
            GUILayout.BeginVertical();
            for (int i = 0; i < selectedBlockList.Count; i++) {
                if (selectedBlockList[i] != null) {
                    GUILayout.BeginHorizontal();
                    GUI.color = (selectedBlock != null && selectedBlock == selectedBlockList[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
                    if (GUILayout.Button(selectedBlockList[i].name, GUILayout.Width(contentLabelWidth))) {
                        selectedBlock = selectedBlockList[i];
                        linesScrollPosition = Vector2.zero;
                    }
                    GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
                    if (GUILayout.Button("Rename", GUILayout.Width(contentButtonWidth))) {
                        RenameBlock(selectedBlockList[i]);
                    }
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.EndVertical();
        }
        GUI.color = Color.white;
    }

    private void DrawBlockInfo() {
        if (selectedBlock != null) {
            Undo.RecordObject(selectedBlock, "Dialogue Block");

            GUILayout.BeginVertical();

            //! End of conversation bool
            GUILayout.BeginHorizontal();
            GUI.color = CustomColor.GetColor(ColorName.PRS_PURPLE);
            GUILayout.Label("End of Conversation", GUILayout.Width(contentLabelWidth));
            GUI.color = Color.white;

            GUI.color = (selectedBlock.endOfConversation) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button("True", GUILayout.Width(contentButtonWidth))) {
                selectedBlock.endOfConversation = true;
            }
            GUI.color = (!selectedBlock.endOfConversation) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button("False", GUILayout.Width(contentButtonWidth))) {
                selectedBlock.endOfConversation = false;
            }
            GUI.color = Color.white;

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Label("Lines", "Header", GUILayout.Width(contentLabelWidth));

            //! Add Line Button
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUI.color = CustomColor.GetColor(ColorName.YALLOW);
            if (GUILayout.Button("Add Line", GUILayout.Width(contentButtonWidth))) {
                DialogueLine newLine = new DialogueLine();
                selectedBlock.lines.Add(newLine);
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();


            GUILayout.EndVertical();
        }
    }

    private void DrawBlockLines() {
        if (selectedBlock != null) {
            GUILayout.Space(20);

            List<DialogueLine> lines = selectedBlock.lines;
            GUILayout.BeginVertical();

            linesScrollPosition = GUILayout.BeginScrollView(linesScrollPosition);

            dialogueTextScrollPosition = GUILayout.BeginScrollView(dialogueTextScrollPosition);
            
            for (int i = 0; i < lines.Count; i++) {
                DrawBlockLineInfo(lines[i], i);
            }

            GUILayout.EndScrollView();

            GUILayout.EndScrollView();

            GUILayout.EndVertical();
        }
    }

    private void DrawBlockLineInfo(DialogueLine line, int lineIndex) {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Line " + (lineIndex + 1), "Header", GUILayout.Width(contentLabelWidth));
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Remove", GUILayout.Width(contentButtonWidth))) {
            selectedBlock.lines.RemoveAt(lineIndex);
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        GUILayout.Label("Emotion", GUILayout.Width(contentLabelWidth));
        GUI.color = Color.white;
        foreach (EmotionName emotion in Enum.GetValues(typeof(EmotionName))) {
            GUI.color = (emotion == line.emotion) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(emotion.ToString(), GUILayout.Width(contentButtonWidth))) {
                line.emotion = emotion;
            }
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        GUILayout.Label("Animation", GUILayout.Width(contentLabelWidth));
        GUI.color = Color.white;
        foreach (DialogueSpeakerAnimation animation in Enum.GetValues(typeof(DialogueSpeakerAnimation))) {
            GUI.color = (animation == line.startAnimation) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(animation.ToString(), GUILayout.Width(contentButtonWidth))) {
                line.startAnimation = animation;
            }
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        GUILayout.Space(10);
        GUILayout.Label("Transition", "Header", GUILayout.Width(contentLabelWidth));

        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        GUILayout.Label("Transition Type", GUILayout.Width(contentLabelWidth));
        GUI.color = Color.white;
        foreach (DialogueLineTransition transition in Enum.GetValues(typeof(DialogueLineTransition))) {
            GUI.color = (transition == line.lineTransition) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(transition.ToString(), GUILayout.Width(contentButtonWidth))) {
                line.lineTransition = transition;
            }
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        if (line.lineTransition == DialogueLineTransition.FadeToBlackWithText) {
            GUILayout.BeginHorizontal();
            GUI.color = CustomColor.GetColor(ColorName.YALLOW);
            if (GUILayout.Button("Add Transition Line", GUILayout.Width(contentButtonWidth))) {
                line.transitionLines.Add("");
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            for (int i = 0; i < line.transitionLines.Count; i++) {
                GUILayout.BeginHorizontal();
                
                GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
                GUILayout.Label("Transition Line " + (i + 1).ToString(), GUILayout.Width(contentLabelWidth));
                GUILayout.FlexibleSpace();
                GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                if (GUILayout.Button("Remove Line", GUILayout.Width(contentButtonWidth))) {
                    line.transitionLines.RemoveAt(i);
                    break;
                }
                GUI.color = Color.white;                
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
                GUILayout.Label("Text", GUILayout.Width(contentLabelWidth));
                GUI.color = Color.white;
                GUI.skin.customStyles[2].alignment = TextAnchor.UpperLeft;
                line.transitionLines[i] = GUILayout.TextArea(line.transitionLines[i], "LeftTextField", GUILayout.Height(GUI.skin.textField.lineHeight * 3));
                GUI.skin.customStyles[2].alignment = TextAnchor.MiddleLeft;
                GUILayout.EndHorizontal();
                
            }
        }

        GUILayout.Space(10);
        GUILayout.Label("Dialogue", "Header", GUILayout.Width(contentLabelWidth));

        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        GUILayout.Label("Text", GUILayout.Width(contentLabelWidth));
        GUI.color = Color.white;
        GUI.skin.customStyles[2].alignment = TextAnchor.UpperLeft;
        line.text = GUILayout.TextArea(line.text, "LeftTextField", GUILayout.Height(GUI.skin.textField.lineHeight * 3));
        GUI.skin.customStyles[2].alignment = TextAnchor.MiddleLeft;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        TextEditor editor = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);

        GUI.skin = baseGUISkin;
        insertColorTagColor = EditorGUILayout.ColorField(insertColorTagColor);
        GUI.skin = myGUISkin;

        if (GUILayout.Button("Insert Color Tags")) {
            string newColorString = "";
            newColorString = ColorUtility.ToHtmlStringRGBA(insertColorTagColor);
            string preTag = "<color=" + "\"#" + newColorString + "\">";
            string postTag = "</color>";

            string selectedText = editor.SelectedText;
            int startIndex = editor.selectIndex;
            int selectedLength = selectedText.Length;

            line.text = line.text.Remove(editor.selectIndex, selectedLength);
            line.text = line.text.Insert(startIndex, preTag + selectedText + postTag);
        }
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }

    #endregion DRAW_METHODS

    // ================================================ //

    void OnGUI() {
        if (windowBGTexture == null) {
            InitTextures();
        }

        baseGUISkin = GUI.skin;
        GUI.skin = myGUISkin;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);

        EditorGUI.BeginDisabledGroup(inputDisabled);
        GUILayout.BeginVertical();

        DrawToolbar();
        DrawConversationInfo();

        GUILayout.EndVertical();
        EditorGUI.EndDisabledGroup();

        if (GUI.changed) {
            if (loadedDataObject != null) {
                EditorUtility.SetDirty(loadedDataObject);
            }
            if (selectedBlock != null) {
                EditorUtility.SetDirty(selectedBlock);
            }
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }

        BeginWindows();
        if (conversationSelectWindow != null) {
            conversationSelectWindow.windowRect = GUILayout.Window(0, conversationSelectWindow.windowRect, DrawSelectWindow, conversationSelectWindow.windowTitle);
        }
        if (textFieldReturnWindow != null) {
            textFieldReturnWindow.windowRect = GUILayout.Window(0, textFieldReturnWindow.windowRect, DrawSelectWindow, textFieldReturnWindow.windowTitle);
        }
        EndWindows();

        GUI.skin = baseGUISkin;
    }

}
