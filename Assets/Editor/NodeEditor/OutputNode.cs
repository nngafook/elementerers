﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class OutputNode : BaseNode {

    private string result = "";

    private BaseInputNode inputNode;
    private Rect inputNodeRect;

    public OutputNode() {
        windowTitle = "Output Node";
        hasInputs = true;
    }

    public override void DrawWindow() {
        base.DrawWindow();

        Event e = Event.current;
        string inputOneTitle = "None";
        if (inputNode) {
            inputOneTitle = inputNode.GetResult();
        }

        GUILayout.Label("Input 1: " + inputOneTitle);
        if (e.type == EventType.Repaint) {
            inputNodeRect = GUILayoutUtility.GetLastRect();
        }

        GUILayout.Label("Result: " + result);
    }

    public override void DrawCurves() {
        if (inputNode) {
            Rect rect = windowRect;
            rect.x += inputNodeRect.x;
            rect.y += inputNodeRect.y + inputNodeRect.height / 2;
            rect.width = 1;
            rect.height = 1;

            NodeEditor.DrawNodeCurve(inputNode.windowRect, rect);
        }
    }

    public override void NodeDeleted(BaseNode node) {
        if (node.Equals(inputNode)) {
            inputNode = null;
        }
    }

    public override BaseInputNode ClickedOnInput(Vector2 pos) {
        BaseInputNode rVal = null;

        pos.x -= windowRect.x;
        pos.y -= windowRect.y;

        if (inputNodeRect.Contains(pos)) {
            rVal = inputNode;
            inputNode = null;
        }

        return rVal;
    }

    public override void SetInput(BaseInputNode input, Vector3 clickPos) {
        clickPos.x -= windowRect.x;
        clickPos.y -= windowRect.y;

        if (inputNodeRect.Contains(clickPos)) {
            inputNode = input;
        }
    }

}
