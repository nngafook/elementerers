﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ComparisonNode : BaseInputNode {

    public enum ComparisonType {
        Greater,
        Less,
        Equal
    }

    private ComparisonType comparisonType;

    private BaseInputNode inputOne;
    private Rect inputOneRect;

    private BaseInputNode inputTwo;
    private Rect inputTwoRect;

    public ComparisonNode() {
        windowTitle = "Comparison Node";
        hasInputs = true;
    }

    public override void DrawWindow() {
        base.DrawWindow();

        Event e = Event.current;
        comparisonType = (ComparisonType)EditorGUILayout.EnumPopup("Comparison Type", comparisonType);

        string inputOneTitle = "None";
        if (inputOne) {
            inputOneTitle = inputOne.GetResult();
        }

        GUILayout.Label("Input 1: " + inputOneTitle);
        if (e.type == EventType.Repaint) {
            inputOneRect = GUILayoutUtility.GetLastRect();
        }

        string inputTwoTitle = "None";
        if (inputTwo) {
            inputTwoTitle = inputTwo.GetResult();
        }

        GUILayout.Label("Input 2: " + inputTwoTitle);
        if (e.type == EventType.Repaint) {
            inputTwoRect = GUILayoutUtility.GetLastRect();
        }
    }

    public override void SetInput(BaseInputNode input, Vector3 clickPos) {
        clickPos.x -= windowRect.x;
        clickPos.y -= windowRect.y;

        if (inputOneRect.Contains(clickPos)) {
            inputOne = input;
        }
        else if (inputTwoRect.Contains(clickPos)) {
            inputTwo = input;
        }
    }

    public override string GetResult() {
        float inputOneValue = 0;
        float inputTwoValue = 0;

        if (inputOne) {
            string inputOneRaw = inputOne.GetResult();
            float.TryParse(inputOneRaw, out inputOneValue);
        }

        if (inputTwo) {
            string inputTwoRaw = inputTwo.GetResult();
            float.TryParse(inputTwoRaw, out inputTwoValue);
        }

        string result = "false";

        switch (comparisonType) {
            case ComparisonType.Greater:
                if (inputOneValue > inputTwoValue) {
                    result = "true";
                }
                break;
            case ComparisonType.Less:
                if (inputOneValue < inputTwoValue) {
                    result = "true";
                }
                break;
            case ComparisonType.Equal:
                if (inputOneValue == inputTwoValue) {
                    result = "true";
                }
                break;
        }

        return result;
    }

    public override void NodeDeleted(BaseNode node) {
        if (node.Equals(inputOne)) {
            inputOne = null;
        }
        if (node.Equals(inputTwo)) {
            inputTwo = null;
        }
    }

    public override BaseInputNode ClickedOnInput(Vector2 pos) {
        BaseInputNode rVal = null;

        pos.x -= windowRect.x;
        pos.y -= windowRect.y;

        if (inputOneRect.Contains(pos)) {
            rVal = inputOne;
            inputOne = null;
        }
        else if (inputTwoRect.Contains(pos)) {
            rVal = inputTwo;
            inputTwo = null;
        }

        return rVal;
    }

    public override void DrawCurves() {
        if (inputOne) {
            Rect rect = windowRect;
            rect.x += inputOneRect.x;
            rect.y += inputOneRect.y + inputOneRect.height / 2;
            rect.width = 1;
            rect.height = 1;

            NodeEditor.DrawNodeCurve(inputOne.windowRect, rect);
        }

        if (inputTwo) {
            Rect rect = windowRect;
            rect.x += inputTwoRect.x;
            rect.y += inputTwoRect.y + inputTwoRect.height / 2;
            rect.width = 1;
            rect.height = 1;

            NodeEditor.DrawNodeCurve(inputTwo.windowRect, rect);
        }
    }

}
