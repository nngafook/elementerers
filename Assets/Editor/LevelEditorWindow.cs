﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;
using UnityEditor.SceneManagement;
using System;
using Object = UnityEngine.Object;
using UnityEditorInternal;
using System.IO;
using System.Linq;

#pragma warning disable 0108, 0219, 0414, 0472

public class LevelEditorWindow : EditorWindow {

    private static LevelEditorWindow instance;

    private Texture2D meleeAttackTexture;
    private Texture2D rangedAttackTexture;
    private Texture2D windowBGTexture;
    private Texture2D bodyPanelBGTexture;
    private Texture2D toolbarBGTexture;

    private Vector2 dataObjectsScrollPosition;
    private Vector2 levelDataPropertiesScrollPosition;
    private Vector2 enemyIconScrollPosition;

    private SerializedObject selectedLevelDataSerializedObject;
    private LevelDataObject selectedLevelDataObject;

    private int enemyIconDimensions = 60;
    private int selectedFileIndex = -1;

    private string newFilename;

    private float windowEdgePadding = 5;
    private float padding = 10;
    private float buttonPanelHeight = 70;
    private float buttonDimension = 60;

    private LevelDataObject[] levelDataList;
    private CharacterDataObject[] enemyDataObjects;
    private List<Texture2D> enemyIcons;

    private ReorderableList levelEnemies;

    private float WindowWidth { get { return (position.width - (windowEdgePadding * 2)); } }
    private float BodyHeight { get { return (position.height - buttonPanelHeight - (windowEdgePadding * 3)); } }
    private float BodyY { get { return (windowEdgePadding * 2) + buttonPanelHeight; } }
    private float FileColumnWidth { get { return WindowWidth / 5; } }
    private float LevelPropertiesColumnWidth { get { return WindowWidth / 2; } }
    private float LevelPropertiesColumnX { get { return FileColumnWidth; } }
    private float ThirdColumnX { get { return FileColumnWidth + LevelPropertiesColumnWidth; } }
    private float ThirdColumnWidth { get { return WindowWidth - FileColumnWidth - LevelPropertiesColumnWidth; } }

    void Awake() {
        this.titleContent = new GUIContent("Level Editor");
        this.minSize = new Vector2(800, 460);
        instance = this;
        InitTextures();
        GetDataObjects();
        GetEnemyIcons();
        SetupReorderableEnemyList();
    }

    void OnEnable() {
        instance = this;
        GetDataObjects();
        GetEnemyIcons();
        SetupReorderableEnemyList();
    }

    void OnFocus() {
        GetDataObjects();
        GetEnemyIcons();
        SetupReorderableEnemyList();
    }

    private void GetDataObjects() {
        levelDataList = Resources.LoadAll<LevelDataObject>("Data/LevelData");

        VerifyFilenames();

        SortFileList();
    }

    private void GetEnemyIcons() {
        enemyIcons = new List<Texture2D>();
        //string dataFolderPath = Application.dataPath + "/Art/2D/CharacterHeads/EnemyCharacters";
        //DirectoryInfo levelDirectoryPath = new DirectoryInfo(dataFolderPath);
        //FileInfo[] infos = levelDirectoryPath.GetFiles().Where(info => !info.Name.EndsWith(".meta")).ToArray();

        //for (int i = 0; i < infos.Length; i++) {
        //    enemyIcons.Add(AssetDatabase.LoadAssetAtPath<Texture2D>(StripSystemPath(infos[i].FullName)));
        //}

        enemyDataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData/Enemies");

        //for (int i = 0; i < enemyDataObjects.Length; i++) {
        //    enemyIcons.Add(MakeTextureFromSprite(enemyDataObjects[i].data.uiImage));
        //}
    }

    private string StripSystemPath(string path) {
        string rVal = path.Substring(path.IndexOf("Assets"));

        return rVal;
    }

    private void VerifyFilenames() {
        for (int i = 0; i < levelDataList.Length; i++) {
            VerifyFilenameWithObjectName(levelDataList[i]);
        }
    }

    private void SortFileList() {
        Array.Sort(levelDataList,
            delegate(LevelDataObject o1, LevelDataObject o2) {
                return o1.data.name.CompareTo(o2.data.name);
            });
    }

    private void VerifyFilenameWithObjectName(LevelDataObject dataObject) {
        if ((selectedLevelDataObject != null) && (selectedLevelDataObject.name != selectedLevelDataObject.data.name)) {
            string assetPath = AssetDatabase.GetAssetPath(dataObject.GetInstanceID());
            AssetDatabase.RenameAsset(assetPath, dataObject.data.name);
            AssetDatabase.SaveAssets();
        }
    }

    private void RenameAsset(string newName, Object obj) {
        string assetPath = AssetDatabase.GetAssetPath(obj.GetInstanceID());
        AssetDatabase.RenameAsset(assetPath, newName);
        AssetDatabase.SaveAssets();
    }

    private void InitTextures() {
        if (meleeAttackTexture == null) {
            meleeAttackTexture = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Icons/Meleeicon.png", typeof(Texture2D)) as Texture2D;
            rangedAttackTexture = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Icons/Rangedicon.png", typeof(Texture2D)) as Texture2D;

            windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            windowBGTexture.SetPixel(0, 0, new Color(0.172549f, 0.2784314f, 0.4392157f, 1f));
            windowBGTexture.Apply();

            bodyPanelBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            bodyPanelBGTexture.SetPixel(0, 0, new Color(0.4588235f, 0.5411765f, 0.6588235f, 1f));
            bodyPanelBGTexture.Apply();

            toolbarBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            toolbarBGTexture.SetPixel(0, 0, new Color(0.07843138f, 0.1843137f, 0.3294118f, 1f));
            toolbarBGTexture.Apply();
        }
    }

    private void SetupReorderableEnemyList() {
        if (selectedLevelDataObject != null) {
            selectedLevelDataSerializedObject = new SerializedObject(selectedLevelDataObject);
            levelEnemies = new ReorderableList(selectedLevelDataSerializedObject, selectedLevelDataSerializedObject.FindProperty("data").FindPropertyRelative("enemiesToSpawn"), true, true, true, true);
            levelEnemies.elementHeight = EditorGUIUtility.singleLineHeight * 2f;

            levelEnemies.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
                //SerializedProperty element = levelEnemies.serializedProperty.GetArrayElementAtIndex(index);
                //selectedLevelDataObject.data.enemiesToSpawn[index] = (CharacterID)EditorGUI.EnumPopup(new Rect(rect.x, rect.y + (EditorGUIUtility.singleLineHeight / 3), rect.width, EditorGUIUtility.singleLineHeight), (CharacterID)element.enumValueIndex);
                if (selectedLevelDataObject.data.enemiesToSpawn[index] != null) {
                    EditorGUI.LabelField(new Rect(rect.x, rect.y + (EditorGUIUtility.singleLineHeight / 3), rect.width, EditorGUIUtility.singleLineHeight), RemoveUnderscores(selectedLevelDataObject.data.enemiesToSpawn[index].ToString()), EditorStyles.boldLabel);
                }
            };

            levelEnemies.drawHeaderCallback = (Rect rect) => {
                EditorGUI.LabelField(rect, "Enemy Spawn List");
            };
        }
    }

    private void AddEnemyToList(CharacterData data) {
        if (selectedLevelDataObject != null) {
            selectedLevelDataObject.data.enemiesToSpawn.Add(data.characterID);
            //SetupReorderableEnemyList();
        }
    }

    private string RemoveUnderscores(string value) {
        string rVal = value.Replace('_', ' ');
        return Utility.ToCamelCase(rVal);
    }

    #region EDITOR_MENU_METHODS
    [MenuItem("Window/Level Editor %&l")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        LevelEditorWindow window = (LevelEditorWindow)EditorWindow.GetWindow(typeof(LevelEditorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }

    private Texture2D MakeTexture(int width, int height, Color col) {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; i++) {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    private Texture2D MakeTextureFromSprite(Sprite sprite) {
        Texture2D rVal = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
        Color[] pixels = sprite.texture.GetPixels((int)sprite.textureRect.x, (int)sprite.textureRect.y, (int)sprite.textureRect.width, (int)sprite.textureRect.height);
        rVal.SetPixels(pixels);
        rVal.Apply();
        return rVal;
    }

    #region DRAW_METHODS

    private void DrawObjectList() {
        GUILayout.BeginHorizontal(GUILayout.Width(FileColumnWidth));
        GUI.DrawTexture(new Rect(0, 0, FileColumnWidth, position.height), toolbarBGTexture, ScaleMode.StretchToFill);

        dataObjectsScrollPosition = GUILayout.BeginScrollView(dataObjectsScrollPosition);

        GUIStyle normalStyle = new GUIStyle(GUI.skin.GetStyle("whiteLabel"));
        normalStyle.normal.background = MakeTexture((int)(WindowWidth / 2), (int)EditorGUIUtility.singleLineHeight, new Color(1.0f, 1.0f, 1.0f, 0.1f));
        GUIStyle selectedStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
        selectedStyle.normal.background = MakeTexture((int)(WindowWidth / 2), (int)EditorGUIUtility.singleLineHeight, new Color(1.0f, 1.0f, 1.0f, 0.5f));
        selectedStyle.normal.textColor = Color.blue;

        GUILayout.Space(windowEdgePadding);
        for (int i = 0; i < levelDataList.Length; i++) {
            if (GUILayout.Button(levelDataList[i].data.name, (i == selectedFileIndex) ? selectedStyle : normalStyle)) {
                selectedFileIndex = i;
                selectedLevelDataObject = levelDataList[i];
                
                SetupReorderableEnemyList();
            }
        }
        GUILayout.Space(windowEdgePadding);

        GUILayout.EndScrollView();

        GUILayout.EndHorizontal();
    }

    private void DrawLevelProperties() {
        GUILayout.BeginHorizontal(GUILayout.Width(LevelPropertiesColumnWidth));
        GUI.DrawTexture(new Rect(FileColumnWidth, 0, LevelPropertiesColumnWidth, position.height), bodyPanelBGTexture, ScaleMode.StretchToFill);

        levelDataPropertiesScrollPosition = GUILayout.BeginScrollView(dataObjectsScrollPosition);

        GUILayout.BeginArea(new Rect(windowEdgePadding, windowEdgePadding, LevelPropertiesColumnWidth - (windowEdgePadding * 2), maxSize.y));

        if (selectedLevelDataObject != null) {
            GUILayout.Space(windowEdgePadding);

            GUILayout.BeginHorizontal();
            newFilename = (string.IsNullOrEmpty(newFilename)) ? selectedLevelDataObject.name : newFilename;
            DrawHorizontalLabeledTextField("Filename", ref newFilename);
            GUI.color = Color.green;
            if (GUILayout.Button("Rename", GUILayout.Width(70))) {
                RenameAsset(newFilename, selectedLevelDataObject);
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            GUILayout.Space(padding);

            LevelData levelData = selectedLevelDataObject.data;

            DrawHorizontalLabeledTextField("Name", ref levelData.name);
            //DrawHorizontalLabeledIntField("Total Enemies", ref levelData.TotalEnemies);
            DrawHorizontalLabeledIntField("Total Bosses", ref levelData.totalBosses);

            GUILayout.Space(padding);
            if (levelEnemies != null) {
                levelEnemies.DoLayoutList();
            }

            GUILayout.Space(padding);
        }

        GUILayout.EndArea();

        GUILayout.EndScrollView();

        GUILayout.EndHorizontal();
    }

    private void DrawEnemyIcons() {
        GUILayout.BeginVertical();
        GUILayout.BeginArea(new Rect(ThirdColumnX + (padding), padding, ThirdColumnWidth, maxSize.y));
        enemyIconScrollPosition = GUILayout.BeginScrollView(enemyIconScrollPosition);

        GUIStyle buttonStyle = new GUIStyle();

        //for (int i = 0; i < enemyIcons.Count; i++) {
        //    if (GUILayout.Button(enemyIcons[i], buttonStyle, GUILayout.Width(enemyIconDimensions), GUILayout.Height(enemyIconDimensions))) {
        //        AddEnemyToList(enemyIcons[i].name);
        //    }
        //    GUILayout.Space(windowEdgePadding);
        //}

        for (int i = 0; i < enemyDataObjects.Length; i++) {
            if (GUILayout.Button(enemyDataObjects[i].data.uiImage.texture, buttonStyle, GUILayout.Width(enemyIconDimensions), GUILayout.Height(enemyIconDimensions))) {
                AddEnemyToList(enemyDataObjects[i].data);
            }
            GUILayout.Space(windowEdgePadding);
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
        GUILayout.EndVertical();
    }

    #endregion DRAW_METHODS

    void OnGUI() {
        GUI.skin.button.wordWrap = true;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);

        if (selectedLevelDataSerializedObject != null) {
            selectedLevelDataSerializedObject.UpdateIfDirtyOrScript();
        }

        GUILayout.BeginHorizontal();
        DrawObjectList();
        DrawLevelProperties();
        DrawEnemyIcons();
        GUILayout.EndHorizontal();

        if (GUI.changed) {
            if (selectedLevelDataObject != null) {
                EditorUtility.SetDirty(selectedLevelDataObject);
            }
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }

        if (selectedLevelDataSerializedObject != null) {
            selectedLevelDataSerializedObject.ApplyModifiedProperties();
        }
    }

}
#pragma warning restore 0108, 0219, 0414, 0472