﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;

public class SpecialAttackSelectionEditorWindow : ScriptableObject {

    private Action<List<SpecialAttackData>> OkCallback;
    private Action CancelCallback;

    private bool shiftHeld = false;
    private Vector2 characterScrollPosition;
    private List<Texture> attackTextures;
    private SpecialAttackDataObject[] dataObjects;

    private List<SpecialAttackData> idsToAdd;

    public Rect windowRect;
    public string windowTitle = "Special Attack Selection";
    public Vector2 minSize = new Vector2(420, 500);

    public int selectedBtnIndex = 0;

    private Texture2D GenerateTextureFromSprite(Sprite aSprite) {
        Rect rect = aSprite.rect;
        Texture2D tex = new Texture2D((int)rect.width, (int)rect.height);
        Color[] data = aSprite.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
        tex.SetPixels(data);
        tex.Apply(true);
        return tex;
    }

    private void AddIDToList(SpecialAttackData data) {
        bool idFound = false;
        for (int i = 0; i < idsToAdd.Count; i++) {
            if (idsToAdd[i] == data) {
                idFound = true;
                break;
            }
        }
        if (!idFound) {
            idsToAdd.Add(data);
        }
    }

    public void Init(ref List<SpecialAttackType> idsToExclude) {
        bool typeFound = false;
        selectedBtnIndex = 0;
        idsToAdd = new List<SpecialAttackData>();
        attackTextures = new List<Texture>();
        dataObjects = null;
        dataObjects = Resources.LoadAll<SpecialAttackDataObject>("Data/SpecialAttackData");
        for (int i = 0; i < dataObjects.Length; i++) {
            typeFound = false;
            for (int j = 0; j < idsToExclude.Count; j++) {
                if (idsToExclude[j] == dataObjects[i].data.specialAttackType) {
                    typeFound = true;
                    break;
                }
            }

            if (!typeFound) {
                if (dataObjects[i].data.uiImage != null) {
                    attackTextures.Add(GenerateTextureFromSprite(dataObjects[i].data.uiImage));
                }
                else {
                    Sprite sprite = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Art/64 flat icons/png/64px/Modifiers_Accuracy.png", typeof(Sprite));
                    attackTextures.Add(GenerateTextureFromSprite(sprite));
                }
            }
        }
    }

    private void DrawSpecialAttackButtons() {
        GUILayout.BeginVertical();
        int totalColumns = 3;
        int i = 0;
        while (i < attackTextures.Count) {
            GUILayout.BeginHorizontal();
            for (int j = 0; j < totalColumns; j++) {
                if (i < attackTextures.Count) {
                    if (GUILayout.Button(attackTextures[i], GUILayout.Width(120), GUILayout.Height(120))) {
                        selectedBtnIndex = i;
                        if (shiftHeld) {
                            AddIDToList(dataObjects[selectedBtnIndex].data);
                        }
                    }
                }
                i++;
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();
    }

    public void SetCallbacks(Action<List<SpecialAttackData>> okCallback, Action cancelCallback) {
        OkCallback = okCallback;
        CancelCallback = cancelCallback;
    }

    public void DrawWindow() {
        shiftHeld = Event.current.shift;

        GUILayout.BeginVertical();
        GUILayout.Space(10);

        characterScrollPosition = GUILayout.BeginScrollView(characterScrollPosition);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        //int preIndex = selGridInt;
        //selGridInt = GUILayout.SelectionGrid(selGridInt, characterOptions.ToArray(), 3, GUILayout.Width(380));
        //if (preIndex != selGridInt) {
        //    if (shiftHeld) {
        //        AddIDToList(dataObjects[selGridInt].data);
        //    }
        //}

        DrawSpecialAttackButtons();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (attackTextures.Count > 0) {
            GUILayout.Label(Utility.EnumNameToReadable(dataObjects[selectedBtnIndex].data.specialAttackType.ToString()));
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            OkCallback = null;
            if (CancelCallback != null) {
                CancelCallback();
            }
        }

        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            AddIDToList(dataObjects[selectedBtnIndex].data);
            CancelCallback = null;
            if (OkCallback != null) {
                OkCallback(idsToAdd);
            }
        }
        GUI.color = Color.white;

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();



        GUILayout.EndVertical();
    }


}
