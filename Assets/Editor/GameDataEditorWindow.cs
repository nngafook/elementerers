﻿using UnityEngine;
using UnityEditor;
using DG.Tweening;
using System.Collections;
using UnityEditor.SceneManagement;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

#pragma warning disable 0414
public class GameDataEditorWindow : EditorWindow {

    #region GP_VARIABLES
    private CharacterID characterIDToAddRemove;
    private Action ActionAwaitingConfirmation;
    private CharacterDataObject[] characterDataObjects;
    private Dictionary<CharacterID, Texture> playerCharacterHeadTextures;
    private string actionConfirmName = "";
    private static bool characterIDsShowing = false;
    private static bool availableCharactersShowing = false;
    private static bool equippedAttacksShowing = false;
    private static bool characterSaveInfosShowing = false;
    private bool waitingOnConfirm = false;
    private int characterIDIndexToRemove = 0;
    #endregion GP_VARIABLES

    private float indentValue = 20;
    //private float textFieldWidth = 200;
    private float buttonHeight = 30;
    private float buttonWidth = 200;
    private float smallButtonWidth = 100;
    private float padding = 10;
    private bool viewingLocalCopy = false;
    private bool inputDisabled = false;

    private AugmentIDDatabaseDataObject augmentIDDatabase;
    private CombatOrderSelectionEditorWindow combatOrderSelectWindow;
    private EnumSelectionEditorWindow enumSelectionWindow;
    private SpecialAttackSelectionEditorWindow specialAttackSelectionWindow;
    private CharacterSelectionEditorWindow characterSelectionWindow;
    private AugmentSelectionEditorWindow augmentSelectionWindow;
    private GameDataObject selectedSO;
    private CharacterSaveInfo characterSaveInfoToEdit;      // Stores the saveInfo that was being edited when a selection window was opened
    private CombatOrder combatOrderToEdit;                      // Stores the combatOrder that was being edited when the combat order selection window was opened

    private List<CombatOrder> combatOrdersBeingViewed = new List<CombatOrder>();

    private Vector2 mousePos;
    private Vector2 inspectorScrollPosition;

    private Texture2D tinyPotato;
    private Texture2D corgiWave;
    private Texture2D infoIcon;
    private Texture2D windowBGTexture;
    private Texture2D menuBGTexture;
    private GUISkin baseGUISkin;
    private GUISkin myGUISkin;

    private float WindowWidth { get { return position.width; } }
    private float MenuWidth { get { return (WindowWidth / 3); } }
    private float ContentWidth { get { return ((WindowWidth / 3) * 2); } }

    void Awake() {
        augmentIDDatabase = AssetDatabase.LoadAssetAtPath<AugmentIDDatabaseDataObject>("Assets/Resources/Data/AugmentIDDatabase.asset");
        this.minSize = new Vector2(1400, 800);
        InitTextures();
        GetGUISkin();
        GetCharacterDataObjects();
        GetPlayerCharacterHeadTextures();
    }

    void OnEnable() {
        InitTextures();
        GetGUISkin();
        GetCharacterDataObjects();
        GetPlayerCharacterHeadTextures();
    }

    void OnDestroy() {
        if (characterSelectionWindow != null) {
            DestroyImmediate(characterSelectionWindow);
            characterSelectionWindow = null;
            inputDisabled = false;
        }
    }

    private void InitTextures() {
        infoIcon = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Art/2D/Icons/InfoIcon.png", typeof(Texture2D));
        corgiWave = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Art/2D/CorgiWave.png", typeof(Texture2D));
        tinyPotato = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Art/2D/TinyPotato.png", typeof(Texture2D));

        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color ( 0.3803922f, 0.6431373f, 0.8666667f, 1f ));
        windowBGTexture.Apply();

        menuBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        menuBGTexture.SetPixel(0, 0, new Color(0.3333333f, 0.5960785f, 0.9098039f, 1f));
        menuBGTexture.Apply(); 
    }

    private void GetGUISkin() {
        myGUISkin = (GUISkin)AssetDatabase.LoadAssetAtPath("Assets/Resources/GameDataEditorGUISkin.guiskin", typeof(GUISkin));
    }

    #region EDITOR_MENU_METHODS
    [MenuItem("Game Tools/Game Data Editor %&e")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        GameDataEditorWindow window = (GameDataEditorWindow)EditorWindow.GetWindow(typeof(GameDataEditorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    #region CIVIL_SERVANTS
    private string RemoveUnderscores(string value) {
        string rVal = value.Replace('_', ' ');
        return Utility.ToCamelCase(rVal);
    }

    private void SetActionForConfirm(string message, Action action) {
        waitingOnConfirm = true;
        actionConfirmName = message;
        ActionAwaitingConfirmation = action;
    }
    #endregion CIVIL_SERVANTS

    #region DRAW_FIELD_METHODS
    private void DrawSelectWindow(int id) {
        if (characterSelectionWindow != null) {
            characterSelectionWindow.DrawWindow();
        }
        if (specialAttackSelectionWindow != null) {
            specialAttackSelectionWindow.DrawWindow();
        }
        if (enumSelectionWindow != null) {
            enumSelectionWindow.DrawWindow();
        }
        if (combatOrderSelectWindow != null) {
            combatOrderSelectWindow.DrawWindow();
        }
        if (augmentSelectionWindow != null) {
            augmentSelectionWindow.DrawWindow();
        }
        GUI.DragWindow();
    }

    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }
    #endregion DRAW_FIELD_METHODS

    #region DRAW_INSPECTORS

    #region GAME_PROGRESS_INSPECTOR

    #region GP_UTILITIES
    private void OpenCharacterSelectWindow() {
        if (characterSelectionWindow == null) {
            List<CharacterID> idsToExclude = (selectedSO as GameProgress).data.availableCharacters;

            characterSelectionWindow = CreateInstance<CharacterSelectionEditorWindow>();
            characterSelectionWindow.Init(ref idsToExclude);
            characterSelectionWindow.SetCallbacks(OnCharacterSelectionOkPressed, OnCharacterSelectionCancelPressed);
            characterSelectionWindow.windowRect = new Rect((Screen.width / 2) - (characterSelectionWindow.minSize.x / 2), (Screen.height / 2) - (characterSelectionWindow.minSize.y / 2), characterSelectionWindow.minSize.x, characterSelectionWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnCharacterSelectionOkPressed(List<CharacterID> idList) {
        AddCharacterToAvailable(idList);
        characterSelectionWindow = null;
        inputDisabled = false;
    }

    private void OnCharacterSelectionCancelPressed() {
        characterSelectionWindow = null;
        inputDisabled = false;
    }

    private void OpenEnumSelectionWindow(Enum e) {
        enumSelectionWindow = CreateInstance<EnumSelectionEditorWindow>();
        enumSelectionWindow.Init(e);
        enumSelectionWindow.windowRect = new Rect((Screen.width / 2) - (enumSelectionWindow.minSize.x / 2), (Screen.height / 2) - (enumSelectionWindow.minSize.y / 2), enumSelectionWindow.minSize.x, enumSelectionWindow.minSize.y);
        inputDisabled = true;
    }

    private void OnSpAtkValueSelected(Enum e) {
        if (!e.Equals(characterSaveInfoToEdit.equippedSpecialAttack)) {
            characterSaveInfoToEdit.equippedSpecialAttack = (SpecialAttackType)(e);
        }
        characterSaveInfoToEdit = null;
        enumSelectionWindow = null;
        inputDisabled = false;
    }

    private void OpenCombatOrderSelectWindow(CombatOrder o) {
        combatOrderSelectWindow = CreateInstance<CombatOrderSelectionEditorWindow>();
        combatOrderSelectWindow.Init(o);
        combatOrderSelectWindow.SetCallbacks(OnCombatOrderSelectWindowClosed);
        combatOrderSelectWindow.windowRect = new Rect((Screen.width / 2) - (combatOrderSelectWindow.minSize.x / 2), (Screen.height / 2) - (combatOrderSelectWindow.minSize.y / 2), combatOrderSelectWindow.minSize.x, combatOrderSelectWindow.minSize.y);
        inputDisabled = true;
    }

    private void OnCombatOrderSelectWindowClosed() {
        combatOrderSelectWindow = null;
        inputDisabled = false;
    }

    private void OpenAugmentSelectionWindow(int slot) {
        if (augmentSelectionWindow == null) {
            augmentSelectionWindow = CreateInstance<AugmentSelectionEditorWindow>();
            augmentSelectionWindow.Init(slot);
            augmentSelectionWindow.SetCallbacks(OnAugmentSelectionOkPressed, OnAugmentSelectionCancelPressed);
            augmentSelectionWindow.windowRect = new Rect((Screen.width / 2) - (augmentSelectionWindow.minSize.x / 2), (Screen.height / 2) - (augmentSelectionWindow.minSize.y / 2), augmentSelectionWindow.minSize.x, augmentSelectionWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnAugmentSelectionOkPressed(AugmentDataObject dataObject, int slotNumber) {
        if (slotNumber == 1) {
            characterSaveInfoToEdit.modifierAugmentSlotID = (dataObject != null) ? dataObject.data.id : "";
        }
        else {
            characterSaveInfoToEdit.potencyAugmentSlotID = (dataObject != null) ? dataObject.data.id : "";
        }
        characterSaveInfoToEdit = null;
        augmentSelectionWindow = null;
        inputDisabled = false;
    }

    private void OnAugmentSelectionCancelPressed() {
        characterSaveInfoToEdit = null;
        augmentSelectionWindow = null;
        inputDisabled = false;
    }

    private void GetCharacterDataObjects() {
        characterDataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData");
    }

    private void GetPlayerCharacterHeadTextures() {
        playerCharacterHeadTextures = new Dictionary<CharacterID, Texture>();
        for (int i = 0; i < characterDataObjects.Length; i++) {
            playerCharacterHeadTextures.Add(characterDataObjects[i].data.characterID, Utility.GenerateTextureFromSprite(characterDataObjects[i].data.uiImage));
        }
    }

    private Texture GetPlayerHead(CharacterID id) {
        Texture rVal = null;
        if (playerCharacterHeadTextures.ContainsKey(id)) {
            rVal = playerCharacterHeadTextures[id];
        }
        else {
            Debug.LogWarning("The ID passed to GetPlayerHead was not in the dictionary".Colored(CustomColor.GetColor(ColorName.ERROR_RED)));
            for (int i = 0; i < characterDataObjects.Length; i++) {
                if (characterDataObjects[i].data.characterID == id) {
                    rVal = Utility.GenerateTextureFromSprite(characterDataObjects[i].data.uiImage);
                }
            }
        }
        return rVal;
    }

    private void AddCharacterToAvailable(List<CharacterID> idList) {
        if (idList.Count > 0) {
            bool characterOwned = false;
            CharacterID id = CharacterID.NONE;
            for (int idIndex = 0; idIndex < idList.Count; idIndex++) {
                characterOwned = false;
                id = idList[idIndex];
                for (int i = 0; i < (selectedSO as GameProgress).data.availableCharacters.Count; i++) {
                    if ((selectedSO as GameProgress).data.availableCharacters[i] == id) {
                        characterOwned = true;
                        break;
                    }
                }

                if (!characterOwned) {
                    (selectedSO as GameProgress).AddUnlockedCharacter(id);
                }
            }
        }
    }

    private void AddCharacterIDToParty() {
        (selectedSO as GameProgress).data.currentPartyIds.Add(CharacterID.NONE);
    }

    private void RemoveCharacterIDFromParty() {
        (selectedSO as GameProgress).data.currentPartyIds.RemoveAt(characterIDIndexToRemove);
        characterIDIndexToRemove = Mathf.Clamp(characterIDIndexToRemove, 0, (selectedSO as GameProgress).data.currentPartyIds.Count - 1);
    }

    private void RecreateSpecialAttackList() {
        (selectedSO as GameProgress).data.RecreateSpecialAttackList();
    }

    private void SaveToJSON() {
        (selectedSO as GameProgress).SaveToJSON();
    }

    private void OverwriteFromJSON() {
        GameProgress.OverwriteFromJSON();
    }

    private void CopySavedFilePath() {
        GUIUtility.systemCopyBuffer = GameProgress.SavedProgressPath;
    }

    private void DeleteGameProgressSaveFile() {
        FileUtil.DeleteFileOrDirectory(GameProgress.SavedProgressPath);
    }

    private void ResetProgressToTemplate() {
        //(selectedSO as GameProgress).data.ResetToTemplate();
        selectedSO = (selectedSO as GameProgress).InitializeFromTemplate();
    }
    #endregion GP_UTILITIES
    
    private void DrawGameProgressInspector() {
        if (!waitingOnConfirm) {
            DrawSingleProperties();
            GUILayout.Space(indentValue);
            DrawAvailableCharacters();
            GUILayout.Space(indentValue);
            DrawCurrentPartyIDs();
            //GUILayout.Space(indentValue);
            //DrawEquippedSpecialAttacks();
            GUILayout.Space(indentValue);
            DrawCharacterSaveInfos();

            if (viewingLocalCopy) {
                GUILayout.Space(indentValue);
                DrawSaveAndLoadButtons();
            }

            GUILayout.Space(indentValue);
            DrawResetToDefault();

            Repaint();
        }
        else {
            DrawConfirmation();
        }
    }

    private void DrawSingleProperties() {
        // Current Scrap
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();

        GUILayout.Label("Current Scrap");
        GUILayout.Space(indentValue);
        GUI.skin.customStyles[2].normal.textColor = CustomColor.GetColor(ColorName.YALLOW);
        (selectedSO as GameProgress).data.currentScrap = EditorGUILayout.IntField((selectedSO as GameProgress).data.currentScrap, "LeftTextField", GUILayout.Height(buttonHeight));
        GUI.skin.customStyles[2].normal.textColor = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    private void DrawAvailableCharacters() {
        GUILayout.BeginHorizontal();
        if (availableCharactersShowing) {
            GUILayout.Space(indentValue);
        }
        GUI.color = (availableCharactersShowing) ? CustomColor.GetColor(ColorName.ERROR_RED) : CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button((availableCharactersShowing) ? "Hide Available Characters" : "View Available Characters", GUILayout.Width(buttonWidth))) {
            availableCharactersShowing = !availableCharactersShowing;
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        if (availableCharactersShowing) {
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(indentValue * 2);
            GUILayout.BeginVertical();

            GUI.color = CustomColor.GetColor(ColorName.WARNING_YELLOW);
            GUILayout.BeginHorizontal();

            GUILayout.Box(infoIcon, "Image", GUILayout.Width(28), GUILayout.Height(28));
            GUILayout.Box("Available Characters have been unlocked by the player.", "textarea", GUILayout.Width(buttonWidth * 2));

            GUILayout.EndHorizontal();
            GUI.color = Color.white;
            GUILayout.Space(5);

            GUILayout.BeginVertical();

            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("Add Character", GUILayout.Width(buttonWidth))) {
                OpenCharacterSelectWindow();
            }

            GUI.color = Color.white;

            GUILayout.Space(indentValue);
            if ((selectedSO as GameProgress).data.availableCharacters.Count == 0) {
                GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                GUILayout.Label("No Characters Available");
                GUI.color = Color.white;
            }
            else {
                for (int i = 0; i < (selectedSO as GameProgress).data.availableCharacters.Count; i++) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(indentValue);
                    GUILayout.Label(i.ToString() + ": " + RemoveUnderscores((selectedSO as GameProgress).data.availableCharacters[i].ToString()), GUILayout.Width(smallButtonWidth));
                    GUILayout.Space(indentValue);
                    GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                    if (GUILayout.Button("Remove", GUILayout.Width(smallButtonWidth))) {
                        (selectedSO as GameProgress).RemoveCharacterInfo((selectedSO as GameProgress).data.availableCharacters[i]);
                        (selectedSO as GameProgress).data.availableCharacters.RemoveAt(i);
                        (selectedSO as GameProgress).SaveToJSON();
                    }
                    GUI.color = Color.white;
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }
            }
            GUILayout.EndVertical();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    private void DrawCurrentPartyIDs() {
        GUILayout.BeginHorizontal();
        if (characterIDsShowing) {
            GUILayout.Space(indentValue);
        }

        GUI.color = (characterIDsShowing) ? CustomColor.GetColor(ColorName.ERROR_RED) : CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button((characterIDsShowing) ? "Hide Current Party" : "View Current Party", GUILayout.Width(buttonWidth))) {
            characterIDsShowing = !characterIDsShowing;
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        if (characterIDsShowing) {
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(indentValue * 2);
            GUILayout.BeginVertical();

            if ((selectedSO as GameProgress).data.currentPartyIds.Count > 5) {
                EditorGUILayout.HelpBox("This should not have more than 5 entries", MessageType.Warning);
            }

            GUILayout.BeginHorizontal();

            GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
            if (GUILayout.Button("Add CharacterID")) {
                SetActionForConfirm("Add New CharacterID To Party", AddCharacterIDToParty);
            }

            GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
            if (GUILayout.Button("Remove Index")) {
                if ((selectedSO as GameProgress).data.currentPartyIds.Count > characterIDIndexToRemove) {
                    SetActionForConfirm("Remove CharacterID From Party", RemoveCharacterIDFromParty);
                }
            }
            GUI.color = Color.white;
            if (GUILayout.Button("-")) {
                characterIDIndexToRemove--;
                characterIDIndexToRemove = Mathf.Clamp(characterIDIndexToRemove, 0, (selectedSO as GameProgress).data.currentPartyIds.Count - 1);
            }
            GUILayout.TextField(characterIDIndexToRemove.ToString());
            if (GUILayout.Button("+")) {
                characterIDIndexToRemove++;
                characterIDIndexToRemove = Mathf.Clamp(characterIDIndexToRemove, 0, (selectedSO as GameProgress).data.currentPartyIds.Count - 1);
            }

            GUILayout.EndHorizontal();

            GUILayout.Space(5);
            for (int i = 0; i < (selectedSO as GameProgress).data.currentPartyIds.Count; i++) {
                (selectedSO as GameProgress).data.currentPartyIds[i] = DrawHorizontalLabeledEnumField<CharacterID>("Party Member " + i.ToString(), (selectedSO as GameProgress).data.currentPartyIds[i]);
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    private void DrawEquippedSpecialAttacks() {
        GUILayout.BeginHorizontal();
        if (equippedAttacksShowing) {
            GUILayout.Space(indentValue);
        }
        GUI.color = (equippedAttacksShowing) ? CustomColor.GetColor(ColorName.ERROR_RED) : CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button((equippedAttacksShowing) ? "Hide Equipped Special Attacks" : "View Equipped Special Attacks", GUILayout.Width(buttonWidth))) {
            equippedAttacksShowing = !equippedAttacksShowing;
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        if (equippedAttacksShowing) {
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(indentValue * 2);
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
            if (GUILayout.Button("Recreate List")) {
                SetActionForConfirm("Recreate Special Attack List", RecreateSpecialAttackList);
            }            
            GUILayout.EndHorizontal();

            GUILayout.Space(5);
            GUI.color = Color.white;
            for (int i = 0; i < (selectedSO as GameProgress).data.characterSaveInfos.Count; i++) {
                GUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(RemoveUnderscores((selectedSO as GameProgress).data.characterSaveInfos[i].ownerID.ToString()));//, GUILayout.Width((Screen.width / 2) - 10));
                (selectedSO as GameProgress).data.characterSaveInfos[i].equippedSpecialAttack = (SpecialAttackType)EditorGUILayout.EnumPopup((selectedSO as GameProgress).data.characterSaveInfos[i].equippedSpecialAttack);//, GUILayout.Width((Screen.width / 2) - 10));
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    private void DrawCharacterSaveInfos() {
        GUI.color = (characterSaveInfosShowing) ? CustomColor.GetColor(ColorName.ERROR_RED) : CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        GUILayout.BeginHorizontal();
        if (characterSaveInfosShowing) {
            GUILayout.Space(indentValue);
        }
        if (GUILayout.Button((characterSaveInfosShowing) ? "Hide Character Save Infos" : "View Character Save Infos", GUILayout.Width(buttonWidth))) {
            characterSaveInfosShowing = !characterSaveInfosShowing;
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        if (characterSaveInfosShowing) {
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(indentValue * 2);
            GUILayout.BeginVertical();

            //if (GUILayout.Button("Recreate Info", GUILayout.Width(buttonWidth))) {
            //    (selectedSO as GameProgress).data.characterSaveInfos.Clear();
            //    for (int i = 0; i < characterDataObjects.Length; i++) {
            //        if (characterDataObjects[i].data.entityType == EntityType.PLAYER) {
            //            CharacterSaveInfo info = new CharacterSaveInfo(characterDataObjects[i].data.characterID, characterDataObjects[i].data.specialAttackType);
            //            (selectedSO as GameProgress).data.characterSaveInfos.Add(info);
            //        }
            //    }
            //}

            List<CharacterSaveInfo> infos = (selectedSO as GameProgress).CharacterSaveInfos;

            if (infos.Count > 0) {

                for (int i = 0; i < infos.Count; i++) {
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(indentValue);
                    GUILayout.Box(GetPlayerHead(infos[i].ownerID), "TransparentImage", GUILayout.Width(64), GUILayout.Height(64));

                    GUILayout.BeginVertical();
                    GUILayout.FlexibleSpace();

                    GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                    GUILayout.Label(Utility.EnumNameToReadable(infos[i].ownerID.ToString()));
                    GUI.color = Color.white;

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Level", GUILayout.Width(smallButtonWidth));
                    //infos[i].level = EditorGUILayout.IntField(infos[i].level, GUILayout.Width(buttonWidth));
                    string levelString = GUILayout.TextField(infos[i].level.ToString(), "LeftTextField", GUILayout.Width(buttonWidth));
                    Int32.TryParse(levelString, out infos[i].level);
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("EXP", GUILayout.Width(smallButtonWidth));
                    //infos[i].level = EditorGUILayout.IntField(infos[i].level, GUILayout.Width(buttonWidth));
                    string expString = GUILayout.TextField(infos[i].exp.ToString(), "LeftTextField", GUILayout.Width(buttonWidth));
                    Int32.TryParse(expString, out infos[i].exp);
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Equipped SP Atk", GUILayout.Width(smallButtonWidth));
                    if (GUILayout.Button(Utility.EnumNameToReadable(infos[i].equippedSpecialAttack.ToString()), GUILayout.Width(buttonWidth))) {
                        characterSaveInfoToEdit = infos[i];
                        OpenEnumSelectionWindow(infos[i].equippedSpecialAttack);
                        enumSelectionWindow.SetCallbacks(OnSpAtkValueSelected);
                    }
                    //infos[i].equippedSpecialAttack = (SpecialAttackType)EditorGUILayout.EnumPopup(infos[i].equippedSpecialAttack, GUILayout.Width(buttonWidth));
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    string buttonLabel = "";

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Modifier Augment", GUILayout.Width(smallButtonWidth));
                    buttonLabel = (infos[i].modifierAugmentSlotID != "") ? augmentIDDatabase.NameFromID(infos[i].modifierAugmentSlotID) : "Modifier Augment";
                    if (GUILayout.Button(buttonLabel, GUILayout.Width(buttonWidth))) {
                        characterSaveInfoToEdit  = infos[i];
                        OpenAugmentSelectionWindow(1);
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Potency Augment", GUILayout.Width(smallButtonWidth));
                    buttonLabel = (infos[i].potencyAugmentSlotID != "") ? augmentIDDatabase.NameFromID(infos[i].potencyAugmentSlotID) : "Potency Augment";
                    if (GUILayout.Button(buttonLabel, GUILayout.Width(buttonWidth))) {
                        characterSaveInfoToEdit = infos[i];
                        OpenAugmentSelectionWindow(2); 
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    GUILayout.BeginVertical();

                    GUI.color = CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
                    GUILayout.Label("Fallback Combat Order");
                    GUI.color = Color.white;
                    if (GUILayout.Button(CombatStrategyUtils.GetReadableCombatOrder(infos[i].fallbackCombatOrder), GUILayout.Width(buttonWidth))) {
                        combatOrderToEdit = infos[i].fallbackCombatOrder;
                        OpenCombatOrderSelectWindow(combatOrderToEdit);
                    }

                    GUILayout.BeginHorizontal();
                    GUI.color = CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
                    string label = "Combat Orders";
                    if (infos[i].combatOrders.Count == 0) {
                        label += " (This list should REALLY have at least one entry)";
                        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
                    }
                    GUILayout.Label(label);
                    GUI.color = Color.white;
                    GUILayout.FlexibleSpace();
                    if (infos[i].combatOrders.Count > 0) {
                        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                        if (GUILayout.Button("Remove", GUILayout.Width(buttonWidth))) {
                            infos[i].combatOrders.RemoveAt(infos[i].combatOrders.Count - 1);
                        }
                    }
                    GUI.color = CustomColor.GetColor(ColorName.YALLOW);
                    if (GUILayout.Button("Add", GUILayout.Width(buttonWidth))) {
                        infos[i].combatOrders.Add(new CombatOrder());
                    }
                    GUI.color = Color.white;
                    GUILayout.EndHorizontal();

                    CombatOrder order = null;
                    int currentColumn = 0;
                    int totalColumns = 3;
                    for (int j = 0; j < infos[i].combatOrders.Count; j++) {
                        order = infos[i].combatOrders[j];
                        if (currentColumn == 0) {
                            GUILayout.BeginHorizontal();
                        }

                        if (GUILayout.Button((j + 1).ToString() + " - " + CombatStrategyUtils.GetReadableCombatOrder(order), GUILayout.Width(buttonWidth))) {
                            combatOrderToEdit = order;
                            OpenCombatOrderSelectWindow(combatOrderToEdit);
                        }

                        currentColumn++;
                        if (currentColumn == totalColumns) {
                            GUILayout.EndHorizontal();
                            currentColumn = 0;
                        }
                    }
                    if (currentColumn != 0) {
                        GUILayout.EndHorizontal();
                    }

                    GUILayout.EndVertical();

                    GUILayout.FlexibleSpace();
                    GUILayout.EndVertical();

                    GUILayout.EndHorizontal();
                    GUILayout.Space(indentValue);
                }
            }
            else {
                if (GUILayout.Button("Create Default List", GUILayout.Width(buttonWidth))) {
                    for (int i = 0; i < characterDataObjects.Length; i++) {
                        if (characterDataObjects[i].data.entityType == EntityType.PLAYER) {
                            CharacterSaveInfo info = new CharacterSaveInfo(characterDataObjects[i].data.characterID, characterDataObjects[i].data.specialAttackType);
                            (selectedSO as GameProgress).data.characterSaveInfos.Add(info);
                        }
                    }
                }
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
    }

    private void DrawResetToDefault() {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
        if (GUILayout.Button((viewingLocalCopy) ? "Reset to Template" : "Reset Values", GUILayout.Width(Screen.width / 4))) {
            SetActionForConfirm("Reset To Template", ResetProgressToTemplate);
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    private void DrawSaveAndLoadButtons() {
        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical();
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("Save To Template")) {
            SetActionForConfirm("Overwrite Template", OverwriteFromJSON);
        }
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("Save Local")) {
            if (viewingLocalCopy) {
                SetActionForConfirm("Overwrite Local", SaveToJSON);
            }
        }
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        GUI.color = CustomColor.GetColor(ColorName.WARNING_YELLOW);
        if (GUILayout.Button("Copy Path")) {
            CopySavedFilePath();
        }
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Clear Save")) {
            SetActionForConfirm("Delete", DeleteGameProgressSaveFile);
        }
        GUILayout.EndVertical();

        GUI.color = Color.white;
        GUILayout.EndHorizontal();
    }

    private void DrawConfirmation() {
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("Confirm " + actionConfirmName + "?");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel")) {
            waitingOnConfirm = false;
            ActionAwaitingConfirmation = null;
        }
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("Confirm")) {
            waitingOnConfirm = false;
            ActionAwaitingConfirmation();
            ActionAwaitingConfirmation = null;
        }

        GUI.color = Color.white;

        GUILayout.EndVertical();
    }
    #endregion GAME_PROGRESS_INSPECTOR

    #region SPECIAL_ATTACK_INVENTORY_INSPECTOR

    #region SAI_UTILITIES
    private void OpenSpecialAttackSelectionWindow() {
        if (specialAttackSelectionWindow == null) {
            List<SpecialAttackData> data = (selectedSO as SpecialAttackDataInventory).items;
            List<SpecialAttackType> itemsToExclude = new List<SpecialAttackType>();
            for (int i = 0; i < data.Count; i++) {
                itemsToExclude.Add(data[i].specialAttackType);
            }

            specialAttackSelectionWindow = CreateInstance<SpecialAttackSelectionEditorWindow>();
            specialAttackSelectionWindow.Init(ref itemsToExclude);
            specialAttackSelectionWindow.SetCallbacks(OnSpecialAttackOkPressed, OnSpecialAttackCancelPressed);
            specialAttackSelectionWindow.windowRect = new Rect((Screen.width / 2) - (specialAttackSelectionWindow.minSize.x / 2), (Screen.height / 2) - (specialAttackSelectionWindow.minSize.y / 2), specialAttackSelectionWindow.minSize.x, specialAttackSelectionWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnSpecialAttackOkPressed(List<SpecialAttackData> types) {
        AddSpecialAttacksToInventory(types);
        specialAttackSelectionWindow = null;
        inputDisabled = false;
    }

    private void OnSpecialAttackCancelPressed() {
        specialAttackSelectionWindow = null;
        inputDisabled = false;
    }

    private void AddSpecialAttacksToInventory(List<SpecialAttackData> data) {
        SpecialAttackDataInventory sadi = (selectedSO as SpecialAttackDataInventory);
        bool idFound = false;
        for (int i = 0; i < data.Count; i++) {
            idFound = false;
            for (int j = 0; j < sadi.items.Count; j++) {
                if (sadi.items[j].specialAttackType == data[i].specialAttackType) {
                    idFound = true;
                    break;
                }
            }
            if (!idFound) {
                sadi.items.Add(data[i]);
            }
        }
    }

    #endregion SAI_UTILITIES

    private void DrawSpecialAttackInventoryInspector() {
        List<SpecialAttackData> items = (selectedSO as SpecialAttackDataInventory).items;

        GUILayout.BeginVertical();
        GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
        GUILayout.Label("Special Attack Inventory", "SubHeader");
        GUI.color = Color.white;

        if (items.Count > 0) {
            for (int i = 0; i < items.Count; i++) {
                GUILayout.BeginHorizontal();
                GUILayout.Space(indentValue);
                GUILayout.Label(items[i].name, GUILayout.Width(buttonWidth));

                GUILayout.Box(infoIcon, "Image", GUILayout.Width(28), GUILayout.Height(28));
                GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                if (GUILayout.Button("Info", GUILayout.Width(buttonWidth))) {

                }
                GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                if (GUILayout.Button("Remove", GUILayout.Width(buttonWidth))) {
                    items.RemoveAt(i);
                }
                GUI.color = Color.white;
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }
        }
        else {
            GUILayout.Label("Nothing to see here");
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Box(corgiWave, "TransparentImage", GUILayout.Width(256), GUILayout.Height(256));
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        GUILayout.EndVertical();
    }
    #endregion SPECIAL_ATTACK_INVENTORY_INSPECTOR

    private void DrawGameSettingsInspector() {
        
    }
    #endregion DRAW_INSPECTORS

    #region DRAW_METHODS
    private void DrawSelectedSODefaultInspector() {
        GUILayout.Space(padding);
        inspectorScrollPosition = GUILayout.BeginScrollView(inspectorScrollPosition);
        if (selectedSO != null) {
            if (selectedSO.GetType() == typeof(GameProgress)) {
                DrawGameProgressInspector();
            }
            else if (selectedSO.GetType() == typeof(SpecialAttackDataInventory)) {
                DrawSpecialAttackInventoryInspector();
            }
            else if (selectedSO.GetType() == typeof(GameSettings)) {
                DrawGameSettingsInspector();
            }
            //editor.OnInspectorGUI();
        }
        GUILayout.EndScrollView();
    }

    private void DrawMenuMascots() {
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUI.skin.customStyles[5].alignment = TextAnchor.LowerLeft;
        GUI.skin.customStyles[5].padding.left = 8;
        GUILayout.Box(tinyPotato, "TransparentImage", GUILayout.Width(128), GUILayout.Height(128));
        GUILayout.FlexibleSpace();
        GUI.skin.customStyles[5].alignment = TextAnchor.LowerRight;
        GUI.skin.customStyles[5].padding.left = 0;
        GUI.skin.customStyles[5].padding.right = 8;
        GUILayout.Box(corgiWave, "TransparentImage", GUILayout.Width(128), GUILayout.Height(128));
        GUILayout.EndHorizontal();
        GUI.skin.customStyles[5].padding.right = 0;
        GUI.skin.customStyles[5].alignment = TextAnchor.UpperLeft;
        GUILayout.EndVertical();
    }

    private void DrawHeader(string headerTitle) {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(headerTitle, "Header");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Space(padding);
    }

    private void DrawMenu() {
        // Create an area that encompasses the entire column, so that I can draw the mascots
        GUILayout.BeginArea(new Rect(0, 0, MenuWidth, position.height));
        // Draws a box behind the texture, giving it the outline
        GUILayout.BeginArea(new Rect(0, 0, MenuWidth, position.height), GUI.skin.box);
        // Draws the stretched "fill" texture, 2 pixels in on all sides, showing piece of the area box behind
        GUI.DrawTexture(new Rect(0, 0, MenuWidth - 1, position.height), menuBGTexture, ScaleMode.StretchToFill);
        
        // Actual content area
        GUILayout.BeginArea(new Rect(padding, padding, MenuWidth - (padding * 2), position.height));
        GUILayout.BeginHorizontal(GUILayout.Width(MenuWidth - (padding * 2)));
        GUILayout.BeginVertical();

        DrawHeader("Menu");

        string path = "";

        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("Game Progress", GUILayout.Height(buttonHeight))) {
            path = "Assets/Resources/Data/GameProgress.asset";
        }

        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        if (GUILayout.Button("Special Attack Inventory", GUILayout.Height(buttonHeight))) {
            path = "Assets/Resources/Data/SpecialAttackDataInventory.asset";
        }

        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        if (GUILayout.Button("Game Settings", GUILayout.Height(buttonHeight))) {
            path= "Assets/Resources/Data/GameSettings.asset";
        }

        if (path != "") {
            selectedSO = (GameDataObject)AssetDatabase.LoadAssetAtPath(path, typeof(GameDataObject));
            if (selectedSO != null && selectedSO.LocalExists()) {
                selectedSO = selectedSO.LoadFromLocalJSON();
            }
            else {
                selectedSO = selectedSO.InitializeFromTemplate();
            }
            inspectorScrollPosition = Vector2.zero;
            viewingLocalCopy = true;
        }

        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("Fix Special Attack Inventory. Select Window buttons have wrong indexes. Add save button.");
        GUI.skin.label.alignment = TextAnchor.UpperLeft;

        GUI.color = Color.white;

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        

        GUILayout.EndArea();
        GUILayout.EndArea();

        DrawMenuMascots();

        GUILayout.EndArea();
        
    }

    private void DrawContent() {

        // Draws a box behind the texture, giving it the outline
        GUILayout.BeginArea(new Rect(MenuWidth, 0, ContentWidth, position.height), GUI.skin.box);
        // Draws the stretched "fill" texture, 2 pixels in on all sides, showing piece of the area box behind
        GUI.DrawTexture(new Rect(1, 0, ContentWidth - 1, position.height), windowBGTexture, ScaleMode.StretchToFill);
        
        // Actual content area
        GUILayout.BeginArea(new Rect(padding, padding, ContentWidth - (padding * 2), position.height - (padding * 2)));
        GUILayout.BeginHorizontal(GUILayout.Width(((WindowWidth / 3) * 2) - (padding * 2)));
        GUILayout.BeginVertical();

        if (selectedSO != null) {
            if (selectedSO.GetType() == typeof(GameProgress)) {
                DrawGameProgressProperties();
            }
            else if (selectedSO.GetType() == typeof(SpecialAttackDataInventory)) {
                DrawSpecialAttackInventoryProperties();
            }
            else if (selectedSO.GetType() == typeof(GameSettings)) {
                DrawGameSettingsProperties();
            }

            //if (GUI.changed) {
                EditorUtility.SetDirty(selectedSO);
            //}
        }

        GUI.color = Color.white;
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
        GUILayout.EndArea();
    }

    private void DrawTemplateLocalCopyButtons() {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUI.color = (viewingLocalCopy) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.gray;
        if (GUILayout.Button("Local Copy", GUILayout.Width(60))) {
            viewingLocalCopy = true;
            inspectorScrollPosition = Vector2.zero;
            selectedSO = selectedSO.LoadFromLocalJSON();
            GUI.FocusControl("FocusAway");
        }

        GUI.color = (!viewingLocalCopy) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.gray;
        if (GUILayout.Button("Template", GUILayout.Width(60))) {
            if (viewingLocalCopy) {
                selectedSO.SaveToLocalJSON();
            }
            viewingLocalCopy = false;
            inspectorScrollPosition = Vector2.zero;
            selectedSO = selectedSO.InitializeFromTemplate();
            GUI.FocusControl("FocusAway");
        }

        GUI.color = Color.white;

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    private void DrawValidateSpecialAttacksButton() {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
        if (GUILayout.Button("Add Special Attack", GUILayout.Width(buttonWidth))) {
            OpenSpecialAttackSelectionWindow();
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    private void DrawGameProgressProperties() {
        GUILayout.BeginVertical();

        DrawHeader("Game Progress");

        DrawTemplateLocalCopyButtons();

        DrawSelectedSODefaultInspector();

        GUILayout.EndVertical();
    }

    private void DrawSpecialAttackInventoryProperties() {
        GUILayout.BeginVertical();

        DrawHeader("Special Attack Inventory");

        DrawTemplateLocalCopyButtons();

        DrawValidateSpecialAttacksButton();

        DrawSelectedSODefaultInspector();

        GUILayout.EndVertical();
    }

    private void DrawGameSettingsProperties() {
        GUILayout.BeginVertical();

        DrawHeader("Game Settings");

        DrawTemplateLocalCopyButtons();

        DrawSelectedSODefaultInspector();

        GUILayout.EndVertical();
    }
    #endregion DRAW_METHODS

    void OnGUI() {
        if (menuBGTexture == null || windowBGTexture == null) {
            InitTextures();
        }

        Event e = Event.current;
        mousePos = e.mousePosition;

        baseGUISkin = GUI.skin;
        GUI.skin = myGUISkin;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);

        EditorGUI.BeginDisabledGroup(inputDisabled);
        GUILayout.BeginHorizontal();

        DrawMenu();
        DrawContent();

        GUILayout.EndHorizontal();
        EditorGUI.EndDisabledGroup();

        if (GUI.changed && !Application.isPlaying) {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }

        BeginWindows();
        if (characterSelectionWindow != null) {
            characterSelectionWindow.windowRect = GUILayout.Window(0, characterSelectionWindow.windowRect, DrawSelectWindow, characterSelectionWindow.windowTitle);
        }
        if (specialAttackSelectionWindow != null) {
            specialAttackSelectionWindow.windowRect = GUILayout.Window(0, specialAttackSelectionWindow.windowRect, DrawSelectWindow, specialAttackSelectionWindow.windowTitle);
        }
        if (enumSelectionWindow != null) {
            enumSelectionWindow.windowRect = GUILayout.Window(0, enumSelectionWindow.windowRect, DrawSelectWindow, enumSelectionWindow.windowTitle);
        }
        if (combatOrderSelectWindow != null) {
            combatOrderSelectWindow.windowRect = GUILayout.Window(0, combatOrderSelectWindow.windowRect, DrawSelectWindow, combatOrderSelectWindow.windowTitle);  
        }
        if (augmentSelectionWindow != null) {
            augmentSelectionWindow.windowRect = GUILayout.Window(0, augmentSelectionWindow.windowRect, DrawSelectWindow, augmentSelectionWindow.windowTitle);
        }
        EndWindows();

        GUI.skin = baseGUISkin;
    }

}
