﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(CombatSlotUtil))]
public class CombatSlotUtilEditor : Editor {

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector();

		if ((target as MonoBehaviour).gameObject.activeSelf) {
			if (GUILayout.Button("Save Positions", GUILayout.Height(40))) {
                EditorUtility.SetDirty((target as CombatSlotUtil).GetComponent<CombatManager>().gameObject);
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
				(target as CombatSlotUtil).SavePositions();
			}

            if (GUILayout.Button("Adjust X Values", GUILayout.Height(40))) {
                EditorUtility.SetDirty((target as CombatSlotUtil).GetComponent<CombatManager>().gameObject);
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                (target as CombatSlotUtil).AdjustXValues();
            }

            if (GUILayout.Button("Adjust Y Values", GUILayout.Height(40))) {
                EditorUtility.SetDirty((target as CombatSlotUtil).GetComponent<CombatManager>().gameObject);
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                (target as CombatSlotUtil).AdjustYValues();
            }

            if (GUILayout.Button("Create Empty Objects", GUILayout.Height(40))) {
                GameObject slotContainer = GameObject.Find("SlotContainer");
                if (slotContainer == null) {
                    slotContainer = new GameObject();
                    slotContainer.name = "SlotContainer";
                    slotContainer.transform.position = Vector3.zero;
                }
                EditorUtility.SetDirty((target as CombatSlotUtil).GetComponent<CombatManager>().gameObject);
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                (target as CombatSlotUtil).CreateSlotTransforms(slotContainer.transform);
            }
		}
	}
}
