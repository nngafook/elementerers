﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(GameSettings))]
public class GameSettingsEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();


        GUILayout.Space(10);
        GUI.color = Color.green;
        if (GUILayout.Button("Save", GUILayout.Height(40))) {
            (target as GameSettings).SaveToJSON();
        }
        GUI.color = Color.yellow;
        if (GUILayout.Button("Load", GUILayout.Height(40))) {
            GameSettings.OverwriteFromJSON();
        }
        GUI.color = Color.white;
    }
	
}
