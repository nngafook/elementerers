﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;

public class CharacterSelectionEditorWindow : ScriptableObject {

    private Action<List<CharacterID>> OkCallback;
    private Action CancelCallback;

    private bool shiftHeld = false;
    private Vector2 characterScrollPosition;
    private List<Texture> characterTextures;
    private List<CharacterID> characterIDOptions;
    private CharacterDataObject[] dataObjects;

    private List<CharacterID> idsToAdd;

    public Rect windowRect;
    public string windowTitle = "Character Selection";
    public Vector2 minSize = new Vector2(420, 500);

    public int selectedBtnIndex = 0;

    private void AddIDToList(CharacterID id) {
        bool idFound = false;
        for (int i = 0; i < idsToAdd.Count; i++) {
            if (idsToAdd[i] == id) {
                idFound = true;
                break;
            }
        }
        if (!idFound) {
            idsToAdd.Add(id);
        }
    }

    public void Init(ref List<CharacterID> idsToExclude) {
        bool typeFound = false;
        idsToAdd = new List<CharacterID>();
        characterTextures = new List<Texture>();
        characterIDOptions = new List<CharacterID>();
        dataObjects = null;
        dataObjects = Resources.LoadAll<CharacterDataObject>("Data/CharacterData");
        for (int i = 0; i < dataObjects.Length; i++) {
            typeFound = false;
            for (int j = 0; j < idsToExclude.Count; j++) {
                if (dataObjects[i].data.characterID == idsToExclude[j]) {
                    typeFound = true;
                    break;
                }
            }

            if (!typeFound) {
                characterTextures.Add(Utility.GenerateTextureFromSprite(dataObjects[i].data.uiImage));
                characterIDOptions.Add(dataObjects[i].data.characterID);
            }
        }
    }

    public void SetCallbacks(Action<List<CharacterID>> okCallback, Action cancelCallback) {
        OkCallback = okCallback;
        CancelCallback = cancelCallback;
    }

    private void DrawCharacterButtons() {
        GUILayout.BeginVertical();
        int totalColumns = 3;
        int i = 0;
        while (i < characterIDOptions.Count) {
            GUILayout.BeginHorizontal();
            for (int j = 0; j < totalColumns; j++) {
                if (i < characterTextures.Count) {
                    if (GUILayout.Button(characterTextures[i], GUILayout.Width(120), GUILayout.Height(120))) {
                        selectedBtnIndex = i;
                        if (shiftHeld) {
                            AddIDToList(characterIDOptions[selectedBtnIndex]);
                        }
                    }
                }
                i++;
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();
    }

    public void DrawWindow() {
        shiftHeld = Event.current.shift;

        GUILayout.BeginVertical();
        GUILayout.Space(10);

        characterScrollPosition = GUILayout.BeginScrollView(characterScrollPosition);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        DrawCharacterButtons();


        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(Utility.EnumNameToReadable(characterIDOptions[selectedBtnIndex].ToString()));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            OkCallback = null;
            if (CancelCallback != null) {
                CancelCallback();
                CancelCallback = null;
            }
        }

        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            AddIDToList(characterIDOptions[selectedBtnIndex]);
            CancelCallback = null;
            if (OkCallback != null) {
                OkCallback(idsToAdd);
                OkCallback = null;
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

       
        
        GUILayout.EndVertical();
    }

}
