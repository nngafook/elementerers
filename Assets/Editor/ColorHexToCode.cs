﻿using System.Globalization;
using UnityEditor;
using UnityEngine;

// Put this file in folder named "Editor"
// This tool will be shown in menu "Window / Color HEX To C#"
public class ColorHexToCode : EditorWindow {
    string colorHex;
    string colorScript;
    string convertButton = "Convert";

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Color HEX To C#")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        ColorHexToCode window = (ColorHexToCode)EditorWindow.GetWindow(typeof(ColorHexToCode), false, "ColorHexToC#");
        window.Show();
    }

    void OnGUI() {
        GUILayout.Label("Color Hex To Code (Remove #)", EditorStyles.boldLabel);

        colorHex = EditorGUILayout.TextField("Color in HEX", colorHex);

        if (GUILayout.Button(convertButton)) {
            colorScript = "";

            if (colorHex != null && colorHex.Length > 0) {
                Color clr = new Color(0f, 0f, 0f);

                clr.r = (float)System.Int32.Parse(colorHex.Substring(0, 2), NumberStyles.AllowHexSpecifier) / 255.0f;
                clr.g = (float)System.Int32.Parse(colorHex.Substring(2, 2), NumberStyles.AllowHexSpecifier) / 255.0f;
                clr.b = (float)System.Int32.Parse(colorHex.Substring(4, 2), NumberStyles.AllowHexSpecifier) / 255.0f;

                if (colorHex.Length == 8) {
                    clr.a = System.Int32.Parse(colorHex.Substring(6, 2), NumberStyles.AllowHexSpecifier) / 255.0f;
                }
                else {
                    clr.a = 1.0f;
                }

                colorScript = "new Color ( " + clr.r.ToString() + "f, " + clr.g.ToString() + "f, " + clr.b.ToString() + "f, " + clr.a.ToString() + "f );";
            }
        }

        EditorGUILayout.TextField("Color in C#", colorScript);
    }
}