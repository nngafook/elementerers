﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

public class CombatOrderSelectionEditorWindow : ScriptableObject {

    private Vector2 actionScrollPosition;
    private Vector2 targetScrollPosition;
    private Vector2 percentageScrollPosition;

    private CombatOrder combatOrder;
    private CombatOrder originalCombatOrder;

    private Action OKCallback;

    private float columnWidth = 130;

    public Rect windowRect;
    public string windowTitle = "Combat Order Editor";
    public Vector2 minSize = new Vector2(420, 500);

    public void Init(CombatOrder o) {
        combatOrder = o;
        originalCombatOrder = new CombatOrder();
        originalCombatOrder.Copy(o);
    }

    public void SetCallbacks(Action callback) {
        OKCallback = callback;
    }

    public void DrawWindow() {
        GUILayout.BeginVertical();
        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        // Action
        GUILayout.BeginVertical(GUILayout.Width(columnWidth));
        actionScrollPosition = GUILayout.BeginScrollView(actionScrollPosition);

        foreach (CombatStrategyActionOrder action in Enum.GetValues(typeof(CombatStrategyActionOrder))) {
            GUI.color = (combatOrder.actionOrder.Equals(action)) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(CombatStrategyUtils.GetReadableName(action))) {
                combatOrder.actionOrder = action;
            }
            GUI.color = Color.white;
        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        // Target
        GUILayout.BeginVertical(GUILayout.Width(columnWidth));
        targetScrollPosition = GUILayout.BeginScrollView(targetScrollPosition);

        if (combatOrder.actionOrder != CombatStrategyActionOrder.Defend && combatOrder.actionOrder != CombatStrategyActionOrder.None) {
            foreach (CombatStrategyTargetOrder target in Enum.GetValues(typeof(CombatStrategyTargetOrder))) {
                GUI.color = (combatOrder.targetOrder.Equals(target)) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
                if (GUILayout.Button(CombatStrategyUtils.GetReadableName(target))) {
                    combatOrder.targetOrder = target;
                }
                GUI.color = Color.white;
            }
        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        // Percentage
        GUILayout.BeginVertical(GUILayout.Width(columnWidth));
        percentageScrollPosition = GUILayout.BeginScrollView(percentageScrollPosition);
        if (combatOrder.actionOrder != CombatStrategyActionOrder.Defend && combatOrder.actionOrder != CombatStrategyActionOrder.None) {
            if (combatOrder.targetOrder == CombatStrategyTargetOrder.TargetHealthAbove || combatOrder.targetOrder == CombatStrategyTargetOrder.TargetHealthBelow) {
                foreach (CombatStrategyPercentage percentage in Enum.GetValues(typeof(CombatStrategyPercentage))) {
                    GUI.color = (combatOrder.targetPercentage.Equals(percentage)) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
                    if (GUILayout.Button(CombatStrategyUtils.GetReadableName(percentage))) {
                        combatOrder.targetPercentage = percentage;
                    }
                    GUI.color = Color.white;
                }
            }
        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            if (OKCallback != null) {
                combatOrder.Copy(originalCombatOrder);
                OKCallback();
                OKCallback = null;
            }
        }
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            if (OKCallback != null) {
                OKCallback();
                OKCallback = null;
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

}
