﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(AugmentDataObject))]
[CanEditMultipleObjects]
public class AugmentDataObjectEditor : Editor {

    private AugmentData augmentData;

    public override void OnInspectorGUI() {

        augmentData = (target as AugmentDataObject).data;

        if (augmentData != null) {
            if (string.IsNullOrEmpty(augmentData.id)) {
                if (GUILayout.Button("Generate ID")) {
                    (target as AugmentDataObject).data.id = Utility.GenerateUniqueStringID();
                }
            }

            if (!string.IsNullOrEmpty(augmentData.id)) {
                AugmentIDDatabaseDataObject idDatabase = AssetDatabase.LoadAssetAtPath<AugmentIDDatabaseDataObject>("Assets/Resources/Data/AugmentIDDatabase.asset");
                if (!idDatabase.HasID(augmentData.id)) {
                    GUI.color = Color.red;
                    if (GUILayout.Button("Add To Database")) {
                        idDatabase.Add(augmentData.id, augmentData.name);
                        EditorUtility.SetDirty(idDatabase);
                        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                    }
                }
                else if (!idDatabase.HasDataAndIsValid(augmentData.id, augmentData.name)) {
                    GUI.color = Color.red;
                    if (GUILayout.Button("Update Info")) {
                        idDatabase.UpdateInfo(augmentData.id, augmentData.name);
                        EditorUtility.SetDirty(idDatabase);
                        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                    }
                }
                GUI.color = Color.white;
            }

            GUILayout.BeginHorizontal();
            GUILayout.Label("ID");
            GUILayout.FlexibleSpace();
            Color baseColor = GUI.skin.label.normal.textColor;
            GUI.skin.label.normal.textColor = Color.red;
            GUI.skin.label.fontStyle = FontStyle.Bold;
            GUILayout.Label(augmentData.id);
            GUI.skin.label.normal.textColor = baseColor;
            GUI.skin.label.fontStyle = FontStyle.Normal;
            GUILayout.EndHorizontal();

            augmentData.name = EditorGUILayout.TextField("Name", augmentData.name);

            GUILayout.Space(10);

            augmentData.augmentType = (AugmentType)EditorGUILayout.EnumPopup("Augment Type", augmentData.augmentType);

            if (augmentData.augmentType == AugmentType.MODIFIER) {
                augmentData.effectType = (ModifierType)EditorGUILayout.EnumPopup("Modifier Type", augmentData.effectType);
            }
            else if (augmentData.augmentType == AugmentType.POTENCY) {
                augmentData.potencyType = (PotencyType)EditorGUILayout.EnumPopup("Potency Type", augmentData.potencyType);
            }

            GUILayout.Label("Values that I'm sure we'll need, but have no idea the context yet.", EditorStyles.boldLabel);
            augmentData.elementalType = (ElementalType)EditorGUILayout.EnumPopup("Elemental Type", augmentData.elementalType);
            augmentData.impact = EditorGUILayout.FloatField("Impact", augmentData.impact);
            augmentData.duration = EditorGUILayout.FloatField("Duration", augmentData.duration);
            augmentData.chance.baseValue = EditorGUILayout.FloatField("Percentage Chance", augmentData.chance.baseValue);

            if (GUI.changed && !Application.isPlaying) {
                EditorUtility.SetDirty((target as AugmentDataObject));
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
        }
    }

}
