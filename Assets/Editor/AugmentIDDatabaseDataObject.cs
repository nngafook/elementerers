﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CreateAssetMenu(menuName = "DataObjects/AugmentIDDatabase")]
public class AugmentIDDatabaseDataObject : ScriptableObject {

    public List<StringStringKeyValue> database;

    public void Add(string id, string name) {
        database.Add(new StringStringKeyValue(id, name));
    }

    public void UpdateInfo(string id, string name) {
        for (int i = 0; i < database.Count; i++) {
            if (database[i].key == id) {
                database[i].value = name;
                break;
            }
        }
        
    }

    public bool HasDataAndIsValid(string id, string name) {
        bool hasId = HasID(id);
        bool hasName = HasName(name);

        return (hasId && hasName);
    }

    public bool HasID(string id) {
        bool rVal = false;

        for (int i = 0; i < database.Count; i++) {
            if (database[i].key == id) {
                rVal = true;
                break;
            }
        }

        return rVal;
    }

    public bool HasName(string name) {
        bool rVal = false;

        for (int i = 0; i < database.Count; i++) {
            if (database[i].value == name) {
                rVal = true;
                break;
            }
        }

        return rVal;
    }

    public string NameFromID(string id) {
        string rVal = "notFound";

        for (int i = 0; i < database.Count; i++) {
            if (database[i].key == id) {
                rVal = database[i].value;
                break;
            }
        }

        return rVal;
    }

    public string IDFromName(string name) {
        string rVal = "notFound";

        for (int i = 0; i < database.Count; i++) {
            if (database[i].value == name) {
                rVal = database[i].key;
                break;
            }
        }

        return rVal;
    }

}
