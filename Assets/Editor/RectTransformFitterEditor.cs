﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(RectTransformFitter))]
public class RectTransformFitterEditor : Editor {

    private RectTransformFitter fitter;

    public override void OnInspectorGUI() {
        base.DrawDefaultInspector();

        GUILayout.Space(10);
        fitter = (RectTransformFitter)target;

        if (fitter.target != null && !fitter.CheckMatch()) {
            EditorGUILayout.HelpBox("RectTransform property does not match Target", MessageType.Warning);
            GUI.color = CustomColor.GetColor(ColorName.SONI_POOP);
            if (GUILayout.Button("Fix")) {
                fitter.Match();
            }
            GUI.color = Color.white;
        }
    }

}
