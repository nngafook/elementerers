﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class EditorMethods {

    #region EDITOR_MENU_METHODS
    [MenuItem("Assets/Copy Path %#c")]
    public static void CopyPath() {
        UnityEngine.Object obj = Selection.activeObject;
        if (obj == null) {
            return;
        }
        GUIUtility.systemCopyBuffer = AssetDatabase.GetAssetPath(obj.GetInstanceID());
        Debug.Log("Path Copied".Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
    }

    [MenuItem("Assets/Create/ScriptableObjects/CharacterDataObject")]
    public static void CreateCharacterData() {
        int suffix = Random.Range(1, 6000);
        string path = "Assets/Resources/Data/CharacterData";
        CharacterDataObject asset = ScriptableObject.CreateInstance<CharacterDataObject>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/CharacterDataObject_" + suffix.ToString() + ".asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources/Data", "CharacterData");
            AssetDatabase.CreateAsset(asset, path + "/CharacterDataObject_" + suffix.ToString() + ".asset");
        }
        //
        //AssetDatabase.SaveAssets();
        //Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/LevelDataObject")]
    public static void CreateLevelData() {
        int suffix = Random.Range(1, 6000);
        string path = "Assets/Resources/Data/LevelData";
        LevelDataObject asset = ScriptableObject.CreateInstance<LevelDataObject>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/LevelDataObject_" + suffix.ToString() + ".asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources/Data", "LevelData");
            AssetDatabase.CreateAsset(asset, path + "/LevelDataObject_" + suffix.ToString() + ".asset");
        }
        //
        //AssetDatabase.SaveAssets();
        //Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/SpecialAttackDataObject")]
    public static void CreateSpecialAttackData() {
        int suffix = Random.Range(1, 6000);
        string path = "Assets/Resources/Data/SpecialAttackData";
        SpecialAttackDataObject asset = ScriptableObject.CreateInstance<SpecialAttackDataObject>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/SpecialAttackDataObject_" + suffix.ToString() + ".asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources/Data", "SpecialAttackData");
            AssetDatabase.CreateAsset(asset, path + "/SpecialAttackDataObject_" + suffix.ToString() + ".asset");
        }
        //
        //AssetDatabase.SaveAssets();
        //Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/AugmentDataObject")]
    public static void CreateAugmentData() {
        int suffix = Random.Range(1, 6000);
        string path = "Assets/Resources/Data/AugmentData";
        AugmentDataObject asset = ScriptableObject.CreateInstance<AugmentDataObject>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/AugmentDataObject_" + suffix.ToString() + ".asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources/Data", "AugmentData");
            AssetDatabase.CreateAsset(asset, path + "/AugmentDataObject_" + suffix.ToString() + ".asset");
        }
        //
        //AssetDatabase.SaveAssets();
        //Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/GameSettings")]
    public static void CreateGameSettingsData() {
        string path = "Assets/Resources/Data";
        GameSettings asset = ScriptableObject.CreateInstance<GameSettings>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/GameSettings.asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources", "Data");
            AssetDatabase.CreateAsset(asset, path + "/GameSettings.asset");
        }
        
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/GameProgress")]
    public static void CreateGameProgressData() {
        string path = "Assets/Resources/Data";
        GameProgress asset = ScriptableObject.CreateInstance<GameProgress>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/GameProgress.asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources", "Data");
            AssetDatabase.CreateAsset(asset, path + "/GameProgress.asset");
        }

        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/SpecialAttackDataInventory")]
    public static void CreateSpecialAttackDataInventoryData() {
        string path = "Assets/Resources/Data";
        SpecialAttackDataInventory asset = ScriptableObject.CreateInstance<SpecialAttackDataInventory>();
        if (AssetDatabase.IsValidFolder(path)) {
            AssetDatabase.CreateAsset(asset, path + "/SpecialAttackDataInventory.asset");
        }
        else {
            AssetDatabase.CreateFolder("Assets/Resources", "Data");
            AssetDatabase.CreateAsset(asset, path + "/SpecialAttackDataInventory.asset");
        }

        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    #endregion EDITOR_MENU_METHODS
}

public class EditorWindowMethods : EditorWindow {
    [MenuItem("Window/Close Window %w")]
    static void CloseWindow() {
        if (focusedWindow != null) {
            if (focusedWindow.GetType() == typeof(LevelEditorWindow) || focusedWindow.GetType() == typeof(GameDataEditorWindow) || focusedWindow.GetType() == typeof(DialogueEditorWindow)) {
                focusedWindow.Close();
            }
        }
    }
}