using System;
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;

public class ShortcutToolbox : EditorWindow {

    private List<string> scenePaths = new List<string>();

    private Texture2D windowBGTexture;
    private Vector2 scenesScrollPosition;

    private void InitTextures() {
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.4980392f, 0.572549f, 0.6901961f, 1f));
        windowBGTexture.Apply();
    }

    void Awake() {
        Init();
        InitTextures();
    }

    void OnEnable() {
        Init();
        InitTextures();
    }

    void OnFocus() {
        Init();
        InitTextures();
    }

    private string StripAssetsPath(string fullPath) {
        string rVal = fullPath.Substring(fullPath.IndexOf("Assets"));
        return rVal;
    }

    private string StripScenesPath(string fullPath) {
        string rVal = Path.GetFileNameWithoutExtension(fullPath);
        return rVal;
    }

    private string RemoveExtension(string filename) {
        return filename.Substring(0, filename.LastIndexOf("."));
    }

    private void Init() {
        scenePaths.Clear();

        string scenesFolderPath = Application.dataPath + "/Scenes";
        DirectoryInfo scenesDirectoryPath = new DirectoryInfo(scenesFolderPath);

        FileInfo[] fileInfos = scenesDirectoryPath.GetFiles("*.*", SearchOption.AllDirectories);
        string path = "";
        //fileInfos.Sort((x, y) => string.Compare(x.Name, y.Name));
        for (int i = 0; i < fileInfos.Length; i++) {
            if (fileInfos[i].Extension == ".unity") {
                path = StripAssetsPath(fileInfos[i].FullName);
                scenePaths.Add(path);
            }
        }
    }

    [MenuItem("Window/Shortcut Toolbox")]
    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(ShortcutToolbox));
    }

    private static void DrawDataObjectOptions() {
        GUILayout.Space(10);
        GUI.color = Color.yellow;
        if (GUILayout.Button("Select Data Objects Folder")) {
            Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/CharacterData");
            EditorGUIUtility.PingObject(Selection.activeObject);
        }
        GUI.color = Color.white;
    }

    private void DrawLoadoutTasks() {
        // TODO: These should get init on start. This is happening every frame
        Camera mainCam = Camera.main;
        LoadoutCameraManager loadCamManager = mainCam.GetComponent<LoadoutCameraManager>();
        CharacterSelectWindow selectWindow = GameObject.Find("CharacterSelectWindow").GetComponent<CharacterSelectWindow>();
        CharacterDetailsWindow detailsWindow = GameObject.Find("CharacterDetailsWindow").GetComponent<CharacterDetailsWindow>();
        SettingsWindow settingsWindow = GameObject.Find("SettingsWindow").GetComponent<SettingsWindow>();
        EquipWindow equipWindow = GameObject.Find("EquipWindow").GetComponent<EquipWindow>();

        loadCamManager.currentCameraPositionName = (LoadoutCameraPositionName)EditorGUILayout.EnumPopup("Position", loadCamManager.currentCameraPositionName);

        if (GUILayout.Button("Go To Position", GUILayout.Height(30))) {
            loadCamManager.EditorGoToCurrent();
        }

        DrawDataObjectOptions();

        GUILayout.Space(10);
        GUILayout.BeginVertical();
        GUILayout.Label("Equip Window");
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Open Equip Window")) {
            equipWindow.EditorOpen();
        }
        if (GUILayout.Button("Close Equip Window")) {
            equipWindow.EditorClose();
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Select Equip Window")) {
            EditorGUIUtility.PingObject(equipWindow);
        }
        GUILayout.EndVertical();

        GUILayout.Space(10);
        GUILayout.BeginVertical();
        GUILayout.Label("Character Select Window");
        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.PRS_PURPLE);
        if (GUILayout.Button("Open Char Window")) {
            selectWindow.GetComponent<RectTransform>().localScale = Vector3.one;
        }
        if (GUILayout.Button("Close Char Window")) {
            selectWindow.GetComponent<RectTransform>().localScale = Vector3.zero;
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Select Char Window")) {
            EditorGUIUtility.PingObject(selectWindow);
        }
        GUILayout.EndVertical();


        GUILayout.Space(10);
        GUILayout.BeginVertical();
        GUILayout.Label("Character Details Window");
        GUI.color = CustomColor.GetColor(ColorName.WARNING_YELLOW);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Open Char Details")) {
            detailsWindow.GetComponent<RectTransform>().localScale = Vector3.one;
        }
        if (GUILayout.Button("Close Char Details")) {
            detailsWindow.GetComponent<RectTransform>().localScale = Vector3.zero;
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Select Char Details")) {
            EditorGUIUtility.PingObject(detailsWindow);
        }
        GUILayout.EndVertical();

        GUILayout.Space(10);
        GUILayout.BeginVertical();
        GUILayout.Label("Settings Window");
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Open Settings Window")) {
            settingsWindow.GetComponent<CanvasGroup>().alpha = 1;
        }
        if (GUILayout.Button("Close Settings Window")) {
            settingsWindow.GetComponent<CanvasGroup>().alpha = 0;
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Select Settings Window")) {
            EditorGUIUtility.PingObject(settingsWindow);
        }
        GUILayout.EndVertical();


        GUI.color = Color.white;
    }

    private void DrawCombatTasks() {
        // TODO: These should get init on start. This is happening every frame
        //ActionButtonBar actionButtonBar = GameObject.Find("ActionButtonBar").GetComponent<ActionButtonBar>();
        CombatResultsWindow resultsWindow = GameObject.Find("CombatResultsWindow").GetComponent<CombatResultsWindow>();

        DrawDataObjectOptions();

        //GUILayout.Space(10);
        //GUILayout.BeginVertical();
        //GUILayout.BeginHorizontal();
        //if (GUILayout.Button("Open Action Bar")) {
        //    actionButtonBar.GetComponent<RectTransform>().localScale = Vector3.one;
        //}
        //if (GUILayout.Button("Close Action Bar")) {
        //    actionButtonBar.GetComponent<RectTransform>().localScale = Vector3.zero;
        //}
        //GUILayout.EndHorizontal();
        //if (GUILayout.Button("Select Action Bar")) {
        //    EditorGUIUtility.PingObject(actionButtonBar);
        //}
        //GUILayout.EndVertical();

        GUI.color = Color.green;
        GUILayout.Space(10);
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Open Combat Results")) {
            resultsWindow.CG().SetAlphaAndBools(true);
        }
        if (GUILayout.Button("Close Combat Results")) {
            resultsWindow.CG().SetAlphaAndBools(false);
        }
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Select Combat Results")) {
            EditorGUIUtility.PingObject(resultsWindow);
        }
        GUILayout.EndVertical();
        GUI.color = Color.white;

    }

    void OnGUI() {
        if (windowBGTexture == null) {
            InitTextures();
        }
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);
        GUILayout.Space(10);

        scenesScrollPosition = GUILayout.BeginScrollView(scenesScrollPosition);
        GUILayout.Label("Scenes", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        for (int i = 0; i < scenePaths.Count; i++) {
            if (GUILayout.Button(StripScenesPath(scenePaths[i]))) {
                //EditorSceneManager.SaveOpenScenes();
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
                    EditorSceneManager.OpenScene(scenePaths[i]);
                }
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(20);
        GUILayout.Label("Tasks", EditorStyles.boldLabel);

        if (SceneManager.GetActiveScene().name == SceneName.LOADOUT) {
            DrawLoadoutTasks();
        }
        else if (SceneManager.GetActiveScene().name == SceneName.COMBAT) {
            DrawCombatTasks();
        }
        else if (SceneManager.GetActiveScene().name == SceneName.DIALOGUE) {
            DrawDataObjectOptions();
        }
        else if (SceneManager.GetActiveScene().name == SceneName.MAIN_MENU) {
            DrawDataObjectOptions();
        }

        GUILayout.EndScrollView();

        if (GUI.changed) {
            if (!EditorSceneManager.GetActiveScene().isDirty) {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
        }
    }
}