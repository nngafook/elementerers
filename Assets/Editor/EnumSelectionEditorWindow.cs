﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

public class EnumSelectionEditorWindow : ScriptableObject {

    private Enum enumType;
    private Vector2 windowScrollPosition;
    private Enum selectedValue;

    private Action<Enum> OKCallback;

    public Rect windowRect;
    public string windowTitle = "Enum Selection";
    public Vector2 minSize = new Vector2(420, 500);

    public void Init(Enum e) {
        enumType = e;
        selectedValue = enumType;
    }

    public void SetCallbacks(Action<Enum> callback) {
        OKCallback = callback;
    }

    public void DrawWindow() {
        GUILayout.BeginVertical();
        GUILayout.Space(10);


        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        windowScrollPosition = GUILayout.BeginScrollView(windowScrollPosition);


        int currentColumn = 0;
        int totalColumns = 3;
        int iter = 0;

        foreach (Enum value in Enum.GetValues(enumType.GetType())) {
            if (currentColumn == 0) {
                GUILayout.BeginHorizontal();

            }

            GUI.color = (value.Equals(selectedValue)) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(value.ToString(), GUILayout.Width(100), GUILayout.Height(100))) {
                selectedValue = value;
            }

            currentColumn++;
            if (currentColumn == totalColumns) {
                GUILayout.EndHorizontal();
                currentColumn = 0;
            }
            iter++;
        }
        if (currentColumn != 0) {
            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            if (OKCallback != null) {
                OKCallback(selectedValue);
                OKCallback = null;
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

}
