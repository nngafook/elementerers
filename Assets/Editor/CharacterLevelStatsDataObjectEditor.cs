﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(CharacterLevelStatsDataObject))]
public class CharacterLevelStatsDataObjectEditor : Editor {

    private CharacterLevelStatsDataObject statsDataObject;
    private static CharacterLevelStats selectedLevelStats;

    private List<CharacterLevelStats> levelStats = new List<CharacterLevelStats>();

    private bool expRequirementsExpanded = false;
    private bool combatOrderCapacitiesExpanded = false;
    private bool maxHealthLevelValuesExpanded = false;
    private bool defenseLevelValuesExpanded = false;
    private bool damageLevelValuesExpanded = false;
    private bool evasionLevelValuesExpanded = false;

    private void CollapseAll() {
        expRequirementsExpanded = false;
        combatOrderCapacitiesExpanded = false;
        maxHealthLevelValuesExpanded = false;
        defenseLevelValuesExpanded = false;
        damageLevelValuesExpanded = false;
        evasionLevelValuesExpanded = false;
    }

    private void ExpandAll() {
        expRequirementsExpanded = true;
        combatOrderCapacitiesExpanded = true;
        maxHealthLevelValuesExpanded = true;
        defenseLevelValuesExpanded = true;
        damageLevelValuesExpanded = true;
        evasionLevelValuesExpanded = true;
    }

    private void DrawIntList(ref List<int> list) {
        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
        if (GUILayout.Button("Clear")) {
            list.Clear();
        }
        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
        if (GUILayout.Button("Remove")) {
            if (list.Count > 0) {
                list.RemoveAt(list.Count - 1);
            }
        }
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("Add")) {
            list.Add(816);
        }
        GUILayout.EndHorizontal();
        GUI.color = Color.white;

        for (int i = 0; i < list.Count; i++) {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            list[i] = EditorGUILayout.IntField("Level " + (i + 1).ToString(), list[i]);
            GUILayout.EndHorizontal();
        }
    }

    private void DrawFloatList(ref List<float> list) {
        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
        if (GUILayout.Button("Clear")) {
            list.Clear();
        }
        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
        if (GUILayout.Button("Remove")) {
            if (list.Count > 0) {
                list.RemoveAt(list.Count - 1);
            }
        }
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("Add")) {
            list.Add(0);
        }
        GUILayout.EndHorizontal();
        GUI.color = Color.white;

        for (int i = 0; i < list.Count; i++) {
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            list[i] = EditorGUILayout.FloatField("Level " + (i + 1).ToString(), list[i]);
            GUILayout.EndHorizontal();
        }
    }

    private void DrawTopButtons() {
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        if (GUILayout.Button("Add New Character Level Stat")) {
            levelStats.Add(new CharacterLevelStats());
        }
        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("Collapse All")) {
            CollapseAll();
        }
        if (GUILayout.Button("Expand All")) {
            ExpandAll();
        }
        GUILayout.EndHorizontal();
        GUI.color = Color.white;
    }

    private void DrawExpRequirements() {
        List<int> expRequirements = selectedLevelStats.expRequirements;

        DrawIntList(ref expRequirements);
    }

    private void DrawCombatOrderCapacities() {
        List<int> combatOrderCapacities = selectedLevelStats.combatOrderCapacities;

        DrawIntList(ref combatOrderCapacities);
    }

    private void DrawMaxHPLevels() {
        List<int> maxHealthLevelValues = selectedLevelStats.maxHealthLevelValues;

        DrawIntList(ref maxHealthLevelValues);
    }

    private void DrawDefenseLevels() {
        List<float> defenseLevelValues = selectedLevelStats.defenseLevelValues;

        DrawFloatList(ref defenseLevelValues);
    }

    private void DrawDamageLevels() {
        List<float> damageLevelValues = selectedLevelStats.damageLevelValues;

        DrawFloatList(ref damageLevelValues);
    }

    private void DrawEvasionLevels() {
        List<float> evasionLevelValus = selectedLevelStats.evasionLevelValues;

        DrawFloatList(ref evasionLevelValus);
    }

    public override void OnInspectorGUI() {

        statsDataObject = (target as CharacterLevelStatsDataObject);

        levelStats = statsDataObject.characterLevelStats;

        GUILayout.BeginVertical();

        DrawTopButtons();
        GUILayout.Space(10);

        for (int i = 0; i < levelStats.Count; i++) {
            GUI.color = (selectedLevelStats != null && selectedLevelStats == levelStats[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            int buttonHeight = (selectedLevelStats != null && selectedLevelStats == levelStats[i]) ? 40 : 20;
            GUILayout.BeginHorizontal();
            if (GUILayout.Button(Utility.EnumNameToReadable(levelStats[i].idOwner.ToString()), GUILayout.Height(buttonHeight))) {
                if ((selectedLevelStats == null) || (selectedLevelStats != null && selectedLevelStats != levelStats[i])) {
                    selectedLevelStats = levelStats[i];
                }
                else {
                    selectedLevelStats = null;
                    CollapseAll();
                }
            }
            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("Remove", GUILayout.Width(75), GUILayout.Height(buttonHeight))) {
                if (levelStats.Count > 0) {
                    levelStats.RemoveAt(i);
                    break;
                }
            }
            GUI.color = CustomColor.GetColor(ColorName.YALLOW);
            if (GUILayout.Button("Duplicate", GUILayout.Width(75), GUILayout.Height(buttonHeight))) {
                if (levelStats.Count > 0) {
                    CharacterLevelStats dupe = new CharacterLevelStats(levelStats[i]);
                    levelStats.Insert(i + 1, dupe);
                }
            }
            GUILayout.EndHorizontal();
            GUI.color = Color.white;

            if (selectedLevelStats != null && levelStats[i] == selectedLevelStats) {
                
                GUILayout.Space(5);
                selectedLevelStats.idOwner = (CharacterID)EditorGUILayout.EnumPopup("Character ID", selectedLevelStats.idOwner);

                GUILayout.Space(5);
                expRequirementsExpanded = EditorGUILayout.Foldout(expRequirementsExpanded, "EXP Requirements (Determinate)");
                if (expRequirementsExpanded) {
                    DrawExpRequirements();
                }

                GUILayout.Space(5);
                combatOrderCapacitiesExpanded = EditorGUILayout.Foldout(combatOrderCapacitiesExpanded, "Combat Order Capacities (Determinate)");
                if (combatOrderCapacitiesExpanded) {
                    DrawCombatOrderCapacities();
                }

                GUILayout.Space(5);
                maxHealthLevelValuesExpanded = EditorGUILayout.Foldout(maxHealthLevelValuesExpanded, "Max HP Levels (Additive)");
                if (maxHealthLevelValuesExpanded) {
                    DrawMaxHPLevels();
                }

                GUILayout.Space(5);
                defenseLevelValuesExpanded = EditorGUILayout.Foldout(defenseLevelValuesExpanded, "DEF Levels (Additive)");
                if (defenseLevelValuesExpanded) {
                    DrawDefenseLevels();
                }

                GUILayout.Space(5);
                damageLevelValuesExpanded = EditorGUILayout.Foldout(damageLevelValuesExpanded, "DMG Levels (Additive)");
                if (damageLevelValuesExpanded) {
                    DrawDamageLevels();
                }

                GUILayout.Space(5);
                evasionLevelValuesExpanded = EditorGUILayout.Foldout(evasionLevelValuesExpanded, "Evasion Levels (Additive)");
                if (evasionLevelValuesExpanded) {
                    DrawEvasionLevels();
                }

            }

            GUILayout.Space(10);
        }

        GUILayout.EndVertical();

        if (GUI.changed) {
            EditorUtility.SetDirty(target);
        }
    }

}
