﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class EditorList
{

    public static void Show(SerializedProperty list, string listName = null, List<string> elementNames = null)
    {
        GUIContent label = new GUIContent(listName);
        EditorGUILayout.PropertyField(list, label);
        EditorGUI.indentLevel += 1;
        label = null;
        if (list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
            for (int i = 0; i < list.arraySize; i++)
            {
                if (elementNames != null)
                {
                    label = new GUIContent(elementNames[i]);
                }
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), label);
            }
        }
        EditorGUI.indentLevel -= 1;
    }

}
