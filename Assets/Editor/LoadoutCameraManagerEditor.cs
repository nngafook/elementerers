﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(LoadoutCameraManager))]
public class LoadoutCameraManagerEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        GUILayout.Space(20);
        GUILayout.Label("Debug Positioning", EditorStyles.boldLabel);
        GUI.color = Color.green;
        (target as LoadoutCameraManager).currentCameraPositionName = (LoadoutCameraPositionName)EditorGUILayout.EnumPopup("Position Name", (target as LoadoutCameraManager).currentCameraPositionName);

        if (GUILayout.Button("Go To Position", GUILayout.Height(30))) {
            (target as LoadoutCameraManager).EditorGoToCurrent();
            //EditorUtility.SetDirty(target);
        }
        GUI.color = Color.white;

        if (GUI.changed) {
            if (!EditorSceneManager.GetActiveScene().isDirty) {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }
        }
    }

}
