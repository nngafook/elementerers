﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Globalization;

public class CustomColorWindow : EditorWindow {

    private Color selectedColor;
    private Color newColor = Color.white;

    private Vector2 colorScrollPosition;

    private float padding = 10;
    private float buttonSize = 128;
    private int totalColumns = 3;

    private float HalfWindowWidth { get { return (position.width / 2); } }

    void Awake() {
        this.minSize = new Vector2(850, 600);
        this.maxSize = this.minSize;
    }

    void OnEnable() {
    }

    private void GetGUISkin() {
        //myGUISkin = (GUISkin)AssetDatabase.LoadAssetAtPath("Assets/Resources/GameDataEditorGUISkin.guiskin", typeof(GUISkin));
    }

    #region EDITOR_MENU_METHODS
    [MenuItem("Game Tools/Custom Color Window", priority = 0)]
    static void Init() {
        // Get existing open window or if none, make a new one:
        CustomColorWindow window = (CustomColorWindow)EditorWindow.GetWindow(typeof(CustomColorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    private string HexToCSharp(string colorHex) {
        string rVal = "";
        if (colorHex != null && colorHex.Length > 0) {
            Color clr = new Color(0f, 0f, 0f);

            clr.r = (float)System.Int32.Parse(colorHex.Substring(0, 2), NumberStyles.AllowHexSpecifier) / 255.0f;
            clr.g = (float)System.Int32.Parse(colorHex.Substring(2, 2), NumberStyles.AllowHexSpecifier) / 255.0f;
            clr.b = (float)System.Int32.Parse(colorHex.Substring(4, 2), NumberStyles.AllowHexSpecifier) / 255.0f;

            if (colorHex.Length == 8) {
                clr.a = System.Int32.Parse(colorHex.Substring(6, 2), NumberStyles.AllowHexSpecifier) / 255.0f;
            }
            else {
                clr.a = 1.0f;
            }

            rVal = "new Color ( " + clr.r.ToString() + "f, " + clr.g.ToString() + "f, " + clr.b.ToString() + "f, " + clr.a.ToString() + "f );";
        }
        return rVal;
    }

    private void DrawLeftColumn() {
        GUILayout.BeginArea(new Rect(padding, padding, HalfWindowWidth, position.height - padding));
        colorScrollPosition = GUILayout.BeginScrollView(colorScrollPosition);

        GUILayout.BeginVertical();

        int i = 0;
        int buttonsDrawn = 0;
        int totalEnumValues = Enum.GetNames(typeof(ColorName)).Length;

        foreach (ColorName color in Enum.GetValues(typeof(ColorName))) {

            if (buttonsDrawn == 0) {
                GUILayout.BeginHorizontal();
            }
            if (i < totalEnumValues) {
                GUI.color = CustomColor.GetColor(color);
                if (GUILayout.Button(Utility.EnumNameToReadable(color.ToString()), GUILayout.Width(buttonSize), GUILayout.Height(buttonSize))) {
                    selectedColor = CustomColor.GetColor(color);
                    string hexColor = ColorUtility.ToHtmlStringRGBA(selectedColor);
                    GUIUtility.systemCopyBuffer = hexColor;
                    Debug.Log(("Color HEX Copied: " + hexColor).Colored(selectedColor));
                }
                buttonsDrawn++;
                if (buttonsDrawn == totalColumns) {
                    GUILayout.EndHorizontal();
                    buttonsDrawn = 0;
                }
            }
            i++;
        }
        if (buttonsDrawn < totalColumns && buttonsDrawn != 0) {
            GUILayout.EndHorizontal();
        }

        GUI.color = Color.white;

        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    private void DrawRightColumn() {
        GUILayout.BeginArea(new Rect(HalfWindowWidth + (padding * 2), padding, HalfWindowWidth - (padding * 3), position.height - padding));
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();

        GUI.color = Color.white;
        newColor = EditorGUILayout.ColorField("New Color", newColor);
        string newColorString = ColorUtility.ToHtmlStringRGBA(newColor);

        Color preLabelNormalColor = GUI.skin.textField.normal.textColor;
        Color preLabelActiveColor = GUI.skin.textField.focused.textColor;
        GUI.skin.textField.normal.textColor = newColor;
        GUI.skin.textField.focused.textColor = newColor;
        newColorString = EditorGUILayout.TextField("Hex Code", newColorString, GUILayout.Width(HalfWindowWidth - (padding * 3)));
        ColorUtility.TryParseHtmlString("#" + newColorString, out newColor);

        GUI.color = Color.white;
        if (GUILayout.Button("Copy New Color Hex")) {
            string copyString = "#" + newColorString;
            GUIUtility.systemCopyBuffer = copyString;
            Debug.Log(("Color HEX Copied: " + copyString).Colored(newColor));
        }

        string cSharpColor = HexToCSharp(newColorString);
        EditorGUILayout.TextField("C#", cSharpColor, GUILayout.Width(HalfWindowWidth - (padding * 3)));
        if (GUILayout.Button("Copy New Color C#")) {
            GUIUtility.systemCopyBuffer = cSharpColor;
            Debug.Log(("Color C# Copied: " + cSharpColor).Colored(newColor));
        }

        GUI.skin.textField.normal.textColor = preLabelNormalColor;
        GUI.skin.textField.focused.textColor = preLabelActiveColor;

        GUI.color = Color.white;
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }


    void OnGUI() {
        GUI.skin.button.wordWrap = true;

        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();


        DrawLeftColumn();

        DrawRightColumn();


        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUI.skin.button.wordWrap = false;
    }


}
