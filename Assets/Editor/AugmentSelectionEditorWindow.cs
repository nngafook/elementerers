﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;

public class AugmentSelectionEditorWindow : ScriptableObject {

    private int slotNumber = 0;

    private Vector2 leftScrollPosition;
    private Vector2 rightScrollPosition;

    private AugmentDataObject selectedData;
    private Action<AugmentDataObject, int> OkCallback;
    private Action CancelCallback;

    private AugmentDataObject[] modifierDataObjects;
    private AugmentDataObject[] potencyDataObjects;

    public Rect windowRect;
    public string windowTitle = "Augment Selection";
    public Vector2 minSize = new Vector2(420, 500);

    public void Init(int slot) {
        slotNumber = slot;
        modifierDataObjects = Resources.LoadAll<AugmentDataObject>("Data/AugmentData/Modifier");
        potencyDataObjects = Resources.LoadAll<AugmentDataObject>("Data/AugmentData/Potency");
    }

    public void SetCallbacks(Action<AugmentDataObject, int> okCallback, Action cancelCallback) {
        OkCallback = okCallback;
        CancelCallback = cancelCallback;
    }

    public void DrawWindow() {
        GUILayout.BeginVertical();
        GUILayout.Space(10);

        GUILayout.BeginHorizontal();
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        GUILayout.FlexibleSpace();
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label("Modifiers", GUILayout.Width(160));
        GUILayout.FlexibleSpace();
        GUILayout.Label("Potency", GUILayout.Width(160));
        GUILayout.FlexibleSpace();
        GUI.skin.label.alignment = TextAnchor.UpperLeft;
        GUILayout.EndHorizontal();

        GUI.color = Color.white;

        GUILayout.BeginHorizontal();

        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        leftScrollPosition = GUILayout.BeginScrollView(leftScrollPosition);
        for (int i = 0; i < modifierDataObjects.Length; i++) {
            GUI.color = (selectedData == modifierDataObjects[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(modifierDataObjects[i].data.name, GUILayout.Width(160))) {
                selectedData = modifierDataObjects[i];
            }
            GUI.color = Color.white;
        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        GUILayout.FlexibleSpace();

        GUILayout.BeginVertical();
        rightScrollPosition = GUILayout.BeginScrollView(rightScrollPosition);
        for (int i = 0; i < potencyDataObjects.Length; i++) {
            GUI.color = (selectedData == potencyDataObjects[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(potencyDataObjects[i].data.name, GUILayout.Width(160))) {
                selectedData = potencyDataObjects[i];
            }
            GUI.color = Color.white;
        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        if (GUILayout.Button("Clear Augment", GUILayout.Width(100))) {
            CancelCallback = null;
            if (OkCallback != null) {
                OkCallback(null, slotNumber);
                OkCallback = null;
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            OkCallback = null;
            if (CancelCallback != null) {
                CancelCallback();
                CancelCallback = null;
            }
        }

        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            CancelCallback = null;
            if (OkCallback != null) {
                OkCallback(selectedData, slotNumber);
                OkCallback = null;
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();



        GUILayout.EndVertical();
    }

}
