﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(LevelDataObject))]
public class LevelDataObjectEditor : Editor {

    private LevelDataObject levelDataObject;
    private LevelData levelData;

    private static TutorialLevelEvent selectedTutorialLevelEvent;

    private int enemyCountToFill = 0;

    private bool enemiesToSpawnExpanded = false;
    private bool arenaEventsExpanded = false;
    private bool arenaWavesExpanded = false;
    private bool waveBoostsAvailableExpanded = false;
    private bool tutorialEventsExpanded = false;
    private bool tutorialMessagesExpanded = false;

    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawArenaProperties() {
        GUILayout.Label("Arena", EditorStyles.boldLabel);
        DrawHorizontalLabeledFloatField("Arena Level Timer", ref levelData.timeBetweenDifficultyIncrease);
        DrawHorizontalLabeledIntField("Area Completion Bonus", ref levelData.arenaCompletionBonus);
        DrawHorizontalLabeledIntField("Waves Before Boost", ref levelData.wavesBeforeBoost);

        GUILayout.Space(20);

        waveBoostsAvailableExpanded = EditorGUILayout.Foldout(waveBoostsAvailableExpanded, "Available Wave Boosts");

        if (waveBoostsAvailableExpanded) {
            if (levelData.waveBoostsAvailable.Count == 0) {
                GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                if (GUILayout.Button("Add", GUILayout.Width(100))) {
                    levelData.waveBoostsAvailable.Add(WaveBoost.Heal);
                }
                GUI.color = Color.white;
            }
            else {
                // Get unused
                List<WaveBoost> unusedWaveBoosts = new List<WaveBoost>();
                bool inUse = false;
                foreach (WaveBoost boost in Enum.GetValues(typeof(WaveBoost))) {
                    inUse = false;
                    for (int i = 0; i < levelData.waveBoostsAvailable.Count; i++) {
                        if (boost == levelData.waveBoostsAvailable[i]) {
                            inUse = true;
                            break;
                        }
                    }
                    if (!inUse && boost != WaveBoost.None) {
                        unusedWaveBoosts.Add(boost);
                    }
                }

                GUILayout.BeginHorizontal(GUILayout.Width((Screen.width / 2) - 50));

                if (unusedWaveBoosts.Count > 0) {
                    GUILayout.BeginVertical();

                    GUILayout.Label("Not Available", EditorStyles.boldLabel);
                    for (int i = 0; i < unusedWaveBoosts.Count; i++) {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label(Utility.CamelCaseToReadable(unusedWaveBoosts[i].ToString()), GUILayout.Width(Screen.width / 4));
                        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                        if (GUILayout.Button("Add", GUILayout.Width(Screen.width / 5))) {
                            levelData.waveBoostsAvailable.Add(unusedWaveBoosts[i]);
                        }
                        GUI.color = Color.white;
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndVertical();
                }

                GUILayout.BeginVertical();
                if (levelData.waveBoostsAvailable.Count > 0) {
                    GUILayout.Label("Available", EditorStyles.boldLabel);
                    for (int i = 0; i < levelData.waveBoostsAvailable.Count; i++) {
                        GUILayout.BeginHorizontal();
                        GUILayout.Label(Utility.CamelCaseToReadable(levelData.waveBoostsAvailable[i].ToString()), GUILayout.Width(Screen.width / 4));
                        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                        if (GUILayout.Button("Remove", GUILayout.Width(Screen.width / 5))) {
                            levelData.waveBoostsAvailable.RemoveAt(i);
                            break;
                        }
                        GUI.color = Color.white;
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndVertical();
                }

                GUILayout.EndHorizontal();
            }
        }

        GUILayout.Space(10);

        //! **Arena Waves**
        arenaWavesExpanded = EditorGUILayout.Foldout(arenaWavesExpanded, "Waves");

        if (arenaWavesExpanded) {
            ArenaWave wave = null;
            if (levelData.arenaWaves.Count == 0) {
                GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                if (GUILayout.Button("Add", GUILayout.Width(100))) {
                    levelData.arenaWaves.Add(new ArenaWave());
                }
                GUI.color = Color.white;
            }
            else {
                for (int i = 0; i < levelData.arenaWaves.Count; i++) {
                    wave = levelData.arenaWaves[i];
                    GUILayout.BeginHorizontal();
                    GUILayout.Space(10);
                    wave.expandedInEditor = EditorGUILayout.Foldout(wave.expandedInEditor, "Wave " + i.ToString());
                    GUILayout.EndHorizontal();

                    if (wave.expandedInEditor) {
                        GUILayout.BeginHorizontal();
                        GUILayout.Space(10);

                        GUILayout.BeginVertical();

                        GUILayout.BeginHorizontal();
                        GUILayout.Space(10);
                        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                        if (GUILayout.Button("Remove", GUILayout.Width(100))) {
                            levelData.arenaWaves.RemoveAt(i);
                        }
                        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                        if (GUILayout.Button("Add", GUILayout.Width(100))) {
                            levelData.arenaWaves.Insert(i + 1, new ArenaWave());
                        }
                        GUI.color = Color.white;
                        GUILayout.EndHorizontal();

                        DrawHorizontalLabeledIntField("Enemies In Wave", ref wave.totalEnemiesInWave);

                        GUILayout.Label("Reward", EditorStyles.boldLabel);
                        DrawHorizontalLabeledIntField("Scrap Reward", ref wave.rewardsData.scrapReward);
                        DrawHorizontalLabeledIntField("EXP Reward", ref wave.rewardsData.expReward);

                        GUILayout.Label("Enemy Pool", EditorStyles.boldLabel);
                        if (wave.enemyPool.Count == 0) {
                            EditorGUILayout.HelpBox("The wave's enemy pool needs at least one character ID", MessageType.Error);
                        }
                        CharacterID id = CharacterID.NONE;

                        // ================== //
                        GUILayout.BeginHorizontal();

                        GUILayout.BeginHorizontal(GUILayout.Width((Screen.width / 2) - 50));
                        GUILayout.BeginVertical();
                        // Get id's not in pool
                        List<CharacterID> idsNotInPool = new List<CharacterID>();
                        bool idFound = false;
                        foreach (CharacterID charID in Enum.GetValues(typeof(CharacterID))) {
                            idFound = false;
                            if ((charID.ToString())[0] == 'e') {
                                for (int index = 0; index < wave.enemyPool.Count; index++) {
                                    if (wave.enemyPool[index] == charID) {
                                        idFound = true;
                                        break;
                                    }
                                }
                                if (!idFound && charID != CharacterID.NONE) {
                                    idsNotInPool.Add(charID);
                                }
                            }
                        }

                        GUILayout.Label("Available", EditorStyles.boldLabel);
                        for (int j = 0; j < idsNotInPool.Count; j++) {
                            id = idsNotInPool[j];
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(Utility.CharacterIDToReadable(id.ToString()), GUILayout.Width(Screen.width / 4));
                            GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                            if (GUILayout.Button("Add To Pool", GUILayout.Width(Screen.width / 5))) {
                                wave.enemyPool.Add(id);
                            }
                            GUI.color = Color.white;
                            GUILayout.FlexibleSpace();
                            GUILayout.EndHorizontal();
                        }
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal(GUILayout.Width((Screen.width / 2) - 50));
                        GUILayout.BeginVertical();
                        GUILayout.Label("In Wave Pool", EditorStyles.boldLabel);

                        for (int j = 0; j < wave.enemyPool.Count; j++) {
                            id = wave.enemyPool[j];
                            GUILayout.BeginHorizontal();
                            GUILayout.Label(Utility.CharacterIDToReadable(id.ToString()), GUILayout.Width(Screen.width / 4));
                            GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                            if (GUILayout.Button("Remove", GUILayout.Width(Screen.width / 5))) {
                                wave.enemyPool.RemoveAt(j);
                                break;
                            }
                            GUI.color = Color.white;
                            GUILayout.FlexibleSpace();
                            GUILayout.EndHorizontal();
                        }
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();

                        GUILayout.EndHorizontal();
                        // ================ //

                        GUILayout.EndHorizontal();      // End horizontal used just for the indent space
                        GUILayout.EndVertical();
                    }
                }
            }
        }

        GUILayout.Space(10);

        //! **Arena Events**
        arenaEventsExpanded = EditorGUILayout.Foldout(arenaEventsExpanded, "Arena Events");
        if (arenaEventsExpanded) {
            ArenaEvent arenaEvent = null;
            if (levelData.arenaEvents.Count == 0) {
                GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                if (GUILayout.Button("Add", GUILayout.Width(100))) {
                    levelData.arenaEvents.Add(new ArenaEvent());
                }
                GUI.color = Color.white;
            }
            else {
                int fallbackCount = 0;
                for (int i = 0; i < levelData.arenaEvents.Count; i++) {
                    if (levelData.arenaEvents[i].isFallback) {
                        fallbackCount++;
                    }
                }

                if (fallbackCount != 1) {
                    EditorGUILayout.HelpBox("Every Arena Level must have ONE event marked as a fallback", MessageType.Error);
                }

                for (int i = 0; i < levelData.arenaEvents.Count; i++) {
                    arenaEvent = levelData.arenaEvents[i];

                    GUILayout.BeginVertical();
                    GUILayout.BeginHorizontal();

                    GUILayout.Label("Arena Event " + i.ToString(), EditorStyles.boldLabel);

                    GUILayout.Space(10);

                    GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                    if (GUILayout.Button("Remove", GUILayout.Width(100))) {
                        levelData.arenaEvents.RemoveAt(i);
                    }
                    GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                    if (GUILayout.Button("Add", GUILayout.Width(100))) {
                        levelData.arenaEvents.Insert(i + 1, new ArenaEvent());
                    }
                    GUI.color = Color.white;

                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Is Fallback");
                    bool preFallback = arenaEvent.isFallback;
                    arenaEvent.isFallback = GUILayout.Toggle(arenaEvent.isFallback, "");
                    if (preFallback != arenaEvent.isFallback && arenaEvent.isFallback) {
                        for (int j = 0; j < levelData.arenaEvents.Count; j++) {
                            if (levelData.arenaEvents[j].isFallback && levelData.arenaEvents[j] != arenaEvent) {
                                levelData.arenaEvents[j].isFallback = false;
                            }
                        }
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();

                    arenaEvent.type = DrawHorizontalLabeledEnumField<ArenaEventType>("Type", arenaEvent.type);
                    DrawHorizontalLabeledFloatField("Chance", ref arenaEvent.chance.baseValue);

                    switch (arenaEvent.type) {
                        case ArenaEventType.EnemyAtkDMGIncrease:
                            DrawHorizontalLabeledFloatField("Enemy Atk DMG Increase", ref arenaEvent.eAtkDMGIncrease);
                            break;
                        case ArenaEventType.EnemySpAtkDMGIncrease:
                            DrawHorizontalLabeledFloatField("Enemy SPAtk DMG Increase", ref arenaEvent.eSPAtkDMGIncrease);
                            break;
                        case ArenaEventType.EnemySpeedIncrease:
                            DrawHorizontalLabeledFloatField("Enemy SPD Increase", ref arenaEvent.eSPDIncrease);
                            break;
                        case ArenaEventType.EnemyTimedRegenAmount:
                            DrawHorizontalLabeledFloatField("Enemy Regen Amount", ref arenaEvent.eRegenAmount);
                            break;
                        case ArenaEventType.PlayerDamageDecrease:
                            DrawHorizontalLabeledFloatField("Player DMG Decrease", ref arenaEvent.pDamageDecrease);
                            break;
                        default:
                            break;
                    }

                    GUILayout.Space(5);
                    GUILayout.EndVertical();
                }
            }
        }
    }

    private void DrawStoryProperties() {
        enemiesToSpawnExpanded = EditorGUILayout.Foldout(enemiesToSpawnExpanded, "Enemies To Spawn");

        if (enemiesToSpawnExpanded) {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Random Fill")) {
                int randomIndex = 0;
                for (int i = 0; i < enemyCountToFill; i++) {
                    randomIndex = UnityEngine.Random.Range(20, 23) * 10; 
                    levelData.enemiesToSpawn.Add((CharacterID)(randomIndex));  
                }
            }
            enemyCountToFill = EditorGUILayout.IntField(enemyCountToFill);
            GUILayout.EndHorizontal();

            if (levelData.enemiesToSpawn.Count == 0) {
                GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                if (GUILayout.Button("Add", GUILayout.Width(100))) {
                    levelData.enemiesToSpawn.Add(CharacterID.NONE);
                }
                GUI.color = Color.white;
            }
            else {
                for (int i = 0; i < levelData.enemiesToSpawn.Count; i++) {
                    GUILayout.BeginHorizontal();

                    GUILayout.Space(10);

                    levelData.enemiesToSpawn[i] = DrawHorizontalLabeledEnumField<CharacterID>("Enemy " + i.ToString(), levelData.enemiesToSpawn[i]);
                    GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                    if (GUILayout.Button("Remove")) {
                        levelData.enemiesToSpawn.RemoveAt(i);
                    }
                    GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                    if (GUILayout.Button("Add")) {
                        levelData.enemiesToSpawn.Insert(i + 1, CharacterID.NONE);
                    }
                    GUI.color = Color.white;

                    GUILayout.EndHorizontal();
                }
            }
        }
    }

    public override void OnInspectorGUI() {
        levelDataObject = (target as LevelDataObject);
        levelData = levelDataObject.data;

        GUILayout.BeginVertical();


        GUILayout.Label("General", EditorStyles.boldLabel);
        DrawHorizontalLabeledTextField("Name", ref levelData.name);

        levelData.levelType = DrawHorizontalLabeledEnumField<LevelType>("Level Type", levelData.levelType);

        GUILayout.Label("Rewards Data", EditorStyles.boldLabel);
        DrawHorizontalLabeledIntField("Scrap Reward", ref levelData.rewardsData.scrapReward);
        DrawHorizontalLabeledIntField("EXP Reward", ref levelData.rewardsData.expReward);

        //! Arena
        if (levelData.levelType == LevelType.Arena) {
            DrawArenaProperties();
        }
        else if (levelData.levelType == LevelType.Story) {  //! Story
            DrawStoryProperties();
        }
        else if (levelData.levelType == LevelType.Tutorial) {  //! Tutorial
            DrawStoryProperties();

            tutorialEventsExpanded = EditorGUILayout.Foldout(tutorialEventsExpanded, "Tutorial Events");

            if (tutorialEventsExpanded) {
                if (levelData.tutorialEvents.Count == 0) {
                    GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                    if (GUILayout.Button("Add", GUILayout.Width(100))) {
                        levelData.tutorialEvents.Add(new TutorialLevelEvent());
                    }
                    GUI.color = Color.white;
                }
                else {
                    int indexWithError = -1;
                    bool errorInKills = false;

                    for (int i = 0; i < levelData.tutorialEvents.Count; i++) {

                        for (int iter = 0; iter < levelData.tutorialEvents.Count; iter++) {
                            if (levelData.tutorialEvents.IsIndexValid(iter + 1) && levelData.tutorialEvents[iter + 1].killsNeeded <= levelData.tutorialEvents[iter].killsNeeded) {
                                errorInKills = true;
                                indexWithError = iter + 1;
                                break;
                            }
                        }

                        GUILayout.BeginHorizontal();
                        bool eventIsSelected = (selectedTutorialLevelEvent != null && selectedTutorialLevelEvent == levelData.tutorialEvents[i]);
                        GUI.color = eventIsSelected ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
                        if (i == indexWithError) {
                            GUI.color = CustomColor.GetColor(ColorName.PRS_PURPLE);
                        }
                        float height = eventIsSelected ? 40 : 20;
                        if (GUILayout.Button("Event " + i.ToString(), GUILayout.Height(height))) {
                            GUI.FocusControl(null);
                            if (eventIsSelected) {
                                selectedTutorialLevelEvent = null;
                            }
                            else {
                                selectedTutorialLevelEvent = levelData.tutorialEvents[i];
                            }
                        }
                        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                        if (GUILayout.Button("Remove", GUILayout.Width(80), GUILayout.Height(height))) {
                            levelData.tutorialEvents.RemoveAt(i);
                            break;
                        }
                        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                        if (GUILayout.Button("Add", GUILayout.Width(80), GUILayout.Height(height))) {
                            levelData.tutorialEvents.Insert(i + 1, new TutorialLevelEvent());
                        }
                        GUILayout.EndHorizontal();
                        GUI.color = Color.white;

                        if (eventIsSelected && selectedTutorialLevelEvent != null) {
                            GUILayout.BeginVertical();
                            
                            if (errorInKills) {
                                EditorGUILayout.HelpBox("Event " + indexWithError.ToString() + " has a lower killsNeeded value than Event " + (indexWithError - 1).ToString() + ", with a killsNeeded value of " + levelData.tutorialEvents[indexWithError - 1].killsNeeded, MessageType.Error);
                            }

                            selectedTutorialLevelEvent.killsNeeded = EditorGUILayout.IntField("Kills Needed", selectedTutorialLevelEvent.killsNeeded);
                            tutorialMessagesExpanded = EditorGUILayout.Foldout(tutorialMessagesExpanded, "Tutorial Messages");

                            if (tutorialMessagesExpanded) {
                                //if (selectedTutorialLevelEvent.messages.Count == 0) {
                                    GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                                    if (GUILayout.Button("Add", GUILayout.Width(100))) {
                                        selectedTutorialLevelEvent.messages.Add("");
                                    }
                                    GUI.color = Color.white;
                                //}
                                //else {
                                    for (int messageIndex = 0; messageIndex < selectedTutorialLevelEvent.messages.Count; messageIndex++) {
                                        selectedTutorialLevelEvent.messages[messageIndex] = EditorGUILayout.TextField("Message", selectedTutorialLevelEvent.messages[messageIndex]);
                                    }
                                //}
                            }

                            GUILayout.EndVertical();
                        }

                    }
                }
            }
        }
        GUILayout.EndVertical();


        if (GUI.changed) {
            EditorUtility.SetDirty(levelDataObject);
        }
    }

}
