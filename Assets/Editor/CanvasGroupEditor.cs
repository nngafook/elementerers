﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(CanvasGroup))]
public class CanvasGroupEditor : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        GUILayout.Space(10);
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        if (GUILayout.Button("Toggle Visible")) {
            (target as CanvasGroup).SetAlphaAndBools(!(target as CanvasGroup).blocksRaycasts);
        }
        GUI.color = Color.white;
    }

}
